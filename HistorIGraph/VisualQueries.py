import json
import sys
import os
from time import sleep
from pathlib import Path

import networkx as nx
from flask import Flask, jsonify, request, session, send_file
from flask_cors import CORS

import hig.neo4j_processing as neo4j_processing
from hig.Neo4jDirectConnector import Neo4jDirectConnector
import hig.dataLoader.dynamicLayoutDot as dynamicLayoutDot
import hig.dataLoader.pipeline as pipeline
from hig.globals import IS_DOCKER

from hig.asyncGraphMetrics.async_network_metrics import compute_metrics, iter_over_async

#  tiles route
# from hig.tiles_server import tiles_api

# sys.stdout = open('/home/alexis/output_apache2_flask.logs', 'w')
# print("VisualQueries begin")
# sys.stdout = sys.__stdout__ # Reset to the standard output
# print("SYS ", sys.path)

if IS_DOCKER:
    sleep(30)

neo4j_connector = Neo4jDirectConnector()
neo4j_connector.find_types_properties()

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
CORS(app)

# app.register_blueprint(tiles_api)


# Flask session config
app.secret_key = "any random string"

current_request = [""]

d3_layout_path = os.path.join(app.root_path, 'd3_layouts/')
print("layout paths ", d3_layout_path)


@app.route('/tilesNE/<zoom>/<x>/<y>', methods=['GET'])
def tiles_NE(zoom, x, y):
    default = 'tiles/tiles_NE/0/0/0.webp'
    default_path = str(Path(__file__).parent.absolute()) + "/" + default

    filename = 'tiles/tiles_NE2/%s/%s/%s.webp' % (zoom, x, y)
    path = str(Path(__file__).parent.absolute()) + "/" + filename

    if os.path.isfile(path):
        return send_file(path)
    else:
        return send_file(default_path)

@app.route('/tilesSR/<zoom>/<x>/<y>', methods=['GET'])
def tiles_SR(zoom, x, y):
    default = 'tiles/tiles_SR/0/0/0.webp'
    default_path = str(Path(__file__).parent.absolute()) + "/" + default

    filename = 'tiles/tiles_SR/%s/%s/%s.webp' % (zoom, x, y)
    path = str(Path(__file__).parent.absolute()) + "/" + filename
    if os.path.isfile(path):
        return send_file(path)
    else:
        return send_file(default_path)


@app.route("/", methods=["GET"])
def base():
    message = {"message": "endpoint"}
    return jsonify(message), 200


@app.route("/loadData", methods=["POST"])
def load_data_neo4j():
    data = request.json
    # main_graph_json = data["mainGraph"]
    # edge_type_key = data["edgeTypeKey"]

    pipeline.pipeline_from_json(data)

    # neo4j_processing.graph_json_to_neo4j(main_graph_json,
    #                                      edge_type_key=edge_type_key)

    return {}, 200


@app.route("/getDatabaseJson", methods=["GET"])
def ask_data_neo4j():
    print("Compute getDatabaseJson")
    neo4j_connector = Neo4jDirectConnector()

    print("Compute properties")
    neo4j_connector.find_types_properties()  # Recompute in case the dataset changed
    properties = neo4j_connector.properties
    properties_json = [prop.to_json() for prop in properties]

    print("To Json")
    database_json = neo4j_connector.to_json()

    data = {
        "data": database_json,
        "properties": properties_json
    }

    print("Sending")
    return jsonify(data)


@app.route("/getHypergraph", methods=["GET"])
def ask_hypergraph_representation():
    print("to hypergraph")
    document_graph, repetition_graph = neo4j_connector.to_hypergraph_representation()

    output = {
        "documentGraph": document_graph,
        "repetitionGraph": repetition_graph
    }

    print("Sending")
    return jsonify(output)


@app.route("/getProperties", methods=["GET"])
def ask_properties_neo4j():
    properties = neo4j_connector.properties
    properties_json = [prop.to_json() for prop in properties]
    return jsonify(properties_json)


def parse_query_request(flask_request):
    json_request = flask_request.json
    main_graph_json = json_request["mainGraph"]
    query = json_request["scriptRequest"]
    edge_types = json_request["edgeTypes"]
    edge_type_key = json_request["edgeTypeKey"]
    return query, main_graph_json, edge_types, edge_type_key


@app.route("/cypherMatch", methods=["POST"])
def cypher_to_match():
    query, main_graph_json, edge_types, edge_type_key = parse_query_request(request)
    current_request[0] = query

    print("Run Query ", query)
    matching_list, matched_nodes, matched_links = neo4j_connector.run(query)
    match_graph = neo4j_connector.graph_match.get_graph()

    print("compute graph metrics")
    metrics = list(iter_over_async(compute_metrics(match_graph)))[-1]

    output = {
        "matching": {
            "matching_list": matching_list,
            "matched_links": matched_links,
            "matched_nodes": matched_nodes
        },
        "metrics": metrics
    }

    # Abort request if more recent one
    if current_request[0] != query:
        return "", 400

    print("Sending Result")
    return jsonify(output)


@app.route("/cypherMatchTwoTimes", methods=["POST"])
def cypher_to_match_twotimes():
    json_input = request.json
    query = json_input["scriptRequest"]

    # matching_list, matched_nodes, matched_links = neo4j_connector.run(query)
    # graph_match = neo4j_connector.graph_match.get_graph()

    connector = Neo4jDirectConnector()
    matching_list, matched_nodes, matched_links = connector.run(query)
    graph_match = connector.graph_match.get_graph()

    output_matching = {
        "matching": {
            "matching_list": matching_list,
            "matched_links": matched_links,
            "matched_nodes": matched_nodes
        }
    }

    def generator_output():
        yield json.dumps(output_matching) + "\n"
        print("match sent")
        # yield from iter_over_async(
        #     compute_metrics(neo4j_connector.graph_match.get_graph(), neo4j_connector.node_types()))
        yield from iter_over_async(
            compute_metrics(graph_match, neo4j_connector.node_types()))
        print("metrics sent")

    return app.response_class(generator_output(), mimetype='application/json')


@app.route("/cypherMatchStream", methods=["POST"])
def cypher_to_match_stream():
    match_metrics_sep_char = "\n"

    query, main_graph_json, edge_types, edge_type_key = parse_query_request(request)
    current_request[0] = query

    print("query ", query)

    # Abort request if more recent one
    if current_request[0] != query:
        return "", 400

    def generator_query():
        # for record_processed, nodes_ids, links_ids in neo4j_connector.run_gen(query):
        #     yield encode_results(record_processed, nodes_ids, links_ids)

        for nodes_ids, links_ids in neo4j_connector.run_gen(query):
            yield encode_results(nodes_ids, links_ids)
        # yield match_metrics_sep_char

    def generator_output():
        yield from generator_query()
        yield from json.dumps(neo4j_connector.records_processed) + "\n"
        yield from iter_over_async(compute_metrics(neo4j_connector.graph_match.get_graph()))
        # return itertools.chain(generator_query(), iter_over_async(compute_metrics(neo4j_connector.graph_match.get_graph())))

    return app.response_class(generator_output(), mimetype='application/json')


def encode_results(nodes_ids, links_ids, sep_char="\n"):
    encoded = f"{json.dumps(nodes_ids)}{sep_char}{json.dumps(links_ids)}{sep_char}"
    return encoded


@app.route("/cypherRequest", methods=["POST"])
def cypher():
    query = request.data.decode()
    results = neo4j_connector.query(query)
    results = [line for line in results]
    # print(results)
    return jsonify(results)
    # current_request[0] = query


@app.route("/cypher", methods=["POST"])
def cypher_request():
    cypher = request.data.decode("utf-8")
    print(cypher)
    request_result, nodes, links = neo4j_connector.run(cypher)
    # print(request_result)
    return jsonify(request_result)


@app.route("/getDynamicLayout", methods=["GET"])
def get_dynamic_layout():
    dynamic_layout_json = dynamicLayoutDot.load_dynamic_layout()
    dynamic_layout_json["metadata"] = neo4j_connector.generate_metadata()
    return jsonify(dynamic_layout_json)


@app.route("/getAdjacency", methods=["POST"])
def get_adjacency():
    graph_json = request.json["graph"]
    graph_json = {"nodes": graph_json["nodes"], "links": graph_json["links"]}
    graph = nx.node_link_graph(graph_json, True, True)
    adjacency = nx.adjacency_data(graph)
    return jsonify(adjacency)


# TODO: id of a dataset is only the number of nodes for now
@app.route("/saveD3ForceLayout/<type>", methods=["POST"])
def save_d3_layout(type):
    pos = request.json
    n_nodes = len(pos)

    path = f'{d3_layout_path}/{type}_{n_nodes}nodes.json'

    with open(path, 'w+') as f:
        json.dump(pos, f)

    return jsonify({})


@app.route("/askD3ForceLayout/<type>", methods=["POST"])
def ask_d3_layout(type):
    n_nodes = request.data.decode("utf-8")
    path = f'{d3_layout_path}/{type}_{n_nodes}nodes.json'

    empty_return = {
        "empty": "empty"
    }

    if os.path.exists(path):
        print("Load existing layout")
        with open(path) as f:
            pos = json.load(f)
            print("sending")
            return jsonify(pos)
    else:
        return jsonify(empty_return)


@app.route("/MST", methods=["POST"])
def ask_MST():
    graph_json = request.json
    G = nx.readwrite.node_link_graph(graph_json)

    persons_ids = set(dict(G.nodes.data("person")).values())
    nodes_by_person = {person: [n for n, attrs in G.nodes.data() if attrs["person"] == person] for person in
                       persons_ids}

    person_to_edges = {}
    for person, person_nodes in nodes_by_person.items():
        H = G.subgraph(person_nodes)
        set_weight_from_coordinates(H)

        tree_edges = nx.algorithms.tree.mst.minimum_spanning_edges(H)
        person_to_edges[person] = list(tree_edges)

    return jsonify(person_to_edges)


def set_weight_from_coordinates(G):
    for u, v, key in G.edges:
        source = G.nodes[u]
        target = G.nodes[v]
        x1, y1 = source["x"], source["y"]
        x2, y2 = target["x"], target["y"]
        weight = norm(x1, y1, x2, y2)
        G.edges[(u, v, key)]["weight"] = weight


def norm(x1, y1, x2, y2):
    import math
    return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


if __name__ == "__main__":
    if IS_DOCKER:
        debug = False
    else:
        debug = True

    if len(sys.argv) > 1:
        app.run(debug=debug, port=sys.argv[1])
    else:
        # app.run(debug=debug, port=10090)
        app.run(debug=debug, port=10090, host='0.0.0.0')
