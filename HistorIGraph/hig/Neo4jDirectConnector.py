import itertools
import json
import time

import neo4j
import networkx as nx
from neo4j import GraphDatabase

from hig.globals import NODETYPE, EDGETYPE, HYPEREDGE_NODE_TYPES, IS_DOCKER
from hig.dataLoader.MetadataStore import MetadataStore
from hig.projection import projection


def generate_link_id(source_id, type, target_id):
    return (source_id, type, target_id)


class Property:
    def __init__(self, name, property_type, node_type):
        self.name = name
        self.type = property_type
        self.node_type = node_type
        self.domain = None

    def to_json(self):
        return {
            "name": self.name,
            "type": self.type,
            NODETYPE: self.node_type,
            "domain": self.domain
        }


class SubgraphMatch:
    def __init__(self, node_list=None, link_list=None, node_ids=None, link_ids=None):
        self.set(node_list, link_list, node_ids, link_ids)

    def set(self, node_list=None, link_list=None, node_ids=None, link_ids=None):
        self.node_list = node_list
        self.link_list = link_list

        if node_ids:
            self.node_ids = node_ids
        elif node_list:
            self.compute_nodes_ids()

        if link_ids:
            self.link_ids = link_ids
        elif link_list:
            self.compute_links_ids()

    def compute_nx_graph(self):
        self.graph = nx.node_link_graph(self.to_dict())

    def get_graph(self):
        self.compute_nx_graph()
        return self.graph

    def compute_nodes_ids(self):
        self.node_ids = [node["id"] for node in self.node_list]

    def compute_links_ids(self):
        self.link_ids = [generate_link_id(link["source"], link["relation_type"], link["target"]) for link in
                         self.link_list]

    def to_dict(self):
        return {
            "nodes": self.node_list,
            "links": self.link_list,
            "type": "subgraph"
        }

    def to_ids_dict(self):
        return {
            "nodes": self.node_ids,
            "links": self.link_ids,
            "type": "subgraph"
        }

    def subgraph_id(self):
        return tuple(self.node_ids + self.link_ids)


class Neo4jDirectConnector:
    def __init__(self, user="neo4j", pwd="neo4j"):
        if IS_DOCKER:
            uri = "bolt://neo4j-service:7687"
        else:
            uri = "bolt://localhost:7687"

        self.uri = uri
        self.user = user
        self.pwd = pwd
        self.driver = None
        self.sessions = None
        self.subgraphs_entities_ids = []

        self.hyperedge_node_type = None
        self.person_node_type = None

        try:
            # self.driver = GraphDatabase.driver(self.uri, auth=(self.user, self.pwd))
            self.driver = GraphDatabase.driver(self.uri, encrypted=False)
        except Exception as e:
            print("Failed to create the driver:", e)

        self.graph_json = None
        self.nodes_ids: set = None
        self.links_ids: set = None
        self.nodes_entries: list = None
        self.link_entries: list = None

        # Needs to be initialized here
        self.graph_match = SubgraphMatch()

    def close(self):
        if self.driver is not None:
            self.driver.close()

    def query(self, query, db=None):
        assert self.driver is not None, "Driver not initialized!"
        self.session = None
        response = None
        try:
            self.session = self.driver.session(database=db) if db is not None else self.driver.session()
            response = list(self.session.run(query))
            # response = self.session.run(query)
        except Exception as e:
            print("Query failed:", e)
        finally:
            if self.session is not None:
                # pass
                self.session.close()
        return response

    def find_types_properties(self):
        self.find_node_labels()
        self.find_relationship_types()
        self.find_properties()
        # self.compute_properties_domain()

    def find_node_labels(self):
        labels = self.query("""CALL db.labels()""")

        self.node_labels = [label["label"] for label in labels]
        self.node_labels = [label for label in self.node_labels if label != 'TTL']
        self.find_document_node_type()

    def find_document_node_type(self):
        self.hyperedge_node_type = None
        for node_type in self.node_labels:
            if node_type in HYPEREDGE_NODE_TYPES:
                if self.hyperedge_node_type is not None:
                    raise "Already one node type as hyperedge"
                self.hyperedge_node_type = node_type
            else:
                self.person_node_type = node_type

    def node_types(self):
        return [self.person_node_type, self.hyperedge_node_type]

    def find_relationship_types(self):
        types = self.query("""CALL db.relationshipTypes""")
        self.relation_types = [r["relationshipType"] for r in types]

    def find_properties(self):
        properties = self.query("""
            MATCH (p)
            WITH distinct p, keys(p) as pKeys
            UNWIND pKeys as Key
            RETURN distinct labels(p) as labels, Key, apoc.map.get(apoc.meta.cypher.types(p), Key, [true]) as type
        """)
        self.properties = [Property(property['Key'], property['type'], property["labels"][0]) for property in
                           properties]

    def compute_properties_domain(self):
        for property in self.properties:
            if property.type != "LIST OF STRING":
                self.compute_property_domain(property)

    def compute_property_domain(self, property):
        # TODO : Split between node and relationships properties
        domain_result = self.query(f"""
            MATCH (n) where EXISTS(n.{property.name}) return n.{property.name}
            UNION MATCH ()-[n]-() where EXISTS(n.{property.name}) return n.{property.name}
        """)
        domain_result = set([v[f"n.{property.name}"] for v in domain_result])
        # print(property.name, domain_result)

        if property.type == "STRING" or property.name == "id":
            property.domain = list(domain_result)
        elif property.type in ["INTEGER", "FLOAT"]:
            property.domain = [min(domain_result), max(domain_result)]


    def process_query_result(self, records):
        self.nodes_ids = set()
        self.links_ids = set()
        self.nodes_entries = []
        self.link_entries = []

        records_processed = []
        self.subgraphs_entities_ids = []

        for record in records:
            self.new_nodes_ids = []
            self.new_links_ids = []

            # TODO : check automorphisms when subgraphs in the return
            # is_automorphism = self.check_automorphism(record)
            # if is_automorphism: continue

            record_processed = {}
            for query_entity_id, matched_entity in record.items():
                if type(matched_entity) == list:
                    record_processed[query_entity_id] = self.process_subgraph(matched_entity)
                elif type(matched_entity) == neo4j.graph.Node:
                    record_processed[query_entity_id] = self.process_node(matched_entity)
                else:
                    record_processed[query_entity_id] = self.process_link(matched_entity)

            records_processed.append(record_processed)

        self.compute_matched_graph()
        self.session.close()

        return records_processed, list(self.nodes_ids), list(self.links_ids)

    def process_query_result_gen(self, records):
        self.nodes_ids = set()
        self.links_ids = set()
        self.nodes_entries = []
        self.link_entries = []

        self.new_nodes_ids = []
        self.new_links_ids = []

        self.subgraphs_entities_ids = []
        self.records_processed = []

        start = time.time()
        for record in records:
            self.new_nodes_ids = []
            self.new_links_ids = []

            # is_automorphism = self.check_automorphism(record)
            # if is_automorphism: continue

            record_processed = {}
            for query_entity_id, matched_entity in record.items():
                # print("T ", query_entity_id, matched_entity)
                if type(matched_entity) == list:
                    record_processed[query_entity_id] = self.process_subgraph(matched_entity)
                elif type(matched_entity) == neo4j.graph.Node:
                    record_processed[query_entity_id] = self.process_node(matched_entity)
                else:
                    record_processed[query_entity_id] = self.process_link(matched_entity)

            self.records_processed.append(record_processed)
            # yield record_processed, self.new_nodes_ids, self.new_links_ids
            yield self.new_nodes_ids, self.new_links_ids

        print("PROCESS RECORDS ", time.time() - start)
        self.compute_matched_graph()
        self.session.close()
        print("EVERYTHING ", time.time() - start)

    def compute_matched_graph(self):
        self.graph_match.set(self.nodes_entries, self.link_entries)

    def process_node(self, matched_entity):
        node_id = str(matched_entity["id"])
        node_processed = {
            "type": "node",
            "labels": list(matched_entity.labels),
            **dict(matched_entity),
            "id": node_id
        }

        if node_id not in self.nodes_ids:
            self.new_nodes_ids.append(node_id)
            self.nodes_ids.add(node_id)
            self.nodes_entries.append(node_processed)

        return node_processed

    def process_link(self, matched_entity):
        relation_type = type(matched_entity).__name__
        source_node_id = str(matched_entity.nodes[0]["id"])
        target_node_id = str(matched_entity.nodes[1]["id"])

        link_processed = {
            "type": "relation",
            "relation_type": relation_type,
            **dict(matched_entity),
            "source": source_node_id,
            "target": target_node_id,
        }

        link_id = generate_link_id(source_node_id, relation_type, target_node_id)
        if link_id not in self.links_ids:
            self.new_links_ids.append(link_id)
            self.links_ids.add(link_id)
            self.link_entries.append(link_processed)

        return link_processed

    # We consider a subgraph as [nodes, relationships] in cypher
    def process_subgraph(self, matched_entity):
        subgraph_nodes = []
        subgraph_links = []
        for node in matched_entity[0]:
            # print("node", node)
            node_processed = self.process_node(node)
            subgraph_nodes.append(node_processed)
        for link in matched_entity[1]:
            link_processed = self.process_link(link)
            subgraph_links.append(link_processed)

        subgraph = SubgraphMatch(subgraph_nodes, subgraph_links)
        # return subgraph.to_dict()
        return subgraph.to_ids_dict()

    def check_automorphism(self, record):
        subgraph_entities_ids = self.subgraph_entities_id(record)
        if subgraph_entities_ids in self.subgraphs_entities_ids:
            return True
        self.subgraphs_entities_ids.append(subgraph_entities_ids)
        return False

    def subgraph_entities_id(self, subgraph):
        subgraph_entities_ids = set()
        for query_entity_id, matched_entity in subgraph.items():
            if type(matched_entity) == neo4j.data.Node:
                subgraph_entities_ids.add(matched_entity["id"])
            elif type(matched_entity) == list:  # Subgraph
                for node in matched_entity[0]:
                    subgraph_entities_ids.add(node["id"])
                for link in matched_entity[1]:
                    relation_type = type(matched_entity).__name__
                    source_node_id = link.nodes[0]["id"]
                    target_node_id = link.nodes[1]["id"]
                    link_id = generate_link_id(source_node_id, relation_type, target_node_id)
                    subgraph_entities_ids.add(link_id)
            else:
                link_type = type(matched_entity).__name__
                link_id = generate_link_id(matched_entity.nodes[0]["id"], link_type, matched_entity.nodes[1]["id"])
                subgraph_entities_ids.add(link_id)

        return subgraph_entities_ids

    def run(self, query):
        response = self.query(query)
        return self.process_query_result(response)

    def run_gen(self, query):
        response = self.query(query)
        return self.process_query_result_gen(response)

    def to_json(self):
        # Use apoc export function and then convert it to regular json network representation
        # self.to_json_file()
        # with open("/var/lib/neo4j/import/" + self.json_name, 'rb') as file:
        #     json_data = [json.loads(jline) for jline in file.read().splitlines()]

        # Without dumping a json file
        json_data = self.query("""CALL apoc.export.json.all(null,{useTypes:true, stream: true})
YIELD file, nodes, relationships, properties, data
RETURN data""")
        # print(json_data)
        json_data = list(json_data)[0].values()[0]
        json_data = [json.loads(item) for item in json_data.split("\n")]

        self.get_neoid_to_id(json_data)
        nodes = []
        links = []
        for entity in json_data:
            if entity["type"] == "node":
                if "properties" in entity:
                    properties = entity["properties"]
                    if "label" in properties: del properties["label"]
                else:
                    properties = {}
                node = {
                    **properties,
                    # "id": entity["id"],
                    NODETYPE: entity["labels"][0],
                }
                nodes.append(node)
            elif entity["type"] == "relationship":
                if "properties" in entity:
                    properties = entity["properties"]
                else:
                    properties = {}

                link = {
                    EDGETYPE: entity["label"],
                    # "source": entity["start"]["id"],
                    # "target": entity["end"]["id"],
                    "source": self.neoid_to_id[entity["start"]["id"]],
                    "target": self.neoid_to_id[entity["end"]["id"]],
                    **properties
                }
                links.append(link)

        self.graph_json = {
            "metadata": self.generate_metadata(),
            "nodes": nodes,
            "links": links
        }

        return self.graph_json

    def to_json_file(self, filename="database_out.json"):
        self.json_name = filename
        self.query("call apoc.export.json.all('{}')".format(filename))

    def to_hypergraph_representation(self):
        graph_json = self.to_json()
        graph = nx.readwrite.node_link_graph(graph_json, True, True)

        repetition_graph_nodes = []
        repetition_graph_links = []

        graph_projected = projection(graph, NODETYPE, self.hyperedge_node_type)

        for link in graph_json["links"]:
            node = {"id": self.person_repetiton_node_id(link["target"], link["source"]), "person": link["target"],
                    "document": link["source"], EDGETYPE: link[EDGETYPE]}
            repetition_graph_nodes.append(node)

        values = set(map(lambda x: x["person"], repetition_graph_nodes))
        nodes_by_person = [[y["id"] for y in repetition_graph_nodes if y["person"] == x] for x in values]

        for person_nodes in nodes_by_person:
            all_pairs = itertools.combinations(person_nodes, 2)
            for pair in all_pairs:
                repetition_link = {"source": pair[0], "target": pair[1], EDGETYPE: "Repetition"}
                repetition_graph_links.append(repetition_link)

        repetition_graph = {"nodes": repetition_graph_nodes, "links": repetition_graph_links}

        return nx.readwrite.node_link_data(graph_projected), repetition_graph

    def person_repetiton_node_id(self, person_id, document_id):
        return f"{person_id}_{document_id}"

    def get_neoid_to_id(self, json_data):
        self.neoid_to_id = {}
        for entity in json_data:
            if entity["type"] == "node":
                self.neoid_to_id[entity["id"]] = entity["properties"]["id"]

    def generate_metadata(self):
        metadata_store = MetadataStore(None)
        metadata = metadata_store.load()
        print("meta1 ", metadata)

        metadata["edgeType"] = EDGETYPE
        metadata["entityType"] = NODETYPE
        metadata["entityTypes"] = [entity_type for entity_type in self.node_labels]
        metadata["attributes"] = [{"name": property.name, "type": property.type} for property in self.properties]

        # metadata = {
        #     "edgeType": EDGETYPE,
        #     "entityType": NODETYPE,
        #     "name": "name",
        #     "entityTypes": [entity_type for entity_type in self.node_labels],
        #     "attributes": [{"name": property.name, "type": property.type} for property in self.properties]
        # }
        print("meta2 ", metadata)
        return metadata

    def delete_all(self):
        self.query("MATCH (n) DETACH DELETE n")

    def create_node(self, type, attributes: dict):
        attributes_str = "{"
        for key, value in attributes.items():
            if not (value):
                continue

            if isinstance(value, str):
                value = f"'{value}'"

            attributes_str += f"{key}:{value},"
        attributes_str = attributes_str[:-1] + "}"

        query = f"CREATE (n:{type} {attributes_str})"
        self.query(query)


if __name__ == "__main__":
    conn = Neo4jDirectConnector(user="neo4j", pwd="neo4j")
    conn.create_node("PERSON", {"name": "bob", "age": 20})

    json_data = conn.to_json()
    # print(json_data)
