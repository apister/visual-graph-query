import itertools
import networkx as nx


def projection(G, node_type_key, projection_node_type="MARRIAGE_ACT"):
    G = nx.MultiGraph(G)
    proj_graph = nx.Graph()

    # nodes_projection_set = [n for n, attrs in G.nodes.data() if attrs[node_type_key] == projection_node_type]
    other_nodes = [n for n, attrs in G.nodes.data() if attrs[node_type_key] != projection_node_type]

    for node, attrs in G.nodes.data():
        if attrs[node_type_key] == projection_node_type:
            proj_graph.add_node(node, **attrs)

    for n in other_nodes:
        neighbors_projection_set = [n2 for n2 in G[n] if G.nodes[n2][node_type_key] == projection_node_type]
        node_pairs = list(itertools.combinations(neighbors_projection_set, 2))
        for pair in node_pairs:
            if (pair[0], pair[1]) in proj_graph.edges:
                proj_graph.edges[pair[0], pair[1]]["weight"] += 1
            else:
                proj_graph.add_edge(*pair, weight=1) # edge type is id of node

    return proj_graph


def projection_with_repetition(G, node_type_key, projection_node_type="MARRIAGE_ACT"):
    G = nx.MultiGraph(G)
    proj_graph = nx.MultiGraph()

    # nodes_projection_set = [n for n, attrs in G.nodes.data() if attrs[node_type_key] == projection_node_type]
    other_nodes = [n for n, attrs in G.nodes.data() if attrs[node_type_key] != projection_node_type]

    for node, attrs in G.nodes.data():
        if attrs[node_type_key] == projection_node_type:
            proj_graph.add_node(node, **attrs)

    for n in other_nodes:
        neighbors_projection_set = [n2 for n2 in G[n] if G.nodes[n2][node_type_key] == projection_node_type]
        node_pairs = list(itertools.combinations(neighbors_projection_set, 2))
        for pair in node_pairs:
            if (pair[0], pair[1], n) in proj_graph.edges:
                proj_graph.edges[pair[0], pair[1], n]["weight"] += 1
            else:
                proj_graph.add_edge(*pair, key=n, weight=1) # edge type is id of node

    return proj_graph