import asyncio

import networkx as nx
import json

from hig.asyncGraphMetrics.clustering import latapy_average_clustering
from hig.asyncGraphMetrics.components import connected_comps
from hig.asyncGraphMetrics.redundency import average_redundancy


class Metric:
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return str(self.value)

    def get(self):
        return self.value

    def set(self, value):
        self.value = value


async def compute_metrics(G, nodetypes, delay=0.5):
    clustering_value = Metric(0)
    cc_value = Metric(0)
    redundancy_value = Metric(0)

    # Non async
    print("compute non async Metrics")
    n_nodes = len(G)
    n_edges = len(G.edges)
    persons = [n for n, attrs in G.nodes.data() if attrs["labels"][0] == nodetypes[0]]
    documents = [n for n, attrs in G.nodes.data() if attrs["labels"][0] == nodetypes[1]]
    n_persons = len(persons)
    n_documents = len(documents)

    if False:
        yield metrics_basics(nodetypes, n_persons, n_documents, n_edges)
    elif n_nodes == 0:
        print("graph empty")
        yield metrics_null_graph(nodetypes)
    else:
        if n_nodes < 1000:
            # density = nx.density(G)
            bipartite_density = nx.algorithms.bipartite.density(G, persons)
            # bipartite_density2 = nx.algorithms.bipartite.density(G, documents)
            spectral_bipartivity = nx.bipartite.spectral_bipartivity(G)
        else:
            bipartite_density, spectral_bipartivity = None, None

        task1 = asyncio.create_task(latapy_average_clustering(G, clustering_value))
        task2 = asyncio.create_task(connected_comps(G, cc_value))
        task3 = asyncio.create_task(average_redundancy(G, redundancy_value))

        print("compute async Metrics")
        while not (task1.done() and task2.done() and task3.done()):
            yield process_metrics(nodetypes, n_persons, n_documents, n_edges, bipartite_density, spectral_bipartivity, clustering_value, redundancy_value, cc_value)
            await asyncio.sleep(delay)
            # print("one step", clustering_value, cc_value, redundancy_value)

        yield process_metrics(nodetypes, n_persons, n_documents, n_edges, bipartite_density, spectral_bipartivity, clustering_value, redundancy_value, cc_value)
        print("metrics finished", clustering_value, cc_value, redundancy_value)


def process_metrics(nodetypes, n_persons, n_documents, n_edges, density, spectral, clustering, redundancy, components):
    metrics = {
        f"Number_of_{nodetypes[0]}": n_persons,
        f"Number_of_{nodetypes[1]}": n_documents,
        f"Number_of_links": n_edges,
        "Number_of_Components": components.get(),
        "Bipartite_Clustering_Coefficient": clustering.get(),
        "Bipartite_Redundancy": redundancy.get(),
    }

    if density:
        metrics["Bipartite_Density"] = density
    if spectral:
        metrics["Spectral_Bipartivity"] = spectral

    return json.dumps(metrics) + "\n"


def metrics_null_graph(nodeTypes):
    metrics = {
        f"Number_of_{nodeTypes[0]}": 0,
        f"Number_of_{nodeTypes[1]}": 0,
        f"Number_of_links": 0,
        "Number_of_Components": 0,
        "Bipartite_Clustering_Coefficient": 0,
        "Bipartite_Redundancy": 0,
    }
    return json.dumps(metrics) + "\n"

def metrics_basics(nodeTypes, n_persons, n_documents, n_edges):
    metrics = {
        f"Number_of_{nodeTypes[0]}": n_persons,
        f"Number_of_{nodeTypes[1]}": n_documents,
        f"Number_of_links": n_edges,
    }
    return json.dumps(metrics) + "\n"


def iter_over_async(ait):
    """
    Transforms async generator to sync
    """
    # loop = asyncio.get_event_loop()
    # loop = asyncio.get_running_loop()
    loop = asyncio.events.new_event_loop()
    ait = ait.__aiter__()

    async def get_next():
        try:
            obj = await ait.__anext__()
            return False, obj
        except StopAsyncIteration:
            return True, None

    while True:
        done, obj = loop.run_until_complete(get_next())
        if done:
            break
        yield obj


if __name__ == "__main__":
    G: nx.Graph = nx.random_geometric_graph(200, 0.2)
    G.add_edge(28342, 423423)
    G.add_edge(342423, 438109)
    G.add_edge(111111, 222222222)

    G = nx.bipartite.random_graph(50, 100, 0.4)

    for v in iter_over_async(compute_metrics(G)):
        print(v)
    # asyncio.run(main())
