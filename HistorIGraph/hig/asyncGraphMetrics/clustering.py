import networkx as nx
import asyncio
import random


def cc_dot(nu, nv):
    return float(len(nu & nv)) / len(nu | nv)


def cc_max(nu, nv):
    return float(len(nu & nv)) / max(len(nu), len(nv))


def cc_min(nu, nv):
    return float(len(nu & nv)) / min(len(nu), len(nv))


modes = {"dot": cc_dot, "min": cc_min, "max": cc_max}


async def latapy_average_clustering(G, average_clustering, nodes=None, mode="dot"):
    if not nx.algorithms.bipartite.is_bipartite(G):
        # raise nx.NetworkXError("Graph is not bipartite")
        average_clustering.set(None)
        return None

    try:
        cc_func = modes[mode]
    except KeyError as e:
        raise nx.NetworkXError(
            "Mode for bipartite clustering must be: dot, min or max"
        ) from e

    if nodes is None:
        nodes = G

    n = len(nodes)

    ccs = {}
    for i, v in enumerate(nodes):
        cc = 0.0
        nbrs2 = {u for nbr in G[v] for u in G[nbr]} - {v}
        for u in nbrs2:
            cc += cc_func(set(G[u]), set(G[v]))
        if cc > 0.0:  # len(nbrs2)>0
            cc /= len(nbrs2)
        ccs[v] = cc

        average_clustering.set((average_clustering.get() * i + cc) / (i + 1))
        await asyncio.sleep(0)

    return average_clustering



# @py_random_state(2)
async def average_clustering(G, clustering, trials=300, seed=1):
    trial_count = 0
    n = len(G)
    triangles = 0
    nodes = list(G)
    # for i in [int(seed.random() * n) for i in range(trials)]:
    for i in [int(random.random()) * n for i in range(trials)]:
        nbrs = list(G[nodes[i]])
        if len(nbrs) < 2:
            continue
        u, v = random.sample(nbrs, 2)
        if u in G[v]:
            triangles += 1

        trial_count += 1
        # clustering = triangles / float(trial_count)
        clustering.set(triangles / float(trial_count))
        # print("clustering ", clustering[0])
        await asyncio.sleep(0)

    return clustering.value