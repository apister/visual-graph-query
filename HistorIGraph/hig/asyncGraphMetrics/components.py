import asyncio
import networkx as nx


async def connected_comps(G, n_connected_comps):
    seen = set()
    # n_connected_comps = 0
    for v in G:
        if v not in seen:
            c = _plain_bfs(G, v)
            seen.update(c)

            n_connected_comps.set(n_connected_comps.value + 1)
            # print("n comps ", n_connected_comps[0])
            await asyncio.sleep(0)

            # yield c
    return n_connected_comps.value


def _plain_bfs(G, source):
    """A fast BFS node generator"""
    G_adj = G.adj
    seen = set()
    nextlevel = {source}
    while nextlevel:
        thislevel = nextlevel
        nextlevel = set()
        for v in thislevel:
            if v not in seen:
                seen.add(v)
                nextlevel.update(G_adj[v])
    return seen