import asyncio
from itertools import combinations

import networkx as nx


async def average_redundancy(G, average_redundancy, nodes=None):
    if nodes is None:
        nodes = G
    # if any(len(G[v]) < 2 for v in nodes):
    #     raise nx.NetworkXError(
    #         "Cannot compute redundancy coefficient for a node"
    #         " that has fewer than two neighbors."
    #     )
    # return {v: _node_redundancy(G, v) for v in nodes}

    i = 0
    for node in nodes:
        if len(G[node]) < 2:
            continue

        redundancy = _node_redundancy(G, node)
        average_redundancy.set((average_redundancy.get() * i + redundancy) / (i + 1))
        await asyncio.sleep(0)

        i += 1


def _node_redundancy(G, v):
    n = len(G[v])
    # TODO On Python 3, we could just use `G[u].keys() & G[w].keys()` instead
    # of instantiating the entire sets.
    overlap = sum(
        1 for (u, w) in combinations(G[v], 2) if (set(G[u]) & set(G[w])) - {v}
    )
    return (2 * overlap) / (n * (n - 1))
