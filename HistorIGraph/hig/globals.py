import os

NODETYPE = "nodeType"
EDGETYPE = "edgeType"

HYPEREDGE_NODE_TYPES = ["DOCUMENT", "MARRIAGE_ACT", "CONTRACT"]

IS_DOCKER = os.environ.get('IS_DOCKER', False)