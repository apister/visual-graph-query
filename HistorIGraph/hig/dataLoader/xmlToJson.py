import json
from bs4 import BeautifulSoup

# In the data
ACTE = "ACTE"
DATE = "date"
EPOUX = "epoux"
EPOUSE = "epouse"
PERE = "pere"
MERE = "mere"
TEMOINS = "temoins"
TEMOIN = "temoin"
PRENOM = "prenom"
NOM = "nom"
CONDITION = "condition"

MENTIONED_AS_HUSBAND = "HUSBAND"
MENTIONED_AS_WIFE = "WIFE"
MENTIONED_AS_HUSBAND_FATHER = "HUSBAND_FATHER"
MENTIONED_AS_HUSBAND_MOTHER = "HUSBAND_MOTHER"
MENTIONED_AS_WIFE_FATHER = "WIFE_FATHER"
MENTIONED_AS_WIFE_MOTHER = "WIFE_MOTHER"
MENTIONED_AS_WITNESS = "WITNESS"

# Neo4j Nodes/Relationships names
PERSON = "PERSON"
ACT = "MARRIAGE_ACT"


def find_name(person_markup):
    # find global name from epoux, temoin, etc markups
    prenom = person_markup.find(PRENOM)
    nom = person_markup.find(NOM)
    condition = person_markup.find(CONDITION)

    if condition:
        condition_txt = condition.text
    else:
        condition_txt = None

    if prenom is not None and nom is not None:
        label = prenom.text + " " + nom.text
        return prenom.text, nom.text, label, condition_txt
    else:
        return None, None, None, None


def get_node(ID, ids_to_nodes, firstname, lastname, fullname, condition):
    if ID in ids_to_nodes:
        node = None
    else:
        node = {
            "id": ID,
            "firstname": firstname,
            "lastname": lastname,
            "name": fullname,
        }
        if condition:
            node["condition"] = condition
        ids_to_nodes[ID] = node

    return node


def transform_person_id(id):
    return "p" + str(id);


def process_role(role, ids_to_nodes):
    ID = role.get("id")
    ID = transform_person_id(ID)
    if ID is not None:
        firstname, lastname, label, condition = find_name(role)
        if label is not None:
            node = get_node(ID, ids_to_nodes, firstname, lastname,
                            label, condition)
            return node
    return None


def verify_date(date):
    try:
        date_int = int(date)
        return True
    except Exception as e:
        try:
            date_int = int(date[-4:])
            return True
        except Exception as e:
            return False

def append_node(node, nodes):
    if node not in nodes:
        nodes.append(node)

def actes_to_json(actes_path, limit=None):
    json_output = {"metadata":
                       {"format": "2.0", "graph type": "bipartite", "nodes": "nodes", "links": "links",
                        "time_slot": "ts", "entity_type": "entity_type", "source_entity_type": "contract",
                        "target_entity_type": "person", "roles": ["husband", "wife", "father", "mother", "witness"]}}
    nodes = []
    links = []

    count = 0
    soup = actes_fp_to_soup(actes_path)
    ids_to_nodes = {}

    for acte in soup.find_all(ACTE):
        try:
            acte_id = acte.get("id")
            # We add an 'a' to differ acts and persons ids
            acte_id = "a" + str(acte_id)

            is_date_valid = False
            date = acte.find(DATE)
            if date:
                date_txt = date.text
                is_date_valid = verify_date(date_txt)

            if not is_date_valid:
                continue

            pere_epoux_node = None
            pere_epouse_node = None
            mere_epoux_node = None
            mere_epouse_node = None
            epoux_node = None
            epouse_node = None

            # EPOUX
            epoux = acte.find(EPOUX)
            if epoux is not None:
                epoux_node = process_role(epoux, ids_to_nodes)

                pere = epoux.find(PERE)
                if pere is not None:
                    pere_epoux_node = process_role(pere, ids_to_nodes)

                mere = epoux.find(MERE)
                if mere is not None:
                    mere_epoux_node = process_role(mere, ids_to_nodes)

            # EPOUSE
            epouse = acte.find(EPOUSE)
            if epouse is not None:
                epouse_node = process_role(epouse, ids_to_nodes)

                pere = epouse.find(PERE)
                if pere is not None:
                    pere_epouse_node = process_role(pere, ids_to_nodes)

                mere = epouse.find(MERE)
                if mere is not None:
                    mere_epouse_node = process_role(mere, ids_to_nodes)

            # TEMOINS
            temoins = acte.find_all(TEMOIN)
            temoins_nodes = []
            if temoins is not None:
                for temoin in temoins:
                    temoin_node = process_role(temoin, ids_to_nodes)
                    temoins_nodes.append(temoin_node)

            if epoux_node or epouse_node:
                count += 1
                acte_node = {
                    "label": ACT,
                    "id": acte_id,
                    "date": date_txt
                }
                nodes.append(acte_node)

                if epoux_node:
                    append_node(epouse_node, nodes)
                    link = {
                        "source": epoux_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_HUSBAND
                    }
                    links.append(link)

                if epouse_node:
                    append_node(epouse_node, nodes)
                    link = {
                        "source": epouse_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_WIFE
                    }
                    links.append(link)

                if pere_epoux_node:
                    append_node(pere_epoux_node, nodes)
                    link = {
                        "source": pere_epoux_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_HUSBAND_FATHER
                    }
                    links.append(link)

                if mere_epoux_node:
                    append_node(mere_epouse_node, nodes)
                    link = {
                        "source": mere_epoux_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_WIFE_FATHER
                    }
                    links.append(link)

                if pere_epouse_node:
                    append_node(pere_epouse_node, nodes)
                    link = {
                        "source": pere_epouse_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_WIFE_FATHER
                    }
                    links.append(link)

                if mere_epouse_node:
                    append_node(mere_epouse_node, nodes)
                    link = {
                        "source": epoux_node["id"],
                        "target": acte_id,
                        "label": MENTIONED_AS_WIFE_MOTHER
                    }
                    links.append(link)

                for temoin_node in temoins_nodes:
                    if temoin_node:
                        append_node(temoin_node, nodes)
                        link = {
                            "source": temoin_node["id"],
                            "target": acte_id,
                            "label": MENTIONED_AS_WITNESS
                        }
                        links.append(link)

            if limit is not None and count == limit:
                break
        except Exception as e:
            print(acte_id)
            print(e)

    json_output["nodes"] = nodes
    json_output["links"] = links

    return json_output


def actes_fp_to_soup(actes_path):
    with open(actes_path) as fp:
        soup = BeautifulSoup(fp, "xml")
    return soup


if __name__ == "__main__":
    json_data = actes_to_json("../../data/actes_051220.xml", limit=50)
    print(json_data)