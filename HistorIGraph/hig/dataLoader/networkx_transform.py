import math
from collections import defaultdict
import pprint


import networkx as nx
from py2neo import Node, Graph
import json
import os
import numpy as np
import igraph as ig
from igraph.drawing.utils import BoundingBox

import hig.dataLoader.pipeline as pipeline
import hig.dataLoader.neo4jLoader as neo4jLoader
import hig.dataLoader.neo4jToNetworkx as neo4jToNetworkx
from hig.dataLoader.networkx_graph_metrics import add_node_metrics
from hig.globals import IS_DOCKER



RADIUS = 12
# We put the size as dependant of N nodes
def compute_canvas_size(n_nodes):
    # k = 10
    k = 15
    return math.sqrt(n_nodes) * RADIUS * 2 * k

WIDTH = 6000
HEIGHT = 6000

TIME_MIN_ATTRIBUTE = "_timeMin"
TIME_MAX_ATTRIBUTE = "_timeMax"
PERSON_DOCUMENTS_ATTRIBUTE = "documents"

pp = pprint.PrettyPrinter(indent=4)

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def networkX_transforms(G, add_date_year=True):
    if add_date_year:
        add_date_year_transform(G)
    add_time_interval(G)

    # compute_layout(G, "spring", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])
    # compute_layout(G, "spectral", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])
    # compute_layout(G, "kamada", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])
    # compute_igraph_layout(G, "davidson", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])
    # compute_igraph_layout(G, "fruchterman", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])
    compute_igraph_layout(G, "DrL", scale=WIDTH, center=[WIDTH / 2, HEIGHT / 2])

    # dynamicLayoutDot.dynamic_layout_transform(G, True)
    # dynamic_node_layout_dot.dynamic_node_layout_transform(G, person_lines=True)
    # add_x_time_constraint(G, 4000)

    print("compute node metrics")
    print(G.graph)
    add_node_metrics(G)


def compute_igraph_layout(G, alg, **kwargs):
    path = f"layout_{alg}_{len(G.nodes)}.json"
    graph_ig = ig.Graph.from_networkx(G)

    if alg == "davidson":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = graph_ig.layout_davidson_harel(maxiter=30)
    elif alg == "DrL":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = graph_ig.layout_drl()
    elif alg == "fruchterman":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = graph_ig.layout_fruchterman_reingold(niter=2000)

    size = compute_canvas_size(len(G))
    WIDTH, HEIGHT = size, size
    print("size ", size)
    positions.fit_into(BoundingBox(0, 0, WIDTH, HEIGHT))
    positions.center([500, 500])
    # positions.scale(200)

    for pos, node_id in zip(positions, G.nodes):
        G.nodes[node_id]["x"] = pos[0]
        G.nodes[node_id]["y"] = pos[1]


def compute_layout(G, alg, **kwargs):
    path = f"layout_{alg}_{len(G.nodes)}.json"

    if alg == "spring":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = nx.spring_layout(G, **kwargs)
    elif alg == "kamada":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = nx.kamada_kawai_layout(G, **kwargs)
    elif alg == "spectral":
        if os.path.exists(path):
            with open(path, 'r') as f:
                positions = json.loads(f.read())
        else:
            positions = nx.spectral_layout(G, **kwargs)

    for node_id, pos in positions.items():
        G.nodes[node_id]["x"] = pos[0]
        G.nodes[node_id]["y"] = pos[1]

    with open(path, 'w+') as f:
        f.write(json.dumps(positions, cls=NumpyEncoder))

    print("layout computation finished")


def add_x_time_constraint(G, width):
    ts_to_nodes = extract_time_slots(G)
    width_per_ts = width / len(ts_to_nodes)

    for i, (ts, nodes) in enumerate(sorted(ts_to_nodes.items())):
        for node in nodes:
            G.nodes[node]["fx"] = i * width_per_ts


def extract_time_slots(G, entity_type="MARRIAGE_ACT", time_key="date_year"):
    ts_to_nodes = defaultdict(set)
    for n, attrs in G.nodes.data():
        if attrs[pipeline.NODE_TYPE_KEY] == entity_type:
            ts = attrs[time_key]
            ts_to_nodes[ts].add(n)
    return ts_to_nodes


def add_date_year_transform(G):
    for n, attrs in G.nodes.data():
        # if attrs[pipeline.NODE_TYPE_KEY] == neo4jLoader.ACT:
        if attrs[G.graph[pipeline.NODE_TYPE_KEY]] == G.graph["source_entity_type"]:
            date = attrs[neo4jLoader.DATE]
            try:
                if len(date) == 4:
                    date_year = int(date)
                else:
                    date_year = int(date[-4:])
            except ValueError:
                date_year = None

            G.nodes[n]['date_year'] = date_year


def add_time_interval(G, add_person_documents_attribute=True):
    node_to_times = defaultdict(list)

    for n, attrs in G.nodes.data():
        if attrs[G.graph[pipeline.NODE_TYPE_KEY]] == G.graph["target_entity_type"]:
            neighbors = list(G.predecessors(n)) + list(G.neighbors(n))

            if neighbors == []:
                continue

            for document in neighbors:
                ts_key = G.graph[pipeline.TIME_KEY]
                if ts_key in G.nodes[document]:
                    ts = G.nodes[document][ts_key]
                else:
                    ts = None

                if ts is not None:
                    node_to_times[n].append((document, ts))

            node_to_times[n] = sorted(node_to_times[n], key=lambda tup: tup[1])
            times = [t[1] for t in node_to_times[n]]

            if len(node_to_times[n]) > 0:
                G.nodes[n][TIME_MIN_ATTRIBUTE] = min(times)

            if len(node_to_times[n]) > 0:
                G.nodes[n][TIME_MAX_ATTRIBUTE] = max(times)

            if add_person_documents_attribute:
                documents = [t[0] for t in node_to_times[n]]
                G.nodes[n][PERSON_DOCUMENTS_ATTRIBUTE] = documents


def nx_to_neo4j(G, neo4j_graph):
    graph_json = nx.node_link_data(G)
    graph_json_to_neo4j(graph_json, neo4j_graph)


def graph_json_to_neo4j(graph_json, neo4j_graph):
    if IS_DOCKER:
        uri = "bolt://neo4j-service:7687"
    else:
        uri = "bolt://localhost:7687"

    # graph = Graph('http://localhost:7474') # TODO: change for Docker
    graph = Graph(uri) # TODO: change for Docker
    neo4j_graph.delete_all()

    print("load nodes")
    for node in graph_json["nodes"]:
        # print(node)
        node_neo = Node(node[graph_json["graph"][pipeline.NODE_TYPE_KEY]], **node)
        graph.create(node_neo)

        # neo4j_graph.create_node(node[graph_json["graph"][pipeline.NODE_TYPE_KEY]], node)

    print("load edges")
    for link in graph_json["links"]:
        source = link['source'] if type(link['source']) is int else f"'{link['source']}'"
        target = link['target'] if type(link['target']) is int else f"'{link['target']}'"

        cypher = f"""
            MATCH (a),(b)
            WHERE a.id = {source} and b.id = {target}
            CREATE (a)-[r:{link[graph_json["graph"][pipeline.EDGE_TYPE_KEY]]}]->(b)
        """
        # neo4j_graph.run(cypher)
        neo4j_graph.query(cypher)



if __name__ == "__main__":
    # print("converting neo4j to networkx")
    json_data = neo4jToNetworkx.neo4j_to_json()
    G = neo4jToNetworkx.json_to_nxGraph(json_data)

    # print("networkx transforms")
    # dynamic_node_layout_dot.dynamic_node_layout_transform(G, person_lines=True)
    networkX_transforms(G)
