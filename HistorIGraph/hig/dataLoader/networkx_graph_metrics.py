import networkx as nx

import hig.dataLoader.neo4jToNetworkx as neo4jToNetworkx


def add_node_metrics(G):
    # To convert Multigraphs to Graphs
    G_simple = nx.Graph(G)

    document_nodes = [n for n, type in G_simple.nodes(G.graph["entityType"]) if type == G.graph["source_entity_type"]]

    # General metrics
    # clustering = nx.algorithms.cluster.clustering(G_simple)
    # centrality = nx.algorithms.centrality.eigenvector_centrality_numpy(G_simple)
    # bc = nx.algorithms.centrality.betweenness_centrality(G_simple)

    # Bipartite metrics
    clustering = nx.algorithms.bipartite.clustering(G_simple, mode="max")
    bc = nx.algorithms.bipartite.betweenness_centrality(G_simple, nodes=document_nodes)
    centrality = nx.algorithms.bipartite.closeness_centrality(G_simple, nodes=document_nodes)


    degrees = nx.degree(G)

    metrics_dicts = {
        "_centrality": centrality,
        "_betweenness_Centrality": bc,
        "_clustering_Coefficient": clustering,
    }

    metrics_lists = {
        "_degree": degrees
    }

    for metric_name, metric_dict in metrics_dicts.items():
        for n_id, value in metric_dict.items():
            G.nodes[n_id][metric_name] = value

    for metric_name, metric_list in metrics_lists.items():
        for n_id, value in metric_list:
            G.nodes[n_id][metric_name] = value


if __name__ == "__main__":
    # print("converting neo4j to networkx")
    json_data = neo4jToNetworkx.neo4j_to_json()
    G = neo4jToNetworkx.json_to_nxGraph(json_data)

    # print("networkx transforms")
    # dynamic_node_layout_dot.dynamic_node_layout_transform(G, person_lines=True)
    add_node_metrics(G)
