from py2neo import Graph
import json

from hig.dataLoader.neo4jLoader import load_actes_to_neo4j
import hig.dataLoader.neo4jToNetworkx as neo4jToNetworkx
from hig.dataLoader.networkx_transform import networkX_transforms, nx_to_neo4j
from hig.dataLoader.neo4jTransforms import neo4j_transforms
from hig.Neo4jDirectConnector import Neo4jDirectConnector

EDGE_TYPE_KEY = "edgeType"
NODE_TYPE_KEY = "entityType"
TIME_KEY = "time_key"

LAYOUT_FOLDER = "layouts"
DYNAMIC_LAYOUT_PATH = LAYOUT_FOLDER + "/dynamic_layout.json"

CANVAS_DIMS = [1200, 600]


def pipeline_from_json(json_data, add_date_year=False):
    # graph = Graph('http://localhost:7474', username='neo4j', password='neo4j')
    # graph = Graph('http://localhost:7474')
    # graph = Graph('neo4j://localhost:7687', secure=True)
    # graph.delete_all()
    graph = Neo4jDirectConnector()
    graph.delete_all()

    print("save metadata from json graph")
    neo4jToNetworkx.save_metadata(json_data)

    print("Load data into networkx")
    G = neo4jToNetworkx.json_to_nxGraph(json_data)

    print("networkx transforms")
    networkX_transforms(G, add_date_year)

    print("load graph back in neo4j")
    nx_to_neo4j(G, graph)

    print("neo4j transforms")
    neo4j_transforms(graph)


def pipeline_from_path(path, add_date_year=False):
    with open(path) as json_file:
        json_data = json.load(json_file)

    pipeline_from_json(json_data)