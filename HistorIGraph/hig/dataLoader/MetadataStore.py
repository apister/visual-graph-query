import json
import os
from pathlib import Path


class MetadataStore:
    def __init__(self, metadata):
        self.metadata = metadata
        self.filename = "metadata.json"
        self.path = str(Path(__file__).parent.absolute()) + "/" + self.filename
        print("PATH ", self.path)

    def save(self):
        with open(self.path, "w") as f:
            json.dump(self.metadata, f)

        # give full permissions
        os.chmod(self.path, 0o777)

    def load(self):
        with open(self.path, "r") as f:
            self.metadata = json.loads(f.read())
        return self.metadata