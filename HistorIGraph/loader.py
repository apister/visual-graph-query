import hig.dataLoader.pipeline as pipeline
from time import sleep

# Buenos Aires

# 1st version (no split)
# pipeline.pipeline_from_path("data/BuenosAires_actes_051220_60.json", True)
# pipeline.pipeline_from_path("data/BuenosAires_actes_051220_None.json", True)

# 2nd version (6 years)
# pipeline.pipeline_from_path("data/BuenosAires_actes_tries_modifies-2021-06-23_None.json", True)
# pipeline.pipeline_from_path("data/BuenosAires_actes_tries_modifies-2021-06-23_None_lieux.json", True)

# split
# pipeline.pipeline_from_path("data/BuenosAires_actes_tries_modifies-2021-06-23_SPLIT_None.json", True)

# Piemont
print("load Piemont")
pipeline.pipeline_from_path("data/Rolla_2modes.json", False)

# pipeline.pipeline_from_path("data/Marie_Boucher_vq.json", False)

# pipeline.pipeline_from_path("data/Dufournaud_nogeoloc_1921.json", False)
# pipeline.pipeline_from_path("data/Dufournaud_geoloc_1921.json", False)
# pipeline.pipeline_from_path("data/Dufournaud_geoloc_1940_onlydeathfilter.json", False) # Data used in the historian meeting
# pipeline.pipeline_from_path("data/Dufournaud_geoloc_1940.json", False)

# Nicole data V3
# pipeline.pipeline_from_path("data/Dufournaud_geoloc_1940_3.json", False)
# pipeline.pipeline_from_path("data/Dufournaud_geoloc_2022_3.json", False)


# Thesis data
# pipeline.pipeline_from_path("data/thesis_sociology_all_2016-2022_True.json", False)
# pipeline.pipeline_from_path("data/thesis_sociology_all_1985-2010_True.json", False)
# pipeline.pipeline_from_path("data/thesis_sociology_all_2000-2010_True.json", False)


