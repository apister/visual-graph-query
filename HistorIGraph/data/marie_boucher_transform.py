import json


path = "Marie_Boucher.json"

with open(path) as data:
    json_data = json.load(data)

for node in json_data["nodes"]:
    node["id"] = str(node["id"])
    node["label"] = node["entity_type"]
    if "ts" in node:
        node["date_year"] = int(node["ts"])

for link in json_data["links"]:
    link["target"] = str(link["target"])
    link["source"] = str(link["source"])

    link["label"] = "relation"

print(json_data)

with open("Marie_Boucher_vq.json", "w") as path:
    json.dump(json_data, path)