// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"7h7Y0":[function(require,module,exports) {
"use strict";
var HMR_HOST = null;
var HMR_PORT = 1236;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "cd95731e37d4ab28";
function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}
function _createForOfIteratorHelper(o, allowArrayLike) {
    var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];
    if (!it) {
        if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
            if (it) o = it;
            var i = 0;
            var F = function F() {
            };
            return {
                s: F,
                n: function n() {
                    if (i >= o.length) return {
                        done: true
                    };
                    return {
                        done: false,
                        value: o[i++]
                    };
                },
                e: function e(_e) {
                    throw _e;
                },
                f: F
            };
        }
        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    var normalCompletion = true, didErr = false, err;
    return {
        s: function s() {
            it = it.call(o);
        },
        n: function n() {
            var step = it.next();
            normalCompletion = step.done;
            return step;
        },
        e: function e(_e2) {
            didErr = true;
            err = _e2;
        },
        f: function f() {
            try {
                if (!normalCompletion && it.return != null) it.return();
            } finally{
                if (didErr) throw err;
            }
        }
    };
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: mixed;
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
*/ var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData,
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function accept(fn) {
            this._acceptCallbacks.push(fn || function() {
            });
        },
        dispose: function dispose(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData = undefined;
}
module.bundle.Module = Module;
var checkedAssets, acceptedAssets, assetsToAccept;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf('http') === 0 ? location.hostname : 'localhost');
}
function getPort() {
    return HMR_PORT || location.port;
} // eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == 'https:' && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? 'wss' : 'ws';
    var ws = new WebSocket(protocol + '://' + hostname + (port ? ':' + port : '') + '/'); // $FlowFixMe
    ws.onmessage = function(event) {
        checkedAssets = {
        };
        acceptedAssets = {
        };
        assetsToAccept = [];
        var data = JSON.parse(event.data);
        if (data.type === 'update') {
            // Remove error overlay if there is one
            if (typeof document !== 'undefined') removeErrorOverlay();
            var assets = data.assets.filter(function(asset) {
                return asset.envHash === HMR_ENV_HASH;
            }); // Handle HMR Update
            var handled = assets.every(function(asset) {
                return asset.type === 'css' || asset.type === 'js' && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear();
                assets.forEach(function(asset) {
                    hmrApply(module.bundle.root, asset);
                });
                for(var i = 0; i < assetsToAccept.length; i++){
                    var id = assetsToAccept[i][1];
                    if (!acceptedAssets[id]) hmrAcceptRun(assetsToAccept[i][0], id);
                }
            } else window.location.reload();
        }
        if (data.type === 'error') {
            // Log parcel errors to console
            var _iterator = _createForOfIteratorHelper(data.diagnostics.ansi), _step;
            try {
                for(_iterator.s(); !(_step = _iterator.n()).done;){
                    var ansiDiagnostic = _step.value;
                    var stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                    console.error('🚨 [parcel]: ' + ansiDiagnostic.message + '\n' + stack + '\n\n' + ansiDiagnostic.hints.join('\n'));
                }
            } catch (err) {
                _iterator.e(err);
            } finally{
                _iterator.f();
            }
            if (typeof document !== 'undefined') {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html); // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        console.error(e.message);
    };
    ws.onclose = function() {
        console.warn('[parcel] 🚨 Connection to the HMR server was lost');
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log('[parcel] ✨ Error resolved');
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement('div');
    overlay.id = OVERLAY_ID;
    var errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    var _iterator2 = _createForOfIteratorHelper(diagnostics), _step2;
    try {
        for(_iterator2.s(); !(_step2 = _iterator2.n()).done;){
            var diagnostic = _step2.value;
            var stack = diagnostic.codeframe ? diagnostic.codeframe : diagnostic.stack;
            errorHTML += "\n      <div>\n        <div style=\"font-size: 18px; font-weight: bold; margin-top: 20px;\">\n          \uD83D\uDEA8 ".concat(diagnostic.message, "\n        </div>\n        <pre>").concat(stack, "</pre>\n        <div>\n          ").concat(diagnostic.hints.map(function(hint) {
                return '<div>💡 ' + hint + '</div>';
            }).join(''), "\n        </div>\n        ").concat(diagnostic.documentation ? "<div>\uD83D\uDCDD <a style=\"color: violet\" href=\"".concat(diagnostic.documentation, "\" target=\"_blank\">Learn more</a></div>") : '', "\n      </div>\n    ");
        }
    } catch (err) {
        _iterator2.e(err);
    } finally{
        _iterator2.f();
    }
    errorHTML += '</div>';
    overlay.innerHTML = errorHTML;
    return overlay;
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute('href', link.getAttribute('href').split('?')[0] + '?' + Date.now()); // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href = links[i].getAttribute('href');
            var hostname = getHostname();
            var servedFromHMRServer = hostname === 'localhost' ? new RegExp('^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):' + getPort()).test(href) : href.indexOf(hostname + ':' + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(window.location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrApply(bundle, asset) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === 'css') reloadCSS();
    else if (asset.type === 'js') {
        var deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                var oldDeps = modules[asset.id][1];
                for(var dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    var id = oldDeps[dep];
                    var parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            var fn = new Function('require', 'module', 'exports', asset.output);
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id1) {
    var modules = bundle.modules;
    if (!modules) return;
    if (modules[id1]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        var deps = modules[id1][1];
        var orphans = [];
        for(var dep in deps){
            var parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        } // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id1];
        delete bundle.cache[id1]; // Now delete the orphans.
        orphans.forEach(function(id) {
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id1);
}
function hmrAcceptCheck(bundle, id, depsByBundle) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
     // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    var parents = getParents(module.bundle.root, id);
    var accepted = false;
    while(parents.length > 0){
        var v = parents.shift();
        var a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            var p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push.apply(parents, _toConsumableArray(p));
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle, id, depsByBundle) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToAccept.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) return true;
}
function hmrAcceptRun(bundle, id) {
    var cached = bundle.cache[id];
    bundle.hotData = {
    };
    if (cached && cached.hot) cached.hot.data = bundle.hotData;
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData);
    });
    delete bundle.cache[id];
    bundle(id);
    cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) // $FlowFixMe[method-unbinding]
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
    });
    acceptedAssets[id] = true;
}

},{}],"7aT0H":[function(require,module,exports) {
/*
 * # Fomantic UI - 2.8.8
 * https://github.com/fomantic/Fomantic-UI
 * http://fomantic-ui.com/
 *
 * Copyright 2022 Contributors
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */ !function(p, h, v, b) {
    p.isFunction = p.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, p.site = p.fn.site = function(e1) {
        var s, i1 = (new Date).getTime(), o1 = [], t1 = e1, n1 = "string" == typeof t1, l = [].slice.call(arguments, 1), c = p.isPlainObject(e1) ? p.extend(!0, {
        }, p.site.settings, e1) : p.extend({
        }, p.site.settings), a1 = c.namespace, u = c.error, r1 = "module-" + a1, d = p(v), f = this, m = d.data(r1), g = {
            initialize: function() {
                g.instantiate();
            },
            instantiate: function() {
                g.verbose("Storing instance of site", g), m = g, d.data(r1, g);
            },
            normalize: function() {
                g.fix.console(), g.fix.requestAnimationFrame();
            },
            fix: {
                console: function() {
                    g.debug("Normalizing window.console"), console !== b && console.log !== b || (g.verbose("Console not available, normalizing events"), g.disable.console()), void 0 !== console.group && void 0 !== console.groupEnd && void 0 !== console.groupCollapsed || (g.verbose("Console group not available, normalizing events"), h.console.group = function() {
                    }, h.console.groupEnd = function() {
                    }, h.console.groupCollapsed = function() {
                    }), void 0 === console.markTimeline && (g.verbose("Mark timeline not available, normalizing events"), h.console.markTimeline = function() {
                    });
                },
                consoleClear: function() {
                    g.debug("Disabling programmatic console clearing"), h.console.clear = function() {
                    };
                },
                requestAnimationFrame: function() {
                    g.debug("Normalizing requestAnimationFrame"), h.requestAnimationFrame === b && (g.debug("RequestAnimationFrame not available, normalizing event"), h.requestAnimationFrame = h.requestAnimationFrame || h.mozRequestAnimationFrame || h.webkitRequestAnimationFrame || h.msRequestAnimationFrame || function(e) {
                        setTimeout(e, 0);
                    });
                }
            },
            moduleExists: function(e) {
                return p.fn[e] !== b && p.fn[e].settings !== b;
            },
            enabled: {
                modules: function(e) {
                    var n = [];
                    return e = e || c.modules, p.each(e, function(e, t) {
                        g.moduleExists(t) && n.push(t);
                    }), n;
                }
            },
            disabled: {
                modules: function(e) {
                    var n = [];
                    return e = e || c.modules, p.each(e, function(e, t) {
                        g.moduleExists(t) || n.push(t);
                    }), n;
                }
            },
            change: {
                setting: function(o, a, e, r) {
                    e = "string" == typeof e ? "all" === e ? c.modules : [
                        e
                    ] : e || c.modules, r = r === b || r, p.each(e, function(e, t) {
                        var n, i = !g.moduleExists(t) || p.fn[t].settings.namespace || !1;
                        g.moduleExists(t) && (g.verbose("Changing default setting", o, a, t), p.fn[t].settings[o] = a, r && i && 0 < (n = p(":data(module-" + i + ")")).length && (g.verbose("Modifying existing settings", n), n[t]("setting", o, a)));
                    });
                },
                settings: function(i, e, o) {
                    e = "string" == typeof e ? [
                        e
                    ] : e || c.modules, o = o === b || o, p.each(e, function(e, t) {
                        var n;
                        g.moduleExists(t) && (g.verbose("Changing default setting", i, t), p.extend(!0, p.fn[t].settings, i), o && a1 && 0 < (n = p(":data(module-" + a1 + ")")).length && (g.verbose("Modifying existing settings", n), n[t]("setting", i)));
                    });
                }
            },
            enable: {
                console: function() {
                    g.console(!0);
                },
                debug: function(e, t) {
                    e = e || c.modules, g.debug("Enabling debug for modules", e), g.change.setting("debug", !0, e, t);
                },
                verbose: function(e, t) {
                    e = e || c.modules, g.debug("Enabling verbose debug for modules", e), g.change.setting("verbose", !0, e, t);
                }
            },
            disable: {
                console: function() {
                    g.console(!1);
                },
                debug: function(e, t) {
                    e = e || c.modules, g.debug("Disabling debug for modules", e), g.change.setting("debug", !1, e, t);
                },
                verbose: function(e, t) {
                    e = e || c.modules, g.debug("Disabling verbose debug for modules", e), g.change.setting("verbose", !1, e, t);
                }
            },
            console: function(e) {
                e ? m.cache.console !== b ? (g.debug("Restoring console function"), h.console = m.cache.console) : g.error(u.console) : (g.debug("Disabling console function"), m.cache.console = h.console, h.console = {
                    clear: function() {
                    },
                    error: function() {
                    },
                    group: function() {
                    },
                    groupCollapsed: function() {
                    },
                    groupEnd: function() {
                    },
                    info: function() {
                    },
                    log: function() {
                    },
                    markTimeline: function() {
                    },
                    warn: function() {
                    }
                });
            },
            destroy: function() {
                g.verbose("Destroying previous site for", d), d.removeData(r1);
            },
            cache: {
            },
            setting: function(e, t) {
                if (p.isPlainObject(e)) p.extend(!0, c, e);
                else {
                    if (t === b) return c[e];
                    c[e] = t;
                }
            },
            internal: function(e, t) {
                if (p.isPlainObject(e)) p.extend(!0, g, e);
                else {
                    if (t === b) return g[e];
                    g[e] = t;
                }
            },
            debug: function() {
                c.debug && (c.performance ? g.performance.log(arguments) : (g.debug = Function.prototype.bind.call(console.info, console, c.name + ":"), g.debug.apply(console, arguments)));
            },
            verbose: function() {
                c.verbose && c.debug && (c.performance ? g.performance.log(arguments) : (g.verbose = Function.prototype.bind.call(console.info, console, c.name + ":"), g.verbose.apply(console, arguments)));
            },
            error: function() {
                g.error = Function.prototype.bind.call(console.error, console, c.name + ":"), g.error.apply(console, arguments);
            },
            performance: {
                log: function(e) {
                    var t, n;
                    c.performance && (n = (t = (new Date).getTime()) - (i1 || t), i1 = t, o1.push({
                        Element: f,
                        Name: e[0],
                        Arguments: [].slice.call(e, 1) || "",
                        "Execution Time": n
                    })), clearTimeout(g.performance.timer), g.performance.timer = setTimeout(g.performance.display, 500);
                },
                display: function() {
                    var e = c.name + ":", n = 0;
                    i1 = !1, clearTimeout(g.performance.timer), p.each(o1, function(e, t) {
                        n += t["Execution Time"];
                    }), e += " " + n + "ms", (console.group !== b || console.table !== b) && 0 < o1.length && (console.groupCollapsed(e), console.table ? console.table(o1) : p.each(o1, function(e, t) {
                        console.log(t.Name + ": " + t["Execution Time"] + "ms");
                    }), console.groupEnd()), o1 = [];
                }
            },
            invoke: function(i, e2, t2) {
                var o, a, n, r = m;
                return e2 = e2 || l, t2 = f || t2, "string" == typeof i && r !== b && (i = i.split(/[\. ]/), o = i.length - 1, p.each(i, function(e, t) {
                    var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                    if (p.isPlainObject(r[n]) && e != o) r = r[n];
                    else {
                        if (r[n] !== b) return a = r[n], !1;
                        if (!p.isPlainObject(r[t]) || e == o) return r[t] !== b ? a = r[t] : g.error(u.method, i), !1;
                        r = r[t];
                    }
                })), p.isFunction(a) ? n = a.apply(t2, e2) : a !== b && (n = a), Array.isArray(s) ? s.push(n) : s !== b ? s = [
                    s,
                    n
                ] : n !== b && (s = n), a;
            }
        };
        return n1 ? (m === b && g.initialize(), g.invoke(t1)) : (m !== b && g.destroy(), g.initialize()), s !== b ? s : this;
    }, p.site.settings = {
        name: "Site",
        namespace: "site",
        error: {
            console: "Console cannot be restored, most likely it was overwritten outside of module",
            method: "The method you called is not defined."
        },
        debug: !1,
        verbose: !1,
        performance: !0,
        modules: [
            "accordion",
            "api",
            "calendar",
            "checkbox",
            "dimmer",
            "dropdown",
            "embed",
            "form",
            "modal",
            "nag",
            "popup",
            "slider",
            "rating",
            "shape",
            "sidebar",
            "state",
            "sticky",
            "tab",
            "toast",
            "transition",
            "visibility",
            "visit"
        ],
        siteNamespace: "site",
        namespaceStub: {
            cache: {
            },
            config: {
            },
            sections: {
            },
            section: {
            },
            utilities: {
            }
        }
    }, p.extend(p.expr[":"], {
        data: p.expr.createPseudo ? p.expr.createPseudo(function(t) {
            return function(e) {
                return !!p.data(e, t);
            };
        }) : function(e, t, n) {
            return !!p.data(e, n[3]);
        }
    });
}(jQuery, window, document), (function(M, I, j, L) {
    "use strict";
    M.isFunction = M.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, I = void 0 !== I && I.Math == Math ? I : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), M.fn.form = function(k) {
        var T, S = M(this), D = S.selector || "", A = (new Date).getTime(), E = [], F = k, P = arguments[1], O = "string" == typeof F, R = [].slice.call(arguments, 1);
        return S.each(function() {
            var n2, d1, t3, e3, f, l1, m, g, p, i2, c1, o2, a2, s1, u1, h = M(this), v = this, b = [], y = !1, r2 = !1, x = !1, C = [
                "clean",
                "clean"
            ], w = {
                initialize: function() {
                    w.get.settings(), O ? (u1 === L && w.instantiate(), w.invoke(F)) : (u1 !== L && (u1.invoke("destroy"), w.refresh()), w.verbose("Initializing form validation", h, f), w.bindEvents(), w.set.defaults(), f.autoCheckRequired && w.set.autoCheck(), w.instantiate());
                },
                instantiate: function() {
                    w.verbose("Storing instance of module", w), u1 = w, h.data(a2, w);
                },
                destroy: function() {
                    w.verbose("Destroying previous module", u1), w.removeEvents(), h.removeData(a2);
                },
                refresh: function() {
                    w.verbose("Refreshing selector cache"), n2 = h.find(g.field), d1 = h.find(g.group), t3 = h.find(g.message), h.find(g.prompt), e3 = h.find(g.submit), h.find(g.clear), h.find(g.reset);
                },
                submit: function() {
                    w.verbose("Submitting form", h), r2 = !0, h.submit();
                },
                attachEvents: function(e4, t) {
                    t = t || "submit", M(e4).on("click" + s1, function(e) {
                        w[t](), e.preventDefault();
                    });
                },
                bindEvents: function() {
                    w.verbose("Attaching form events"), h.on("submit" + s1, w.validate.form).on("blur" + s1, g.field, w.event.field.blur).on("click" + s1, g.submit, w.submit).on("click" + s1, g.reset, w.reset).on("click" + s1, g.clear, w.clear), f.keyboardShortcuts && h.on("keydown" + s1, g.field, w.event.field.keydown), n2.each(function(e, t) {
                        var n = M(t), t = n.prop("type"), t = w.get.changeEvent(t, n);
                        n.on(t + s1, w.event.field.change);
                    }), f.preventLeaving && M(I).on("beforeunload" + s1, w.event.beforeUnload), n2.on("change click keyup keydown blur", function(e) {
                        w.determine.isDirty();
                    }), h.on("dirty" + s1, function(e) {
                        f.onDirty.call();
                    }), h.on("clean" + s1, function(e) {
                        f.onClean.call();
                    });
                },
                clear: function() {
                    n2.each(function(e, t) {
                        var n = M(t), i = n.parent(), o = n.closest(d1), a = o.find(g.prompt), r = n.closest(g.uiCalendar), s = n.data(m.defaultValue) || "", l = i.is(g.uiCheckbox), c = i.is(g.uiDropdown) && w.can.useElement("dropdown"), t = 0 < r.length && w.can.useElement("calendar");
                        o.hasClass(p.error) && (w.verbose("Resetting error on field", o), o.removeClass(p.error), a.remove()), c ? (w.verbose("Resetting dropdown value", i, s), i.dropdown("clear", !0)) : l ? n.prop("checked", !1) : t ? r.calendar("clear") : (w.verbose("Resetting field value", n, s), n.val(""));
                    }), w.remove.states();
                },
                reset: function() {
                    n2.each(function(e, t) {
                        var n = M(t), i = n.parent(), o = n.closest(d1), a = n.closest(g.uiCalendar), r = o.find(g.prompt), s = n.data(m.defaultValue), l = i.is(g.uiCheckbox), c = i.is(g.uiDropdown) && w.can.useElement("dropdown"), u = 0 < a.length && w.can.useElement("calendar"), t = o.hasClass(p.error);
                        s !== L && (t && (w.verbose("Resetting error on field", o), o.removeClass(p.error), r.remove()), c ? (w.verbose("Resetting dropdown value", i, s), i.dropdown("restore defaults", !0)) : l ? (w.verbose("Resetting checkbox value", i, s), n.prop("checked", s)) : u ? a.calendar("set date", s) : (w.verbose("Resetting field value", n, s), n.val(s)));
                    }), w.remove.states();
                },
                determine: {
                    isValid: function() {
                        var n = !0;
                        return M.each(l1, function(e, t) {
                            w.validate.field(t, e, !0) || (n = !1);
                        }), n;
                    },
                    isDirty: function(e) {
                        var i = !1;
                        n2.each(function(e, t) {
                            var n = M(t), t = 0 < n.filter(g.checkbox).length ? w.is.checkboxDirty(n) : w.is.fieldDirty(n);
                            n.data(f.metadata.isDirty, t), i |= t;
                        }), i ? w.set.dirty() : w.set.clean();
                    }
                },
                is: {
                    bracketedRule: function(e) {
                        return e.type && e.type.match(f.regExp.bracket);
                    },
                    shorthandRules: function(e) {
                        return "string" == typeof e || Array.isArray(e);
                    },
                    empty: function(e) {
                        return !e || 0 === e.length || (e.is(g.checkbox) ? !e.is(":checked") : w.is.blank(e));
                    },
                    blank: function(e) {
                        return "" === String(e.val()).trim();
                    },
                    valid: function(e5, n) {
                        var i = !0;
                        return e5 ? (w.verbose("Checking if field is valid", e5), w.validate.field(l1[e5], e5, !!n)) : (w.verbose("Checking if form is valid"), M.each(l1, function(e, t) {
                            w.is.valid(e, n) || (i = !1);
                        }), i);
                    },
                    dirty: function() {
                        return x;
                    },
                    clean: function() {
                        return !x;
                    },
                    fieldDirty: function(e) {
                        var t = e.data(m.defaultValue);
                        null == t ? t = "" : Array.isArray(t) && (t = t.toString());
                        var n = e.val();
                        null == n ? n = "" : Array.isArray(n) && (n = n.toString());
                        e = /^(true|false)$/i;
                        return e.test(t) && e.test(n) ? !new RegExp("^" + t + "$", "i").test(n) : n !== t;
                    },
                    checkboxDirty: function(e) {
                        return e.data(m.defaultValue) !== e.is(":checked");
                    },
                    justDirty: function() {
                        return "dirty" === C[0];
                    },
                    justClean: function() {
                        return "clean" === C[0];
                    }
                },
                removeEvents: function() {
                    h.off(s1), n2.off(s1), e3.off(s1), n2.off(s1);
                },
                event: {
                    field: {
                        keydown: function(e) {
                            var t = M(this), n = e.which, i = t.is(g.input), o = t.is(g.checkbox), a = 0 < t.closest(g.uiDropdown).length, r = 13;
                            n == 27 && (w.verbose("Escape key pressed blurring field"), t[0].blur()), e.ctrlKey || n != r || !i || a || o || (y || (t.one("keyup" + s1, w.event.field.keyup), w.submit(), w.debug("Enter pressed on input submitting form")), y = !0);
                        },
                        keyup: function() {
                            y = !1;
                        },
                        blur: function(e) {
                            var t = M(this), n = t.closest(d1), i = w.get.validation(t);
                            i && ("blur" == f.on || n.hasClass(p.error) && f.revalidate) && (w.debug("Revalidating field", t, i), w.validate.field(i), f.inline || w.validate.form(!1, !0));
                        },
                        change: function(e) {
                            var t = M(this), n = t.closest(d1), i = w.get.validation(t);
                            i && ("change" == f.on || n.hasClass(p.error) && f.revalidate) && (clearTimeout(w.timer), w.timer = setTimeout(function() {
                                w.debug("Revalidating field", t, i), w.validate.field(i), f.inline || w.validate.form(!1, !0);
                            }, f.delay));
                        }
                    },
                    beforeUnload: function(e) {
                        if (w.is.dirty() && !r2) return (e = e || I.event) && (e.returnValue = f.text.leavingMessage), f.text.leavingMessage;
                    }
                },
                get: {
                    ancillaryValue: function(e) {
                        return !(!e.type || !e.value && !w.is.bracketedRule(e)) && (e.value !== L ? e.value : e.type.match(f.regExp.bracket)[1] + "");
                    },
                    ruleName: function(e) {
                        return w.is.bracketedRule(e) ? e.type.replace(e.type.match(f.regExp.bracket)[0], "") : e.type;
                    },
                    changeEvent: function(e, t) {
                        return "checkbox" == e || "radio" == e || "hidden" == e || t.is("select") ? "change" : w.get.inputEvent();
                    },
                    inputEvent: function() {
                        return j.createElement("input").oninput !== L ? "input" : j.createElement("input").onpropertychange !== L ? "propertychange" : "keyup";
                    },
                    fieldsFromShorthand: function(e6) {
                        var i = {
                        };
                        return M.each(e6, function(n, e) {
                            Array.isArray(e) || "object" != typeof e ? ("string" == typeof e && (e = [
                                e
                            ]), i[n] = {
                                rules: []
                            }, M.each(e, function(e, t) {
                                i[n].rules.push({
                                    type: t
                                });
                            })) : i[n] = e;
                        }), i;
                    },
                    prompt: function(e, t) {
                        var n = w.get.ruleName(e), i = w.get.ancillaryValue(e), o = w.get.field(t.identifier), a = o.val(), r = M.isFunction(e.prompt) ? e.prompt(a) : e.prompt || f.prompt[n] || f.text.unspecifiedRule, s = -1 !== r.search("{value}"), l = -1 !== r.search("{name}");
                        return i && 0 <= i.indexOf("..") && (a = i.split("..", 2), e.prompt || (r += ("" === a[0] ? f.prompt.maxValue.replace(/\{ruleValue\}/g, "{max}") : "" === a[1] ? f.prompt.minValue.replace(/\{ruleValue\}/g, "{min}") : f.prompt.range).replace(/\{name\}/g, " " + f.text.and)), r = (r = r.replace(/\{min\}/g, a[0])).replace(/\{max\}/g, a[1])), s && (r = r.replace(/\{value\}/g, o.val())), l && (o = 1 == (l = o.closest(g.group).find("label").eq(0)).length ? l.text() : o.prop("placeholder") || f.text.unspecifiedField, r = r.replace(/\{name\}/g, o)), r = (r = r.replace(/\{identifier\}/g, t.identifier)).replace(/\{ruleValue\}/g, i), e.prompt || w.verbose("Using default validation prompt for type", r, n), r;
                    },
                    settings: function() {
                        var e;
                        M.isPlainObject(k) ? 0 < (e = Object.keys(k)).length && k[e[0]].identifier !== L && k[e[0]].rules !== L ? (f = M.extend(!0, {
                        }, M.fn.form.settings, P), l1 = M.extend({
                        }, M.fn.form.settings.defaults, k), w.error(f.error.oldSyntax, v), w.verbose("Extending settings from legacy parameters", l1, f)) : (k.fields && (k.fields = w.get.fieldsFromShorthand(k.fields)), f = M.extend(!0, {
                        }, M.fn.form.settings, k), l1 = M.extend({
                        }, M.fn.form.settings.defaults, f.fields), w.verbose("Extending settings", l1, f)) : (f = M.fn.form.settings, l1 = M.fn.form.settings.defaults, w.verbose("Using default form validation", l1, f)), o2 = f.namespace, m = f.metadata, g = f.selector, p = f.className, i2 = f.regExp, c1 = f.error, a2 = "module-" + o2, s1 = "." + o2, ((u1 = h.data(a2)) || w).refresh();
                    },
                    field: function(e) {
                        var t;
                        return w.verbose("Finding field with identifier", e), e = w.escape.string(e), 0 < (t = n2.filter("#" + e)).length || 0 < (t = n2.filter('[name="' + e + '"]')).length || 0 < (t = n2.filter('[name="' + e + '[]"]')).length || 0 < (t = n2.filter("[data-" + m.validate + '="' + e + '"]')).length ? t : M("<input/>");
                    },
                    fields: function(e) {
                        var n = M();
                        return M.each(e, function(e, t) {
                            n = n.add(w.get.field(t));
                        }), n;
                    },
                    validation: function(i) {
                        var o, a;
                        return !!l1 && (M.each(l1, function(e, n) {
                            a = n.identifier || e, M.each(w.get.field(a), function(e, t) {
                                if (t == i[0]) return n.identifier = a, o = n, !1;
                            });
                        }), o || !1);
                    },
                    value: function(e) {
                        var t = [];
                        return t.push(e), w.get.values.call(v, t)[e];
                    },
                    values: function(e) {
                        var e = Array.isArray(e) ? w.get.fields(e) : n2, d = {
                        };
                        return e.each(function(e, t) {
                            var n = M(t), i = n.closest(g.uiCalendar), o = n.prop("name"), a = n.val(), r = n.is(g.checkbox), s = n.is(g.radio), l = -1 !== o.indexOf("[]"), t = 0 < i.length && w.can.useElement("calendar"), n = !!r && n.is(":checked");
                            if (o) {
                                if (l) o = o.replace("[]", ""), d[o] || (d[o] = []), r ? n ? d[o].push(a || !0) : d[o].push(!1) : d[o].push(a);
                                else if (s) d[o] !== L && !1 !== d[o] || (d[o] = !!n && (a || !0));
                                else if (r) d[o] = !!n && (a || !0);
                                else if (t) {
                                    var c = i.calendar("get date");
                                    if (null !== c) {
                                        if ("date" == f.dateHandling) d[o] = c;
                                        else if ("input" == f.dateHandling) d[o] = i.calendar("get input date");
                                        else if ("formatter" == f.dateHandling) {
                                            var u = i.calendar("setting", "type");
                                            switch(u){
                                                case "date":
                                                    d[o] = f.formatter.date(c);
                                                    break;
                                                case "datetime":
                                                    d[o] = f.formatter.datetime(c);
                                                    break;
                                                case "time":
                                                    d[o] = f.formatter.time(c);
                                                    break;
                                                case "month":
                                                    d[o] = f.formatter.month(c);
                                                    break;
                                                case "year":
                                                    d[o] = f.formatter.year(c);
                                                    break;
                                                default:
                                                    w.debug("Wrong calendar mode", i, u), d[o] = "";
                                            }
                                        }
                                    } else d[o] = "";
                                } else d[o] = a;
                            }
                        }), d;
                    },
                    dirtyFields: function() {
                        return n2.filter(function(e, t) {
                            return M(t).data(m.isDirty);
                        });
                    }
                },
                has: {
                    field: function(e) {
                        return w.verbose("Checking for existence of a field with identifier", e), "string" != typeof (e = w.escape.string(e)) && w.error(c1.identifier, e), 0 < n2.filter("#" + e).length || 0 < n2.filter('[name="' + e + '"]').length || 0 < n2.filter("[data-" + m.validate + '="' + e + '"]').length;
                    }
                },
                can: {
                    useElement: function(e) {
                        return M.fn[e] !== L || (w.error(c1.noElement.replace("{element}", e)), !1);
                    }
                },
                escape: {
                    string: function(e) {
                        return (e = String(e)).replace(i2.escape, "\\$&");
                    }
                },
                add: {
                    rule: function(e, t) {
                        w.add.field(e, t);
                    },
                    field: function(n, e7) {
                        l1[n] !== L && l1[n].rules !== L || (l1[n] = {
                            rules: []
                        });
                        var i = {
                            rules: []
                        };
                        w.is.shorthandRules(e7) ? (e7 = Array.isArray(e7) ? e7 : [
                            e7
                        ], M.each(e7, function(e, t) {
                            i.rules.push({
                                type: t
                            });
                        })) : i.rules = e7.rules, M.each(i.rules, function(e8, t) {
                            0 == M.grep(l1[n].rules, function(e) {
                                return e.type == t.type;
                            }).length && l1[n].rules.push(t);
                        }), w.debug("Adding rules", i.rules, l1);
                    },
                    fields: function(e) {
                        l1 = M.extend({
                        }, l1, w.get.fieldsFromShorthand(e));
                    },
                    prompt: function(e, t, n) {
                        var i = w.get.field(e).closest(d1), o = i.children(g.prompt), a = 0 !== o.length;
                        t = "string" == typeof t ? [
                            t
                        ] : t, w.verbose("Adding field error state", e), n || i.addClass(p.error), f.inline && (a || (o = f.templates.prompt(t, p.label)).appendTo(i), o.html(t[0]), a ? w.verbose("Inline errors are disabled, no inline error added", e) : f.transition && w.can.useElement("transition") && h.transition("is supported") ? (w.verbose("Displaying error with css transition", f.transition), o.transition(f.transition + " in", f.duration)) : (w.verbose("Displaying error with fallback javascript animation"), o.fadeIn(f.duration)));
                    },
                    errors: function(e) {
                        w.debug("Adding form error messages", e), w.set.error(), t3.html(f.templates.error(e));
                    }
                },
                remove: {
                    errors: function() {
                        w.debug("Removing form error messages"), t3.empty();
                    },
                    states: function() {
                        h.removeClass(p.error).removeClass(p.success), f.inline || w.remove.errors(), w.determine.isDirty();
                    },
                    rule: function(n, e9) {
                        var i = Array.isArray(e9) ? e9 : [
                            e9
                        ];
                        if (l1[n] !== L && Array.isArray(l1[n].rules)) return e9 === L ? (w.debug("Removed all rules"), void (l1[n].rules = [])) : void M.each(l1[n].rules, function(e, t) {
                            t && -1 !== i.indexOf(t.type) && (w.debug("Removed rule", t.type), l1[n].rules.splice(e, 1));
                        });
                    },
                    field: function(e) {
                        e = Array.isArray(e) ? e : [
                            e
                        ];
                        M.each(e, function(e, t) {
                            w.remove.rule(t);
                        });
                    },
                    rules: function(e, n) {
                        Array.isArray(e) ? M.each(e, function(e, t) {
                            w.remove.rule(t, n);
                        }) : w.remove.rule(e, n);
                    },
                    fields: function(e) {
                        w.remove.field(e);
                    },
                    prompt: function(e) {
                        var t = w.get.field(e).closest(d1), n = t.children(g.prompt);
                        t.removeClass(p.error), f.inline && n.is(":visible") && (w.verbose("Removing prompt for field", e), f.transition && w.can.useElement("transition") && h.transition("is supported") ? n.transition(f.transition + " out", f.duration, function() {
                            n.remove();
                        }) : n.fadeOut(f.duration, function() {
                            n.remove();
                        }));
                    }
                },
                set: {
                    success: function() {
                        h.removeClass(p.error).addClass(p.success);
                    },
                    defaults: function() {
                        n2.each(function(e, t) {
                            var n = M(t), i = n.parent(), o = 0 < n.filter(g.checkbox).length, a = i.is(g.uiDropdown) && w.can.useElement("dropdown"), r = n.closest(g.uiCalendar), t = 0 < r.length && w.can.useElement("calendar"), o = o ? n.is(":checked") : n.val();
                            a ? i.dropdown("save defaults") : t && r.calendar("refresh"), n.data(m.defaultValue, o), n.data(m.isDirty, !1);
                        });
                    },
                    error: function() {
                        h.removeClass(p.success).addClass(p.error);
                    },
                    value: function(e, t) {
                        var n = {
                        };
                        return n[e] = t, w.set.values.call(v, n);
                    },
                    values: function(e10) {
                        M.isEmptyObject(e10) || M.each(e10, function(e, t4) {
                            var n, i = w.get.field(e), o = i.parent(), a = i.closest(g.uiCalendar), r = Array.isArray(t4), s = o.is(g.uiCheckbox) && w.can.useElement("checkbox"), l = o.is(g.uiDropdown) && w.can.useElement("dropdown"), c = i.is(g.radio) && s, e = 0 < a.length && w.can.useElement("calendar");
                            0 < i.length && (r && s ? (w.verbose("Selecting multiple", t4, i), o.checkbox("uncheck"), M.each(t4, function(e, t) {
                                n = i.filter('[value="' + t + '"]'), o = n.parent(), 0 < n.length && o.checkbox("check");
                            })) : c ? (w.verbose("Selecting radio value", t4, i), i.filter('[value="' + t4 + '"]').parent(g.uiCheckbox).checkbox("check")) : s ? (w.verbose("Setting checkbox value", t4, o), !0 === t4 || 1 === t4 ? o.checkbox("check") : o.checkbox("uncheck")) : l ? (w.verbose("Setting dropdown value", t4, o), o.dropdown("set selected", t4)) : e ? a.calendar("set date", t4) : (w.verbose("Setting field value", t4, i), i.val(t4)));
                        });
                    },
                    dirty: function() {
                        w.verbose("Setting state dirty"), x = !0, C[0] = C[1], C[1] = "dirty", w.is.justClean() && h.trigger("dirty");
                    },
                    clean: function() {
                        w.verbose("Setting state clean"), x = !1, C[0] = C[1], C[1] = "clean", w.is.justDirty() && h.trigger("clean");
                    },
                    asClean: function() {
                        w.set.defaults(), w.set.clean();
                    },
                    asDirty: function() {
                        w.set.defaults(), w.set.dirty();
                    },
                    autoCheck: function() {
                        w.debug("Enabling auto check on required fields"), n2.each(function(e11, t) {
                            var n = M(t), i = M(t).closest(d1), o = 0 < n.filter(g.checkbox).length, a = n.prop("required") || i.hasClass(p.required) || i.parent().hasClass(p.required), r = n.is(":disabled") || i.hasClass(p.disabled) || i.parent().hasClass(p.disabled), t = w.get.validation(n), i = !!t && 0 !== M.grep(t.rules, function(e) {
                                return "empty" == e.type;
                            }), n = t.identifier || n.attr("id") || n.attr("name") || n.data(m.validate);
                            !a || r || i || n === L || (o ? (w.verbose("Adding 'checked' rule on field", n), w.add.rule(n, "checked")) : (w.verbose("Adding 'empty' rule on field", n), w.add.rule(n, "empty")));
                        });
                    },
                    optional: function(n, i) {
                        i = !1 !== i, M.each(l1, function(e, t) {
                            n != e && n != t.identifier || (t.optional = i);
                        });
                    }
                },
                validate: {
                    form: function(e, t) {
                        var n, i = w.get.values();
                        if (y) return !1;
                        if (b = [], w.determine.isValid()) {
                            if (w.debug("Form has no validation errors, submitting"), w.set.success(), f.inline || w.remove.errors(), !0 !== t) return f.onSuccess.call(v, e, i);
                        } else if (w.debug("Form has errors"), r2 = !1, w.set.error(), f.inline || w.add.errors(b), e && h.data("moduleApi") !== L && e.stopImmediatePropagation(), f.errorFocus && (e = !0, "string" == typeof f.errorFocus ? (e = (n = M(f.errorFocus)).is("[tabindex]")) || n.attr("tabindex", -1) : n = d1.filter("." + p.error).first().find(g.field), n.focus(), e || n.removeAttr("tabindex")), !0 !== t) return f.onFailure.call(v, b, i);
                    },
                    field: function(i, e, o) {
                        o = o === L || o, "string" == typeof i && (w.verbose("Validating field", i), i = l1[e = i]);
                        var a = i.identifier || e, t5 = w.get.field(a), e = !!i.depends && w.get.field(i.depends), r = !0, s = [];
                        return i.identifier || (w.debug("Using field name as identifier", a), i.identifier = a), !t5.filter(":not(:disabled)").length ? w.debug("Field is disabled. Skipping", a) : i.optional && w.is.blank(t5) ? w.debug("Field is optional and blank. Skipping", a) : i.depends && w.is.empty(e) ? w.debug("Field depends on another value that is not present or empty. Skipping", e) : i.rules !== L && (o && t5.closest(d1).removeClass(p.error), M.each(i.rules, function(e, t) {
                            var n;
                            !w.has.field(a) || 0 < (n = w.validate.rule(i, t, !0) || []).length && (w.debug("Field is invalid", a, t.type), s.push(w.get.prompt(t, i)), r = !1, o && M(n).closest(d1).addClass(p.error));
                        })), r ? (o && (w.remove.prompt(a, s), f.onValid.call(t5)), !0) : (o && (b = b.concat(s), w.add.prompt(a, s, !0), f.onInvalid.call(t5, s)), !1);
                    },
                    rule: function(e12, n, t6) {
                        function i(e) {
                            var t = (t = (l ? M(e).filter(":checked") : M(e)).val()) === L || "" === t || null === t ? "" : f.shouldTrim && !1 !== n.shouldTrim || n.shouldTrim ? String(t + "").trim() : String(t + "");
                            return r.call(e, t, a, h);
                        }
                        var o = w.get.field(e12.identifier), a = w.get.ancillaryValue(n), e12 = w.get.ruleName(n), r = f.rules[e12], s = [], l = o.is(g.checkbox);
                        if (M.isFunction(r)) return l ? i(o) || (s = o) : M.each(o, function(e, t) {
                            i(t) || s.push(t);
                        }), t6 ? s : !(0 < s.length);
                        w.error(c1.noRule, e12);
                    }
                },
                setting: function(e, t) {
                    if (M.isPlainObject(e)) M.extend(!0, f, e);
                    else {
                        if (t === L) return f[e];
                        f[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (M.isPlainObject(e)) M.extend(!0, w, e);
                    else {
                        if (t === L) return w[e];
                        w[e] = t;
                    }
                },
                debug: function() {
                    !f.silent && f.debug && (f.performance ? w.performance.log(arguments) : (w.debug = Function.prototype.bind.call(console.info, console, f.name + ":"), w.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !f.silent && f.verbose && f.debug && (f.performance ? w.performance.log(arguments) : (w.verbose = Function.prototype.bind.call(console.info, console, f.name + ":"), w.verbose.apply(console, arguments)));
                },
                error: function() {
                    f.silent || (w.error = Function.prototype.bind.call(console.error, console, f.name + ":"), w.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        f.performance && (n = (t = (new Date).getTime()) - (A || t), A = t, E.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: v,
                            "Execution Time": n
                        })), clearTimeout(w.performance.timer), w.performance.timer = setTimeout(w.performance.display, 500);
                    },
                    display: function() {
                        var e = f.name + ":", n = 0;
                        A = !1, clearTimeout(w.performance.timer), M.each(E, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", D && (e += " '" + D + "'"), 1 < S.length && (e += " (" + S.length + ")"), (console.group !== L || console.table !== L) && 0 < E.length && (console.groupCollapsed(e), console.table ? console.table(E) : M.each(E, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), E = [];
                    }
                },
                invoke: function(i, e13, t7) {
                    var o, a, n, r = u1;
                    return e13 = e13 || R, t7 = v || t7, "string" == typeof i && r !== L && (i = i.split(/[\. ]/), o = i.length - 1, M.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (M.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== L) return a = r[n], !1;
                            if (!M.isPlainObject(r[t]) || e == o) return r[t] !== L && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), M.isFunction(a) ? n = a.apply(t7, e13) : a !== L && (n = a), Array.isArray(T) ? T.push(n) : T !== L ? T = [
                        T,
                        n
                    ] : n !== L && (T = n), a;
                }
            };
            w.initialize();
        }), T !== L ? T : this;
    }, M.fn.form.settings = {
        name: "Form",
        namespace: "form",
        debug: !1,
        verbose: !1,
        performance: !0,
        fields: !1,
        keyboardShortcuts: !0,
        on: "submit",
        inline: !1,
        delay: 200,
        revalidate: !0,
        shouldTrim: !0,
        transition: "scale",
        duration: 200,
        autoCheckRequired: !1,
        preventLeaving: !1,
        errorFocus: !1,
        dateHandling: "date",
        onValid: function() {
        },
        onInvalid: function() {
        },
        onSuccess: function() {
            return !0;
        },
        onFailure: function() {
            return !1;
        },
        onDirty: function() {
        },
        onClean: function() {
        },
        metadata: {
            defaultValue: "default",
            validate: "validate",
            isDirty: "isDirty"
        },
        regExp: {
            htmlID: /^[a-zA-Z][\w:.-]*$/g,
            bracket: /\[(.*)\]/i,
            decimal: /^\d+\.?\d*$/,
            email: /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
            escape: /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|:,=@]/g,
            flags: /^\/(.*)\/(.*)?/,
            integer: /^\-?\d+$/,
            number: /^\-?\d*(\.\d+)?$/,
            url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i
        },
        text: {
            and: "and",
            unspecifiedRule: "Please enter a valid value",
            unspecifiedField: "This field",
            leavingMessage: "There are unsaved changes on this page which will be discarded if you continue."
        },
        prompt: {
            range: "{name} must be in a range from {min} to {max}",
            maxValue: "{name} must have a maximum value of {ruleValue}",
            minValue: "{name} must have a minimum value of {ruleValue}",
            empty: "{name} must have a value",
            checked: "{name} must be checked",
            email: "{name} must be a valid e-mail",
            url: "{name} must be a valid url",
            regExp: "{name} is not formatted correctly",
            integer: "{name} must be an integer",
            decimal: "{name} must be a decimal number",
            number: "{name} must be set to a number",
            is: '{name} must be "{ruleValue}"',
            isExactly: '{name} must be exactly "{ruleValue}"',
            not: '{name} cannot be set to "{ruleValue}"',
            notExactly: '{name} cannot be set to exactly "{ruleValue}"',
            contain: '{name} must contain "{ruleValue}"',
            containExactly: '{name} must contain exactly "{ruleValue}"',
            doesntContain: '{name} cannot contain  "{ruleValue}"',
            doesntContainExactly: '{name} cannot contain exactly "{ruleValue}"',
            minLength: "{name} must be at least {ruleValue} characters",
            length: "{name} must be at least {ruleValue} characters",
            exactLength: "{name} must be exactly {ruleValue} characters",
            maxLength: "{name} cannot be longer than {ruleValue} characters",
            match: "{name} must match {ruleValue} field",
            different: "{name} must have a different value than {ruleValue} field",
            creditCard: "{name} must be a valid credit card number",
            minCount: "{name} must have at least {ruleValue} choices",
            exactCount: "{name} must have exactly {ruleValue} choices",
            maxCount: "{name} must have {ruleValue} or less choices"
        },
        selector: {
            checkbox: 'input[type="checkbox"], input[type="radio"]',
            clear: ".clear",
            field: 'input:not(.search):not([type="file"]), textarea, select',
            group: ".field",
            input: 'input:not([type="file"])',
            message: ".error.message",
            prompt: ".prompt.label",
            radio: 'input[type="radio"]',
            reset: '.reset:not([type="reset"])',
            submit: '.submit:not([type="submit"])',
            uiCheckbox: ".ui.checkbox",
            uiDropdown: ".ui.dropdown",
            uiCalendar: ".ui.calendar"
        },
        className: {
            error: "error",
            label: "ui basic red pointing prompt label",
            pressed: "down",
            success: "success",
            required: "required",
            disabled: "disabled"
        },
        error: {
            identifier: "You must specify a string identifier for each field",
            method: "The method you called is not defined.",
            noRule: "There is no rule matching the one you specified",
            oldSyntax: "Starting in 2.0 forms now only take a single settings object. Validation settings converted to new syntax automatically.",
            noElement: "This module requires ui {element}"
        },
        templates: {
            error: function(e) {
                var n = '<ul class="list">';
                return M.each(e, function(e, t) {
                    n += "<li>" + t + "</li>";
                }), M(n += "</ul>");
            },
            prompt: function(e, t) {
                return M("<div/>").addClass(t).html(e[0]);
            }
        },
        formatter: {
            date: function(e) {
                return Intl.DateTimeFormat("en-GB").format(e);
            },
            datetime: function(e) {
                return Intl.DateTimeFormat("en-GB", {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit",
                    second: "2-digit"
                }).format(e);
            },
            time: function(e) {
                return Intl.DateTimeFormat("en-GB", {
                    hour: "2-digit",
                    minute: "2-digit",
                    second: "2-digit"
                }).format(e);
            },
            month: function(e) {
                return Intl.DateTimeFormat("en-GB", {
                    month: "2-digit",
                    year: "numeric"
                }).format(e);
            },
            year: function(e) {
                return Intl.DateTimeFormat("en-GB", {
                    year: "numeric"
                }).format(e);
            }
        },
        rules: {
            empty: function(e) {
                return !(e === L || "" === e || Array.isArray(e) && 0 === e.length);
            },
            checked: function() {
                return 0 < M(this).filter(":checked").length;
            },
            email: function(e) {
                return M.fn.form.settings.regExp.email.test(e);
            },
            url: function(e) {
                return M.fn.form.settings.regExp.url.test(e);
            },
            regExp: function(e, t) {
                if (t instanceof RegExp) return e.match(t);
                var n, i = t.match(M.fn.form.settings.regExp.flags);
                return i && (t = 2 <= i.length ? i[1] : t, n = 3 <= i.length ? i[2] : ""), e.match(new RegExp(t, n));
            },
            minValue: function(e, t) {
                return M.fn.form.settings.rules.range(e, t + "..", "number");
            },
            maxValue: function(e, t) {
                return M.fn.form.settings.rules.range(e, ".." + t, "number");
            },
            integer: function(e, t) {
                return M.fn.form.settings.rules.range(e, t, "integer");
            },
            range: function(e, t, n) {
                var i, o;
                return (n = "string" == typeof n ? M.fn.form.settings.regExp[n] : n) instanceof RegExp || (n = M.fn.form.settings.regExp.integer), t && -1 === [
                    "",
                    ".."
                ].indexOf(t) && (-1 == t.indexOf("..") ? n.test(t) && (i = o = +t) : (t = t.split("..", 2), n.test(t[0]) && (i = +t[0]), n.test(t[1]) && (o = +t[1]))), n.test(e) && (i === L || i <= e) && (o === L || e <= o);
            },
            decimal: function(e, t) {
                return M.fn.form.settings.rules.range(e, t, "decimal");
            },
            number: function(e, t) {
                return M.fn.form.settings.rules.range(e, t, "number");
            },
            is: function(e, t) {
                return t = "string" == typeof t ? t.toLowerCase() : t, (e = "string" == typeof e ? e.toLowerCase() : e) == t;
            },
            isExactly: function(e, t) {
                return e == t;
            },
            not: function(e, t) {
                return (e = "string" == typeof e ? e.toLowerCase() : e) != (t = "string" == typeof t ? t.toLowerCase() : t);
            },
            notExactly: function(e, t) {
                return e != t;
            },
            contains: function(e, t) {
                return t = t.replace(M.fn.form.settings.regExp.escape, "\\$&"), -1 !== e.search(new RegExp(t, "i"));
            },
            containsExactly: function(e, t) {
                return t = t.replace(M.fn.form.settings.regExp.escape, "\\$&"), -1 !== e.search(new RegExp(t));
            },
            doesntContain: function(e, t) {
                return t = t.replace(M.fn.form.settings.regExp.escape, "\\$&"), -1 === e.search(new RegExp(t, "i"));
            },
            doesntContainExactly: function(e, t) {
                return t = t.replace(M.fn.form.settings.regExp.escape, "\\$&"), -1 === e.search(new RegExp(t));
            },
            minLength: function(e, t) {
                return e !== L && e.length >= t;
            },
            length: function(e, t) {
                return e !== L && e.length >= t;
            },
            exactLength: function(e, t) {
                return e !== L && e.length == t;
            },
            maxLength: function(e, t) {
                return e !== L && e.length <= t;
            },
            match: function(e, t, n) {
                var i, o;
                return 0 < (o = n.find('[data-validate="' + t + '"]')).length || 0 < (o = n.find("#" + t)).length || 0 < (o = n.find('[name="' + t + '"]')).length ? i = o.val() : 0 < (o = n.find('[name="' + t + '[]"]')).length && (i = o), i !== L && e.toString() == i.toString();
            },
            different: function(e, t, n) {
                var i, o;
                return 0 < (o = n.find('[data-validate="' + t + '"]')).length || 0 < (o = n.find("#" + t)).length || 0 < (o = n.find('[name="' + t + '"]')).length ? i = o.val() : 0 < (o = n.find('[name="' + t + '[]"]')).length && (i = o), i !== L && e.toString() !== i.toString();
            },
            creditCard: function(n, e) {
                var i, o, a = {
                    visa: {
                        pattern: /^4/,
                        length: [
                            16
                        ]
                    },
                    amex: {
                        pattern: /^3[47]/,
                        length: [
                            15
                        ]
                    },
                    mastercard: {
                        pattern: /^5[1-5]/,
                        length: [
                            16
                        ]
                    },
                    discover: {
                        pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
                        length: [
                            16
                        ]
                    },
                    unionPay: {
                        pattern: /^(62|88)/,
                        length: [
                            16,
                            17,
                            18,
                            19
                        ]
                    },
                    jcb: {
                        pattern: /^35(2[89]|[3-8][0-9])/,
                        length: [
                            16
                        ]
                    },
                    maestro: {
                        pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
                        length: [
                            12,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18,
                            19
                        ]
                    },
                    dinersClub: {
                        pattern: /^(30[0-5]|^36)/,
                        length: [
                            14
                        ]
                    },
                    laser: {
                        pattern: /^(6304|670[69]|6771)/,
                        length: [
                            16,
                            17,
                            18,
                            19
                        ]
                    },
                    visaElectron: {
                        pattern: /^(4026|417500|4508|4844|491(3|7))/,
                        length: [
                            16
                        ]
                    }
                }, r = !1, e = "string" == typeof e && e.split(",");
                if ("string" == typeof n && 0 !== n.length) {
                    if (n = n.replace(/[\s\-]/g, ""), e && (M.each(e, function(e, t) {
                        (o = a[t]) && (i = {
                            length: -1 !== M.inArray(n.length, o.length),
                            pattern: -1 !== n.search(o.pattern)
                        }).length && i.pattern && (r = !0);
                    }), !r)) return !1;
                    if ((e = {
                        number: -1 !== M.inArray(n.length, a.unionPay.length),
                        pattern: -1 !== n.search(a.unionPay.pattern)
                    }).number && e.pattern) return !0;
                    for(var t = n.length, s = 0, l = [
                        [
                            0,
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8,
                            9
                        ],
                        [
                            0,
                            2,
                            4,
                            6,
                            8,
                            1,
                            3,
                            5,
                            7,
                            9
                        ]
                    ], c = 0; t--;)c += l[s][parseInt(n.charAt(t), 10)], s ^= 1;
                    return c % 10 == 0 && 0 < c;
                }
            },
            minCount: function(e, t) {
                return 0 == t || (1 == t ? "" !== e : e.split(",").length >= t);
            },
            exactCount: function(e, t) {
                return 0 == t ? "" === e : 1 == t ? "" !== e && -1 === e.search(",") : e.split(",").length == t;
            },
            maxCount: function(e, t) {
                return 0 != t && (1 == t ? -1 === e.search(",") : e.split(",").length <= t);
            }
        }
    };
})(jQuery, window, document), (function(k, T, S) {
    "use strict";
    k.isFunction = k.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, T = void 0 !== T && T.Math == Math ? T : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), k.fn.accordion = function(a3) {
        var h, v = k(this), b = (new Date).getTime(), y = [], x = a3, C = "string" == typeof x, w = [].slice.call(arguments, 1);
        return v.each(function() {
            var e14, r3 = k.isPlainObject(a3) ? k.extend(!0, {
            }, k.fn.accordion.settings, a3) : k.extend({
            }, k.fn.accordion.settings), s = r3.className, t8 = r3.namespace, l = r3.selector, c = r3.error, n3 = "." + t8, i3 = "module-" + t8, o3 = v.selector || "", u = k(this), d = u.find(l.title), f = u.find(l.content), m = this, g = u.data(i3), p = {
                initialize: function() {
                    p.debug("Initializing", u), p.bind.events(), r3.observeChanges && p.observeChanges(), p.instantiate();
                },
                instantiate: function() {
                    g = p, u.data(i3, p);
                },
                destroy: function() {
                    p.debug("Destroying previous instance", u), u.off(n3).removeData(i3);
                },
                refresh: function() {
                    d = u.find(l.title), f = u.find(l.content);
                },
                observeChanges: function() {
                    "MutationObserver" in T && ((e14 = new MutationObserver(function(e) {
                        p.debug("DOM tree modified, updating selector cache"), p.refresh();
                    })).observe(m, {
                        childList: !0,
                        subtree: !0
                    }), p.debug("Setting up mutation observer", e14));
                },
                bind: {
                    events: function() {
                        p.debug("Binding delegated events"), u.on(r3.on + n3, l.trigger, p.event.click);
                    }
                },
                event: {
                    click: function() {
                        p.toggle.call(this);
                    }
                },
                toggle: function(e) {
                    var t = e !== S ? "number" == typeof e ? d.eq(e) : k(e).closest(l.title) : k(this).closest(l.title), n = t.next(f), i = n.hasClass(s.animating), e = n.hasClass(s.active), n = e && !i, i = !e && i;
                    p.debug("Toggling visibility of content", t), n || i ? r3.collapsible ? p.close.call(t) : p.debug("Cannot close accordion content collapsing is disabled") : p.open.call(t);
                },
                open: function(e) {
                    var t = e !== S ? "number" == typeof e ? d.eq(e) : k(e).closest(l.title) : k(this).closest(l.title), n = t.next(f), e = n.hasClass(s.animating);
                    n.hasClass(s.active) || e ? p.debug("Accordion already open, skipping", n) : (p.debug("Opening accordion content", t), r3.onOpening.call(n), r3.onChanging.call(n), r3.exclusive && p.closeOthers.call(t), t.addClass(s.active), n.stop(!0, !0).addClass(s.animating), r3.animateChildren && (k.fn.transition !== S && u.transition("is supported") ? n.children().transition({
                        animation: "fade in",
                        queue: !1,
                        useFailSafe: !0,
                        debug: r3.debug,
                        verbose: r3.verbose,
                        duration: r3.duration,
                        skipInlineHidden: !0,
                        onComplete: function() {
                            n.children().removeClass(s.transition);
                        }
                    }) : n.children().stop(!0, !0).animate({
                        opacity: 1
                    }, r3.duration, p.resetOpacity)), n.slideDown(r3.duration, r3.easing, function() {
                        n.removeClass(s.animating).addClass(s.active), p.reset.display.call(this), r3.onOpen.call(this), r3.onChange.call(this);
                    }));
                },
                close: function(e) {
                    var t = e !== S ? "number" == typeof e ? d.eq(e) : k(e).closest(l.title) : k(this).closest(l.title), n = t.next(f), i = n.hasClass(s.animating), e = n.hasClass(s.active);
                    !e && !(!e && i) || e && i || (p.debug("Closing accordion content", n), r3.onClosing.call(n), r3.onChanging.call(n), t.removeClass(s.active), n.stop(!0, !0).addClass(s.animating), r3.animateChildren && (k.fn.transition !== S && u.transition("is supported") ? n.children().transition({
                        animation: "fade out",
                        queue: !1,
                        useFailSafe: !0,
                        debug: r3.debug,
                        verbose: r3.verbose,
                        duration: r3.duration,
                        skipInlineHidden: !0
                    }) : n.children().stop(!0, !0).animate({
                        opacity: 0
                    }, r3.duration, p.resetOpacity)), n.slideUp(r3.duration, r3.easing, function() {
                        n.removeClass(s.animating).removeClass(s.active), p.reset.display.call(this), r3.onClose.call(this), r3.onChange.call(this);
                    }));
                },
                closeOthers: function(e) {
                    var t, n = e !== S ? d.eq(e) : k(this).closest(l.title), i = n.parents(l.content).prev(l.title), o = n.closest(l.accordion), e = l.title + "." + s.active + ":visible", n = l.content + "." + s.active + ":visible", a = r3.closeNested ? (t = o.find(e).not(i)).next(f) : (t = o.find(e).not(i), a = o.find(n).find(e).not(i), (t = t.not(a)).next(f));
                    0 < t.length && (p.debug("Exclusive enabled, closing other content", t), t.removeClass(s.active), a.removeClass(s.animating).stop(!0, !0), r3.animateChildren && (k.fn.transition !== S && u.transition("is supported") ? a.children().transition({
                        animation: "fade out",
                        useFailSafe: !0,
                        debug: r3.debug,
                        verbose: r3.verbose,
                        duration: r3.duration,
                        skipInlineHidden: !0
                    }) : a.children().stop(!0, !0).animate({
                        opacity: 0
                    }, r3.duration, p.resetOpacity)), a.slideUp(r3.duration, r3.easing, function() {
                        k(this).removeClass(s.active), p.reset.display.call(this);
                    }));
                },
                reset: {
                    display: function() {
                        p.verbose("Removing inline display from element", this), k(this).css("display", ""), "" === k(this).attr("style") && k(this).attr("style", "").removeAttr("style");
                    },
                    opacity: function() {
                        p.verbose("Removing inline opacity from element", this), k(this).css("opacity", ""), "" === k(this).attr("style") && k(this).attr("style", "").removeAttr("style");
                    }
                },
                setting: function(e, t) {
                    if (p.debug("Changing setting", e, t), k.isPlainObject(e)) k.extend(!0, r3, e);
                    else {
                        if (t === S) return r3[e];
                        k.isPlainObject(r3[e]) ? k.extend(!0, r3[e], t) : r3[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (p.debug("Changing internal", e, t), t === S) return p[e];
                    k.isPlainObject(e) ? k.extend(!0, p, e) : p[e] = t;
                },
                debug: function() {
                    !r3.silent && r3.debug && (r3.performance ? p.performance.log(arguments) : (p.debug = Function.prototype.bind.call(console.info, console, r3.name + ":"), p.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !r3.silent && r3.verbose && r3.debug && (r3.performance ? p.performance.log(arguments) : (p.verbose = Function.prototype.bind.call(console.info, console, r3.name + ":"), p.verbose.apply(console, arguments)));
                },
                error: function() {
                    r3.silent || (p.error = Function.prototype.bind.call(console.error, console, r3.name + ":"), p.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        r3.performance && (n = (t = (new Date).getTime()) - (b || t), b = t, y.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: m,
                            "Execution Time": n
                        })), clearTimeout(p.performance.timer), p.performance.timer = setTimeout(p.performance.display, 500);
                    },
                    display: function() {
                        var e = r3.name + ":", n = 0;
                        b = !1, clearTimeout(p.performance.timer), k.each(y, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", o3 && (e += " '" + o3 + "'"), (console.group !== S || console.table !== S) && 0 < y.length && (console.groupCollapsed(e), console.table ? console.table(y) : k.each(y, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), y = [];
                    }
                },
                invoke: function(i, e15, t9) {
                    var o, a, n, r = g;
                    return e15 = e15 || w, t9 = m || t9, "string" == typeof i && r !== S && (i = i.split(/[\. ]/), o = i.length - 1, k.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (k.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== S) return a = r[n], !1;
                            if (!k.isPlainObject(r[t]) || e == o) return r[t] !== S ? a = r[t] : p.error(c.method, i), !1;
                            r = r[t];
                        }
                    })), k.isFunction(a) ? n = a.apply(t9, e15) : a !== S && (n = a), Array.isArray(h) ? h.push(n) : h !== S ? h = [
                        h,
                        n
                    ] : n !== S && (h = n), a;
                }
            };
            C ? (g === S && p.initialize(), p.invoke(x)) : (g !== S && g.invoke("destroy"), p.initialize());
        }), h !== S ? h : this;
    }, k.fn.accordion.settings = {
        name: "Accordion",
        namespace: "accordion",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        on: "click",
        observeChanges: !0,
        exclusive: !0,
        collapsible: !0,
        closeNested: !1,
        animateChildren: !0,
        duration: 350,
        easing: "easeOutQuad",
        onOpening: function() {
        },
        onClosing: function() {
        },
        onChanging: function() {
        },
        onOpen: function() {
        },
        onClose: function() {
        },
        onChange: function() {
        },
        error: {
            method: "The method you called is not defined"
        },
        className: {
            active: "active",
            animating: "animating",
            transition: "transition"
        },
        selector: {
            accordion: ".accordion",
            title: ".title",
            trigger: ".title",
            content: ".content"
        }
    }, k.extend(k.easing, {
        easeOutQuad: function(e, t, n, i, o) {
            return -i * (t /= o) * (t - 2) + n;
        }
    });
})(jQuery, window, void 0), (function(oe, S1, D1, ae) {
    "use strict";
    oe.isFunction = oe.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, S1 = void 0 !== S1 && S1.Math == Math ? S1 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), oe.fn.calendar = function(h1) {
        var v1, e16 = oe(this), b1 = e16.selector || "", y1 = (new Date).getTime(), x1 = [], C1 = h1, w1 = "string" == typeof C1, k1 = [].slice.call(arguments, 1), T1 = {
            5: {
                row: 4,
                column: 3
            },
            10: {
                row: 3,
                column: 2
            },
            15: {
                row: 2,
                column: 2
            },
            20: {
                row: 3,
                column: 1
            },
            30: {
                row: 2,
                column: 1
            }
        }, ie = [
            "",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight"
        ];
        return e16.each(function() {
            var d2, e17, $ = oe.isPlainObject(h1) ? oe.extend(!0, {
            }, oe.fn.calendar.settings, h1) : oe.extend({
            }, oe.fn.calendar.settings), G = $.className, t10 = $.namespace, i4 = $.selector, J = $.formatter, n4 = $.parser, Z = $.metadata, _ = T1[$.minTimeGap], s2 = $.error, o4 = "." + t10, a4 = "module-" + t10, r4 = oe(this), l2 = r4.find(i4.input), ee = r4.find(i4.popup), c2 = r4.find(i4.activator), u2 = this, f1 = r4.data(a4), m1 = !1, te = r4.hasClass(G.inverted), g1 = !1, p1 = !1, ne = {
                initialize: function() {
                    ne.debug("Initializing calendar for", u2, r4), d2 = ne.get.isTouch(), ne.setup.config(), ne.setup.popup(), ne.setup.inline(), ne.setup.input(), ne.setup.date(), ne.create.calendar(), ne.bind.events(), ne.observeChanges(), ne.instantiate();
                },
                instantiate: function() {
                    ne.verbose("Storing instance of calendar"), f1 = ne, r4.data(a4, f1);
                },
                destroy: function() {
                    ne.verbose("Destroying previous calendar for", u2), r4.removeData(a4), ne.unbind.events(), ne.disconnect.classObserver();
                },
                setup: {
                    config: function() {
                        null !== ne.get.minDate() && ne.set.minDate(r4.data(Z.minDate)), null !== ne.get.maxDate() && ne.set.maxDate(r4.data(Z.maxDate)), ne.setting("type", ne.get.type()), ne.setting("on", $.on || (l2.length ? "focus" : "click"));
                    },
                    popup: function() {
                        var e, t, n;
                        $.inline || (c2.length || (c2 = r4.children().first()).length) && (oe.fn.popup !== ae ? (ee.length || (n = 0 !== (t = c2.parent()).closest(i4.append).length ? "appendTo" : "prependTo", ee = oe("<div/>").addClass(G.popup)[n](t)), ee.addClass(G.calendar), te && ee.addClass(G.inverted), e = function() {
                            return ne.refreshTooltips(), $.onVisible.apply(ee, arguments);
                        }, n = $.onHidden, l2.length || (ee.attr("tabindex", "0"), e = function() {
                            return ne.refreshTooltips(), ne.focus(), $.onVisible.apply(ee, arguments);
                        }, n = function() {
                            return ne.blur(), $.onHidden.apply(ee, arguments);
                        }), t = ne.setting("on"), n = oe.extend({
                        }, $.popupOptions, {
                            popup: ee,
                            on: t,
                            hoverable: "hover" === t,
                            closable: "click" === t,
                            onShow: function() {
                                return ne.set.focusDate(ne.get.date()), ne.set.mode(ne.get.validatedMode($.startMode)), $.onShow.apply(ee, arguments);
                            },
                            onVisible: e,
                            onHide: $.onHide,
                            onHidden: n
                        }), ne.popup(n)) : ne.error(s2.popup));
                    },
                    inline: function() {
                        c2.length && !$.inline || ($.inline = !0, ee = oe("<div/>").addClass(G.calendar).appendTo(r4), l2.length || ee.attr("tabindex", "0"));
                    },
                    input: function() {
                        $.touchReadonly && l2.length && d2 && l2.prop("readonly", !0), ne.check.disabled();
                    },
                    date: function() {
                        var e;
                        $.initialDate ? e = n4.date($.initialDate, $) : r4.data(Z.date) !== ae ? e = n4.date(r4.data(Z.date), $) : l2.length && (e = n4.date(l2.val(), $)), ne.set.date(e, $.formatInput, !1), ne.set.mode(ne.get.mode(), !1);
                    }
                },
                trigger: {
                    change: function() {
                        var e, t = l2[0];
                        t && (e = D1.createEvent("HTMLEvents"), ne.verbose("Triggering native change event"), e.initEvent("change", !0, !1), t.dispatchEvent(e));
                    }
                },
                create: {
                    calendar: function() {
                        var e, t, n, i = ne.get.mode(), o = new Date, a = ne.get.date(), r = ne.get.focusDate(), s = ne.helper.dateInRange(r || a || $.initialDate || o);
                        r || ne.set.focusDate(r = s, !1, !1);
                        var l = "year" === i, c = "month" === i, u = "day" === i, d = "hour" === i, f = "minute" === i, m = "time" === $.type, g = Math.max($.multiMonth, 1), p = u ? ne.get.monthOffset() : 0, h = s.getMinutes(), v = s.getHours(), b = s.getDate(), y = s.getMonth() + p, x = s.getFullYear(), C = u ? $.showWeekNumbers ? 8 : 7 : d ? 4 : _.column, w = u || d ? 6 : _.row, k = u ? g : 1, T = ee, S = T.hasClass("left") ? "right center" : "left center";
                        for(T.empty(), 1 < k && (n = oe("<div/>").addClass(G.grid).appendTo(T)), t = 0; t < k; t++){
                            1 < k && (T = oe("<div/>").addClass(G.column).appendTo(n));
                            var D = y + t, A = (new Date(x, D, 1).getDay() - $.firstDayOfWeek % 7 + 7) % 7;
                            !$.constantHeight && u && (j = new Date(x, D + 1, 0).getDate() + A, w = Math.ceil(j / 7));
                            var E = l ? 10 : c ? 1 : 0, F = u ? 1 : 0, P = d || f ? 1 : 0, O = d || f ? b : 1, R = new Date(x - E, D - F, O - P, v), M = new Date(x + E, D + F, O + P, v), I = l ? new Date(10 * Math.ceil(x / 10) - 9, 0, 0) : c ? new Date(x, 0, 0) : u ? new Date(x, D, 0) : new Date(x, D, b, -1), j = l ? new Date(10 * Math.ceil(x / 10) + 1, 0, 1) : c ? new Date(x + 1, 0, 1) : u ? new Date(x, D + 1, 1) : new Date(x, D, b + 1), E = i;
                            u && $.showWeekNumbers && (E += " andweek");
                            F = oe("<table/>").addClass(G.table).addClass(E).addClass(ie[C] + " column").appendTo(T);
                            te && F.addClass(G.inverted);
                            var L = C;
                            if (!m) {
                                var O = oe("<thead/>").appendTo(F), V = oe("<tr/>").appendTo(O), q = oe("<th/>").attr("colspan", "" + C).appendTo(V), P = l || c ? new Date(x, 0, 1) : u ? new Date(x, D, 1) : new Date(x, D, b, v, h), E = oe("<span/>").addClass(G.link).appendTo(q);
                                E.text(J.header(P, i, $));
                                var z, P = c ? $.disableYear ? "day" : "year" : u ? $.disableMonth ? "year" : "month" : "day";
                                if (E.data(Z.mode, P), 0 === t && ((z = oe("<span/>").addClass(G.prev).appendTo(q)).data(Z.focusDate, R), z.toggleClass(G.disabledCell, !ne.helper.isDateInRange(I, i)), oe("<i/>").addClass(G.prevIcon).appendTo(z)), t === k - 1 && ((z = oe("<span/>").addClass(G.next).appendTo(q)).data(Z.focusDate, M), z.toggleClass(G.disabledCell, !ne.helper.isDateInRange(j, i)), oe("<i/>").addClass(G.nextIcon).appendTo(z)), u) for(V = oe("<tr/>").appendTo(O), $.showWeekNumbers && ((q = oe("<th/>").appendTo(V)).text($.text.weekNo), q.addClass(G.weekCell), L--), H = 0; H < L; H++)(q = oe("<th/>").appendTo(V)).text(J.dayColumnHeader((H + $.firstDayOfWeek) % 7, $));
                            }
                            for(var N = oe("<tbody/>").appendTo(F), H = l ? 10 * Math.ceil(x / 10) - 9 : u ? 1 - A : 0, U = 0; U < w; U++)for(V = oe("<tr/>").appendTo(N), u && $.showWeekNumbers && ((q = oe("<th/>").appendTo(V)).text(ne.get.weekOfYear(x, D, H + 1 - $.firstDayOfWeek)), q.addClass(G.weekCell)), e = 0; e < L; e++, H++){
                                var B = l ? new Date(H, D, 1, v, h) : c ? new Date(x, H, 1, v, h) : u ? new Date(x, D, H, v, h) : d ? new Date(x, D, b, H) : new Date(x, D, b, v, H * $.minTimeGap), W = l ? H : c ? $.text.monthsShort[H] : u ? B.getDate() : J.time(B, $, !0);
                                (q = oe("<td/>").addClass(G.cell).appendTo(V)).text(W), q.data(Z.date, B);
                                var Y, Q = u && B.getMonth() !== (D + 12) % 12, X = !$.selectAdjacentDays && Q || !ne.helper.isDateInRange(B, i) || $.isDisabled(B, i) || ne.helper.isDisabled(B, i) || !ne.helper.isEnabled(B, i);
                                X ? null !== (K = ne.helper.findDayAsObject(B, i, $.disabledDates)) && K[Z.message] && (q.attr("data-tooltip", K[Z.message]), q.attr("data-position", K[Z.position] || S), (K[Z.inverted] || te && K[Z.inverted] === ae) && q.attr("data-inverted", ""), K[Z.variation] && q.attr("data-variation", K[Z.variation])) : null !== (Y = ne.helper.findDayAsObject(B, i, $.eventDates)) && (q.addClass(Y[Z.class] || $.eventClass), Y[Z.message] && (q.attr("data-tooltip", Y[Z.message]), q.attr("data-position", Y[Z.position] || S), (Y[Z.inverted] || te && Y[Z.inverted] === ae) && q.attr("data-inverted", ""), Y[Z.variation] && q.attr("data-variation", Y[Z.variation])));
                                var W = ne.helper.dateEqual(B, a, i), K = ne.helper.dateEqual(B, o, i);
                                q.toggleClass(G.adjacentCell, Q && !Y), q.toggleClass(G.disabledCell, X), q.toggleClass(G.activeCell, W && !(Q && X)), d || f || q.toggleClass(G.todayCell, !Q && K), J.cell(q, B, {
                                    mode: i,
                                    adjacent: Q,
                                    disabled: X,
                                    active: W,
                                    today: K
                                }), ne.helper.dateEqual(B, r, i) && ne.set.focusDate(B, !1, !1);
                            }
                            $.today && (A = oe("<tr/>").appendTo(N), (A = oe("<td/>").attr("colspan", "" + C).addClass(G.today).appendTo(A)).text(J.today($)), A.data(Z.date, o)), ne.update.focus(!1, F), $.inline && ne.refreshTooltips();
                        }
                    }
                },
                update: {
                    focus: function(e18, t11) {
                        t11 = t11 || ee;
                        var r = ne.get.mode(), n5 = ne.get.date(), s = ne.get.focusDate(), l = ne.get.startDate(), c = ne.get.endDate(), u = (e18 ? s : null) || n5 || (d2 ? null : s);
                        t11.find("td").each(function() {
                            var e, t, n, i, o = oe(this), a = o.data(Z.date);
                            a && (e = o.hasClass(G.disabledCell), t = o.hasClass(G.activeCell), n = o.hasClass(G.adjacentCell), i = ne.helper.dateEqual(a, s, r), a = !!u && (!!l && ne.helper.isDateInRange(a, r, l, u) || !!c && ne.helper.isDateInRange(a, r, u, c)), o.toggleClass(G.focusCell, i && (!d2 || m1) && (!n || $.selectAdjacentDays && n) && !e), ne.helper.isTodayButton(o) || o.toggleClass(G.rangeCell, a && !t && !e));
                        });
                    }
                },
                refresh: function() {
                    ne.create.calendar();
                },
                refreshTooltips: function() {
                    var i = oe(S1).width();
                    ee.find("td[data-position]").each(function() {
                        var e = oe(this), t = S1.getComputedStyle(e[0], ":after").width.replace(/[^0-9\.]/g, ""), n = e.attr("data-position"), t = i - e.width() - (parseInt(t, 10) || 250) > e.offset().left ? "right" : "left";
                        -1 === n.indexOf(t) && e.attr("data-position", n.replace(/(left|right)/, t));
                    });
                },
                bind: {
                    events: function() {
                        ne.debug("Binding events"), ee.on("mousedown" + o4, ne.event.mousedown), ee.on("touchstart" + o4, ne.event.mousedown), ee.on("mouseup" + o4, ne.event.mouseup), ee.on("touchend" + o4, ne.event.mouseup), ee.on("mouseover" + o4, ne.event.mouseover), l2.length ? (l2.on("input" + o4, ne.event.inputChange), l2.on("focus" + o4, ne.event.inputFocus), l2.on("blur" + o4, ne.event.inputBlur), l2.on("keydown" + o4, ne.event.keydown)) : ee.on("keydown" + o4, ne.event.keydown);
                    }
                },
                unbind: {
                    events: function() {
                        ne.debug("Unbinding events"), ee.off(o4), l2.length && l2.off(o4);
                    }
                },
                event: {
                    mouseover: function(e) {
                        var t = oe(e.target).data(Z.date), e = 1 === e.buttons;
                        t && ne.set.focusDate(t, !1, !0, e);
                    },
                    mousedown: function(e) {
                        l2.length && e.preventDefault(), m1 = 0 <= e.type.indexOf("touch");
                        e = oe(e.target).data(Z.date);
                        e && ne.set.focusDate(e, !1, !0, !0);
                    },
                    mouseup: function(e) {
                        ne.focus(), e.preventDefault(), e.stopPropagation(), m1 = !1;
                        var t, n, i = oe(e.target);
                        i.hasClass("disabled") || (t = (i = (n = i.parent()).data(Z.date) || n.data(Z.focusDate) || n.data(Z.mode) ? n : i).data(Z.date), e = i.data(Z.focusDate), n = i.data(Z.mode), t && !1 !== $.onSelect.call(u2, t, ne.get.mode()) ? (i = i.hasClass(G.today), ne.selectDate(t, i)) : e ? ne.set.focusDate(e) : n && ne.set.mode(n));
                    },
                    keydown: function(e) {
                        var t, n, i, o, a, r, s, l = e.which;
                        27 !== l && 9 !== l || ne.popup("hide"), ne.popup("is visible") && (37 === l || 38 === l || 39 === l || 40 === l ? (a = "day" === (r = ne.get.mode()) ? 7 : "hour" === r ? 4 : "minute" === r ? _.column : 3, s = 37 === l ? -1 : 38 === l ? -a : 39 == l ? 1 : a, s *= "minute" === r ? $.minTimeGap : 1, n = (t = ne.get.focusDate() || ne.get.date() || new Date).getFullYear() + ("year" === r ? s : 0), i = t.getMonth() + ("month" === r ? s : 0), o = t.getDate() + ("day" === r ? s : 0), a = t.getHours() + ("hour" === r ? s : 0), s = t.getMinutes() + ("minute" === r ? s : 0), s = new Date(n, i, o, a, s), "time" === $.type && (s = ne.helper.mergeDateTime(t, s)), ne.helper.isDateInRange(s, r) && ne.set.focusDate(s)) : 13 === l && (r = ne.get.mode(), (s = ne.get.focusDate()) && !$.isDisabled(s, r) && !ne.helper.isDisabled(s, r) && ne.helper.isEnabled(s, r) && ne.selectDate(s), e.preventDefault(), e.stopPropagation())), 38 !== l && 40 !== l || (e.preventDefault(), ne.popup("show"));
                    },
                    inputChange: function() {
                        var e = l2.val(), e = n4.date(e, $);
                        ne.set.date(e, !1);
                    },
                    inputFocus: function() {
                        ee.addClass(G.active);
                    },
                    inputBlur: function() {
                        var e;
                        ee.removeClass(G.active), $.formatInput && (e = ne.get.date(), e = J.datetime(e, $), l2.val(e)), p1 && (ne.trigger.change(), p1 = !1);
                    },
                    class: {
                        mutation: function(e19) {
                            e19.forEach(function(e) {
                                "class" === e.attributeName && ne.check.disabled();
                            });
                        }
                    }
                },
                observeChanges: function() {
                    "MutationObserver" in S1 && (e17 = new MutationObserver(ne.event.class.mutation), ne.debug("Setting up mutation observer", e17), ne.observe.class());
                },
                disconnect: {
                    classObserver: function() {
                        l2.length && e17 && e17.disconnect();
                    }
                },
                observe: {
                    class: function() {
                        l2.length && e17 && e17.observe(r4[0], {
                            attributes: !0
                        });
                    }
                },
                is: {
                    disabled: function() {
                        return r4.hasClass(G.disabled);
                    }
                },
                check: {
                    disabled: function() {
                        l2.attr("tabindex", ne.is.disabled() ? -1 : 0);
                    }
                },
                get: {
                    weekOfYear: function(e, t, n) {
                        return t = Date.UTC(e, t, n + 3) / 86400000, n = Math.floor(t / 7), t = new Date(604800000 * n).getUTCFullYear(), n - Math.floor(Date.UTC(t, 0, 7) / 604800000) + 1;
                    },
                    date: function() {
                        return ne.helper.sanitiseDate(r4.data(Z.date)) || null;
                    },
                    inputDate: function() {
                        return l2.val();
                    },
                    focusDate: function() {
                        return r4.data(Z.focusDate) || null;
                    },
                    startDate: function() {
                        var e = ne.get.calendarModule($.startCalendar);
                        return (e ? e.get.date() : r4.data(Z.startDate)) || null;
                    },
                    endDate: function() {
                        var e = ne.get.calendarModule($.endCalendar);
                        return (e ? e.get.date() : r4.data(Z.endDate)) || null;
                    },
                    minDate: function() {
                        return r4.data(Z.minDate) || null;
                    },
                    maxDate: function() {
                        return r4.data(Z.maxDate) || null;
                    },
                    monthOffset: function() {
                        return r4.data(Z.monthOffset) || 0;
                    },
                    mode: function() {
                        var e = r4.data(Z.mode) || $.startMode;
                        return ne.get.validatedMode(e);
                    },
                    validatedMode: function(e) {
                        var t = ne.get.validModes();
                        return 0 <= oe.inArray(e, t) ? e : "time" === $.type ? "hour" : "month" === $.type ? "month" : "year" === $.type ? "year" : "day";
                    },
                    type: function() {
                        return r4.data(Z.type) || $.type;
                    },
                    validModes: function() {
                        var e = [];
                        return "time" !== $.type && ($.disableYear && "year" !== $.type || e.push("year"), ($.disableMonth || "year" === $.type) && "month" !== $.type || e.push("month"), 0 <= $.type.indexOf("date") && e.push("day")), 0 <= $.type.indexOf("time") && (e.push("hour"), $.disableMinute || e.push("minute")), e;
                    },
                    isTouch: function() {
                        try {
                            return D1.createEvent("TouchEvent"), !0;
                        } catch (e) {
                            return !1;
                        }
                    },
                    calendarModule: function(e) {
                        return e ? (e = !(e instanceof oe) ? oe(e).first() : e).data(a4) : null;
                    }
                },
                set: {
                    date: function(e, t, n) {
                        t = !1 !== t, n = !1 !== n, e = ne.helper.sanitiseDate(e), e = ne.helper.dateInRange(e);
                        var i = ne.get.mode(), o = J.datetime(e, $);
                        if (n && !1 === $.onBeforeChange.call(u2, e, o, i)) return !1;
                        if (ne.set.focusDate(e), $.isDisabled(e, i)) return !1;
                        var a = ne.get.endDate();
                        a && e && a < e && ne.set.endDate(ae), ne.set.dataKeyValue(Z.date, e), t && l2.length && l2.val(o), n && $.onChange.call(u2, e, o, i);
                    },
                    startDate: function(e, t) {
                        e = ne.helper.sanitiseDate(e);
                        var n = ne.get.calendarModule($.startCalendar);
                        n && n.set.date(e), ne.set.dataKeyValue(Z.startDate, e, t);
                    },
                    endDate: function(e, t) {
                        e = ne.helper.sanitiseDate(e);
                        var n = ne.get.calendarModule($.endCalendar);
                        n && n.set.date(e), ne.set.dataKeyValue(Z.endDate, e, t);
                    },
                    focusDate: function(e, t, n, i) {
                        e = ne.helper.sanitiseDate(e), e = ne.helper.dateInRange(e);
                        var o = "day" === ne.get.mode(), a = ne.get.focusDate();
                        o && e && a && (a = 12 * (e.getFullYear() - a.getFullYear()) + e.getMonth() - a.getMonth()) && (a = ne.get.monthOffset() - a, ne.set.monthOffset(a, !1));
                        e = ne.set.dataKeyValue(Z.focusDate, e, !!e && t);
                        n = !1 !== n && e && !1 === t || g1 != i, g1 = i, n && ne.update.focus(i);
                    },
                    minDate: function(e) {
                        e = ne.helper.sanitiseDate(e), null !== $.maxDate && $.maxDate <= e ? ne.verbose("Unable to set minDate variable bigger that maxDate variable", e, $.maxDate) : (ne.setting("minDate", e), ne.set.dataKeyValue(Z.minDate, e));
                    },
                    maxDate: function(e) {
                        e = ne.helper.sanitiseDate(e), null !== $.minDate && $.minDate >= e ? ne.verbose("Unable to set maxDate variable lower that minDate variable", e, $.minDate) : (ne.setting("maxDate", e), ne.set.dataKeyValue(Z.maxDate, e));
                    },
                    monthOffset: function(e, t) {
                        var n = Math.max($.multiMonth, 1);
                        e = Math.max(1 - n, Math.min(0, e)), ne.set.dataKeyValue(Z.monthOffset, e, t);
                    },
                    mode: function(e, t) {
                        ne.set.dataKeyValue(Z.mode, e, t);
                    },
                    dataKeyValue: function(e, t, n) {
                        var i = r4.data(e), i = i === t || i <= t && t <= i;
                        return t ? r4.data(e, t) : r4.removeData(e), (n = !1 !== n && !i) && ne.refresh(), !i;
                    }
                },
                selectDate: function(e, t) {
                    ne.verbose("New date selection", e);
                    var n, i = ne.get.mode();
                    t || "minute" === i || $.disableMinute && "hour" === i || "date" === $.type && "day" === i || "month" === $.type && "month" === i || "year" === $.type && "year" === i ? !1 === ne.set.date(e) || (p1 = !0, $.closable && (ne.popup("hide"), (n = ne.get.calendarModule($.endCalendar)) && ("focus" !== n.setting("on") && n.popup("show"), n.focus()))) : (n = "year" === i ? $.disableMonth ? "day" : "month" : "month" === i ? "day" : "day" === i ? "hour" : "minute", ne.set.mode(n), "hour" === i || "day" === i && ne.get.date() ? ne.set.date(e, !0, !1) : ne.set.focusDate(e));
                },
                changeDate: function(e) {
                    ne.set.date(e);
                },
                clear: function() {
                    ne.set.date(ae);
                },
                popup: function() {
                    return c2.popup.apply(c2, arguments);
                },
                focus: function() {
                    (l2.length ? l2 : ee).focus();
                },
                blur: function() {
                    (l2.length ? l2 : ee).blur();
                },
                helper: {
                    isDisabled: function(n, i) {
                        return ("day" === i || "month" === i || "year" === i) && ("day" === i && -1 !== $.disabledDaysOfWeek.indexOf(n.getDay()) || $.disabledDates.some(function(e20) {
                            if ((e20 = "string" == typeof e20 ? ne.helper.sanitiseDate(e20) : e20) instanceof Date) return ne.helper.dateEqual(n, e20, i);
                            if (null !== e20 && "object" == typeof e20) {
                                if (e20[Z.year]) return "number" == typeof e20[Z.year] ? n.getFullYear() == e20[Z.year] : Array.isArray(e20[Z.year]) ? -1 < e20[Z.year].indexOf(n.getFullYear()) : void 0;
                                if (e20[Z.month]) {
                                    if ("number" == typeof e20[Z.month]) return n.getMonth() == e20[Z.month];
                                    if (Array.isArray(e20[Z.month])) return -1 < e20[Z.month].indexOf(n.getMonth());
                                    if (e20[Z.month] instanceof Date) {
                                        var t = ne.helper.sanitiseDate(e20[Z.month]);
                                        return n.getMonth() == t.getMonth() && n.getFullYear() == t.getFullYear();
                                    }
                                } else if (e20[Z.date] && "day" === i) return e20[Z.date] instanceof Date ? ne.helper.dateEqual(n, ne.helper.sanitiseDate(e20[Z.date]), i) : Array.isArray(e20[Z.date]) ? e20[Z.date].some(function(e) {
                                    return ne.helper.dateEqual(n, e, i);
                                }) : void 0;
                            }
                        }));
                    },
                    isEnabled: function(t, n) {
                        return "day" !== n || 0 === $.enabledDates.length || $.enabledDates.some(function(e) {
                            return (e = "string" == typeof e ? ne.helper.sanitiseDate(e) : e) instanceof Date ? ne.helper.dateEqual(t, e, n) : null !== e && "object" == typeof e && e[Z.date] ? ne.helper.dateEqual(t, ne.helper.sanitiseDate(e[Z.date]), n) : void 0;
                        });
                    },
                    findDayAsObject: function(t, n, e21) {
                        if ("day" === n || "month" === n || "year" === n) for(var i, o = 0; o < e21.length; o++){
                            if ((i = "string" == typeof (i = e21[o]) ? ne.helper.sanitiseDate(i) : i) instanceof Date && ne.helper.dateEqual(t, i, n)) {
                                var a = {
                                };
                                return a[Z.date] = i, a;
                            }
                            if (null !== i && "object" == typeof i) {
                                if (i[Z.year]) {
                                    if ("number" == typeof i[Z.year] && t.getFullYear() == i[Z.year]) return i;
                                    if (Array.isArray(i[Z.year]) && -1 < i[Z.year].indexOf(t.getFullYear())) return i;
                                } else if (i[Z.month]) {
                                    if ("number" == typeof i[Z.month] && t.getMonth() == i[Z.month]) return i;
                                    if (Array.isArray(i[Z.month])) {
                                        if (-1 < i[Z.month].indexOf(t.getMonth())) return i;
                                    } else if (i[Z.month] instanceof Date) {
                                        a = ne.helper.sanitiseDate(i[Z.month]);
                                        if (t.getMonth() == a.getMonth() && t.getFullYear() == a.getFullYear()) return i;
                                    }
                                } else if (i[Z.date] && "day" === n) {
                                    if (i[Z.date] instanceof Date && ne.helper.dateEqual(t, ne.helper.sanitiseDate(i[Z.date]), n)) return i;
                                    if (Array.isArray(i[Z.date]) && i[Z.date].some(function(e) {
                                        return ne.helper.dateEqual(t, e, n);
                                    })) return i;
                                }
                            }
                        }
                        return null;
                    },
                    sanitiseDate: function(e) {
                        return !(e = !(e instanceof Date) ? n4.date("" + e, $) : e) || isNaN(e.getTime()) ? null : e;
                    },
                    dateDiff: function(e, t, n) {
                        var i = "time" === $.type, o = "year" === (n = n || "day"), a = o || "month" === n, r = "minute" === n, n = r || "hour" === n;
                        return e = new Date(i ? 2000 : e.getFullYear(), i || o ? 0 : e.getMonth(), i || a ? 1 : e.getDate(), n ? e.getHours() : 0, r ? $.minTimeGap * Math.floor(e.getMinutes() / $.minTimeGap) : 0), (t = new Date(i ? 2000 : t.getFullYear(), i || o ? 0 : t.getMonth(), i || a ? 1 : t.getDate(), n ? t.getHours() : 0, r ? $.minTimeGap * Math.floor(t.getMinutes() / $.minTimeGap) : 0)).getTime() - e.getTime();
                    },
                    dateEqual: function(e, t, n) {
                        return !!e && !!t && 0 === ne.helper.dateDiff(e, t, n);
                    },
                    isDateInRange: function(e, t, n, i) {
                        var o;
                        return n || i || (n = (o = ne.get.startDate()) && $.minDate ? new Date(Math.max(o, $.minDate)) : o || $.minDate, i = $.maxDate), n = n && new Date(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), $.minTimeGap * Math.ceil(n.getMinutes() / $.minTimeGap)), !(!e || n && 0 < ne.helper.dateDiff(e, n, t) || i && 0 < ne.helper.dateDiff(i, e, t));
                    },
                    dateInRange: function(e, t, n) {
                        t || n || (t = (i = ne.get.startDate()) && $.minDate ? new Date(Math.max(i, $.minDate)) : i || $.minDate, n = $.maxDate), t = t && new Date(t.getFullYear(), t.getMonth(), t.getDate(), t.getHours(), $.minTimeGap * Math.ceil(t.getMinutes() / $.minTimeGap));
                        var i = "time" === $.type;
                        return e && (t && 0 < ne.helper.dateDiff(e, t, "minute") ? i ? ne.helper.mergeDateTime(e, t) : t : n && 0 < ne.helper.dateDiff(n, e, "minute") ? i ? ne.helper.mergeDateTime(e, n) : n : e);
                    },
                    mergeDateTime: function(e, t) {
                        return e && t ? new Date(e.getFullYear(), e.getMonth(), e.getDate(), t.getHours(), t.getMinutes()) : t;
                    },
                    isTodayButton: function(e) {
                        return e.text() === $.text.today;
                    }
                },
                setting: function(e, t) {
                    if (ne.debug("Changing setting", e, t), oe.isPlainObject(e)) oe.extend(!0, $, e);
                    else {
                        if (t === ae) return $[e];
                        oe.isPlainObject($[e]) ? oe.extend(!0, $[e], t) : $[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (oe.isPlainObject(e)) oe.extend(!0, ne, e);
                    else {
                        if (t === ae) return ne[e];
                        ne[e] = t;
                    }
                },
                debug: function() {
                    !$.silent && $.debug && ($.performance ? ne.performance.log(arguments) : (ne.debug = Function.prototype.bind.call(console.info, console, $.name + ":"), ne.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !$.silent && $.verbose && $.debug && ($.performance ? ne.performance.log(arguments) : (ne.verbose = Function.prototype.bind.call(console.info, console, $.name + ":"), ne.verbose.apply(console, arguments)));
                },
                error: function() {
                    $.silent || (ne.error = Function.prototype.bind.call(console.error, console, $.name + ":"), ne.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        $.performance && (n = (t = (new Date).getTime()) - (y1 || t), y1 = t, x1.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: u2,
                            "Execution Time": n
                        })), clearTimeout(ne.performance.timer), ne.performance.timer = setTimeout(ne.performance.display, 500);
                    },
                    display: function() {
                        var e = $.name + ":", n = 0;
                        y1 = !1, clearTimeout(ne.performance.timer), oe.each(x1, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", b1 && (e += " '" + b1 + "'"), (console.group !== ae || console.table !== ae) && 0 < x1.length && (console.groupCollapsed(e), console.table ? console.table(x1) : oe.each(x1, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), x1 = [];
                    }
                },
                invoke: function(i, e22, t12) {
                    var o, a, n, r = f1;
                    return e22 = e22 || k1, t12 = u2 || t12, "string" == typeof i && r !== ae && (i = i.split(/[\. ]/), o = i.length - 1, oe.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (oe.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== ae) return a = r[n], !1;
                            if (!oe.isPlainObject(r[t]) || e == o) return r[t] !== ae ? a = r[t] : ne.error(s2.method, i), !1;
                            r = r[t];
                        }
                    })), oe.isFunction(a) ? n = a.apply(t12, e22) : a !== ae && (n = a), Array.isArray(v1) ? v1.push(n) : v1 !== ae ? v1 = [
                        v1,
                        n
                    ] : n !== ae && (v1 = n), a;
                }
            };
            w1 ? (f1 === ae && ne.initialize(), ne.invoke(C1)) : (f1 !== ae && f1.invoke("destroy"), ne.initialize());
        }), v1 !== ae ? v1 : this;
    }, oe.fn.calendar.settings = {
        name: "Calendar",
        namespace: "calendar",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !1,
        type: "datetime",
        firstDayOfWeek: 0,
        constantHeight: !0,
        today: !1,
        closable: !0,
        monthFirst: !0,
        touchReadonly: !0,
        inline: !1,
        on: null,
        initialDate: null,
        startMode: !1,
        minDate: null,
        maxDate: null,
        ampm: !0,
        disableYear: !1,
        disableMonth: !1,
        disableMinute: !1,
        formatInput: !0,
        startCalendar: null,
        endCalendar: null,
        multiMonth: 1,
        minTimeGap: 5,
        showWeekNumbers: null,
        disabledDates: [],
        disabledDaysOfWeek: [],
        enabledDates: [],
        eventDates: [],
        centuryBreak: 60,
        currentCentury: 2000,
        selectAdjacentDays: !1,
        popupOptions: {
            position: "bottom left",
            lastResort: "bottom left",
            prefer: "opposite",
            hideOnScroll: !1
        },
        text: {
            days: [
                "S",
                "M",
                "T",
                "W",
                "T",
                "F",
                "S"
            ],
            months: [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            monthsShort: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            ],
            today: "Today",
            now: "Now",
            am: "AM",
            pm: "PM",
            weekNo: "Week"
        },
        formatter: {
            header: function(e, t, n) {
                return "year" === t ? n.formatter.yearHeader(e, n) : "month" === t ? n.formatter.monthHeader(e, n) : "day" === t ? n.formatter.dayHeader(e, n) : "hour" === t ? n.formatter.hourHeader(e, n) : n.formatter.minuteHeader(e, n);
            },
            yearHeader: function(e, t) {
                e = 10 * Math.ceil(e.getFullYear() / 10);
                return e - 9 + " - " + (2 + e);
            },
            monthHeader: function(e, t) {
                return e.getFullYear();
            },
            dayHeader: function(e, t) {
                return t.text.months[e.getMonth()] + " " + e.getFullYear();
            },
            hourHeader: function(e, t) {
                return t.formatter.date(e, t);
            },
            minuteHeader: function(e, t) {
                return t.formatter.date(e, t);
            },
            dayColumnHeader: function(e, t) {
                return t.text.days[e];
            },
            datetime: function(e, t) {
                if (!e) return "";
                var n = "time" === t.type ? "" : t.formatter.date(e, t), e = t.type.indexOf("time") < 0 ? "" : t.formatter.time(e, t, !1);
                return n + ("datetime" === t.type ? " " : "") + e;
            },
            date: function(e, t) {
                if (!e) return "";
                var n = e.getDate(), i = t.text.months[e.getMonth()], e = e.getFullYear();
                return "year" === t.type ? e : "month" === t.type ? i + " " + e : (t.monthFirst ? i + " " + n : n + " " + i) + ", " + e;
            },
            time: function(e, t, n) {
                if (!e) return "";
                var i = e.getHours(), o = e.getMinutes(), e = "";
                return t.ampm && (e = " " + (i < 12 ? t.text.am : t.text.pm), i = 0 === i ? 12 : 12 < i ? i - 12 : i), i + ":" + (o < 10 ? "0" : "") + o + e;
            },
            today: function(e) {
                return "date" === e.type ? e.text.today : e.text.now;
            },
            cell: function(e, t, n) {
            }
        },
        parser: {
            date: function(e, t) {
                if (e instanceof Date) return e;
                if (!e) return null;
                if (0 === (e = String(e).trim()).length) return null;
                e.match(/^[0-9]{4}[\/\-\.][0-9]{1,2}[\/\-\.][0-9]{1,2}$/) && (e = e.replace(/[\/\-\.]/g, "/") + " 00:00:00"), e = t.monthFirst || !e.match(/^[0-9]{1,2}[\/\-\.]/) ? e : e.replace(/[\/\-\.]/g, "/").replace(/([0-9]+)\/([0-9]+)/, "$2/$1");
                var n, i, o, a = new Date(e);
                if (!(null !== e.match(/^[0-9]+$/)) && !isNaN(a.getDate())) return a;
                e = e.toLowerCase();
                var r, s, l, c = -1, u = -1, d = -1, f = -1, m = -1, g = ae, p = "time" === t.type, a = t.type.indexOf("time") < 0, h = e.split(t.regExp.dateWords), v = e.split(t.regExp.dateNumbers);
                if (!a) {
                    for(g = 0 <= oe.inArray(t.text.am.toLowerCase(), h) || !(0 <= oe.inArray(t.text.pm.toLowerCase(), h)) && ae, n = 0; n < v.length; n++)if (0 <= (s = v[n]).indexOf(":")) {
                        if (u < 0 || c < 0) for(l = s.split(":"), o = 0; o < Math.min(2, l.length); o++)i = parseInt(l[o]), isNaN(i) && (i = 0), 0 === o ? u = i % 24 : c = i % 60;
                        v.splice(n, 1);
                    }
                }
                if (!p) {
                    for(n = 0; n < h.length; n++)if (!((r = h[n]).length <= 0)) {
                        for(i = 0; i < t.text.months.length; i++)if (t.text.months[i].substring(0, r.length).toLowerCase() === r) {
                            f = i + 1;
                            break;
                        }
                        if (0 <= f) break;
                    }
                    for(n = 0; n < v.length; n++)if (i = parseInt(v[n]), !isNaN(i) && i >= t.centuryBreak && n === v.length - 1) {
                        i <= 99 && (i += t.currentCentury - 100), m = i, v.splice(n, 1);
                        break;
                    }
                    if (f < 0) {
                        for(n = 0; n < v.length; n++)if (o = 1 < n || t.monthFirst ? n : 1 === n ? 0 : 1, i = parseInt(v[o]), !isNaN(i) && 1 <= i && i <= 12) {
                            f = i, v.splice(o, 1);
                            break;
                        }
                    }
                    for(n = 0; n < v.length; n++)if (i = parseInt(v[n]), !isNaN(i) && 1 <= i && i <= 31) {
                        d = i, v.splice(n, 1);
                        break;
                    }
                    if (m < 0) {
                        for(n = v.length - 1; 0 <= n; n--)if (i = parseInt(v[n]), !isNaN(i)) {
                            i <= 99 && (i += t.currentCentury), m = i, v.splice(n, 1);
                            break;
                        }
                    }
                }
                if (!a) {
                    if (u < 0) {
                        for(n = 0; n < v.length; n++)if (i = parseInt(v[n]), !isNaN(i) && 0 <= i && i <= 23) {
                            u = i, v.splice(n, 1);
                            break;
                        }
                    }
                    if (c < 0) {
                        for(n = 0; n < v.length; n++)if (i = parseInt(v[n]), !isNaN(i) && 0 <= i && i <= 59) {
                            c = i, v.splice(n, 1);
                            break;
                        }
                    }
                }
                if (c < 0 && u < 0 && d < 0 && f < 0 && m < 0) return null;
                c < 0 && (c = 0), u < 0 && (u = 0), d < 0 && (d = 1), f < 0 && (f = 1), m < 0 && (m = (new Date).getFullYear()), g !== ae && (g ? 12 === u && (u = 0) : u < 12 && (u += 12));
                g = new Date(m, f - 1, d, u, c);
                return g.getMonth() === f - 1 && g.getFullYear() === m || (g = new Date(m, f, 0, u, c)), isNaN(g.getTime()) ? null : g;
            }
        },
        onBeforeChange: function(e, t, n) {
            return !0;
        },
        onChange: function(e, t, n) {
        },
        onShow: function() {
        },
        onVisible: function() {
        },
        onHide: function() {
        },
        onHidden: function() {
        },
        onSelect: function(e, t) {
        },
        isDisabled: function(e, t) {
            return !1;
        },
        selector: {
            popup: ".ui.popup",
            input: "input",
            activator: "input",
            append: ".inline.field,.inline.fields"
        },
        regExp: {
            dateWords: /[^A-Za-z\u00C0-\u024F]+/g,
            dateNumbers: /[^\d:]+/g
        },
        error: {
            popup: "UI Popup, a required component is not included in this page",
            method: "The method you called is not defined."
        },
        className: {
            calendar: "calendar",
            active: "active",
            popup: "ui popup",
            grid: "ui equal width grid",
            column: "column",
            table: "ui celled center aligned unstackable table",
            inverted: "inverted",
            prev: "prev link",
            next: "next link",
            prevIcon: "chevron left icon",
            nextIcon: "chevron right icon",
            link: "link",
            cell: "link",
            disabledCell: "disabled",
            weekCell: "disabled",
            adjacentCell: "adjacent",
            activeCell: "active",
            rangeCell: "range",
            focusCell: "focus",
            todayCell: "today",
            today: "today link",
            disabled: "disabled"
        },
        metadata: {
            date: "date",
            focusDate: "focusDate",
            startDate: "startDate",
            endDate: "endDate",
            minDate: "minDate",
            maxDate: "maxDate",
            mode: "mode",
            type: "type",
            monthOffset: "monthOffset",
            message: "message",
            class: "class",
            inverted: "inverted",
            variation: "variation",
            position: "position",
            month: "month",
            year: "year"
        },
        eventClass: "blue"
    };
})(jQuery, window, document), (function(S, D, A, E) {
    "use strict";
    S.isFunction = S.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, D = void 0 !== D && D.Math == Math ? D : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), S.fn.checkbox = function(u3) {
        var d3, e23 = S(this), f2 = e23.selector || "", x = (new Date).getTime(), C = [], w = u3, k = "string" == typeof w, T = [].slice.call(arguments, 1);
        return e23.each(function() {
            var e24, m = S.extend(!0, {
            }, S.fn.checkbox.settings, u3), t13 = m.className, n6 = m.namespace, g = m.selector, s3 = m.error, i5 = "." + n6, o5 = "module-" + n6, p = S(this), a5 = S(this).children(g.label), h = S(this).children(g.input), v = h[0], r5 = !1, b = !1, l3 = p.data(o5), c3 = this, y = {
                initialize: function() {
                    y.verbose("Initializing checkbox", m), y.create.label(), y.bind.events(), y.set.tabbable(), y.hide.input(), y.observeChanges(), y.instantiate(), y.setup();
                },
                instantiate: function() {
                    y.verbose("Storing instance of module", y), l3 = y, p.data(o5, y);
                },
                destroy: function() {
                    y.verbose("Destroying module"), y.unbind.events(), y.show.input(), p.removeData(o5);
                },
                fix: {
                    reference: function() {
                        p.is(g.input) && (y.debug("Behavior called on <input> adjusting invoked element"), p = p.closest(g.checkbox), y.refresh());
                    }
                },
                setup: function() {
                    y.set.initialLoad(), y.is.indeterminate() ? (y.debug("Initial value is indeterminate"), y.indeterminate()) : y.is.checked() ? (y.debug("Initial value is checked"), y.check()) : (y.debug("Initial value is unchecked"), y.uncheck()), y.remove.initialLoad();
                },
                refresh: function() {
                    a5 = p.children(g.label), h = p.children(g.input), v = h[0];
                },
                hide: {
                    input: function() {
                        y.verbose("Modifying <input> z-index to be unselectable"), h.addClass(t13.hidden);
                    }
                },
                show: {
                    input: function() {
                        y.verbose("Modifying <input> z-index to be selectable"), h.removeClass(t13.hidden);
                    }
                },
                observeChanges: function() {
                    "MutationObserver" in D && ((e24 = new MutationObserver(function(e) {
                        y.debug("DOM tree modified, updating selector cache"), y.refresh();
                    })).observe(c3, {
                        childList: !0,
                        subtree: !0
                    }), y.debug("Setting up mutation observer", e24));
                },
                attachEvents: function(e, t) {
                    var n = S(e);
                    t = S.isFunction(y[t]) ? y[t] : y.toggle, 0 < n.length ? (y.debug("Attaching checkbox events to element", e, t), n.on("click" + i5, t)) : y.error(s3.notFound);
                },
                preventDefaultOnInputTarget: function() {
                    "undefined" != typeof event && null !== event && S(event.target).is(g.input) && (y.verbose("Preventing default check action after manual check action"), event.preventDefault());
                },
                event: {
                    change: function(e) {
                        y.should.ignoreCallbacks() || m.onChange.call(v);
                    },
                    click: function(e) {
                        var t = S(e.target);
                        t.is(g.input) ? y.verbose("Using default check action on initialized checkbox") : t.is(g.link) ? y.debug("Clicking link inside checkbox, skipping toggle") : (y.toggle(), h.focus(), e.preventDefault());
                    },
                    keydown: function(e) {
                        var t = e.which, n = 13, i = 32, o = 27, a = 37, r = 38, s = 39, l = 40, c = y.get.radios(), u = c.index(p), d = c.length, f = !1;
                        if (t == a || t == r ? f = (0 === u ? d : u) - 1 : t != s && t != l || (f = u === d - 1 ? 0 : u + 1), !y.should.ignoreCallbacks() && !1 !== f) {
                            if (!1 === m.beforeUnchecked.apply(v)) return y.verbose("Option not allowed to be unchecked, cancelling key navigation"), !1;
                            if (!1 === m.beforeChecked.apply(S(c[f]).children(g.input)[0])) return y.verbose("Next option should not allow check, cancelling key navigation"), !1;
                        }
                        b = t == o ? (y.verbose("Escape key pressed blurring field"), h.blur(), !0) : !(e.ctrlKey || !(t == i || t == n && m.enableEnterKey)) && (y.verbose("Enter/space key pressed, toggling checkbox"), y.toggle(), !0);
                    },
                    keyup: function(e) {
                        b && e.preventDefault();
                    }
                },
                check: function() {
                    y.should.allowCheck() && (y.debug("Checking checkbox", h), y.set.checked(), y.should.ignoreCallbacks() || (m.onChecked.call(v), y.trigger.change()), y.preventDefaultOnInputTarget());
                },
                uncheck: function() {
                    y.should.allowUncheck() && (y.debug("Unchecking checkbox"), y.set.unchecked(), y.should.ignoreCallbacks() || (m.onUnchecked.call(v), y.trigger.change()), y.preventDefaultOnInputTarget());
                },
                indeterminate: function() {
                    y.should.allowIndeterminate() ? y.debug("Checkbox is already indeterminate") : (y.debug("Making checkbox indeterminate"), y.set.indeterminate(), y.should.ignoreCallbacks() || (m.onIndeterminate.call(v), y.trigger.change()));
                },
                determinate: function() {
                    y.should.allowDeterminate() ? y.debug("Checkbox is already determinate") : (y.debug("Making checkbox determinate"), y.set.determinate(), y.should.ignoreCallbacks() || (m.onDeterminate.call(v), y.trigger.change()));
                },
                enable: function() {
                    y.is.enabled() ? y.debug("Checkbox is already enabled") : (y.debug("Enabling checkbox"), y.set.enabled(), y.should.ignoreCallbacks() || (m.onEnable.call(v), m.onEnabled.call(v), y.trigger.change()));
                },
                disable: function() {
                    y.is.disabled() ? y.debug("Checkbox is already disabled") : (y.debug("Disabling checkbox"), y.set.disabled(), y.should.ignoreCallbacks() || (m.onDisable.call(v), m.onDisabled.call(v), y.trigger.change()));
                },
                get: {
                    radios: function() {
                        var e = y.get.name();
                        return S('input[name="' + e + '"]').closest(g.checkbox);
                    },
                    otherRadios: function() {
                        return y.get.radios().not(p);
                    },
                    name: function() {
                        return h.attr("name");
                    }
                },
                is: {
                    initialLoad: function() {
                        return r5;
                    },
                    radio: function() {
                        return h.hasClass(t13.radio) || "radio" == h.attr("type");
                    },
                    indeterminate: function() {
                        return h.prop("indeterminate") !== E && h.prop("indeterminate");
                    },
                    checked: function() {
                        return h.prop("checked") !== E && h.prop("checked");
                    },
                    disabled: function() {
                        return h.prop("disabled") !== E && h.prop("disabled");
                    },
                    enabled: function() {
                        return !y.is.disabled();
                    },
                    determinate: function() {
                        return !y.is.indeterminate();
                    },
                    unchecked: function() {
                        return !y.is.checked();
                    }
                },
                should: {
                    allowCheck: function() {
                        return y.is.determinate() && y.is.checked() && !y.is.initialLoad() ? (y.debug("Should not allow check, checkbox is already checked"), !1) : !(!y.should.ignoreCallbacks() && !1 === m.beforeChecked.apply(v)) || (y.debug("Should not allow check, beforeChecked cancelled"), !1);
                    },
                    allowUncheck: function() {
                        return y.is.determinate() && y.is.unchecked() && !y.is.initialLoad() ? (y.debug("Should not allow uncheck, checkbox is already unchecked"), !1) : !(!y.should.ignoreCallbacks() && !1 === m.beforeUnchecked.apply(v)) || (y.debug("Should not allow uncheck, beforeUnchecked cancelled"), !1);
                    },
                    allowIndeterminate: function() {
                        return y.is.indeterminate() && !y.is.initialLoad() ? (y.debug("Should not allow indeterminate, checkbox is already indeterminate"), !1) : !(!y.should.ignoreCallbacks() && !1 === m.beforeIndeterminate.apply(v)) || (y.debug("Should not allow indeterminate, beforeIndeterminate cancelled"), !1);
                    },
                    allowDeterminate: function() {
                        return y.is.determinate() && !y.is.initialLoad() ? (y.debug("Should not allow determinate, checkbox is already determinate"), !1) : !(!y.should.ignoreCallbacks() && !1 === m.beforeDeterminate.apply(v)) || (y.debug("Should not allow determinate, beforeDeterminate cancelled"), !1);
                    },
                    ignoreCallbacks: function() {
                        return r5 && !m.fireOnInit;
                    }
                },
                can: {
                    change: function() {
                        return !(p.hasClass(t13.disabled) || p.hasClass(t13.readOnly) || h.prop("disabled") || h.prop("readonly"));
                    },
                    uncheck: function() {
                        return "boolean" == typeof m.uncheckable ? m.uncheckable : !y.is.radio();
                    }
                },
                set: {
                    initialLoad: function() {
                        r5 = !0;
                    },
                    checked: function() {
                        y.verbose("Setting class to checked"), p.removeClass(t13.indeterminate).addClass(t13.checked), y.is.radio() && y.uncheckOthers(), y.is.indeterminate() || !y.is.checked() ? (y.verbose("Setting state to checked", v), h.prop("indeterminate", !1).prop("checked", !0)) : y.debug("Input is already checked, skipping input property change");
                    },
                    unchecked: function() {
                        y.verbose("Removing checked class"), p.removeClass(t13.indeterminate).removeClass(t13.checked), y.is.indeterminate() || !y.is.unchecked() ? (y.debug("Setting state to unchecked"), h.prop("indeterminate", !1).prop("checked", !1)) : y.debug("Input is already unchecked");
                    },
                    indeterminate: function() {
                        y.verbose("Setting class to indeterminate"), p.addClass(t13.indeterminate), y.is.indeterminate() ? y.debug("Input is already indeterminate, skipping input property change") : (y.debug("Setting state to indeterminate"), h.prop("indeterminate", !0));
                    },
                    determinate: function() {
                        y.verbose("Removing indeterminate class"), p.removeClass(t13.indeterminate), y.is.determinate() ? y.debug("Input is already determinate, skipping input property change") : (y.debug("Setting state to determinate"), h.prop("indeterminate", !1));
                    },
                    disabled: function() {
                        y.verbose("Setting class to disabled"), p.addClass(t13.disabled), y.is.disabled() ? y.debug("Input is already disabled, skipping input property change") : (y.debug("Setting state to disabled"), h.prop("disabled", "disabled"));
                    },
                    enabled: function() {
                        y.verbose("Removing disabled class"), p.removeClass(t13.disabled), y.is.enabled() ? y.debug("Input is already enabled, skipping input property change") : (y.debug("Setting state to enabled"), h.prop("disabled", !1));
                    },
                    tabbable: function() {
                        y.verbose("Adding tabindex to checkbox"), h.attr("tabindex") === E && h.attr("tabindex", 0);
                    }
                },
                remove: {
                    initialLoad: function() {
                        r5 = !1;
                    }
                },
                trigger: {
                    change: function() {
                        var e, t = h[0];
                        t && (e = A.createEvent("HTMLEvents"), y.verbose("Triggering native change event"), e.initEvent("change", !0, !1), t.dispatchEvent(e));
                    }
                },
                create: {
                    label: function() {
                        0 < h.prevAll(g.label).length ? (h.prev(g.label).detach().insertAfter(h), y.debug("Moving existing label", a5)) : y.has.label() || (a5 = S("<label>").insertAfter(h), y.debug("Creating label", a5));
                    }
                },
                has: {
                    label: function() {
                        return 0 < a5.length;
                    }
                },
                bind: {
                    events: function() {
                        y.verbose("Attaching checkbox events"), p.on("click" + i5, y.event.click).on("change" + i5, y.event.change).on("keydown" + i5, g.input, y.event.keydown).on("keyup" + i5, g.input, y.event.keyup);
                    }
                },
                unbind: {
                    events: function() {
                        y.debug("Removing events"), p.off(i5);
                    }
                },
                uncheckOthers: function() {
                    var e = y.get.otherRadios();
                    y.debug("Unchecking other radios", e), e.removeClass(t13.checked);
                },
                toggle: function() {
                    y.can.change() ? y.is.indeterminate() || y.is.unchecked() ? (y.debug("Currently unchecked"), y.check()) : y.is.checked() && y.can.uncheck() && (y.debug("Currently checked"), y.uncheck()) : y.is.radio() || y.debug("Checkbox is read-only or disabled, ignoring toggle");
                },
                setting: function(e, t) {
                    if (y.debug("Changing setting", e, t), S.isPlainObject(e)) S.extend(!0, m, e);
                    else {
                        if (t === E) return m[e];
                        S.isPlainObject(m[e]) ? S.extend(!0, m[e], t) : m[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (S.isPlainObject(e)) S.extend(!0, y, e);
                    else {
                        if (t === E) return y[e];
                        y[e] = t;
                    }
                },
                debug: function() {
                    !m.silent && m.debug && (m.performance ? y.performance.log(arguments) : (y.debug = Function.prototype.bind.call(console.info, console, m.name + ":"), y.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !m.silent && m.verbose && m.debug && (m.performance ? y.performance.log(arguments) : (y.verbose = Function.prototype.bind.call(console.info, console, m.name + ":"), y.verbose.apply(console, arguments)));
                },
                error: function() {
                    m.silent || (y.error = Function.prototype.bind.call(console.error, console, m.name + ":"), y.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        m.performance && (n = (t = (new Date).getTime()) - (x || t), x = t, C.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: c3,
                            "Execution Time": n
                        })), clearTimeout(y.performance.timer), y.performance.timer = setTimeout(y.performance.display, 500);
                    },
                    display: function() {
                        var e = m.name + ":", n = 0;
                        x = !1, clearTimeout(y.performance.timer), S.each(C, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", f2 && (e += " '" + f2 + "'"), (console.group !== E || console.table !== E) && 0 < C.length && (console.groupCollapsed(e), console.table ? console.table(C) : S.each(C, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), C = [];
                    }
                },
                invoke: function(i, e25, t14) {
                    var o, a, n, r = l3;
                    return e25 = e25 || T, t14 = c3 || t14, "string" == typeof i && r !== E && (i = i.split(/[\. ]/), o = i.length - 1, S.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (S.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== E) return a = r[n], !1;
                            if (!S.isPlainObject(r[t]) || e == o) return r[t] !== E ? a = r[t] : y.error(s3.method, i), !1;
                            r = r[t];
                        }
                    })), S.isFunction(a) ? n = a.apply(t14, e25) : a !== E && (n = a), Array.isArray(d3) ? d3.push(n) : d3 !== E ? d3 = [
                        d3,
                        n
                    ] : n !== E && (d3 = n), a;
                }
            };
            k ? (l3 === E && y.initialize(), y.invoke(w)) : (l3 !== E && l3.invoke("destroy"), y.initialize());
        }), d3 !== E ? d3 : this;
    }, S.fn.checkbox.settings = {
        name: "Checkbox",
        namespace: "checkbox",
        silent: !1,
        debug: !1,
        verbose: !0,
        performance: !0,
        uncheckable: "auto",
        fireOnInit: !1,
        enableEnterKey: !0,
        onChange: function() {
        },
        beforeChecked: function() {
        },
        beforeUnchecked: function() {
        },
        beforeDeterminate: function() {
        },
        beforeIndeterminate: function() {
        },
        onChecked: function() {
        },
        onUnchecked: function() {
        },
        onDeterminate: function() {
        },
        onIndeterminate: function() {
        },
        onEnable: function() {
        },
        onDisable: function() {
        },
        onEnabled: function() {
        },
        onDisabled: function() {
        },
        className: {
            checked: "checked",
            indeterminate: "indeterminate",
            disabled: "disabled",
            hidden: "hidden",
            radio: "radio",
            readOnly: "read-only"
        },
        error: {
            method: "The method you called is not defined"
        },
        selector: {
            checkbox: ".ui.checkbox",
            label: "label, .box",
            input: 'input[type="checkbox"], input[type="radio"]',
            link: "a[href]"
        }
    };
})(jQuery, window, document), (function(k, e26, T, S) {
    "use strict";
    k.isFunction = k.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e26 = void 0 !== e26 && e26.Math == Math ? e26 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), k.fn.dimmer = function(p) {
        var h, v = k(this), b = (new Date).getTime(), y = [], x = p, C = "string" == typeof x, w = [].slice.call(arguments, 1);
        return v.each(function() {
            var o6, t15, a6 = k.isPlainObject(p) ? k.extend(!0, {
            }, k.fn.dimmer.settings, p) : k.extend({
            }, k.fn.dimmer.settings), n7 = a6.selector, e27 = a6.namespace, i6 = a6.className, s = a6.error, r6 = "." + e27, l = "module-" + e27, c = v.selector || "", u = "ontouchstart" in T.documentElement ? "touchstart" : "click", d = k(this), f = this, m = d.data(l), g = {
                preinitialize: function() {
                    o6 = g.is.dimmer() ? (t15 = d.parent(), d) : (t15 = d, g.has.dimmer() ? a6.dimmerName ? t15.find(n7.dimmer).filter("." + a6.dimmerName) : t15.find(n7.dimmer) : g.create());
                },
                initialize: function() {
                    g.debug("Initializing dimmer", a6), g.bind.events(), g.set.dimmable(), g.instantiate();
                },
                instantiate: function() {
                    g.verbose("Storing instance of module", g), m = g, d.data(l, m);
                },
                destroy: function() {
                    g.verbose("Destroying previous module", o6), g.unbind.events(), g.remove.variation(), t15.off(r6);
                },
                bind: {
                    events: function() {
                        "hover" == a6.on ? t15.on("mouseenter" + r6, g.show).on("mouseleave" + r6, g.hide) : "click" == a6.on && t15.on(u + r6, g.toggle), g.is.page() && (g.debug("Setting as a page dimmer", t15), g.set.pageDimmer()), g.is.closable() && (g.verbose("Adding dimmer close event", o6), t15.on(u + r6, n7.dimmer, g.event.click));
                    }
                },
                unbind: {
                    events: function() {
                        d.removeData(l), t15.off(r6);
                    }
                },
                event: {
                    click: function(e) {
                        g.verbose("Determining if event occurred on dimmer", e), 0 !== o6.find(e.target).length && !k(e.target).is(n7.content) || (g.hide(), e.stopImmediatePropagation());
                    }
                },
                addContent: function(e) {
                    e = k(e);
                    g.debug("Add content to dimmer", e), e.parent()[0] !== o6[0] && e.detach().appendTo(o6);
                },
                create: function() {
                    var e = k(a6.template.dimmer(a6));
                    return a6.dimmerName && (g.debug("Creating named dimmer", a6.dimmerName), e.addClass(a6.dimmerName)), e.appendTo(t15), e;
                },
                show: function(e) {
                    e = k.isFunction(e) ? e : function() {
                    }, g.debug("Showing dimmer", o6, a6), g.set.variation(), g.is.dimmed() && !g.is.animating() || !g.is.enabled() ? g.debug("Dimmer is already shown or disabled") : (g.animate.show(e), a6.onShow.call(f), a6.onChange.call(f));
                },
                hide: function(e) {
                    e = k.isFunction(e) ? e : function() {
                    }, g.is.dimmed() || g.is.animating() ? (g.debug("Hiding dimmer", o6), g.animate.hide(e), a6.onHide.call(f), a6.onChange.call(f)) : g.debug("Dimmer is not visible");
                },
                toggle: function() {
                    g.verbose("Toggling dimmer visibility", o6), g.is.dimmed() ? g.is.closable() && g.hide() : g.show();
                },
                animate: {
                    show: function(e) {
                        e = k.isFunction(e) ? e : function() {
                        }, a6.useCSS && k.fn.transition !== S && o6.transition("is supported") ? (a6.useFlex ? (g.debug("Using flex dimmer"), g.remove.legacy()) : (g.debug("Using legacy non-flex dimmer"), g.set.legacy()), "auto" !== a6.opacity && g.set.opacity(), o6.transition({
                            displayType: a6.useFlex ? "flex" : "block",
                            animation: (a6.transition.showMethod || a6.transition) + " in",
                            queue: !1,
                            duration: g.get.duration(),
                            useFailSafe: !0,
                            onStart: function() {
                                g.set.dimmed();
                            },
                            onComplete: function() {
                                g.set.active(), e();
                            }
                        })) : (g.verbose("Showing dimmer animation with javascript"), g.set.dimmed(), "auto" == a6.opacity && (a6.opacity = 0.8), o6.stop().css({
                            opacity: 0,
                            width: "100%",
                            height: "100%"
                        }).fadeTo(g.get.duration(), a6.opacity, function() {
                            o6.removeAttr("style"), g.set.active(), e();
                        }));
                    },
                    hide: function(e) {
                        e = k.isFunction(e) ? e : function() {
                        }, a6.useCSS && k.fn.transition !== S && o6.transition("is supported") ? (g.verbose("Hiding dimmer with css"), o6.transition({
                            displayType: a6.useFlex ? "flex" : "block",
                            animation: (a6.transition.hideMethod || a6.transition) + " out",
                            queue: !1,
                            duration: g.get.duration(),
                            useFailSafe: !0,
                            onComplete: function() {
                                g.remove.dimmed(), g.remove.variation(), g.remove.active(), e();
                            }
                        })) : (g.verbose("Hiding dimmer with javascript"), o6.stop().fadeOut(g.get.duration(), function() {
                            g.remove.dimmed(), g.remove.active(), o6.removeAttr("style"), e();
                        }));
                    }
                },
                get: {
                    dimmer: function() {
                        return o6;
                    },
                    duration: function() {
                        return g.is.active() ? a6.transition.hideDuration || a6.duration.hide || a6.duration : a6.transition.showDuration || a6.duration.show || a6.duration;
                    }
                },
                has: {
                    dimmer: function() {
                        return a6.dimmerName ? 0 < d.find(n7.dimmer).filter("." + a6.dimmerName).length : 0 < d.find(n7.dimmer).length;
                    }
                },
                is: {
                    active: function() {
                        return o6.hasClass(i6.active);
                    },
                    animating: function() {
                        return o6.is(":animated") || o6.hasClass(i6.animating);
                    },
                    closable: function() {
                        return "auto" == a6.closable ? "hover" != a6.on : a6.closable;
                    },
                    dimmer: function() {
                        return d.hasClass(i6.dimmer);
                    },
                    dimmable: function() {
                        return d.hasClass(i6.dimmable);
                    },
                    dimmed: function() {
                        return t15.hasClass(i6.dimmed);
                    },
                    disabled: function() {
                        return t15.hasClass(i6.disabled);
                    },
                    enabled: function() {
                        return !g.is.disabled();
                    },
                    page: function() {
                        return t15.is("body");
                    },
                    pageDimmer: function() {
                        return o6.hasClass(i6.pageDimmer);
                    }
                },
                can: {
                    show: function() {
                        return !o6.hasClass(i6.disabled);
                    }
                },
                set: {
                    opacity: function(e) {
                        var t = o6.css("background-color"), n = t.split(","), i = n && 3 <= n.length;
                        e = 0 === a6.opacity ? 0 : a6.opacity || e, t = i ? (n[2] = n[2].replace(")", ""), n[3] = e + ")", n.join(",")) : "rgba(0, 0, 0, " + e + ")", g.debug("Setting opacity to", e), o6.css("background-color", t);
                    },
                    legacy: function() {
                        o6.addClass(i6.legacy);
                    },
                    active: function() {
                        o6.addClass(i6.active);
                    },
                    dimmable: function() {
                        t15.addClass(i6.dimmable);
                    },
                    dimmed: function() {
                        t15.addClass(i6.dimmed);
                    },
                    pageDimmer: function() {
                        o6.addClass(i6.pageDimmer);
                    },
                    disabled: function() {
                        o6.addClass(i6.disabled);
                    },
                    variation: function(e) {
                        (e = e || a6.variation) && o6.addClass(e);
                    }
                },
                remove: {
                    active: function() {
                        o6.removeClass(i6.active);
                    },
                    legacy: function() {
                        o6.removeClass(i6.legacy);
                    },
                    dimmed: function() {
                        t15.removeClass(i6.dimmed);
                    },
                    disabled: function() {
                        o6.removeClass(i6.disabled);
                    },
                    variation: function(e) {
                        (e = e || a6.variation) && o6.removeClass(e);
                    }
                },
                setting: function(e, t) {
                    if (g.debug("Changing setting", e, t), k.isPlainObject(e)) k.extend(!0, a6, e);
                    else {
                        if (t === S) return a6[e];
                        k.isPlainObject(a6[e]) ? k.extend(!0, a6[e], t) : a6[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (k.isPlainObject(e)) k.extend(!0, g, e);
                    else {
                        if (t === S) return g[e];
                        g[e] = t;
                    }
                },
                debug: function() {
                    !a6.silent && a6.debug && (a6.performance ? g.performance.log(arguments) : (g.debug = Function.prototype.bind.call(console.info, console, a6.name + ":"), g.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !a6.silent && a6.verbose && a6.debug && (a6.performance ? g.performance.log(arguments) : (g.verbose = Function.prototype.bind.call(console.info, console, a6.name + ":"), g.verbose.apply(console, arguments)));
                },
                error: function() {
                    a6.silent || (g.error = Function.prototype.bind.call(console.error, console, a6.name + ":"), g.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        a6.performance && (n = (t = (new Date).getTime()) - (b || t), b = t, y.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: f,
                            "Execution Time": n
                        })), clearTimeout(g.performance.timer), g.performance.timer = setTimeout(g.performance.display, 500);
                    },
                    display: function() {
                        var e = a6.name + ":", n = 0;
                        b = !1, clearTimeout(g.performance.timer), k.each(y, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", c && (e += " '" + c + "'"), 1 < v.length && (e += " (" + v.length + ")"), (console.group !== S || console.table !== S) && 0 < y.length && (console.groupCollapsed(e), console.table ? console.table(y) : k.each(y, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), y = [];
                    }
                },
                invoke: function(i, e28, t16) {
                    var o, a, n, r = m;
                    return e28 = e28 || w, t16 = f || t16, "string" == typeof i && r !== S && (i = i.split(/[\. ]/), o = i.length - 1, k.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (k.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== S) return a = r[n], !1;
                            if (!k.isPlainObject(r[t]) || e == o) return r[t] !== S ? a = r[t] : g.error(s.method, i), !1;
                            r = r[t];
                        }
                    })), k.isFunction(a) ? n = a.apply(t16, e28) : a !== S && (n = a), Array.isArray(h) ? h.push(n) : h !== S ? h = [
                        h,
                        n
                    ] : n !== S && (h = n), a;
                }
            };
            g.preinitialize(), C ? (m === S && g.initialize(), g.invoke(x)) : (m !== S && m.invoke("destroy"), g.initialize());
        }), h !== S ? h : this;
    }, k.fn.dimmer.settings = {
        name: "Dimmer",
        namespace: "dimmer",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        useFlex: !0,
        dimmerName: !1,
        variation: !1,
        closable: "auto",
        useCSS: !0,
        transition: "fade",
        on: !1,
        opacity: "auto",
        duration: {
            show: 500,
            hide: 500
        },
        displayLoader: !1,
        loaderText: !1,
        loaderVariation: "",
        onChange: function() {
        },
        onShow: function() {
        },
        onHide: function() {
        },
        error: {
            method: "The method you called is not defined."
        },
        className: {
            active: "active",
            animating: "animating",
            dimmable: "dimmable",
            dimmed: "dimmed",
            dimmer: "dimmer",
            disabled: "disabled",
            hide: "hide",
            legacy: "legacy",
            pageDimmer: "page",
            show: "show",
            loader: "ui loader"
        },
        selector: {
            dimmer: "> .ui.dimmer",
            content: ".ui.dimmer > .content, .ui.dimmer > .content > .center"
        },
        template: {
            dimmer: function(e) {
                var t, n = k("<div/>").addClass("ui dimmer");
                return e.displayLoader && (t = k("<div/>").addClass(e.className.loader).addClass(e.loaderVariation), e.loaderText && (t.text(e.loaderText), t.addClass("text")), n.append(t)), n;
            }
        }
    };
})(jQuery, window, document), (function(te, ne, ie, oe) {
    "use strict";
    te.isFunction = te.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, ne = void 0 !== ne && ne.Math == Math ? ne : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), te.fn.dropdown = function(B) {
        var W, Y = te(this), Q = te(ie), X = Y.selector || "", K = "ontouchstart" in ie.documentElement, $ = K ? "touchstart" : "click", G = (new Date).getTime(), J = [], Z = B, _ = "string" == typeof Z, ee = [].slice.call(arguments, 1);
        return Y.each(function(n8) {
            var d4, e29, t17, i7, o7, a7, r7, s4, l4, f3 = te.isPlainObject(B) ? te.extend(!0, {
            }, te.fn.dropdown.settings, B) : te.extend({
            }, te.fn.dropdown.settings), m = f3.className, g = f3.message, c4 = f3.fields, p = f3.keys, h = f3.metadata, u4 = f3.namespace, v = f3.regExp, b = f3.selector, y = f3.error, x = f3.templates, C = "." + u4, w = "module-" + u4, k = te(this), T = te(f3.context), S = k.find(b.text), D = k.find(b.search), A = k.find(b.sizer), E = k.find(b.input), F = k.find(b.icon), P = k.find(b.clearIcon), O = 0 < k.prev().find(b.text).length ? k.prev().find(b.text) : k.prev(), R = k.children(b.menu), M = R.find(b.item), I = f3.hideDividers ? M.parent().children(b.divider) : te(), j = !1, L = !1, V = !1, q = !1, z = this, N = !1, H = k.data(w), U = {
                initialize: function() {
                    U.debug("Initializing dropdown", f3), U.is.alreadySetup() ? U.setup.reference() : (f3.ignoreDiacritics && !String.prototype.normalize && (f3.ignoreDiacritics = !1, U.error(y.noNormalize, z)), U.setup.layout(), f3.values && (U.set.initialLoad(), U.change.values(f3.values), U.remove.initialLoad()), U.refreshData(), U.save.defaults(), U.restore.selected(), U.create.id(), U.bind.events(), U.observeChanges(), U.instantiate());
                },
                instantiate: function() {
                    U.verbose("Storing instance of dropdown", U), H = U, k.data(w, U);
                },
                destroy: function() {
                    U.verbose("Destroying previous dropdown", k), U.remove.tabbable(), U.remove.active(), R.transition("stop all"), R.removeClass(m.visible).addClass(m.hidden), k.off(C).removeData(w), R.off(C), Q.off(o7), U.disconnect.menuObserver(), U.disconnect.selectObserver(), U.disconnect.classObserver();
                },
                observeChanges: function() {
                    "MutationObserver" in ne && (r7 = new MutationObserver(U.event.select.mutation), s4 = new MutationObserver(U.event.menu.mutation), l4 = new MutationObserver(U.event.class.mutation), U.debug("Setting up mutation observer", r7, s4, l4), U.observe.select(), U.observe.menu(), U.observe.class());
                },
                disconnect: {
                    menuObserver: function() {
                        s4 && s4.disconnect();
                    },
                    selectObserver: function() {
                        r7 && r7.disconnect();
                    },
                    classObserver: function() {
                        l4 && l4.disconnect();
                    }
                },
                observe: {
                    select: function() {
                        U.has.input() && r7 && r7.observe(k[0], {
                            childList: !0,
                            subtree: !0
                        });
                    },
                    menu: function() {
                        U.has.menu() && s4 && s4.observe(R[0], {
                            childList: !0,
                            subtree: !0
                        });
                    },
                    class: function() {
                        U.has.search() && l4 && l4.observe(k[0], {
                            attributes: !0
                        });
                    }
                },
                create: {
                    id: function() {
                        a7 = (Math.random().toString(16) + "000000000").substr(2, 8), o7 = "." + a7, U.verbose("Creating unique id for element", a7);
                    },
                    userChoice: function(e) {
                        var n, i;
                        return !!(e = e || U.get.userValues()) && (e = Array.isArray(e) ? e : [
                            e
                        ], te.each(e, function(e, t) {
                            !1 === U.get.item(t) && (i = f3.templates.addition(U.add.variables(g.addResult, t)), i = te("<div />").html(i).attr("data-" + h.value, t).attr("data-" + h.text, t).addClass(m.addition).addClass(m.item), f3.hideAdditions && i.addClass(m.hidden), n = n === oe ? i : n.add(i), U.verbose("Creating user choices for value", t, i));
                        }), n);
                    },
                    userLabels: function(e) {
                        var t18 = U.get.userValues();
                        t18 && (U.debug("Adding user labels", t18), te.each(t18, function(e, t) {
                            U.verbose("Adding custom user value"), U.add.label(t, t);
                        }));
                    },
                    menu: function() {
                        R = te("<div />").addClass(m.menu).appendTo(k);
                    },
                    sizer: function() {
                        A = te("<span />").addClass(m.sizer).insertAfter(D);
                    }
                },
                search: function(e) {
                    e = e !== oe ? e : U.get.query(), U.verbose("Searching for query", e), !1 === f3.fireOnInit && U.is.initialLoad() ? U.verbose("Skipping callback on initial load", f3.onSearch) : U.has.minCharacters(e) && !1 !== f3.onSearch.call(z, e) ? U.filter(e) : U.hide(null, !0);
                },
                select: {
                    firstUnfiltered: function() {
                        U.verbose("Selecting first non-filtered element"), U.remove.selectedItem(), M.not(b.unselectable).not(b.addition + b.hidden).eq(0).addClass(m.selected);
                    },
                    nextAvailable: function(e) {
                        var t = (e = e.eq(0)).nextAll(b.item).not(b.unselectable).eq(0), e = e.prevAll(b.item).not(b.unselectable).eq(0);
                        0 < t.length ? (U.verbose("Moving selection to", t), t.addClass(m.selected)) : (U.verbose("Moving selection to", e), e.addClass(m.selected));
                    }
                },
                setup: {
                    api: function() {
                        var e = {
                            debug: f3.debug,
                            urlData: {
                                value: U.get.value(),
                                query: U.get.query()
                            },
                            on: !1
                        };
                        U.verbose("First request, initializing API"), k.api(e);
                    },
                    layout: function() {
                        k.is("select") && (U.setup.select(), U.setup.returnedObject()), U.has.menu() || U.create.menu(), U.is.clearable() && !U.has.clearItem() && (U.verbose("Adding clear icon"), P = te("<i />").addClass("remove icon").insertBefore(S)), U.is.search() && !U.has.search() && (U.verbose("Adding search input"), D = te("<input />").addClass(m.search).prop("autocomplete", U.is.chrome() ? "fomantic-search" : "off").insertBefore(S)), U.is.multiple() && U.is.searchSelection() && !U.has.sizer() && U.create.sizer(), f3.allowTab && U.set.tabbable();
                    },
                    select: function() {
                        var e = U.get.selectValues();
                        U.debug("Dropdown initialized on a select", e), 0 < (E = k.is("select") ? k : E).parent(b.dropdown).length ? (U.debug("UI dropdown already exists. Creating dropdown menu only"), k = E.closest(b.dropdown), U.has.menu() || U.create.menu(), R = k.children(b.menu), U.setup.menu(e)) : (U.debug("Creating entire dropdown from select"), k = te("<div />").attr("class", E.attr("class")).addClass(m.selection).addClass(m.dropdown).html(x.dropdown(e, c4, f3.preserveHTML, f3.className)).insertBefore(E), E.hasClass(m.multiple) && !1 === E.prop("multiple") && (U.error(y.missingMultiple), E.prop("multiple", !0)), E.is("[multiple]") && U.set.multiple(), E.prop("disabled") && (U.debug("Disabling dropdown"), k.addClass(m.disabled)), E.removeAttr("required").removeAttr("class").detach().prependTo(k)), U.refresh();
                    },
                    menu: function(e) {
                        R.html(x.menu(e, c4, f3.preserveHTML, f3.className)), M = R.find(b.item), I = f3.hideDividers ? M.parent().children(b.divider) : te();
                    },
                    reference: function() {
                        U.debug("Dropdown behavior was called on select, replacing with closest dropdown"), k = k.parent(b.dropdown), H = k.data(w), z = k.get(0), U.refresh(), U.setup.returnedObject();
                    },
                    returnedObject: function() {
                        var e = Y.slice(0, n8), t = Y.slice(n8 + 1);
                        Y = e.add(k).add(t);
                    }
                },
                refresh: function() {
                    U.refreshSelectors(), U.refreshData();
                },
                refreshItems: function() {
                    M = R.find(b.item), I = f3.hideDividers ? M.parent().children(b.divider) : te();
                },
                refreshSelectors: function() {
                    U.verbose("Refreshing selector cache"), S = k.find(b.text), D = k.find(b.search), E = k.find(b.input), F = k.find(b.icon), O = 0 < k.prev().find(b.text).length ? k.prev().find(b.text) : k.prev(), R = k.children(b.menu), M = R.find(b.item), I = f3.hideDividers ? M.parent().children(b.divider) : te();
                },
                refreshData: function() {
                    U.verbose("Refreshing cached metadata"), M.removeData(h.text).removeData(h.value);
                },
                clearData: function() {
                    U.verbose("Clearing metadata"), M.removeData(h.text).removeData(h.value), k.removeData(h.defaultText).removeData(h.defaultValue).removeData(h.placeholderText);
                },
                clearItems: function() {
                    R.empty(), U.refreshItems();
                },
                toggle: function() {
                    U.verbose("Toggling menu visibility"), U.is.active() ? U.hide() : U.show();
                },
                show: function(e, t) {
                    if (e = te.isFunction(e) ? e : function() {
                    }, (N || q) && U.is.remote() && U.is.noApiCache() && U.clearItems(), !U.can.show() && U.is.remote() && (U.debug("No API results retrieved, searching before show"), U.queryRemote(U.get.query(), U.show, [
                        e,
                        t
                    ])), U.can.show() && !U.is.active()) {
                        if (U.debug("Showing dropdown"), !U.has.message() || U.has.maxSelections() || U.has.allResultsFiltered() || U.remove.message(), U.is.allFiltered()) return !0;
                        !1 !== f3.onShow.call(z) && U.animate.show(function() {
                            U.can.click() && U.bind.intent(), U.has.search() && !t && U.focusSearch(), U.set.visible(), e.call(z);
                        });
                    }
                },
                hide: function(e, t) {
                    e = te.isFunction(e) ? e : function() {
                    }, U.is.active() && !U.is.animatingOutward() ? (U.debug("Hiding dropdown"), !1 !== f3.onHide.call(z) && U.animate.hide(function() {
                        U.remove.visible(), U.is.focusedOnSearch() && !0 !== t && D.blur(), e.call(z);
                    })) : U.can.click() && U.unbind.intent(), N = q = !1;
                },
                hideOthers: function() {
                    U.verbose("Finding other dropdowns to hide"), Y.not(k).has(b.menu + "." + m.visible).dropdown("hide");
                },
                hideMenu: function() {
                    U.verbose("Hiding menu  instantaneously"), U.remove.active(), U.remove.visible(), R.transition("hide");
                },
                hideSubMenus: function() {
                    var e = R.children(b.item).find(b.menu);
                    U.verbose("Hiding sub menus", e), e.transition("hide");
                },
                bind: {
                    events: function() {
                        U.bind.keyboardEvents(), U.bind.inputEvents(), U.bind.mouseEvents();
                    },
                    keyboardEvents: function() {
                        U.verbose("Binding keyboard events"), k.on("keydown" + C, U.event.keydown), U.has.search() && k.on(U.get.inputEvent() + C, b.search, U.event.input), U.is.multiple() && Q.on("keydown" + o7, U.event.document.keydown);
                    },
                    inputEvents: function() {
                        U.verbose("Binding input change events"), k.on("change" + C, b.input, U.event.change);
                    },
                    mouseEvents: function() {
                        U.verbose("Binding mouse events"), U.is.multiple() && k.on($ + C, b.label, U.event.label.click).on($ + C, b.remove, U.event.remove.click), U.is.searchSelection() ? (k.on("mousedown" + C, U.event.mousedown).on("mouseup" + C, U.event.mouseup).on("mousedown" + C, b.menu, U.event.menu.mousedown).on("mouseup" + C, b.menu, U.event.menu.mouseup).on($ + C, b.icon, U.event.icon.click).on($ + C, b.clearIcon, U.event.clearIcon.click).on("focus" + C, b.search, U.event.search.focus).on($ + C, b.search, U.event.search.focus).on("blur" + C, b.search, U.event.search.blur).on($ + C, b.text, U.event.text.focus), U.is.multiple() && k.on($ + C, U.event.click).on($ + C, U.event.search.focus)) : ("click" == f3.on ? k.on($ + C, b.icon, U.event.icon.click).on($ + C, U.event.test.toggle) : "hover" == f3.on ? k.on("mouseenter" + C, U.delay.show).on("mouseleave" + C, U.delay.hide) : k.on(f3.on + C, U.toggle), k.on("mousedown" + C, U.event.mousedown).on("mouseup" + C, U.event.mouseup).on("focus" + C, U.event.focus).on($ + C, b.clearIcon, U.event.clearIcon.click), U.has.menuSearch() ? k.on("blur" + C, b.search, U.event.search.blur) : k.on("blur" + C, U.event.blur)), R.on((K ? "touchstart" : "mouseenter") + C, b.item, U.event.item.mouseenter).on("mouseleave" + C, b.item, U.event.item.mouseleave).on("click" + C, b.item, U.event.item.click);
                    },
                    intent: function() {
                        U.verbose("Binding hide intent event to document"), K && Q.on("touchstart" + o7, U.event.test.touch).on("touchmove" + o7, U.event.test.touch), Q.on($ + o7, U.event.test.hide);
                    }
                },
                unbind: {
                    intent: function() {
                        U.verbose("Removing hide intent event from document"), K && Q.off("touchstart" + o7).off("touchmove" + o7), Q.off($ + o7);
                    }
                },
                filter: function(e30) {
                    function t19() {
                        U.is.multiple() && U.filterActive(), (e30 || !e30 && 0 == U.get.activeItem().length) && U.select.firstUnfiltered(), U.has.allResultsFiltered() ? f3.onNoResults.call(z, n) ? f3.allowAdditions ? f3.hideAdditions && (U.verbose("User addition with no menu, setting empty style"), U.set.empty(), U.hideMenu()) : (U.verbose("All items filtered, showing message", n), U.add.message(g.noResults)) : (U.verbose("All items filtered, hiding dropdown", n), U.hideMenu()) : (U.remove.empty(), U.remove.message()), f3.allowAdditions && U.add.userSuggestion(U.escape.htmlEntities(e30)), U.is.searchSelection() && U.can.show() && U.is.focusedOnSearch() && U.show();
                    }
                    var n = e30 !== oe ? e30 : U.get.query();
                    f3.useLabels && U.has.maxSelections() || (f3.apiSettings ? U.can.useAPI() ? U.queryRemote(n, function() {
                        f3.filterRemoteData && U.filterItems(n);
                        var e = E.val();
                        Array.isArray(e) || (e = e && "" !== e ? e.split(f3.delimiter) : []), U.is.multiple() && te.each(e, function(e, t) {
                            M.filter('[data-value="' + t + '"]').addClass(m.filtered);
                        }), U.focusSearch(!0), t19();
                    }) : U.error(y.noAPI) : (U.filterItems(n), t19()));
                },
                queryRemote: function(e31, n, i) {
                    Array.isArray(i) || (i = [
                        i
                    ]);
                    e31 = {
                        errorDuration: !1,
                        cache: "local",
                        throttle: f3.throttle,
                        urlData: {
                            query: e31
                        },
                        onError: function() {
                            U.add.message(g.serverError), N = q = !1, n.apply(null, i);
                        },
                        onFailure: function() {
                            U.add.message(g.serverError), N = q = !1, n.apply(null, i);
                        },
                        onSuccess: function(e) {
                            var t = e[c4.remoteValues];
                            Array.isArray(t) || (t = []), U.remove.message();
                            e = {
                            };
                            e[c4.values] = t, U.setup.menu(e), 0 !== t.length || f3.allowAdditions ? "" !== (t = U.is.multiple() ? U.get.values() : U.get.value()) && (U.verbose("Value(s) present after click icon, select value(s) in items"), U.set.selected(t, null, null, !0)) : U.add.message(g.noResults), N = q = !1, n.apply(null, i);
                        }
                    };
                    k.api("get request") || U.setup.api(), e31 = te.extend(!0, {
                    }, e31, f3.apiSettings), k.api("setting", e31).api("query");
                },
                filterItems: function(e32) {
                    var n = U.remove.diacritics(e32 !== oe ? e32 : U.get.query()), i = null, t20 = U.escape.string(n), e32 = (f3.ignoreSearchCase ? "i" : "") + "gm", o = new RegExp("^" + t20, e32);
                    U.has.query() && (i = [], U.verbose("Searching for matching values", n), M.each(function() {
                        var e, t = te(this);
                        if (t.hasClass(m.unfilterable)) return i.push(this), !0;
                        if ("both" === f3.match || "text" === f3.match) {
                            if (-1 !== (e = U.remove.diacritics(String(U.get.choiceText(t, !1)))).search(o)) return i.push(this), !0;
                            if ("exact" === f3.fullTextSearch && U.exactSearch(n, e)) return i.push(this), !0;
                            if (!0 === f3.fullTextSearch && U.fuzzySearch(n, e)) return i.push(this), !0;
                        }
                        return ("both" === f3.match || "value" === f3.match) && (-1 !== (e = U.remove.diacritics(String(U.get.choiceValue(t, e)))).search(o) || "exact" === f3.fullTextSearch && U.exactSearch(n, e) || !0 === f3.fullTextSearch && U.fuzzySearch(n, e)) ? (i.push(this), !0) : void 0;
                    })), U.debug("Showing only matched items", n), U.remove.filteredItem(), i && M.not(i).addClass(m.filtered), U.has.query() ? !0 === f3.hideDividers ? I.addClass(m.hidden) : "empty" === f3.hideDividers && I.removeClass(m.hidden).filter(function() {
                        var e = te(this).nextUntil(b.item);
                        return 0 === (e.length ? e : te(this)).nextUntil(b.divider).filter(b.item + ":not(." + m.filtered + ")").length;
                    }).addClass(m.hidden) : I.removeClass(m.hidden);
                },
                fuzzySearch: function(e, t) {
                    var n = t.length, i = e.length;
                    if (e = f3.ignoreSearchCase ? e.toLowerCase() : e, t = f3.ignoreSearchCase ? t.toLowerCase() : t, n < i) return !1;
                    if (i === n) return e === t;
                    e: for(var o = 0, a = 0; o < i; o++){
                        for(var r = e.charCodeAt(o); a < n;)if (t.charCodeAt(a++) === r) continue e;
                        return !1;
                    }
                    return !0;
                },
                exactSearch: function(e, t) {
                    return e = f3.ignoreSearchCase ? e.toLowerCase() : e, -1 < (t = f3.ignoreSearchCase ? t.toLowerCase() : t).indexOf(e);
                },
                filterActive: function() {
                    f3.useLabels && M.filter("." + m.active).addClass(m.filtered);
                },
                focusSearch: function(e) {
                    U.has.search() && !U.is.focusedOnSearch() && (e ? (k.off("focus" + C, b.search), D.focus(), k.on("focus" + C, b.search, U.event.search.focus)) : D.focus());
                },
                blurSearch: function() {
                    U.has.search() && D.blur();
                },
                forceSelection: function() {
                    var e = M.not(m.filtered).filter("." + m.selected).eq(0), t = M.not(m.filtered).filter("." + m.active).eq(0), e = 0 < e.length ? e : t, t = 0 < e.length;
                    f3.allowAdditions || t && !U.is.multiple() ? (U.debug("Forcing partial selection to selected item", e), U.event.item.click.call(e, {
                    }, !0)) : U.remove.searchTerm();
                },
                change: {
                    values: function(e) {
                        f3.allowAdditions || U.clear(), U.debug("Creating dropdown with specified values", e);
                        var t21 = {
                        };
                        t21[c4.values] = e, U.setup.menu(t21), te.each(e, function(e, t) {
                            if (1 == t.selected && (U.debug("Setting initial selection to", t[c4.value]), U.set.selected(t[c4.value]), !U.is.multiple())) return !1;
                        }), U.has.selectInput() && (U.disconnect.selectObserver(), E.html(""), E.append("<option disabled selected value></option>"), te.each(e, function(e, t) {
                            var n = f3.templates.deQuote(t[c4.value]), t = f3.templates.escape(t[c4.name] || "", f3.preserveHTML);
                            E.append('<option value="' + n + '">' + t + "</option>");
                        }), U.observe.select());
                    }
                },
                event: {
                    change: function() {
                        V || (U.debug("Input changed, updating selection"), U.set.selected());
                    },
                    focus: function() {
                        f3.showOnFocus && !j && U.is.hidden() && !t17 && (N = !0, U.show());
                    },
                    blur: function(e) {
                        t17 = ie.activeElement === this, j || t17 || (U.remove.activeLabel(), U.hide());
                    },
                    mousedown: function() {
                        U.is.searchSelection() ? i7 = !0 : j = !0;
                    },
                    mouseup: function() {
                        U.is.searchSelection() ? i7 = !1 : j = !1;
                    },
                    click: function(e) {
                        te(e.target).is(k) && (U.is.focusedOnSearch() ? U.show() : U.focusSearch());
                    },
                    search: {
                        focus: function(e) {
                            j = !0, U.is.multiple() && U.remove.activeLabel(), N || U.is.active() || !(f3.showOnFocus || "focus" !== e.type && "focusin" !== e.type) || (N = !0, U.search());
                        },
                        blur: function(e) {
                            t17 = ie.activeElement === this, U.is.searchSelection() && !i7 && (L || t17 || (f3.forceSelection ? U.forceSelection() : f3.allowAdditions || U.remove.searchTerm(), U.hide())), i7 = !1;
                        }
                    },
                    clearIcon: {
                        click: function(e) {
                            U.clear(), U.is.searchSelection() && U.remove.searchTerm(), U.hide(), e.stopPropagation();
                        }
                    },
                    icon: {
                        click: function(e) {
                            q = !0, U.has.search() ? U.is.active() ? U.blurSearch() : f3.showOnFocus ? U.focusSearch() : U.toggle() : U.toggle(), e.stopPropagation();
                        }
                    },
                    text: {
                        focus: function(e) {
                            j = !0, U.focusSearch();
                        }
                    },
                    input: function(e) {
                        (U.is.multiple() || U.is.searchSelection()) && U.set.filtered(), clearTimeout(U.timer), U.timer = setTimeout(U.search, f3.delay.search);
                    },
                    label: {
                        click: function(e) {
                            var t = te(this), n = k.find(b.label), i = n.filter("." + m.active), o = t.nextAll("." + m.active), a = t.prevAll("." + m.active), a = (0 < o.length ? t.nextUntil(o) : t.prevUntil(a)).add(i).add(t);
                            e.shiftKey ? (i.removeClass(m.active), a.addClass(m.active)) : e.ctrlKey ? t.toggleClass(m.active) : (i.removeClass(m.active), t.addClass(m.active)), f3.onLabelSelect.apply(this, n.filter("." + m.active)), e.stopPropagation();
                        }
                    },
                    remove: {
                        click: function(e) {
                            var t = te(this).parent();
                            t.hasClass(m.active) ? U.remove.activeLabels() : U.remove.activeLabels(t), e.stopPropagation();
                        }
                    },
                    test: {
                        toggle: function(e) {
                            var t = U.is.multiple() ? U.show : U.toggle;
                            U.is.bubbledLabelClick(e) || U.is.bubbledIconClick(e) || (U.is.multiple() && (!U.is.multiple() || U.is.active()) || (N = !0), U.determine.eventOnElement(e, t) && e.preventDefault());
                        },
                        touch: function(e) {
                            U.determine.eventOnElement(e, function() {
                                "touchstart" == e.type ? U.timer = setTimeout(function() {
                                    U.hide();
                                }, f3.delay.touch) : "touchmove" == e.type && clearTimeout(U.timer);
                            }), e.stopPropagation();
                        },
                        hide: function(e) {
                            U.determine.eventInModule(e, U.hide) && z.id && te(e.target).attr("for") === z.id && e.preventDefault();
                        }
                    },
                    class: {
                        mutation: function(e33) {
                            e33.forEach(function(e) {
                                "class" === e.attributeName && U.check.disabled();
                            });
                        }
                    },
                    select: {
                        mutation: function(e) {
                            U.debug("<select> modified, recreating menu"), U.is.selectMutation(e) && (U.disconnect.selectObserver(), U.refresh(), U.setup.select(), U.set.selected(), U.observe.select());
                        }
                    },
                    menu: {
                        mutation: function(e) {
                            var t = e[0], e = t.addedNodes ? te(t.addedNodes[0]) : te(!1), t = t.removedNodes ? te(t.removedNodes[0]) : te(!1), e = e.add(t), t = e.is(b.addition) || 0 < e.closest(b.addition).length, e = e.is(b.message) || 0 < e.closest(b.message).length;
                            t || e ? (U.debug("Updating item selector cache"), U.refreshItems()) : (U.debug("Menu modified, updating selector cache"), U.refresh());
                        },
                        mousedown: function() {
                            L = !0;
                        },
                        mouseup: function() {
                            L = !1;
                        }
                    },
                    item: {
                        mouseenter: function(e) {
                            var t = te(e.target), n = te(this), i = n.children(b.menu), o = n.siblings(b.item).children(b.menu), n = 0 < i.length;
                            0 < i.find(t).length || !n || (clearTimeout(U.itemTimer), U.itemTimer = setTimeout(function() {
                                U.verbose("Showing sub-menu", i), te.each(o, function() {
                                    U.animate.hide(!1, te(this));
                                }), U.animate.show(!1, i);
                            }, f3.delay.show), e.preventDefault());
                        },
                        mouseleave: function(e) {
                            var t = te(this).children(b.menu);
                            0 < t.length && (clearTimeout(U.itemTimer), U.itemTimer = setTimeout(function() {
                                U.verbose("Hiding sub-menu", t), U.animate.hide(!1, t);
                            }, f3.delay.hide));
                        },
                        click: function(e, t) {
                            var n = te(this), i = te(e ? e.target : ""), o = n.find(b.menu), a = U.get.choiceText(n), r = U.get.choiceValue(n, a), e = 0 < o.length, i = 0 < o.find(i).length;
                            "input" !== ie.activeElement.tagName.toLowerCase() && te(ie.activeElement).blur(), i || e && !f3.allowCategorySelection || (U.is.searchSelection() && (f3.allowAdditions && U.remove.userAddition(), U.remove.searchTerm(), U.is.focusedOnSearch() || 1 == t || U.focusSearch(!0)), f3.useLabels || (U.remove.filteredItem(), U.set.scrollPosition(n)), U.determine.selectAction.call(this, a, r));
                        }
                    },
                    document: {
                        keydown: function(e) {
                            var t, n, i, o, a, r, s, l, c, u, d, f = e.which;
                            U.is.inObject(f, p) && ((n = (t = k.find(b.label)).filter("." + m.active)).data(h.value), u = t.index(n), d = t.length, i = 0 < n.length, o = 1 < n.length, a = 0 === u, r = u + 1 == d, s = U.is.searchSelection(), l = U.is.focusedOnSearch(), c = U.is.focused(), d = (u = l && 0 === U.get.caretPosition(!1)) && 0 !== U.get.caretPosition(!0), s && !i && !l || (f == p.leftArrow ? !c && !u || i ? i && (e.shiftKey ? U.verbose("Adding previous label to selection") : (U.verbose("Selecting previous label"), t.removeClass(m.active)), a && !o ? n.addClass(m.active) : n.prev(b.siblingLabel).addClass(m.active).end(), e.preventDefault()) : (U.verbose("Selecting previous label"), t.last().addClass(m.active)) : f == p.rightArrow ? (c && !i && t.first().addClass(m.active), i && (e.shiftKey ? U.verbose("Adding next label to selection") : (U.verbose("Selecting next label"), t.removeClass(m.active)), r ? s ? l ? t.removeClass(m.active) : U.focusSearch() : (o ? n.next(b.siblingLabel) : n).addClass(m.active) : n.next(b.siblingLabel).addClass(m.active), e.preventDefault())) : f == p.deleteKey || f == p.backspace ? i ? (U.verbose("Removing active labels"), r && s && !l && U.focusSearch(), n.last().next(b.siblingLabel).addClass(m.active), U.remove.activeLabels(n), e.preventDefault()) : !u || d || i || f != p.backspace || (U.verbose("Removing last label on input backspace"), n = t.last().addClass(m.active), U.remove.activeLabels(n)) : n.removeClass(m.active)));
                        }
                    },
                    keydown: function(e) {
                        var t = e.which;
                        if (U.is.inObject(t, p)) {
                            var n, i = M.not(b.unselectable).filter("." + m.selected).eq(0), o = R.children("." + m.active).eq(0), a = 0 < i.length ? i : o, r = 0 < a.length ? a.siblings(":not(." + m.filtered + ")").addBack() : R.children(":not(." + m.filtered + ")"), s = a.children(b.menu), l = a.closest(b.menu), c = l.hasClass(m.visible) || l.hasClass(m.animating) || 0 < l.parent(b.menu).length, u = 0 < s.length, d = 0 < a.length, i = 0 < a.not(b.unselectable).length, o = t == p.delimiter && f3.allowAdditions && U.is.multiple();
                            if (f3.allowAdditions && f3.hideAdditions && (t == p.enter || o) && i && (U.verbose("Selecting item from keyboard shortcut", a), U.event.item.click.call(a, e), U.is.searchSelection() && U.remove.searchTerm(), U.is.multiple() && e.preventDefault()), U.is.visible()) {
                                if (t != p.enter && !o || (t == p.enter && d && u && !f3.allowCategorySelection ? (U.verbose("Pressed enter on unselectable category, opening sub menu"), t = p.rightArrow) : i && (U.verbose("Selecting item from keyboard shortcut", a), U.event.item.click.call(a, e), U.is.searchSelection() && (U.remove.searchTerm(), U.is.multiple() && D.focus())), e.preventDefault()), d && (t == p.leftArrow && l[0] !== R[0] && (U.verbose("Left key pressed, closing sub-menu"), U.animate.hide(!1, l), a.removeClass(m.selected), l.closest(b.item).addClass(m.selected), e.preventDefault()), t == p.rightArrow && u && (U.verbose("Right key pressed, opening sub-menu"), U.animate.show(!1, s), a.removeClass(m.selected), s.find(b.item).eq(0).addClass(m.selected), e.preventDefault())), t == p.upArrow) {
                                    if (n = (d && c ? a.prevAll(b.item + ":not(" + b.unselectable + ")") : M).eq(0), r.index(n) < 0) return U.verbose("Up key pressed but reached top of current menu"), void e.preventDefault();
                                    U.verbose("Up key pressed, changing active item"), a.removeClass(m.selected), n.addClass(m.selected), U.set.scrollPosition(n), f3.selectOnKeydown && U.is.single() && U.set.selectedItem(n), e.preventDefault();
                                }
                                if (t == p.downArrow) {
                                    if (0 === (n = (d && c ? a.nextAll(b.item + ":not(" + b.unselectable + ")") : M).eq(0)).length) return U.verbose("Down key pressed but reached bottom of current menu"), void e.preventDefault();
                                    U.verbose("Down key pressed, changing active item"), M.removeClass(m.selected), n.addClass(m.selected), U.set.scrollPosition(n), f3.selectOnKeydown && U.is.single() && U.set.selectedItem(n), e.preventDefault();
                                }
                                t == p.pageUp && (U.scrollPage("up"), e.preventDefault()), t == p.pageDown && (U.scrollPage("down"), e.preventDefault()), t == p.escape && (U.verbose("Escape key pressed, closing dropdown"), U.hide());
                            } else o && e.preventDefault(), t != p.downArrow || U.is.visible() || (U.verbose("Down key pressed, showing dropdown"), U.show(), e.preventDefault());
                        } else U.has.search() || U.set.selectedLetter(String.fromCharCode(t));
                    }
                },
                trigger: {
                    change: function() {
                        var e, t = E[0];
                        t && (e = ie.createEvent("HTMLEvents"), U.verbose("Triggering native change event"), e.initEvent("change", !0, !1), t.dispatchEvent(e));
                    }
                },
                determine: {
                    selectAction: function(e, t) {
                        d4 = !0, U.verbose("Determining action", f3.action), te.isFunction(U.action[f3.action]) ? (U.verbose("Triggering preset action", f3.action, e, t), U.action[f3.action].call(z, e, t, this)) : te.isFunction(f3.action) ? (U.verbose("Triggering user action", f3.action, e, t), f3.action.call(z, e, t, this)) : U.error(y.action, f3.action), d4 = !1;
                    },
                    eventInModule: function(e, t) {
                        var n = te(e.target), e = 0 < n.closest(ie.documentElement).length, n = 0 < n.closest(k).length;
                        return t = te.isFunction(t) ? t : function() {
                        }, e && !n ? (U.verbose("Triggering event", t), t(), !0) : (U.verbose("Event occurred in dropdown, canceling callback"), !1);
                    },
                    eventOnElement: function(e, t) {
                        var n = te(e.target), i = n.closest(b.siblingLabel), e = ie.body.contains(e.target), i = 0 === k.find(i).length || !(U.is.multiple() && f3.useLabels), n = 0 === n.closest(R).length;
                        return t = te.isFunction(t) ? t : function() {
                        }, e && i && n ? (U.verbose("Triggering event", t), t(), !0) : (U.verbose("Event occurred in dropdown menu, canceling callback"), !1);
                    }
                },
                action: {
                    nothing: function() {
                    },
                    activate: function(e, t, n) {
                        t = t !== oe ? t : e, U.can.activate(te(n)) && (U.set.selected(t, te(n)), U.is.multiple() || U.hideAndClear());
                    },
                    select: function(e, t, n) {
                        t = t !== oe ? t : e, U.can.activate(te(n)) && (U.set.value(t, e, te(n)), U.is.multiple() || U.hideAndClear());
                    },
                    combo: function(e, t, n) {
                        U.set.selected(t = t !== oe ? t : e, te(n)), U.hideAndClear();
                    },
                    hide: function(e, t, n) {
                        U.set.value(t, e, te(n)), U.hideAndClear();
                    }
                },
                get: {
                    id: function() {
                        return a7;
                    },
                    defaultText: function() {
                        return k.data(h.defaultText);
                    },
                    defaultValue: function() {
                        return k.data(h.defaultValue);
                    },
                    placeholderText: function() {
                        return "auto" != f3.placeholder && "string" == typeof f3.placeholder ? f3.placeholder : k.data(h.placeholderText) || "";
                    },
                    text: function() {
                        return f3.preserveHTML ? S.html() : S.text();
                    },
                    query: function() {
                        return String(D.val()).trim();
                    },
                    searchWidth: function(e) {
                        return e = e !== oe ? e : D.val(), A.text(e), Math.ceil(A.width() + 1);
                    },
                    selectionCount: function() {
                        var e = U.get.values();
                        return U.is.multiple() ? Array.isArray(e) ? e.length : 0 : "" !== U.get.value() ? 1 : 0;
                    },
                    transition: function(e) {
                        return "auto" === f3.transition ? U.is.upward(e) ? "slide up" : "slide down" : f3.transition;
                    },
                    userValues: function() {
                        var e34 = U.get.values();
                        return !!e34 && (e34 = Array.isArray(e34) ? e34 : [
                            e34
                        ], te.grep(e34, function(e) {
                            return !1 === U.get.item(e);
                        }));
                    },
                    uniqueArray: function(n) {
                        return te.grep(n, function(e, t) {
                            return te.inArray(e, n) === t;
                        });
                    },
                    caretPosition: function(e) {
                        var t, n, i = D.get(0);
                        return e && "selectionEnd" in i ? i.selectionEnd : !e && "selectionStart" in i ? i.selectionStart : ie.selection ? (i.focus(), n = (t = ie.selection.createRange()).text.length, e ? n : (t.moveStart("character", -i.value.length), t.text.length - n)) : void 0;
                    },
                    value: function() {
                        var e = 0 < E.length ? E.val() : k.data(h.value), t = Array.isArray(e) && 1 === e.length && "" === e[0];
                        return e === oe || t ? "" : e;
                    },
                    values: function(e) {
                        var t = U.get.value();
                        return "" === t ? "" : !U.has.selectInput() && U.is.multiple() ? "string" == typeof t ? (e ? t : U.escape.htmlEntities(t)).split(f3.delimiter) : "" : t;
                    },
                    remoteValues: function() {
                        var e = U.get.values(), i = !1;
                        return e && te.each(e = "string" == typeof e ? [
                            e
                        ] : e, function(e, t) {
                            var n = U.read.remoteData(t);
                            U.verbose("Restoring value from session data", n, t), n && ((i = i || {
                            })[t] = n);
                        }), i;
                    },
                    choiceText: function(e, t) {
                        if (t = t !== oe ? t : f3.preserveHTML, e) return 0 < e.find(b.menu).length && (U.verbose("Retrieving text of element with sub-menu"), (e = e.clone()).find(b.menu).remove(), e.find(b.menuIcon).remove()), e.data(h.text) !== oe ? e.data(h.text) : t ? e.html() && e.html().trim() : e.text() && e.text().trim();
                    },
                    choiceValue: function(e, t) {
                        return t = t || U.get.choiceText(e), !!e && (e.data(h.value) !== oe ? String(e.data(h.value)) : "string" == typeof t ? String(f3.ignoreSearchCase ? t.toLowerCase() : t).trim() : String(t));
                    },
                    inputEvent: function() {
                        var e = D[0];
                        return !!e && (e.oninput !== oe ? "input" : e.onpropertychange !== oe ? "propertychange" : "keyup");
                    },
                    selectValues: function() {
                        var a = {
                        }, r = [], s = [];
                        return k.find("option").each(function() {
                            var e = te(this), t = e.html(), n = e.attr("disabled"), i = e.attr("value") !== oe ? e.attr("value") : t, o = e.data(h.text) !== oe ? e.data(h.text) : t, e = e.parent("optgroup");
                            "auto" === f3.placeholder && "" === i ? a.placeholder = t : (e.length === r.length && e[0] === r[0] || (s.push({
                                type: "header",
                                divider: f3.headerDivider,
                                name: e.attr("label") || ""
                            }), r = e), s.push({
                                name: t,
                                value: i,
                                text: o,
                                disabled: n
                            }));
                        }), f3.placeholder && "auto" !== f3.placeholder && (U.debug("Setting placeholder value to", f3.placeholder), a.placeholder = f3.placeholder), f3.sortSelect ? (!0 === f3.sortSelect ? s.sort(function(e, t) {
                            return e.name.localeCompare(t.name);
                        }) : "natural" === f3.sortSelect ? s.sort(function(e, t) {
                            return e.name.toLowerCase().localeCompare(t.name.toLowerCase());
                        }) : te.isFunction(f3.sortSelect) && s.sort(f3.sortSelect), a[c4.values] = s, U.debug("Retrieved and sorted values from select", a)) : (a[c4.values] = s, U.debug("Retrieved values from select", a)), a;
                    },
                    activeItem: function() {
                        return M.filter("." + m.active);
                    },
                    selectedItem: function() {
                        var e = M.not(b.unselectable).filter("." + m.selected);
                        return 0 < e.length ? e : M.eq(0);
                    },
                    itemWithAdditions: function(e) {
                        var t = U.get.item(e), e = U.create.userChoice(e);
                        return t = e && 0 < e.length ? 0 < t.length ? t.add(e) : e : t;
                    },
                    item: function(n, i) {
                        var e35, o, a = !1;
                        return n = n !== oe ? n : U.get.values() !== oe ? U.get.values() : U.get.text(), e35 = (o = U.is.multiple() && Array.isArray(n)) ? 0 < n.length : n !== oe && null !== n, i = "" === n || !1 === n || !0 === n || i || !1, e35 && M.each(function() {
                            var e36 = te(this), t = U.get.choiceText(e36), t = U.get.choiceValue(e36, t);
                            if (null !== t && t !== oe) {
                                if (o) -1 !== te.inArray(U.escape.htmlEntities(String(t)), n.map(function(e) {
                                    return String(e);
                                })) && (a = a ? a.add(e36) : e36);
                                else if (i) {
                                    if (U.verbose("Ambiguous dropdown value using strict type check", e36, n), t === n) return a = e36, !0;
                                } else if (f3.ignoreCase && (t = t.toLowerCase(), n = n.toLowerCase()), U.escape.htmlEntities(String(t)) === U.escape.htmlEntities(String(n))) return U.verbose("Found select item by value", t, n), a = e36, !0;
                            }
                        }), a;
                    },
                    displayType: function() {
                        return k.hasClass("column") ? "flex" : f3.displayType;
                    }
                },
                check: {
                    maxSelections: function(e) {
                        return !f3.maxSelections || ((e = e !== oe ? e : U.get.selectionCount()) >= f3.maxSelections ? (U.debug("Maximum selection count reached"), f3.useLabels && (M.addClass(m.filtered), U.add.message(g.maxSelections)), !0) : (U.verbose("No longer at maximum selection count"), U.remove.message(), U.remove.filteredItem(), U.is.searchSelection() && U.filterItems(), !1));
                    },
                    disabled: function() {
                        D.attr("tabindex", U.is.disabled() ? -1 : 0);
                    }
                },
                restore: {
                    defaults: function(e) {
                        U.clear(e), U.restore.defaultText(), U.restore.defaultValue();
                    },
                    defaultText: function() {
                        var e = U.get.defaultText();
                        e === U.get.placeholderText ? (U.debug("Restoring default placeholder text", e), U.set.placeholderText(e)) : (U.debug("Restoring default text", e), U.set.text(e));
                    },
                    placeholderText: function() {
                        U.set.placeholderText();
                    },
                    defaultValue: function() {
                        var e = U.get.defaultValue();
                        e !== oe && (U.debug("Restoring default value", e), "" !== e ? (U.set.value(e), U.set.selected()) : (U.remove.activeItem(), U.remove.selectedItem()));
                    },
                    labels: function() {
                        f3.allowAdditions && (f3.useLabels || (U.error(y.labels), f3.useLabels = !0), U.debug("Restoring selected values"), U.create.userLabels()), U.check.maxSelections();
                    },
                    selected: function() {
                        U.restore.values(), U.is.multiple() ? (U.debug("Restoring previously selected values and labels"), U.restore.labels()) : U.debug("Restoring previously selected values");
                    },
                    values: function() {
                        U.set.initialLoad(), f3.apiSettings && f3.saveRemoteData && U.get.remoteValues() ? U.restore.remoteValues() : U.set.selected();
                        var e = U.get.value();
                        !e || "" === e || Array.isArray(e) && 0 === e.length ? E.addClass(m.noselection) : E.removeClass(m.noselection), U.remove.initialLoad();
                    },
                    remoteValues: function() {
                        var e37 = U.get.remoteValues();
                        U.debug("Recreating selected from session data", e37), e37 && (U.is.single() ? te.each(e37, function(e, t) {
                            U.set.text(t);
                        }) : te.each(e37, function(e, t) {
                            U.add.label(e, t);
                        }));
                    }
                },
                read: {
                    remoteData: function(e) {
                        if (ne.Storage !== oe) return (e = sessionStorage.getItem(e)) !== oe && e;
                        U.error(y.noStorage);
                    }
                },
                save: {
                    defaults: function() {
                        U.save.defaultText(), U.save.placeholderText(), U.save.defaultValue();
                    },
                    defaultValue: function() {
                        var e = U.get.value();
                        U.verbose("Saving default value as", e), k.data(h.defaultValue, e);
                    },
                    defaultText: function() {
                        var e = U.get.text();
                        U.verbose("Saving default text as", e), k.data(h.defaultText, e);
                    },
                    placeholderText: function() {
                        var e;
                        !1 !== f3.placeholder && S.hasClass(m.placeholder) && (e = U.get.text(), U.verbose("Saving placeholder text as", e), k.data(h.placeholderText, e));
                    },
                    remoteData: function(e, t) {
                        ne.Storage !== oe ? (U.verbose("Saving remote data to session storage", t, e), sessionStorage.setItem(t, e)) : U.error(y.noStorage);
                    }
                },
                clear: function(e) {
                    U.is.multiple() && f3.useLabels ? U.remove.labels(k.find(b.label), e) : (U.remove.activeItem(), U.remove.selectedItem(), U.remove.filteredItem()), U.set.placeholderText(), U.clearValue(e);
                },
                clearValue: function(e) {
                    U.set.value("", null, null, e);
                },
                scrollPage: function(e, t) {
                    var n = t || U.get.selectedItem(), i = n.closest(b.menu), o = i.outerHeight(), a = i.scrollTop(), t = M.eq(0).outerHeight(), o = Math.floor(o / t), a = (i.prop("scrollHeight"), "up" == e ? a - t * o : a + t * o), t = M.not(b.unselectable), o = "up" == e ? t.index(n) - o : t.index(n) + o, t = ("up" == e ? 0 <= o : o < t.length) ? t.eq(o) : "up" == e ? t.first() : t.last();
                    0 < t.length && (U.debug("Scrolling page", e, t), n.removeClass(m.selected), t.addClass(m.selected), f3.selectOnKeydown && U.is.single() && U.set.selectedItem(t), i.scrollTop(a));
                },
                set: {
                    filtered: function() {
                        var e = U.is.multiple(), t = U.is.searchSelection(), n = e && t, i = t ? U.get.query() : "", o = "string" == typeof i && 0 < i.length, t = U.get.searchWidth(), i = "" !== i;
                        e && o && (U.verbose("Adjusting input width", t, f3.glyphWidth), D.css("width", t)), o || n && i ? (U.verbose("Hiding placeholder text"), S.addClass(m.filtered)) : e && (!n || i) || (U.verbose("Showing placeholder text"), S.removeClass(m.filtered));
                    },
                    empty: function() {
                        k.addClass(m.empty);
                    },
                    loading: function() {
                        k.addClass(m.loading);
                    },
                    placeholderText: function(e) {
                        e = e || U.get.placeholderText(), U.debug("Setting placeholder text", e), U.set.text(e), S.addClass(m.placeholder);
                    },
                    tabbable: function() {
                        U.is.searchSelection() ? (U.debug("Added tabindex to searchable dropdown"), D.val(""), U.check.disabled(), R.attr("tabindex", -1)) : (U.debug("Added tabindex to dropdown"), k.attr("tabindex") === oe && (k.attr("tabindex", 0), R.attr("tabindex", -1)));
                    },
                    initialLoad: function() {
                        U.verbose("Setting initial load"), e29 = !0;
                    },
                    activeItem: function(e) {
                        f3.allowAdditions && 0 < e.filter(b.addition).length ? e.addClass(m.filtered) : e.addClass(m.active);
                    },
                    partialSearch: function(e) {
                        var t = U.get.query().length;
                        D.val(e.substr(0, t));
                    },
                    scrollPosition: function(e, t) {
                        var n, i, o = (e = e || U.get.selectedItem()).closest(b.menu), a = e && 0 < e.length;
                        t = t !== oe && t, 0 === U.get.activeItem().length && (t = !1), e && 0 < o.length && a && (e.position().top, o.addClass(m.loading), e = (a = o.scrollTop()) - o.offset().top + e.offset().top, t || (i = a + o.height() < e + 5, n = e - 5 < a), U.debug("Scrolling to active item", e), (t || n || i) && o.scrollTop(e), o.removeClass(m.loading));
                    },
                    text: function(e) {
                        "combo" === f3.action ? (U.debug("Changing combo button text", e, O), f3.preserveHTML ? O.html(e) : O.text(e)) : "activate" === f3.action && (e !== U.get.placeholderText() && S.removeClass(m.placeholder), U.debug("Changing text", e, S), S.removeClass(m.filtered), f3.preserveHTML ? S.html(e) : S.text(e));
                    },
                    selectedItem: function(e) {
                        var t = U.get.choiceValue(e), n = U.get.choiceText(e, !1), i = U.get.choiceText(e, !0);
                        U.debug("Setting user selection to item", e), U.remove.activeItem(), U.set.partialSearch(n), U.set.activeItem(e), U.set.selected(t, e), U.set.text(i);
                    },
                    selectedLetter: function(e) {
                        var t = M.filter("." + m.selected), n = 0 < t.length && U.has.firstLetter(t, e), i = !1;
                        n && (n = t.nextAll(M).eq(0), U.has.firstLetter(n, e) && (i = n)), i || M.each(function() {
                            if (U.has.firstLetter(te(this), e)) return i = te(this), !1;
                        }), i && (U.verbose("Scrolling to next value with letter", e), U.set.scrollPosition(i), t.removeClass(m.selected), i.addClass(m.selected), f3.selectOnKeydown && U.is.single() && U.set.selectedItem(i));
                    },
                    direction: function(e) {
                        "auto" == f3.direction ? (e ? U.is.upward(e) && U.remove.upward(e) : U.remove.upward(), (U.can.openDownward(e) ? U.remove : U.set).upward(e), U.is.leftward(e) || U.can.openRightward(e) || U.set.leftward(e)) : "upward" == f3.direction && U.set.upward(e);
                    },
                    upward: function(e) {
                        (e || k).addClass(m.upward);
                    },
                    leftward: function(e) {
                        (e || R).addClass(m.leftward);
                    },
                    value: function(e, t, n, i) {
                        e === oe || "" === e || Array.isArray(e) && 0 === e.length ? E.addClass(m.noselection) : E.removeClass(m.noselection);
                        var o = U.escape.value(e), a = 0 < E.length, r = U.get.values(), s = e !== oe ? String(e) : e;
                        if (a) {
                            if (!f3.allowReselection && s == r && (U.verbose("Skipping value update already same value", e, r), !U.is.initialLoad())) return;
                            U.is.single() && U.has.selectInput() && U.can.extendSelect() && (U.debug("Adding user option", e), U.add.optionValue(e)), U.debug("Updating input value", o, r), V = !0, E.val(o), !1 === f3.fireOnInit && U.is.initialLoad() ? U.debug("Input native change event ignored on initial load") : !0 !== i && U.trigger.change(), V = !1;
                        } else U.verbose("Storing value in metadata", o, E), o !== r && k.data(h.value, s);
                        !1 === f3.fireOnInit && U.is.initialLoad() ? U.verbose("No callback on initial load", f3.onChange) : !0 !== i && f3.onChange.call(z, e, t, n);
                    },
                    active: function() {
                        k.addClass(m.active);
                    },
                    multiple: function() {
                        k.addClass(m.multiple);
                    },
                    visible: function() {
                        k.addClass(m.visible);
                    },
                    exactly: function(e, t) {
                        U.debug("Setting selected to exact values"), U.clear(), U.set.selected(e, t);
                    },
                    selected: function(e38, s, l, c) {
                        var u = U.is.multiple();
                        (s = f3.allowAdditions ? s || U.get.itemWithAdditions(e38) : s || U.get.item(e38)) && (U.debug("Setting selected menu item to", s), U.is.multiple() && U.remove.searchWidth(), U.is.single() ? (U.remove.activeItem(), U.remove.selectedItem()) : f3.useLabels && U.remove.selectedItem(), s.each(function() {
                            var e = te(this), t = U.get.choiceText(e), n = U.get.choiceValue(e, t), i = e.hasClass(m.filtered), o = e.hasClass(m.active), a = e.hasClass(m.addition), r = u && 1 == s.length;
                            u ? !o || a ? (f3.apiSettings && f3.saveRemoteData && U.save.remoteData(t, n), f3.useLabels ? (U.add.label(n, t, r), U.add.value(n, t, e), U.set.activeItem(e), U.filterActive(), U.select.nextAvailable(s)) : (U.add.value(n, t, e), U.set.text(U.add.variables(g.count)), U.set.activeItem(e))) : i || !f3.useLabels && !d4 || (U.debug("Selected active value, removing label"), U.remove.selected(n)) : (f3.apiSettings && f3.saveRemoteData && U.save.remoteData(t, n), c || U.set.text(t), U.set.value(n, t, e, l), e.addClass(m.active).addClass(m.selected));
                        }), c || U.remove.searchTerm());
                    }
                },
                add: {
                    label: function(e, t, n) {
                        var i, o = U.is.searchSelection() ? D : S, a = U.escape.value(e);
                        f3.ignoreCase && (a = a.toLowerCase()), i = te("<a />").addClass(m.label).attr("data-" + h.value, a).html(x.label(a, t, f3.preserveHTML, f3.className)), i = f3.onLabelCreate.call(i, a, t), U.has.label(e) ? U.debug("User selection already exists, skipping", a) : (f3.label.variation && i.addClass(f3.label.variation), !0 === n ? (U.debug("Animating in label", i), i.addClass(m.hidden).insertBefore(o).transition({
                            animation: f3.label.transition,
                            debug: f3.debug,
                            verbose: f3.verbose,
                            duration: f3.label.duration
                        })) : (U.debug("Adding selection label", i), i.insertBefore(o)));
                    },
                    message: function(e) {
                        var t = R.children(b.message), e = f3.templates.message(U.add.variables(e));
                        0 < t.length ? t.html(e) : te("<div/>").html(e).addClass(m.message).appendTo(R);
                    },
                    optionValue: function(e) {
                        var t = U.escape.value(e);
                        0 < E.find('option[value="' + U.escape.string(t) + '"]').length || (U.disconnect.selectObserver(), U.is.single() && (U.verbose("Removing previous user addition"), E.find("option." + m.addition).remove()), te("<option/>").prop("value", t).addClass(m.addition).html(e).appendTo(E), U.verbose("Adding user addition as an <option>", e), U.observe.select());
                    },
                    userSuggestion: function(e) {
                        var t = R.children(b.addition), n = U.get.item(e), i = n && n.not(b.addition).length, n = 0 < t.length;
                        f3.useLabels && U.has.maxSelections() || ("" === e || i ? t.remove() : (n ? (t.data(h.value, e).data(h.text, e).attr("data-" + h.value, e).attr("data-" + h.text, e).removeClass(m.filtered), f3.hideAdditions || (n = f3.templates.addition(U.add.variables(g.addResult, e)), t.html(n)), U.verbose("Replacing user suggestion with new value", t)) : ((t = U.create.userChoice(e)).prependTo(R), U.verbose("Adding item choice to menu corresponding with user choice addition", t)), f3.hideAdditions && !U.is.allFiltered() || t.addClass(m.selected).siblings().removeClass(m.selected), U.refreshItems()));
                    },
                    variables: function(e, t) {
                        var n, i = -1 !== e.search("{count}"), o = -1 !== e.search("{maxCount}"), a = -1 !== e.search("{term}");
                        return U.verbose("Adding templated variables to message", e), i && (n = U.get.selectionCount(), e = e.replace("{count}", n)), o && (n = U.get.selectionCount(), e = e.replace("{maxCount}", f3.maxSelections)), a && (t = t || U.get.query(), e = e.replace("{term}", t)), e;
                    },
                    value: function(e, t, n) {
                        var i, o = U.get.values(!0);
                        U.has.value(e) ? U.debug("Value already selected") : "" !== e ? (i = Array.isArray(o) ? (i = o.concat([
                            e
                        ]), U.get.uniqueArray(i)) : [
                            e
                        ], U.has.selectInput() ? U.can.extendSelect() && (U.debug("Adding value to select", e, i, E), U.add.optionValue(e)) : (i = i.join(f3.delimiter), U.debug("Setting hidden input to delimited value", i, E)), !1 === f3.fireOnInit && U.is.initialLoad() ? U.verbose("Skipping onadd callback on initial load", f3.onAdd) : f3.onAdd.call(z, e, t, n), U.set.value(i, t, n), U.check.maxSelections()) : U.debug("Cannot select blank values from multiselect");
                    }
                },
                remove: {
                    active: function() {
                        k.removeClass(m.active);
                    },
                    activeLabel: function() {
                        k.find(b.label).removeClass(m.active);
                    },
                    empty: function() {
                        k.removeClass(m.empty);
                    },
                    loading: function() {
                        k.removeClass(m.loading);
                    },
                    initialLoad: function() {
                        e29 = !1;
                    },
                    upward: function(e) {
                        (e || k).removeClass(m.upward);
                    },
                    leftward: function(e) {
                        (e || R).removeClass(m.leftward);
                    },
                    visible: function() {
                        k.removeClass(m.visible);
                    },
                    activeItem: function() {
                        M.removeClass(m.active);
                    },
                    filteredItem: function() {
                        f3.useLabels && U.has.maxSelections() || ((f3.useLabels && U.is.multiple() ? M.not("." + m.active) : M).removeClass(m.filtered), f3.hideDividers && I.removeClass(m.hidden), U.remove.empty());
                    },
                    optionValue: function(e) {
                        var t = U.escape.value(e), e = E.find('option[value="' + U.escape.string(t) + '"]');
                        0 < e.length && e.hasClass(m.addition) && (r7 && (r7.disconnect(), U.verbose("Temporarily disconnecting mutation observer")), e.remove(), U.verbose("Removing user addition as an <option>", t), r7 && r7.observe(E[0], {
                            childList: !0,
                            subtree: !0
                        }));
                    },
                    message: function() {
                        R.children(b.message).remove();
                    },
                    searchWidth: function() {
                        D.css("width", "");
                    },
                    searchTerm: function() {
                        U.verbose("Cleared search term"), D.val(""), U.set.filtered();
                    },
                    userAddition: function() {
                        M.filter(b.addition).remove();
                    },
                    selected: function(e39, t22, i) {
                        if (!(t22 = f3.allowAdditions ? t22 || U.get.itemWithAdditions(e39) : t22 || U.get.item(e39))) return !1;
                        t22.each(function() {
                            var e = te(this), t = U.get.choiceText(e), n = U.get.choiceValue(e, t);
                            U.is.multiple() ? f3.useLabels ? (U.remove.value(n, t, e, i), U.remove.label(n)) : (U.remove.value(n, t, e, i), 0 === U.get.selectionCount() ? U.set.placeholderText() : U.set.text(U.add.variables(g.count))) : U.remove.value(n, t, e, i), e.removeClass(m.filtered).removeClass(m.active), f3.useLabels && e.removeClass(m.selected);
                        });
                    },
                    selectedItem: function() {
                        M.removeClass(m.selected);
                    },
                    value: function(e, t, n, i) {
                        var o, a = U.get.values();
                        e = U.escape.htmlEntities(e), U.has.selectInput() ? (U.verbose("Input is <select> removing selected option", e), o = U.remove.arrayValue(e, a), U.remove.optionValue(e)) : (U.verbose("Removing from delimited values", e), o = (o = U.remove.arrayValue(e, a)).join(f3.delimiter)), !1 === f3.fireOnInit && U.is.initialLoad() ? U.verbose("No callback on initial load", f3.onRemove) : f3.onRemove.call(z, e, t, n), U.set.value(o, t, n, i), U.check.maxSelections();
                    },
                    arrayValue: function(t, e40) {
                        return Array.isArray(e40) || (e40 = [
                            e40
                        ]), e40 = te.grep(e40, function(e) {
                            return t != e;
                        }), U.verbose("Removed value from delimited string", t, e40), e40;
                    },
                    label: function(e, t) {
                        e = U.escape.value(e), e = k.find(b.label).filter("[data-" + h.value + '="' + U.escape.string(f3.ignoreCase ? e.toLowerCase() : e) + '"]');
                        U.verbose("Removing label", e), e.remove();
                    },
                    activeLabels: function(e) {
                        e = e || k.find(b.label).filter("." + m.active), U.verbose("Removing active label selections", e), U.remove.labels(e);
                    },
                    labels: function(e41, o) {
                        e41 = e41 || k.find(b.label), U.verbose("Removing labels", e41), e41.each(function() {
                            var e = te(this), t = e.data(h.value), n = t !== oe ? String(t) : t, i = U.is.userValue(n);
                            !1 !== f3.onLabelRemove.call(e, t) ? (U.remove.message(), i ? (U.remove.value(n, n, U.get.item(n), o), U.remove.label(n)) : U.remove.selected(n, !1, o)) : U.debug("Label remove callback cancelled removal");
                        });
                    },
                    tabbable: function() {
                        U.is.searchSelection() ? (U.debug("Searchable dropdown initialized"), D.removeAttr("tabindex")) : (U.debug("Simple selection dropdown initialized"), k.removeAttr("tabindex")), R.removeAttr("tabindex");
                    },
                    diacritics: function(e) {
                        return f3.ignoreDiacritics ? e.normalize("NFD").replace(/[\u0300-\u036f]/g, "") : e;
                    }
                },
                has: {
                    menuSearch: function() {
                        return U.has.search() && 0 < D.closest(R).length;
                    },
                    clearItem: function() {
                        return 0 < P.length;
                    },
                    search: function() {
                        return 0 < D.length;
                    },
                    sizer: function() {
                        return 0 < A.length;
                    },
                    selectInput: function() {
                        return E.is("select");
                    },
                    minCharacters: function(e) {
                        return f3.minCharacters && !q ? (e = e !== oe ? String(e) : String(U.get.query())).length >= f3.minCharacters : (q = !1, true);
                    },
                    firstLetter: function(e, t) {
                        return !(!e || 0 === e.length || "string" != typeof t) && (e = U.get.choiceText(e, !1), (t = t.toLowerCase()) == String(e).charAt(0).toLowerCase());
                    },
                    input: function() {
                        return 0 < E.length;
                    },
                    items: function() {
                        return 0 < M.length;
                    },
                    menu: function() {
                        return 0 < R.length;
                    },
                    subMenu: function(e) {
                        return 0 < (e || R).find(b.menu).length;
                    },
                    message: function() {
                        return 0 !== R.children(b.message).length;
                    },
                    label: function(e) {
                        var t = U.escape.value(e), e = k.find(b.label);
                        return f3.ignoreCase && (t = t.toLowerCase()), 0 < e.filter("[data-" + h.value + '="' + U.escape.string(t) + '"]').length;
                    },
                    maxSelections: function() {
                        return f3.maxSelections && U.get.selectionCount() >= f3.maxSelections;
                    },
                    allResultsFiltered: function() {
                        var e = M.not(b.addition);
                        return e.filter(b.unselectable).length === e.length;
                    },
                    userSuggestion: function() {
                        return 0 < R.children(b.addition).length;
                    },
                    query: function() {
                        return "" !== U.get.query();
                    },
                    value: function(e) {
                        return f3.ignoreCase ? U.has.valueIgnoringCase(e) : U.has.valueMatchingCase(e);
                    },
                    valueMatchingCase: function(e) {
                        var t = U.get.values(!0);
                        return !!(Array.isArray(t) ? t && -1 !== te.inArray(e, t) : t == e);
                    },
                    valueIgnoringCase: function(n) {
                        var e = U.get.values(!0), i = !1;
                        return Array.isArray(e) || (e = [
                            e
                        ]), te.each(e, function(e, t) {
                            if (String(n).toLowerCase() == String(t).toLowerCase()) return i = !0, false;
                        }), i;
                    }
                },
                is: {
                    active: function() {
                        return k.hasClass(m.active);
                    },
                    animatingInward: function() {
                        return R.transition("is inward");
                    },
                    animatingOutward: function() {
                        return R.transition("is outward");
                    },
                    bubbledLabelClick: function(e) {
                        return te(e.target).is("select, input") && 0 < k.closest("label").length;
                    },
                    bubbledIconClick: function(e) {
                        return 0 < te(e.target).closest(F).length;
                    },
                    chrome: function() {
                        return !(!ne.chrome || !ne.chrome.webstore && !ne.chrome.runtime);
                    },
                    alreadySetup: function() {
                        return k.is("select") && k.parent(b.dropdown).data(w) !== oe && 0 === k.prev().length;
                    },
                    animating: function(e) {
                        return e ? e.transition && e.transition("is animating") : R.transition && R.transition("is animating");
                    },
                    leftward: function(e) {
                        return (e || R).hasClass(m.leftward);
                    },
                    clearable: function() {
                        return k.hasClass(m.clearable) || f3.clearable;
                    },
                    disabled: function() {
                        return k.hasClass(m.disabled);
                    },
                    focused: function() {
                        return ie.activeElement === k[0];
                    },
                    focusedOnSearch: function() {
                        return ie.activeElement === D[0];
                    },
                    allFiltered: function() {
                        return (U.is.multiple() || U.has.search()) && !(0 == f3.hideAdditions && U.has.userSuggestion()) && !U.has.message() && U.has.allResultsFiltered();
                    },
                    hidden: function(e) {
                        return !U.is.visible(e);
                    },
                    initialLoad: function() {
                        return e29;
                    },
                    inObject: function(n, e) {
                        var i = !1;
                        return te.each(e, function(e, t) {
                            if (t == n) return i = !0;
                        }), i;
                    },
                    multiple: function() {
                        return k.hasClass(m.multiple);
                    },
                    remote: function() {
                        return f3.apiSettings && U.can.useAPI();
                    },
                    noApiCache: function() {
                        return f3.apiSettings && !f3.apiSettings.cache;
                    },
                    single: function() {
                        return !U.is.multiple();
                    },
                    selectMutation: function(e) {
                        var n = !1;
                        return te.each(e, function(e, t) {
                            if (te(t.target).is("select") || te(t.addedNodes).is("select")) return n = !0, false;
                        }), n;
                    },
                    search: function() {
                        return k.hasClass(m.search);
                    },
                    searchSelection: function() {
                        return U.has.search() && 1 === D.parent(b.dropdown).length;
                    },
                    selection: function() {
                        return k.hasClass(m.selection);
                    },
                    userValue: function(e) {
                        return -1 !== te.inArray(e, U.get.userValues());
                    },
                    upward: function(e) {
                        return (e || k).hasClass(m.upward);
                    },
                    visible: function(e) {
                        return (e || R).hasClass(m.visible);
                    },
                    verticallyScrollableContext: function() {
                        var e = T.get(0) !== ne && T.css("overflow-y");
                        return "auto" == e || "scroll" == e;
                    },
                    horizontallyScrollableContext: function() {
                        var e = T.get(0) !== ne && T.css("overflow-X");
                        return "auto" == e || "scroll" == e;
                    }
                },
                can: {
                    activate: function(e) {
                        return !!f3.useLabels || !U.has.maxSelections() || !(!U.has.maxSelections() || !e.hasClass(m.active));
                    },
                    openDownward: function(e) {
                        var t = e || R, n = !0;
                        return t.addClass(m.loading), e = {
                            context: {
                                offset: T.get(0) === ne ? {
                                    top: 0,
                                    left: 0
                                } : T.offset(),
                                scrollTop: T.scrollTop(),
                                height: T.outerHeight()
                            },
                            menu: {
                                offset: t.offset(),
                                height: t.outerHeight()
                            }
                        }, U.is.verticallyScrollableContext() && (e.menu.offset.top += e.context.scrollTop), U.has.subMenu(t) && (e.menu.height += t.find(b.menu).first().outerHeight()), n = (e = {
                            above: e.context.scrollTop <= e.menu.offset.top - e.context.offset.top - e.menu.height,
                            below: e.context.scrollTop + e.context.height >= e.menu.offset.top - e.context.offset.top + e.menu.height
                        }).below ? (U.verbose("Dropdown can fit in context downward", e), !0) : e.above ? (U.verbose("Dropdown cannot fit below, opening upward", e), !1) : (U.verbose("Dropdown cannot fit in either direction, favoring downward", e), !0), t.removeClass(m.loading), n;
                    },
                    openRightward: function(e) {
                        var t = e || R, n = !0;
                        return t.addClass(m.loading), e = {
                            context: {
                                offset: T.get(0) === ne ? {
                                    top: 0,
                                    left: 0
                                } : T.offset(),
                                scrollLeft: T.scrollLeft(),
                                width: T.outerWidth()
                            },
                            menu: {
                                offset: t.offset(),
                                width: t.outerWidth()
                            }
                        }, U.is.horizontallyScrollableContext() && (e.menu.offset.left += e.context.scrollLeft), (e = e.menu.offset.left - e.context.offset.left + e.menu.width >= e.context.scrollLeft + e.context.width) && (U.verbose("Dropdown cannot fit in context rightward", e), n = !1), t.removeClass(m.loading), n;
                    },
                    click: function() {
                        return K || "click" == f3.on;
                    },
                    extendSelect: function() {
                        return f3.allowAdditions || f3.apiSettings;
                    },
                    show: function() {
                        return !U.is.disabled() && (U.has.items() || U.has.message());
                    },
                    useAPI: function() {
                        return te.fn.api !== oe;
                    }
                },
                animate: {
                    show: function(e, t) {
                        var n = t || R, i = t ? function() {
                        } : function() {
                            U.hideSubMenus(), U.hideOthers(), U.set.active();
                        };
                        e = te.isFunction(e) ? e : function() {
                        }, U.verbose("Doing menu show animation", n), U.set.direction(t), t = f3.transition.showMethod || U.get.transition(t), U.is.selection() && U.set.scrollPosition(U.get.selectedItem(), !0), (U.is.hidden(n) || U.is.animating(n)) && ("none" === t ? (i(), n.transition({
                            displayType: U.get.displayType()
                        }).transition("show"), e.call(z)) : te.fn.transition !== oe && k.transition("is supported") ? n.transition({
                            animation: t + " in",
                            debug: f3.debug,
                            verbose: f3.verbose,
                            duration: f3.transition.showDuration || f3.duration,
                            queue: !0,
                            onStart: i,
                            displayType: U.get.displayType(),
                            onComplete: function() {
                                e.call(z);
                            }
                        }) : U.error(y.noTransition, t));
                    },
                    hide: function(e, t) {
                        var n = t || R, i = t ? function() {
                        } : function() {
                            U.can.click() && U.unbind.intent(), U.remove.active();
                        }, t = f3.transition.hideMethod || U.get.transition(t);
                        e = te.isFunction(e) ? e : function() {
                        }, (U.is.visible(n) || U.is.animating(n)) && (U.verbose("Doing menu hide animation", n), "none" === t ? (i(), n.transition({
                            displayType: U.get.displayType()
                        }).transition("hide"), e.call(z)) : te.fn.transition !== oe && k.transition("is supported") ? n.transition({
                            animation: t + " out",
                            duration: f3.transition.hideDuration || f3.duration,
                            debug: f3.debug,
                            verbose: f3.verbose,
                            queue: !1,
                            onStart: i,
                            displayType: U.get.displayType(),
                            onComplete: function() {
                                e.call(z);
                            }
                        }) : U.error(y.transition));
                    }
                },
                hideAndClear: function() {
                    U.remove.searchTerm(), U.has.maxSelections() || (U.has.search() ? U.hide(function() {
                        U.remove.filteredItem();
                    }) : U.hide());
                },
                delay: {
                    show: function() {
                        U.verbose("Delaying show event to ensure user intent"), clearTimeout(U.timer), U.timer = setTimeout(U.show, f3.delay.show);
                    },
                    hide: function() {
                        U.verbose("Delaying hide event to ensure user intent"), clearTimeout(U.timer), U.timer = setTimeout(U.hide, f3.delay.hide);
                    }
                },
                escape: {
                    value: function(e) {
                        var t23 = Array.isArray(e), n = "string" == typeof e, i = !n && !t23, n = n && -1 !== e.search(v.quote), o = [];
                        return i || !n ? e : (U.debug("Encoding quote values for use in select", e), t23 ? (te.each(e, function(e, t) {
                            o.push(t.replace(v.quote, "&quot;"));
                        }), o) : e.replace(v.quote, "&quot;"));
                    },
                    string: function(e) {
                        return (e = String(e)).replace(v.escape, "\\$&");
                    },
                    htmlEntities: function(e) {
                        var t = {
                            "<": "&lt;",
                            ">": "&gt;",
                            '"': "&quot;",
                            "'": "&#x27;",
                            "`": "&#x60;"
                        };
                        return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                            return t[e];
                        }) : e;
                    }
                },
                setting: function(e, t) {
                    if (U.debug("Changing setting", e, t), te.isPlainObject(e)) te.extend(!0, f3, e);
                    else {
                        if (t === oe) return f3[e];
                        te.isPlainObject(f3[e]) ? te.extend(!0, f3[e], t) : f3[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (te.isPlainObject(e)) te.extend(!0, U, e);
                    else {
                        if (t === oe) return U[e];
                        U[e] = t;
                    }
                },
                debug: function() {
                    !f3.silent && f3.debug && (f3.performance ? U.performance.log(arguments) : (U.debug = Function.prototype.bind.call(console.info, console, f3.name + ":"), U.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !f3.silent && f3.verbose && f3.debug && (f3.performance ? U.performance.log(arguments) : (U.verbose = Function.prototype.bind.call(console.info, console, f3.name + ":"), U.verbose.apply(console, arguments)));
                },
                error: function() {
                    f3.silent || (U.error = Function.prototype.bind.call(console.error, console, f3.name + ":"), U.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        f3.performance && (n = (t = (new Date).getTime()) - (G || t), G = t, J.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: z,
                            "Execution Time": n
                        })), clearTimeout(U.performance.timer), U.performance.timer = setTimeout(U.performance.display, 500);
                    },
                    display: function() {
                        var e = f3.name + ":", n = 0;
                        G = !1, clearTimeout(U.performance.timer), te.each(J, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", X && (e += " '" + X + "'"), (console.group !== oe || console.table !== oe) && 0 < J.length && (console.groupCollapsed(e), console.table ? console.table(J) : te.each(J, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), J = [];
                    }
                },
                invoke: function(i, e42, t24) {
                    var o, a, n, r = H;
                    return e42 = e42 || ee, t24 = z || t24, "string" == typeof i && r !== oe && (i = i.split(/[\. ]/), o = i.length - 1, te.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (te.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== oe) return a = r[n], !1;
                            if (!te.isPlainObject(r[t]) || e == o) return r[t] !== oe ? a = r[t] : U.error(y.method, i), !1;
                            r = r[t];
                        }
                    })), te.isFunction(a) ? n = a.apply(t24, e42) : a !== oe && (n = a), Array.isArray(W) ? W.push(n) : W !== oe ? W = [
                        W,
                        n
                    ] : n !== oe && (W = n), a;
                }
            };
            _ ? (H === oe && U.initialize(), U.invoke(Z)) : (H !== oe && H.invoke("destroy"), U.initialize());
        }), W !== oe ? W : Y;
    }, te.fn.dropdown.settings = {
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        on: "click",
        action: "activate",
        values: !1,
        clearable: !1,
        apiSettings: !1,
        selectOnKeydown: !0,
        minCharacters: 0,
        filterRemoteData: !1,
        saveRemoteData: !0,
        throttle: 200,
        context: ne,
        direction: "auto",
        keepOnScreen: !0,
        match: "both",
        fullTextSearch: !1,
        ignoreDiacritics: !1,
        hideDividers: !1,
        placeholder: "auto",
        preserveHTML: !0,
        sortSelect: !1,
        forceSelection: !0,
        allowAdditions: !1,
        ignoreCase: !1,
        ignoreSearchCase: !0,
        hideAdditions: !0,
        maxSelections: !1,
        useLabels: !0,
        delimiter: ",",
        showOnFocus: !0,
        allowReselection: !1,
        allowTab: !0,
        allowCategorySelection: !1,
        fireOnInit: !1,
        transition: "auto",
        duration: 200,
        displayType: !1,
        glyphWidth: 1.037,
        headerDivider: !0,
        label: {
            transition: "scale",
            duration: 200,
            variation: !1
        },
        delay: {
            hide: 300,
            show: 200,
            search: 20,
            touch: 50
        },
        onChange: function(e, t, n) {
        },
        onAdd: function(e, t, n) {
        },
        onRemove: function(e, t, n) {
        },
        onSearch: function(e) {
        },
        onLabelSelect: function(e) {
        },
        onLabelCreate: function(e, t) {
            return te(this);
        },
        onLabelRemove: function(e) {
            return !0;
        },
        onNoResults: function(e) {
            return !0;
        },
        onShow: function() {
        },
        onHide: function() {
        },
        name: "Dropdown",
        namespace: "dropdown",
        message: {
            addResult: "Add <b>{term}</b>",
            count: "{count} selected",
            maxSelections: "Max {maxCount} selections",
            noResults: "No results found.",
            serverError: "There was an error contacting the server"
        },
        error: {
            action: "You called a dropdown action that was not defined",
            alreadySetup: "Once a select has been initialized behaviors must be called on the created ui dropdown",
            labels: "Allowing user additions currently requires the use of labels.",
            missingMultiple: "<select> requires multiple property to be set to correctly preserve multiple values",
            method: "The method you called is not defined.",
            noAPI: "The API module is required to load resources remotely",
            noStorage: "Saving remote data requires session storage",
            noTransition: "This module requires ui transitions <https://github.com/Semantic-Org/UI-Transition>",
            noNormalize: '"ignoreDiacritics" setting will be ignored. Browser does not support String().normalize(). You may consider including <https://cdn.jsdelivr.net/npm/unorm@1.4.1/lib/unorm.min.js> as a polyfill.'
        },
        regExp: {
            escape: /[-[\]{}()*+?.,\\^$|#\s:=@]/g,
            quote: /"/g
        },
        metadata: {
            defaultText: "defaultText",
            defaultValue: "defaultValue",
            placeholderText: "placeholder",
            text: "text",
            value: "value"
        },
        fields: {
            remoteValues: "results",
            values: "values",
            disabled: "disabled",
            name: "name",
            description: "description",
            descriptionVertical: "descriptionVertical",
            value: "value",
            text: "text",
            type: "type",
            image: "image",
            imageClass: "imageClass",
            icon: "icon",
            iconClass: "iconClass",
            class: "class",
            divider: "divider"
        },
        keys: {
            backspace: 8,
            delimiter: 188,
            deleteKey: 46,
            enter: 13,
            escape: 27,
            pageUp: 33,
            pageDown: 34,
            leftArrow: 37,
            upArrow: 38,
            rightArrow: 39,
            downArrow: 40
        },
        selector: {
            addition: ".addition",
            divider: ".divider, .header",
            dropdown: ".ui.dropdown",
            hidden: ".hidden",
            icon: "> .dropdown.icon",
            input: '> input[type="hidden"], > select',
            item: ".item",
            label: "> .label",
            remove: "> .label > .delete.icon",
            siblingLabel: ".label",
            menu: ".menu",
            message: ".message",
            menuIcon: ".dropdown.icon",
            search: "input.search, .menu > .search > input, .menu input.search",
            sizer: "> span.sizer",
            text: "> .text:not(.icon)",
            unselectable: ".disabled, .filtered",
            clearIcon: "> .remove.icon"
        },
        className: {
            active: "active",
            addition: "addition",
            animating: "animating",
            description: "description",
            descriptionVertical: "vertical",
            disabled: "disabled",
            empty: "empty",
            dropdown: "ui dropdown",
            filtered: "filtered",
            hidden: "hidden transition",
            icon: "icon",
            image: "image",
            item: "item",
            label: "ui label",
            loading: "loading",
            menu: "menu",
            message: "message",
            multiple: "multiple",
            placeholder: "default",
            sizer: "sizer",
            search: "search",
            selected: "selected",
            selection: "selection",
            text: "text",
            upward: "upward",
            leftward: "left",
            visible: "visible",
            clearable: "clearable",
            noselection: "noselection",
            delete: "delete",
            header: "header",
            divider: "divider",
            groupIcon: "",
            unfilterable: "unfilterable"
        }
    }, te.fn.dropdown.settings.templates = {
        deQuote: function(e, t) {
            return String(e).replace(/"/g, t ? "&quot;" : "");
        },
        escape: function(e, t) {
            if (t) return e;
            var n = {
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#x27;",
                "`": "&#x60;"
            };
            return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                return n[e];
            }) : e;
        },
        dropdown: function(e, t, n, i) {
            var o = e.placeholder || !1, a = "", r = te.fn.dropdown.settings.templates.escape;
            return a += '<i class="dropdown icon"></i>', a += o ? '<div class="default text">' + r(o, n) + "</div>" : '<div class="text"></div>', a += '<div class="' + i.menu + '">', a += te.fn.dropdown.settings.templates.menu(e, t, n, i), a += "</div>";
        },
        menu: function(e, l, c, u) {
            var e = e[l.values] || [], d = "", f = te.fn.dropdown.settings.templates.escape, m = te.fn.dropdown.settings.templates.deQuote;
            return te.each(e, function(e, t) {
                var n, i, o, a, r = t[l.type] || "item", s = -1 !== r.indexOf("menu");
                "item" === r || s ? (n = t[l.text] ? ' data-text="' + m(t[l.text], !0) + '"' : "", i = t[l.disabled] ? u.disabled + " " : "", o = t[l.descriptionVertical] ? u.descriptionVertical + " " : "", a = "" != f(t[l.description] || "", c), d += '<div class="' + i + o + (t[l.class] ? m(t[l.class]) : u.item) + '" data-value="' + m(t[l.value], !0) + '"' + n + ">", s && (d += '<i class="' + (-1 !== r.indexOf("left") ? "left" : "") + ' dropdown icon"></i>'), t[l.image] && (d += '<img class="' + (t[l.imageClass] ? m(t[l.imageClass]) : u.image) + '" src="' + m(t[l.image]) + '">'), t[l.icon] && (d += '<i class="' + m(t[l.icon]) + " " + (t[l.iconClass] ? m(t[l.iconClass]) : u.icon) + '"></i>'), a && (d += '<span class="' + u.description + '">' + f(t[l.description] || "", c) + "</span>", d += s ? "" : '<span class="' + u.text + '">'), s && (d += '<span class="' + u.text + '">'), d += f(t[l.name] || "", c), s ? (d += "</span>", d += '<div class="' + r + '">', d += te.fn.dropdown.settings.templates.menu(t, l, c, u), d += "</div>") : a && (d += "</span>"), d += "</div>") : "header" === r && (a = f(t[l.name] || "", c), r = t[l.icon] ? m(t[l.icon]) : u.groupIcon, "" === a && "" === r || (d += '<div class="' + (t[l.class] ? m(t[l.class]) : u.header) + '">', "" !== r && (d += '<i class="' + r + " " + (t[l.iconClass] ? m(t[l.iconClass]) : u.icon) + '"></i>'), d += a, d += "</div>"), t[l.divider] && (d += '<div class="' + u.divider + '"></div>'));
            }), d;
        },
        label: function(e, t, n, i) {
            return (0, te.fn.dropdown.settings.templates.escape)(t, n) + '<i class="' + i.delete + ' icon"></i>';
        },
        message: function(e) {
            return e;
        },
        addition: function(e) {
            return e;
        }
    };
})(jQuery, window, document), (function(T, e43, S) {
    "use strict";
    T.isFunction = T.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e43 = void 0 !== e43 && e43.Math == Math ? e43 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), T.fn.embed = function(p) {
        var h, v = T(this), b = v.selector || "", y = (new Date).getTime(), x = [], C = p, w = "string" == typeof C, k = [].slice.call(arguments, 1);
        return v.each(function() {
            var i8 = T.isPlainObject(p) ? T.extend(!0, {
            }, T.fn.embed.settings, p) : T.extend({
            }, T.fn.embed.settings), e44 = i8.selector, t25 = i8.className, o8 = i8.sources, s = i8.error, a8 = i8.metadata, n9 = i8.namespace, r8 = i8.templates, l = "." + n9, c = "module-" + n9, u = T(this), d = (u.find(e44.placeholder), u.find(e44.icon), u.find(e44.embed)), f = this, m = u.data(c), g = {
                initialize: function() {
                    g.debug("Initializing embed"), g.determine.autoplay(), g.create(), g.bind.events(), g.instantiate();
                },
                instantiate: function() {
                    g.verbose("Storing instance of module", g), m = g, u.data(c, g);
                },
                destroy: function() {
                    g.verbose("Destroying previous instance of embed"), g.reset(), u.removeData(c).off(l);
                },
                refresh: function() {
                    g.verbose("Refreshing selector cache"), u.find(e44.placeholder), u.find(e44.icon), d = u.find(e44.embed);
                },
                bind: {
                    events: function() {
                        g.has.placeholder() && (g.debug("Adding placeholder events"), u.on("click" + l, e44.placeholder, g.createAndShow).on("click" + l, e44.icon, g.createAndShow));
                    }
                },
                create: function() {
                    g.get.placeholder() ? g.createPlaceholder() : g.createAndShow();
                },
                createPlaceholder: function(e) {
                    var t = g.get.icon(), n = g.get.url();
                    g.generate.embed(n);
                    e = e || g.get.placeholder(), u.html(r8.placeholder(e, t)), g.debug("Creating placeholder for embed", e, t);
                },
                createEmbed: function(e) {
                    g.refresh(), e = e || g.get.url(), d = T("<div/>").addClass(t25.embed).html(g.generate.embed(e)).appendTo(u), i8.onCreate.call(f, e), g.debug("Creating embed object", d);
                },
                changeEmbed: function(e) {
                    d.html(g.generate.embed(e));
                },
                createAndShow: function() {
                    g.createEmbed(), g.show();
                },
                change: function(e, t, n) {
                    g.debug("Changing video to ", e, t, n), u.data(a8.source, e).data(a8.id, t), n ? u.data(a8.url, n) : u.removeData(a8.url), g.has.embed() ? g.changeEmbed() : g.create();
                },
                reset: function() {
                    g.debug("Clearing embed and showing placeholder"), g.remove.data(), g.remove.active(), g.remove.embed(), g.showPlaceholder(), i8.onReset.call(f);
                },
                show: function() {
                    g.debug("Showing embed"), g.set.active(), i8.onDisplay.call(f);
                },
                hide: function() {
                    g.debug("Hiding embed"), g.showPlaceholder();
                },
                showPlaceholder: function() {
                    g.debug("Showing placeholder image"), g.remove.active(), i8.onPlaceholderDisplay.call(f);
                },
                get: {
                    id: function() {
                        return i8.id || u.data(a8.id);
                    },
                    placeholder: function() {
                        return i8.placeholder || u.data(a8.placeholder);
                    },
                    icon: function() {
                        return i8.icon || (u.data(a8.icon) !== S ? u.data(a8.icon) : g.determine.icon());
                    },
                    source: function(e) {
                        return i8.source || (u.data(a8.source) !== S ? u.data(a8.source) : g.determine.source());
                    },
                    type: function() {
                        var e = g.get.source();
                        return o8[e] !== S && o8[e].type;
                    },
                    url: function() {
                        return i8.url || (u.data(a8.url) !== S ? u.data(a8.url) : g.determine.url());
                    }
                },
                determine: {
                    autoplay: function() {
                        g.should.autoplay() && (i8.autoplay = !0);
                    },
                    source: function(n) {
                        var i = !1;
                        return (n = n || g.get.url()) && T.each(o8, function(e, t) {
                            if (-1 !== n.search(t.domain)) return i = e, !1;
                        }), i;
                    },
                    icon: function() {
                        var e = g.get.source();
                        return o8[e] !== S && o8[e].icon;
                    },
                    url: function() {
                        var e = i8.id || u.data(a8.id), t = i8.source || u.data(a8.source), e = o8[t] !== S && o8[t].url.replace("{id}", e);
                        return e && u.data(a8.url, e), e;
                    }
                },
                set: {
                    active: function() {
                        u.addClass(t25.active);
                    }
                },
                remove: {
                    data: function() {
                        u.removeData(a8.id).removeData(a8.icon).removeData(a8.placeholder).removeData(a8.source).removeData(a8.url);
                    },
                    active: function() {
                        u.removeClass(t25.active);
                    },
                    embed: function() {
                        d.empty();
                    }
                },
                encode: {
                    parameters: function(e) {
                        var t, n = [];
                        for(t in e)n.push(encodeURIComponent(t) + "=" + encodeURIComponent(e[t]));
                        return n.join("&amp;");
                    }
                },
                generate: {
                    embed: function(e) {
                        g.debug("Generating embed html");
                        var t, n = g.get.source();
                        return (e = g.get.url(e)) ? (t = g.generate.parameters(n), t = r8.iframe(e, t)) : g.error(s.noURL, u), t;
                    },
                    parameters: function(e, t) {
                        e = o8[e] && o8[e].parameters !== S ? o8[e].parameters(i8) : {
                        };
                        return (t = t || i8.parameters) && (e = T.extend({
                        }, e, t)), e = i8.onEmbed(e), g.encode.parameters(e);
                    }
                },
                has: {
                    embed: function() {
                        return 0 < d.length;
                    },
                    placeholder: function() {
                        return i8.placeholder || u.data(a8.placeholder);
                    }
                },
                should: {
                    autoplay: function() {
                        return "auto" === i8.autoplay ? i8.placeholder || u.data(a8.placeholder) !== S : i8.autoplay;
                    }
                },
                is: {
                    video: function() {
                        return "video" == g.get.type();
                    }
                },
                setting: function(e, t) {
                    if (g.debug("Changing setting", e, t), T.isPlainObject(e)) T.extend(!0, i8, e);
                    else {
                        if (t === S) return i8[e];
                        T.isPlainObject(i8[e]) ? T.extend(!0, i8[e], t) : i8[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (T.isPlainObject(e)) T.extend(!0, g, e);
                    else {
                        if (t === S) return g[e];
                        g[e] = t;
                    }
                },
                debug: function() {
                    !i8.silent && i8.debug && (i8.performance ? g.performance.log(arguments) : (g.debug = Function.prototype.bind.call(console.info, console, i8.name + ":"), g.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !i8.silent && i8.verbose && i8.debug && (i8.performance ? g.performance.log(arguments) : (g.verbose = Function.prototype.bind.call(console.info, console, i8.name + ":"), g.verbose.apply(console, arguments)));
                },
                error: function() {
                    i8.silent || (g.error = Function.prototype.bind.call(console.error, console, i8.name + ":"), g.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        i8.performance && (n = (t = (new Date).getTime()) - (y || t), y = t, x.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: f,
                            "Execution Time": n
                        })), clearTimeout(g.performance.timer), g.performance.timer = setTimeout(g.performance.display, 500);
                    },
                    display: function() {
                        var e = i8.name + ":", n = 0;
                        y = !1, clearTimeout(g.performance.timer), T.each(x, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", b && (e += " '" + b + "'"), 1 < v.length && (e += " (" + v.length + ")"), (console.group !== S || console.table !== S) && 0 < x.length && (console.groupCollapsed(e), console.table ? console.table(x) : T.each(x, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), x = [];
                    }
                },
                invoke: function(i, e45, t26) {
                    var o, a, n, r = m;
                    return e45 = e45 || k, t26 = f || t26, "string" == typeof i && r !== S && (i = i.split(/[\. ]/), o = i.length - 1, T.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (T.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== S) return a = r[n], !1;
                            if (!T.isPlainObject(r[t]) || e == o) return r[t] !== S ? a = r[t] : g.error(s.method, i), !1;
                            r = r[t];
                        }
                    })), T.isFunction(a) ? n = a.apply(t26, e45) : a !== S && (n = a), Array.isArray(h) ? h.push(n) : h !== S ? h = [
                        h,
                        n
                    ] : n !== S && (h = n), a;
                }
            };
            w ? (m === S && g.initialize(), g.invoke(C)) : (m !== S && m.invoke("destroy"), g.initialize());
        }), h !== S ? h : this;
    }, T.fn.embed.settings = {
        name: "Embed",
        namespace: "embed",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        icon: !1,
        source: !1,
        url: !1,
        id: !1,
        autoplay: "auto",
        color: "#444444",
        hd: !0,
        brandedUI: !1,
        parameters: !1,
        onDisplay: function() {
        },
        onPlaceholderDisplay: function() {
        },
        onReset: function() {
        },
        onCreate: function(e) {
        },
        onEmbed: function(e) {
            return e;
        },
        metadata: {
            id: "id",
            icon: "icon",
            placeholder: "placeholder",
            source: "source",
            url: "url"
        },
        error: {
            noURL: "No URL specified",
            method: "The method you called is not defined"
        },
        className: {
            active: "active",
            embed: "embed"
        },
        selector: {
            embed: ".embed",
            placeholder: ".placeholder",
            icon: ".icon"
        },
        sources: {
            youtube: {
                name: "youtube",
                type: "video",
                icon: "video play",
                domain: "youtube.com",
                url: "//www.youtube.com/embed/{id}",
                parameters: function(e) {
                    return {
                        autohide: !e.brandedUI,
                        autoplay: e.autoplay,
                        color: e.color || S,
                        hq: e.hd,
                        jsapi: e.api,
                        modestbranding: !e.brandedUI
                    };
                }
            },
            vimeo: {
                name: "vimeo",
                type: "video",
                icon: "video play",
                domain: "vimeo.com",
                url: "//player.vimeo.com/video/{id}",
                parameters: function(e) {
                    return {
                        api: e.api,
                        autoplay: e.autoplay,
                        byline: e.brandedUI,
                        color: e.color || S,
                        portrait: e.brandedUI,
                        title: e.brandedUI
                    };
                }
            }
        },
        templates: {
            iframe: function(e, t) {
                return t && (e += "?" + t), '<iframe src="' + e + '" width="100%" height="100%" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            },
            placeholder: function(e, t) {
                var n = "";
                return t && (n += '<i class="' + t + ' icon"></i>'), e && (n += '<img class="placeholder" src="' + e + '">'), n;
            }
        },
        api: !1,
        onPause: function() {
        },
        onPlay: function() {
        },
        onStop: function() {
        }
    };
})(jQuery, window, void 0), (function(z, N, H, U) {
    "use strict";
    z.isFunction = z.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, N = void 0 !== N && N.Math == Math ? N : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), z.fn.modal = function(A) {
        var E, e46 = z(this), F = z(N), P = z(H), O = z("body"), R = e46.selector || "", M = (new Date).getTime(), I = [], j = A, L = "string" == typeof j, V = [].slice.call(arguments, 1), q = N.requestAnimationFrame || N.mozRequestAnimationFrame || N.webkitRequestAnimationFrame || N.msRequestAnimationFrame || function(e) {
            setTimeout(e, 0);
        };
        return e46.each(function() {
            var o9, a9, e47, i9, t27, r9, s, n10, l, c, u = z.isPlainObject(A) ? z.extend(!0, {
            }, z.fn.modal.settings, A) : z.extend({
            }, z.fn.modal.settings), d = u.selector, f = u.className, m = u.namespace, g = u.fields, p = u.error, h = "." + m, v = "module-" + m, b = z(this), y = z(u.context), x = b.find(d.close), C = this, w = b.hasClass("modal") ? b.data(v) : U, k = !1, T = "", S = "", D = {
                initialize: function() {
                    var a;
                    b.hasClass("modal") || (D.create.modal(), z.isFunction(u.onHidden) || (u.onHidden = function() {
                        D.destroy(), b.remove();
                    })), b.addClass(u.class), "" !== u.title && b.find(d.title).html(D.helpers.escape(u.title, u.preserveHTML)).addClass(u.classTitle), "" !== u.content && b.find(d.content).html(D.helpers.escape(u.content, u.preserveHTML)).addClass(u.classContent), D.has.configActions() && (0 === (a = b.find(d.actions).addClass(u.classActions)).length ? a = z("<div/>", {
                        class: f.actions + " " + (u.classActions || "")
                    }).appendTo(b) : a.empty(), u.actions.forEach(function(e) {
                        var t = e[g.icon] ? '<i class="' + D.helpers.deQuote(e[g.icon]) + ' icon"></i>' : "", n = D.helpers.escape(e[g.text] || "", u.preserveHTML), i = D.helpers.deQuote(e[g.class] || ""), o = e[g.click] && z.isFunction(e[g.click]) ? e[g.click] : function() {
                        };
                        a.append(z("<button/>", {
                            html: t + n,
                            class: f.button + " " + i,
                            click: function() {
                                !1 !== o.call(C, b) && D.hide();
                            }
                        }));
                    })), D.cache = {
                    }, D.verbose("Initializing dimmer", y), D.create.id(), D.create.dimmer(), u.allowMultiple && D.create.innerDimmer(), u.centered || b.addClass("top aligned"), D.refreshModals(), D.bind.events(), u.observeChanges && D.observeChanges(), D.instantiate(), u.autoShow && D.show();
                },
                instantiate: function() {
                    D.verbose("Storing instance of modal"), w = D, b.data(v, w);
                },
                create: {
                    modal: function() {
                        b = z("<div/>", {
                            class: f.modal
                        }), u.closeIcon && (x = z("<i/>", {
                            class: f.close
                        }), b.append(x)), "" !== u.title && z("<div/>", {
                            class: f.title
                        }).appendTo(b), "" !== u.content && z("<div/>", {
                            class: f.content
                        }).appendTo(b), D.has.configActions() && z("<div/>", {
                            class: f.actions
                        }).appendTo(b), y.append(b);
                    },
                    dimmer: function() {
                        var e = {
                            debug: u.debug,
                            dimmerName: "modals"
                        }, e = z.extend(!0, e, u.dimmerSettings);
                        z.fn.dimmer !== U ? (D.debug("Creating dimmer"), i9 = y.dimmer(e), u.detachable ? (D.verbose("Modal is detachable, moving content into dimmer"), i9.dimmer("add content", b)) : D.set.undetached(), t27 = i9.dimmer("get dimmer")) : D.error(p.dimmer);
                    },
                    id: function() {
                        l = (Math.random().toString(16) + "000000000").substr(2, 8), n10 = "." + l, D.verbose("Creating unique id for element", l);
                    },
                    innerDimmer: function() {
                        0 == b.find(d.dimmer).length && b.prepend('<div class="ui inverted dimmer"></div>');
                    }
                },
                destroy: function() {
                    c && c.disconnect(), D.verbose("Destroying previous modal"), b.removeData(v).off(h), F.off(n10), t27.off(n10), x.off(h), y.dimmer("destroy");
                },
                observeChanges: function() {
                    "MutationObserver" in N && ((c = new MutationObserver(function(e) {
                        D.debug("DOM tree modified, refreshing"), D.refresh();
                    })).observe(C, {
                        childList: !0,
                        subtree: !0
                    }), D.debug("Setting up mutation observer", c));
                },
                refresh: function() {
                    D.remove.scrolling(), D.cacheSizes(), D.can.useFlex() || D.set.modalOffset(), D.set.screenHeight(), D.set.type();
                },
                refreshModals: function() {
                    a9 = b.siblings(d.modal), o9 = a9.add(b);
                },
                attachEvents: function(e, t) {
                    var n = z(e);
                    t = z.isFunction(D[t]) ? D[t] : D.toggle, 0 < n.length ? (D.debug("Attaching modal events to element", e, t), n.off(h).on("click" + h, t)) : D.error(p.notFound, e);
                },
                bind: {
                    events: function() {
                        D.verbose("Attaching events"), b.on("click" + h, d.close, D.event.close).on("click" + h, d.approve, D.event.approve).on("click" + h, d.deny, D.event.deny), F.on("resize" + n10, D.event.resize);
                    },
                    scrollLock: function() {
                        i9.get(0).addEventListener("touchmove", D.event.preventScroll, {
                            passive: !1
                        });
                    }
                },
                unbind: {
                    scrollLock: function() {
                        i9.get(0).removeEventListener("touchmove", D.event.preventScroll, {
                            passive: !1
                        });
                    }
                },
                get: {
                    id: function() {
                        return (Math.random().toString(16) + "000000000").substr(2, 8);
                    },
                    element: function() {
                        return b;
                    },
                    settings: function() {
                        return u;
                    }
                },
                event: {
                    approve: function() {
                        k || !1 === u.onApprove.call(C, z(this)) ? D.verbose("Approve callback returned false cancelling hide") : (k = !0, D.hide(function() {
                            k = !1;
                        }));
                    },
                    preventScroll: function(e) {
                        -1 !== e.target.className.indexOf("dimmer") && e.preventDefault();
                    },
                    deny: function() {
                        k || !1 === u.onDeny.call(C, z(this)) ? D.verbose("Deny callback returned false cancelling hide") : (k = !0, D.hide(function() {
                            k = !1;
                        }));
                    },
                    close: function() {
                        D.hide();
                    },
                    mousedown: function(e) {
                        var t = z(e.target), n = D.is.rtl();
                        (r9 = 0 < t.closest(d.modal).length) && D.verbose("Mouse down event registered inside the modal"), (s = D.is.scrolling() && (!n && z(N).outerWidth() - u.scrollbarWidth <= e.clientX || n && u.scrollbarWidth >= e.clientX)) && D.verbose("Mouse down event registered inside the scrollbar");
                    },
                    mouseup: function(e) {
                        if (u.closable) {
                            if (r9) D.debug("Dimmer clicked but mouse down was initially registered inside the modal");
                            else if (s) D.debug("Dimmer clicked but mouse down was initially registered inside the scrollbar");
                            else {
                                var t = 0 < z(e.target).closest(d.modal).length, e = z.contains(H.documentElement, e.target);
                                if (!t && e && D.is.active() && b.hasClass(f.front)) {
                                    if (D.debug("Dimmer clicked, hiding all modals"), u.allowMultiple) {
                                        if (!D.hideAll()) return;
                                    } else if (!D.hide()) return;
                                    D.remove.clickaway();
                                }
                            }
                        } else D.verbose("Dimmer clicked but closable setting is disabled");
                    },
                    debounce: function(e, t) {
                        clearTimeout(D.timer), D.timer = setTimeout(e, t);
                    },
                    keyboard: function(e) {
                        27 == e.which && (u.closable ? (D.debug("Escape key pressed hiding modal"), b.hasClass(f.front) && D.hide()) : D.debug("Escape key pressed, but closable is set to false"), e.preventDefault());
                    },
                    resize: function() {
                        i9.dimmer("is active") && (D.is.animating() || D.is.active()) && q(D.refresh);
                    }
                },
                toggle: function() {
                    D.is.active() || D.is.animating() ? D.hide() : D.show();
                },
                show: function(e) {
                    e = z.isFunction(e) ? e : function() {
                    }, D.refreshModals(), D.set.dimmerSettings(), D.set.dimmerStyles(), D.showModal(e);
                },
                hide: function(e) {
                    return e = z.isFunction(e) ? e : function() {
                    }, D.refreshModals(), D.hideModal(e);
                },
                showModal: function(e) {
                    e = z.isFunction(e) ? e : function() {
                    }, D.is.animating() || !D.is.active() ? (D.showDimmer(), D.cacheSizes(), D.set.bodyMargin(), D.can.useFlex() ? D.remove.legacy() : (D.set.legacy(), D.set.modalOffset(), D.debug("Using non-flex legacy modal positioning.")), D.set.screenHeight(), D.set.type(), D.set.clickaway(), !u.allowMultiple && D.others.active() ? D.hideOthers(D.showModal) : (k = !1, u.allowMultiple && (D.others.active() && a9.filter("." + f.active).find(d.dimmer).addClass("active"), u.detachable && b.detach().appendTo(t27)), u.onShow.call(C), u.transition && z.fn.transition !== U && b.transition("is supported") ? (D.debug("Showing modal with css animations"), b.transition({
                        debug: u.debug,
                        animation: (u.transition.showMethod || u.transition) + " in",
                        queue: u.queue,
                        duration: u.transition.showDuration || u.duration,
                        useFailSafe: !0,
                        onComplete: function() {
                            u.onVisible.apply(C), u.keyboardShortcuts && D.add.keyboardShortcuts(), D.save.focus(), D.set.active(), u.autofocus && D.set.autofocus(), e();
                        }
                    })) : D.error(p.noTransition))) : D.debug("Modal is already visible");
                },
                hideModal: function(e, t, n) {
                    var i = a9.filter("." + f.active).last();
                    if (e = z.isFunction(e) ? e : function() {
                    }, D.debug("Hiding modal"), !1 === u.onHide.call(C, z(this))) return D.verbose("Hide callback returned false cancelling hide"), k = !1;
                    (D.is.animating() || D.is.active()) && (u.transition && z.fn.transition !== U && b.transition("is supported") ? (D.remove.active(), b.transition({
                        debug: u.debug,
                        animation: (u.transition.hideMethod || u.transition) + " out",
                        queue: u.queue,
                        duration: u.transition.hideDuration || u.duration,
                        useFailSafe: !0,
                        onStart: function() {
                            D.others.active() || D.others.animating() || t || D.hideDimmer(), u.keyboardShortcuts && !D.others.active() && D.remove.keyboardShortcuts();
                        },
                        onComplete: function() {
                            D.unbind.scrollLock(), u.allowMultiple && (i.addClass(f.front), b.removeClass(f.front), (n ? o9 : i).find(d.dimmer).removeClass("active")), z.isFunction(u.onHidden) && u.onHidden.call(C), D.remove.dimmerStyles(), D.restore.focus(), e();
                        }
                    })) : D.error(p.noTransition));
                },
                showDimmer: function() {
                    i9.dimmer("is animating") || !i9.dimmer("is active") ? (D.save.bodyMargin(), D.debug("Showing dimmer"), i9.dimmer("show")) : D.debug("Dimmer already visible");
                },
                hideDimmer: function() {
                    i9.dimmer("is animating") || i9.dimmer("is active") ? (D.unbind.scrollLock(), i9.dimmer("hide", function() {
                        D.restore.bodyMargin(), D.remove.clickaway(), D.remove.screenHeight();
                    })) : D.debug("Dimmer is not visible cannot hide");
                },
                hideAll: function(n) {
                    var e = o9.filter("." + f.active + ", ." + f.animating);
                    if (n = z.isFunction(n) ? n : function() {
                    }, 0 < e.length) {
                        D.debug("Hiding all visible modals");
                        var i = !0;
                        return z(e.get().reverse()).each(function(e, t) {
                            i = i && z(t).modal("hide modal", n, !1, !0);
                        }), i && D.hideDimmer(), i;
                    }
                },
                hideOthers: function(e) {
                    var t = a9.filter("." + f.active + ", ." + f.animating);
                    e = z.isFunction(e) ? e : function() {
                    }, 0 < t.length && (D.debug("Hiding other modals", a9), t.modal("hide modal", e, !0));
                },
                others: {
                    active: function() {
                        return 0 < a9.filter("." + f.active).length;
                    },
                    animating: function() {
                        return 0 < a9.filter("." + f.animating).length;
                    }
                },
                add: {
                    keyboardShortcuts: function() {
                        D.verbose("Adding keyboard shortcuts"), P.on("keyup" + h, D.event.keyboard);
                    }
                },
                save: {
                    focus: function() {
                        0 < z(H.activeElement).closest(b).length || (e47 = z(H.activeElement).blur());
                    },
                    bodyMargin: function() {
                        T = O.css("margin-" + (D.can.leftBodyScrollbar() ? "left" : "right"));
                        var e = parseInt(T.replace(/[^\d.]/g, "")), t = N.innerWidth - H.documentElement.clientWidth;
                        S = e + t;
                    }
                },
                restore: {
                    focus: function() {
                        e47 && 0 < e47.length && u.restoreFocus && e47.focus();
                    },
                    bodyMargin: function() {
                        var n = D.can.leftBodyScrollbar() ? "left" : "right";
                        O.css("margin-" + n, T), O.find(d.bodyFixed.replace("right", n)).each(function() {
                            var e = z(this), t = "fixed" === e.css("position") ? "padding-" + n : n;
                            e.css(t, "");
                        });
                    }
                },
                remove: {
                    active: function() {
                        b.removeClass(f.active);
                    },
                    legacy: function() {
                        b.removeClass(f.legacy);
                    },
                    clickaway: function() {
                        u.detachable || b.off("mousedown" + n10), t27.off("mousedown" + n10), t27.off("mouseup" + n10);
                    },
                    dimmerStyles: function() {
                        t27.removeClass(f.inverted), i9.removeClass(f.blurring);
                    },
                    bodyStyle: function() {
                        "" === O.attr("style") && (D.verbose("Removing style attribute"), O.removeAttr("style"));
                    },
                    screenHeight: function() {
                        D.debug("Removing page height"), O.css("height", "");
                    },
                    keyboardShortcuts: function() {
                        D.verbose("Removing keyboard shortcuts"), P.off("keyup" + h);
                    },
                    scrolling: function() {
                        i9.removeClass(f.scrolling), b.removeClass(f.scrolling);
                    }
                },
                cacheSizes: function() {
                    b.addClass(f.loading);
                    var e = b.prop("scrollHeight"), t = b.outerWidth(), n = b.outerHeight();
                    D.cache.pageHeight !== U && 0 === n || (z.extend(D.cache, {
                        pageHeight: z(H).outerHeight(),
                        width: t,
                        height: n + u.offset,
                        scrollHeight: e + u.offset,
                        contextHeight: ("body" == u.context ? z(N) : i9).height()
                    }), D.cache.topOffset = -D.cache.height / 2), b.removeClass(f.loading), D.debug("Caching modal and container sizes", D.cache);
                },
                helpers: {
                    deQuote: function(e) {
                        return String(e).replace(/"/g, "");
                    },
                    escape: function(e, t) {
                        if (t) return e;
                        var n = {
                            "<": "&lt;",
                            ">": "&gt;",
                            '"': "&quot;",
                            "'": "&#x27;",
                            "`": "&#x60;"
                        };
                        return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                            return n[e];
                        }) : e;
                    }
                },
                can: {
                    leftBodyScrollbar: function() {
                        return D.cache.leftBodyScrollbar === U && (D.cache.leftBodyScrollbar = D.is.rtl() && (D.is.iframe && !D.is.firefox() || D.is.safari() || D.is.edge() || D.is.ie())), D.cache.leftBodyScrollbar;
                    },
                    useFlex: function() {
                        return "auto" === u.useFlex ? u.detachable && !D.is.ie() : (u.useFlex && D.is.ie() ? D.debug("useFlex true is not supported in IE") : u.useFlex && !u.detachable && D.debug("useFlex true in combination with detachable false is not supported"), u.useFlex);
                    },
                    fit: function() {
                        var e = D.cache.contextHeight, t = D.cache.contextHeight / 2, n = D.cache.topOffset, i = D.cache.scrollHeight, o = D.cache.height, a = u.padding;
                        return o < i ? t + n + i + a < e : o + 2 * a < e;
                    }
                },
                has: {
                    configActions: function() {
                        return Array.isArray(u.actions) && 0 < u.actions.length;
                    }
                },
                is: {
                    active: function() {
                        return b.hasClass(f.active);
                    },
                    ie: function() {
                        var e, t;
                        return D.cache.isIE === U && (e = !N.ActiveXObject && "ActiveXObject" in N, t = "ActiveXObject" in N, D.cache.isIE = e || t), D.cache.isIE;
                    },
                    animating: function() {
                        return b.transition("is supported") ? b.transition("is animating") : b.is(":visible");
                    },
                    scrolling: function() {
                        return i9.hasClass(f.scrolling);
                    },
                    modernBrowser: function() {
                        return !(N.ActiveXObject || "ActiveXObject" in N);
                    },
                    rtl: function() {
                        return D.cache.isRTL === U && (D.cache.isRTL = "rtl" === O.attr("dir") || "rtl" === O.css("direction")), D.cache.isRTL;
                    },
                    safari: function() {
                        return D.cache.isSafari === U && (D.cache.isSafari = /constructor/i.test(N.HTMLElement) || !!N.ApplePaySession), D.cache.isSafari;
                    },
                    edge: function() {
                        return D.cache.isEdge === U && (D.cache.isEdge = !!N.setImmediate && !D.is.ie()), D.cache.isEdge;
                    },
                    firefox: function() {
                        return D.cache.isFirefox === U && (D.cache.isFirefox = !!N.InstallTrigger), D.cache.isFirefox;
                    },
                    iframe: function() {
                        return !(self === top);
                    }
                },
                set: {
                    autofocus: function() {
                        var e = b.find("[tabindex], :input").filter(":visible").filter(function() {
                            return 0 === z(this).closest(".disabled").length;
                        }), t = e.filter("[autofocus]"), e = (0 < t.length ? t : e).first();
                        0 < e.length && e.focus();
                    },
                    bodyMargin: function() {
                        var n = D.can.leftBodyScrollbar() ? "left" : "right";
                        (u.detachable || D.can.fit()) && O.css("margin-" + n, S + "px"), O.find(d.bodyFixed.replace("right", n)).each(function() {
                            var e = z(this), t = "fixed" === e.css("position") ? "padding-" + n : n;
                            e.css(t, "calc(" + e.css(t) + " + " + S + "px)");
                        });
                    },
                    clickaway: function() {
                        u.detachable || b.on("mousedown" + n10, D.event.mousedown), t27.on("mousedown" + n10, D.event.mousedown), t27.on("mouseup" + n10, D.event.mouseup);
                    },
                    dimmerSettings: function() {
                        var e;
                        z.fn.dimmer !== U ? (e = {
                            debug: u.debug,
                            dimmerName: "modals",
                            closable: "auto",
                            useFlex: D.can.useFlex(),
                            duration: {
                                show: u.transition.showDuration || u.duration,
                                hide: u.transition.hideDuration || u.duration
                            }
                        }, e = z.extend(!0, e, u.dimmerSettings), u.inverted && (e.variation = e.variation !== U ? e.variation + " inverted" : "inverted"), y.dimmer("setting", e)) : D.error(p.dimmer);
                    },
                    dimmerStyles: function() {
                        u.inverted ? t27.addClass(f.inverted) : t27.removeClass(f.inverted), u.blurring ? i9.addClass(f.blurring) : i9.removeClass(f.blurring);
                    },
                    modalOffset: function() {
                        var e;
                        u.detachable ? b.css({
                            marginTop: !b.hasClass("aligned") && D.can.fit() ? -D.cache.height / 2 : u.padding / 2,
                            marginLeft: -D.cache.width / 2
                        }) : (e = D.can.fit(), b.css({
                            top: !b.hasClass("aligned") && e ? z(H).scrollTop() + (D.cache.contextHeight - D.cache.height) / 2 : !e || b.hasClass("top") ? z(H).scrollTop() + u.padding : z(H).scrollTop() + (D.cache.contextHeight - D.cache.height - u.padding),
                            marginLeft: -D.cache.width / 2
                        })), D.verbose("Setting modal offset for legacy mode");
                    },
                    screenHeight: function() {
                        D.can.fit() ? O.css("height", "") : b.hasClass("bottom") || (D.debug("Modal is taller than page content, resizing page height"), O.css("height", D.cache.height + 2 * u.padding));
                    },
                    active: function() {
                        b.addClass(f.active + " " + f.front), a9.filter("." + f.active).removeClass(f.front);
                    },
                    scrolling: function() {
                        i9.addClass(f.scrolling), b.addClass(f.scrolling), D.unbind.scrollLock();
                    },
                    legacy: function() {
                        b.addClass(f.legacy);
                    },
                    type: function() {
                        D.can.fit() ? (D.verbose("Modal fits on screen"), D.others.active() || D.others.animating() || (D.remove.scrolling(), D.bind.scrollLock())) : b.hasClass("bottom") ? D.verbose("Bottom aligned modal not fitting on screen is unsupported for scrolling") : (D.verbose("Modal cannot fit on screen setting to scrolling"), D.set.scrolling());
                    },
                    undetached: function() {
                        i9.addClass(f.undetached);
                    }
                },
                setting: function(e, t) {
                    if (D.debug("Changing setting", e, t), z.isPlainObject(e)) z.extend(!0, u, e);
                    else {
                        if (t === U) return u[e];
                        z.isPlainObject(u[e]) ? z.extend(!0, u[e], t) : u[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (z.isPlainObject(e)) z.extend(!0, D, e);
                    else {
                        if (t === U) return D[e];
                        D[e] = t;
                    }
                },
                debug: function() {
                    !u.silent && u.debug && (u.performance ? D.performance.log(arguments) : (D.debug = Function.prototype.bind.call(console.info, console, u.name + ":"), D.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !u.silent && u.verbose && u.debug && (u.performance ? D.performance.log(arguments) : (D.verbose = Function.prototype.bind.call(console.info, console, u.name + ":"), D.verbose.apply(console, arguments)));
                },
                error: function() {
                    u.silent || (D.error = Function.prototype.bind.call(console.error, console, u.name + ":"), D.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        u.performance && (n = (t = (new Date).getTime()) - (M || t), M = t, I.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: C,
                            "Execution Time": n
                        })), clearTimeout(D.performance.timer), D.performance.timer = setTimeout(D.performance.display, 500);
                    },
                    display: function() {
                        var e = u.name + ":", n = 0;
                        M = !1, clearTimeout(D.performance.timer), z.each(I, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", R && (e += " '" + R + "'"), (console.group !== U || console.table !== U) && 0 < I.length && (console.groupCollapsed(e), console.table ? console.table(I) : z.each(I, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), I = [];
                    }
                },
                invoke: function(i, e48, t28) {
                    var o, a, n, r = w;
                    return e48 = e48 || V, t28 = C || t28, "string" == typeof i && r !== U && (i = i.split(/[\. ]/), o = i.length - 1, z.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (z.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== U) return a = r[n], !1;
                            if (!z.isPlainObject(r[t]) || e == o) return r[t] !== U && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), z.isFunction(a) ? n = a.apply(t28, e48) : a !== U && (n = a), Array.isArray(E) ? E.push(n) : E !== U ? E = [
                        E,
                        n
                    ] : n !== U && (E = n), a;
                }
            };
            L ? (w === U && (z.isFunction(u.templates[j]) && (u.autoShow = !0, u.className.modal = u.className.template, u = z.extend(!0, {
            }, u, u.templates[j].apply(D, V)), f = u.className, u.namespace, g = u.fields, p = u.error), D.initialize()), z.isFunction(u.templates[j]) || D.invoke(j)) : (w !== U && w.invoke("destroy"), D.initialize(), E = b);
        }), E !== U ? E : this;
    }, z.fn.modal.settings = {
        name: "Modal",
        namespace: "modal",
        useFlex: "auto",
        offset: 0,
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        observeChanges: !1,
        allowMultiple: !1,
        detachable: !0,
        closable: !0,
        autofocus: !0,
        restoreFocus: !0,
        autoShow: !1,
        inverted: !1,
        blurring: !1,
        centered: !0,
        dimmerSettings: {
            closable: !1,
            useCSS: !0
        },
        keyboardShortcuts: !0,
        context: "body",
        queue: !1,
        duration: 500,
        transition: "scale",
        padding: 50,
        scrollbarWidth: 10,
        title: "",
        content: "",
        class: "",
        classTitle: "",
        classContent: "",
        classActions: "",
        closeIcon: !1,
        actions: !1,
        preserveHTML: !0,
        fields: {
            class: "class",
            text: "text",
            icon: "icon",
            click: "click"
        },
        onShow: function() {
        },
        onVisible: function() {
        },
        onHide: function() {
            return !0;
        },
        onHidden: !1,
        onApprove: function() {
            return !0;
        },
        onDeny: function() {
            return !0;
        },
        selector: {
            title: "> .header",
            content: "> .content",
            actions: "> .actions",
            close: "> .close",
            approve: ".actions .positive, .actions .approve, .actions .ok",
            deny: ".actions .negative, .actions .deny, .actions .cancel",
            modal: ".ui.modal",
            dimmer: "> .ui.dimmer",
            bodyFixed: "> .ui.fixed.menu, > .ui.right.toast-container, > .ui.right.sidebar, > .ui.fixed.nag, > .ui.fixed.nag > .close",
            prompt: ".ui.input > input"
        },
        error: {
            dimmer: "UI Dimmer, a required component is not included in this page",
            method: "The method you called is not defined.",
            notFound: "The element you specified could not be found"
        },
        className: {
            active: "active",
            animating: "animating",
            blurring: "blurring",
            inverted: "inverted",
            legacy: "legacy",
            loading: "loading",
            scrolling: "scrolling",
            undetached: "undetached",
            front: "front",
            close: "close icon",
            button: "ui button",
            modal: "ui modal",
            title: "header",
            content: "content",
            actions: "actions",
            template: "ui tiny modal",
            ok: "positive",
            cancel: "negative",
            prompt: "ui fluid input"
        },
        text: {
            ok: "Ok",
            cancel: "Cancel"
        }
    }, z.fn.modal.settings.templates = {
        getArguments: function(e) {
            e = [].slice.call(e);
            return z.isPlainObject(e[0]) ? z.extend({
                handler: function() {
                },
                content: "",
                title: ""
            }, e[0]) : (z.isFunction(e[e.length - 1]) || e.push(function() {
            }), {
                handler: e.pop(),
                content: e.pop() || "",
                title: e.pop() || ""
            });
        },
        alert: function() {
            var e = this.get.settings(), t = e.templates.getArguments(arguments);
            return {
                title: t.title,
                content: t.content,
                actions: [
                    {
                        text: e.text.ok,
                        class: e.className.ok,
                        click: t.handler
                    }
                ]
            };
        },
        confirm: function() {
            var e = this.get.settings(), t = e.templates.getArguments(arguments);
            return {
                title: t.title,
                content: t.content,
                actions: [
                    {
                        text: e.text.ok,
                        class: e.className.ok,
                        click: function() {
                            t.handler(!0);
                        }
                    },
                    {
                        text: e.text.cancel,
                        class: e.className.cancel,
                        click: function() {
                            t.handler(!1);
                        }
                    }
                ]
            };
        },
        prompt: function() {
            var t = this, e49 = this.get.settings(), n = e49.templates.getArguments(arguments);
            return 0 === z(z.parseHTML(n.content)).filter(".ui.input").length && (n.content += '<p><div class="' + e49.className.prompt + '"><input placeholder="' + this.helpers.deQuote(n.placeholder || "") + '" type="text" value="' + this.helpers.deQuote(n.defaultValue || "") + '"></div></p>'), {
                title: n.title,
                content: n.content,
                actions: [
                    {
                        text: e49.text.ok,
                        class: e49.className.ok,
                        click: function() {
                            var e = t.get.settings(), e = t.get.element().find(e.selector.prompt)[0];
                            n.handler(z(e).val());
                        }
                    },
                    {
                        text: e49.text.cancel,
                        class: e49.className.cancel,
                        click: function() {
                            n.handler(null);
                        }
                    }
                ]
            };
        }
    };
})(jQuery, window, document), (function(x, C, w, k) {
    "use strict";
    x.isFunction = x.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, C = void 0 !== C && C.Math == Math ? C : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), x.fn.nag = function(f) {
        var m, e50 = x(this), g = e50.selector || "", p = (new Date).getTime(), h = [], v = f, b = "string" == typeof v, y = [].slice.call(arguments, 1);
        return e50.each(function() {
            var i10, o10 = x.isPlainObject(f) ? x.extend(!0, {
            }, x.fn.nag.settings, f) : x.extend({
            }, x.fn.nag.settings), e51 = o10.selector, s = o10.error, t29 = o10.namespace, n11 = "." + t29, a10 = t29 + "-module", r10 = x(this), l = o10.context ? x(o10.context) : x("body"), c = this, u = r10.data(a10), d = {
                initialize: function() {
                    d.verbose("Initializing element"), i10 = d.get.storage(), r10.on("click" + n11, e51.close, d.dismiss).data(a10, d), o10.detachable && r10.parent()[0] !== l[0] && r10.detach().prependTo(l), 0 < o10.displayTime && setTimeout(d.hide, o10.displayTime), d.show();
                },
                destroy: function() {
                    d.verbose("Destroying instance"), r10.removeData(a10).off(n11);
                },
                show: function() {
                    if (d.should.show() && !r10.is(":visible")) {
                        if (!1 === o10.onShow.call(c)) return d.debug("onShow callback returned false, cancelling nag animation"), !1;
                        d.debug("Showing nag", o10.animation.show), "fade" === o10.animation.show ? r10.fadeIn(o10.duration, o10.easing, o10.onVisible) : r10.slideDown(o10.duration, o10.easing, o10.onVisible);
                    }
                },
                hide: function() {
                    if (!1 === o10.onHide.call(c)) return d.debug("onHide callback returned false, cancelling nag animation"), !1;
                    d.debug("Hiding nag", o10.animation.hide), "fade" === o10.animation.hide ? r10.fadeOut(o10.duration, o10.easing, o10.onHidden) : r10.slideUp(o10.duration, o10.easing, o10.onHidden);
                },
                dismiss: function(e) {
                    !1 !== d.hide() && o10.storageMethod && (d.debug("Dismissing nag", o10.storageMethod, o10.key, o10.value, o10.expires), d.storage.set(o10.key, o10.value)), e.stopImmediatePropagation(), e.preventDefault();
                },
                should: {
                    show: function() {
                        return o10.persist ? (d.debug("Persistent nag is set, can show nag"), !0) : d.storage.get(o10.key) != o10.value.toString() ? (d.debug("Stored value is not set, can show nag", d.storage.get(o10.key)), !0) : (d.debug("Stored value is set, cannot show nag", d.storage.get(o10.key)), !1);
                    }
                },
                get: {
                    expirationDate: function(e) {
                        if ((e = "number" == typeof e ? new Date(Date.now() + 86400000 * e) : e) instanceof Date && e.getTime()) return e.toUTCString();
                        d.error(s.expiresFormat);
                    },
                    storage: function() {
                        return "localstorage" === o10.storageMethod && C.localStorage !== k ? (d.debug("Using local storage"), C.localStorage) : "sessionstorage" === o10.storageMethod && C.sessionStorage !== k ? (d.debug("Using session storage"), C.sessionStorage) : "cookie" in w ? (d.debug("Using cookie"), {
                            setItem: function(e, t, n) {
                                e = encodeURIComponent(e).replace(/%(2[346B]|5E|60|7C)/g, decodeURIComponent).replace(/[()]/g, escape), t = encodeURIComponent(t).replace(/%(2[346BF]|3[AC-F]|40|5[BDE]|60|7[BCD])/g, decodeURIComponent);
                                var i, o = "";
                                for(i in n)n.hasOwnProperty(i) && (o += "; " + i, "string" == typeof n[i] && (o += "=" + n[i].split(";")[0]));
                                w.cookie = e + "=" + t + o;
                            },
                            getItem: function(e) {
                                for(var t = w.cookie.split("; "), n = 0, i = t.length; n < i; n++){
                                    var o = t[n].split("=");
                                    if (e === o[0].replace(/(%[\dA-F]{2})+/gi, decodeURIComponent)) return o[1] || "";
                                }
                            },
                            removeItem: function(e, t) {
                                i10.setItem(e, "", t);
                            }
                        }) : void d.error(s.noStorage);
                    },
                    storageOptions: function() {
                        var e = {
                        };
                        return o10.expires && (e.expires = d.get.expirationDate(o10.expires)), o10.domain && (e.domain = o10.domain), o10.path && (e.path = o10.path), o10.secure && (e.secure = o10.secure), o10.samesite && (e.samesite = o10.samesite), e;
                    }
                },
                clear: function() {
                    d.storage.remove(o10.key);
                },
                storage: {
                    set: function(e, t) {
                        var n = d.get.storageOptions();
                        i10 === C.localStorage && n.expires && (d.debug("Storing expiration value in localStorage", e, n.expires), i10.setItem(e + o10.expirationKey, n.expires)), d.debug("Value stored", e, t);
                        try {
                            i10.setItem(e, t, n);
                        } catch (e52) {
                            d.error(s.setItem, e52);
                        }
                    },
                    get: function(e) {
                        var t, n = i10.getItem(e);
                        return i10 !== C.localStorage || null !== (t = i10.getItem(e + o10.expirationKey)) && t !== k && new Date(t) < new Date && (d.debug("Value in localStorage has expired. Deleting key", e), d.storage.remove(e), n = null), n = "undefined" == n || "null" == n || n === k || null === n ? k : n;
                    },
                    remove: function(e) {
                        var t = d.get.storageOptions();
                        t.expires = d.get.expirationDate(-1), i10 === C.localStorage && i10.removeItem(e + o10.expirationKey), i10.removeItem(e, t);
                    }
                },
                setting: function(e, t) {
                    if (d.debug("Changing setting", e, t), x.isPlainObject(e)) x.extend(!0, o10, e);
                    else {
                        if (t === k) return o10[e];
                        x.isPlainObject(o10[e]) ? x.extend(!0, o10[e], t) : o10[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (x.isPlainObject(e)) x.extend(!0, d, e);
                    else {
                        if (t === k) return d[e];
                        d[e] = t;
                    }
                },
                debug: function() {
                    !o10.silent && o10.debug && (o10.performance ? d.performance.log(arguments) : (d.debug = Function.prototype.bind.call(console.info, console, o10.name + ":"), d.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !o10.silent && o10.verbose && o10.debug && (o10.performance ? d.performance.log(arguments) : (d.verbose = Function.prototype.bind.call(console.info, console, o10.name + ":"), d.verbose.apply(console, arguments)));
                },
                error: function() {
                    o10.silent || (d.error = Function.prototype.bind.call(console.error, console, o10.name + ":"), d.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        o10.performance && (n = (t = (new Date).getTime()) - (p || t), p = t, h.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: c,
                            "Execution Time": n
                        })), clearTimeout(d.performance.timer), d.performance.timer = setTimeout(d.performance.display, 500);
                    },
                    display: function() {
                        var e = o10.name + ":", n = 0;
                        p = !1, clearTimeout(d.performance.timer), x.each(h, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", g && (e += " '" + g + "'"), (console.group !== k || console.table !== k) && 0 < h.length && (console.groupCollapsed(e), console.table ? console.table(h) : x.each(h, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), h = [];
                    }
                },
                invoke: function(i, e53, t30) {
                    var o, a, n, r = u;
                    return e53 = e53 || y, t30 = c || t30, "string" == typeof i && r !== k && (i = i.split(/[\. ]/), o = i.length - 1, x.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (x.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== k) return a = r[n], !1;
                            if (!x.isPlainObject(r[t]) || e == o) return r[t] !== k ? a = r[t] : d.error(s.method, i), !1;
                            r = r[t];
                        }
                    })), x.isFunction(a) ? n = a.apply(t30, e53) : a !== k && (n = a), Array.isArray(m) ? m.push(n) : m !== k ? m = [
                        m,
                        n
                    ] : n !== k && (m = n), a;
                }
            };
            b ? (u === k && d.initialize(), d.invoke(v)) : (u !== k && u.invoke("destroy"), d.initialize());
        }), m !== k ? m : this;
    }, x.fn.nag.settings = {
        name: "Nag",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        namespace: "Nag",
        persist: !1,
        displayTime: 0,
        animation: {
            show: "slide",
            hide: "slide"
        },
        context: !1,
        detachable: !1,
        expires: 30,
        domain: !1,
        path: "/",
        secure: !1,
        samesite: !1,
        storageMethod: "cookie",
        key: "nag",
        value: "dismiss",
        expirationKey: "ExpirationDate",
        error: {
            noStorage: "Unsupported storage method",
            method: "The method you called is not defined.",
            setItem: "Unexpected error while setting value",
            expiresFormat: '"expires" must be a number of days or a Date Object'
        },
        className: {
            bottom: "bottom",
            fixed: "fixed"
        },
        selector: {
            close: "> .close.icon"
        },
        duration: 500,
        easing: "easeOutQuad",
        onShow: function() {
        },
        onVisible: function() {
        },
        onHide: function() {
        },
        onHidden: function() {
        }
    }, x.extend(x.easing, {
        easeOutQuad: function(e, t, n, i, o) {
            return -i * (t /= o) * (t - 2) + n;
        }
    });
})(jQuery, window, document), (function(L, V, q, z) {
    "use strict";
    L.isFunction = L.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, V = void 0 !== V && V.Math == Math ? V : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), L.fn.popup = function(T) {
        var S, e54 = L(this), D = L(q), A = L(V), E = L("body"), F = e54.selector || "", P = "ontouchstart" in q.documentElement ? "touchstart" : "click", O = (new Date).getTime(), R = [], M = T, I = "string" == typeof M, j = [].slice.call(arguments, 1);
        return e54.each(function() {
            var c, s5, e55, t31, n12, u = L.isPlainObject(T) ? L.extend(!0, {
            }, L.fn.popup.settings, T) : L.extend({
            }, L.fn.popup.settings), i11 = u.selector, d = u.className, f = u.error, m = u.metadata, o11 = u.namespace, a11 = "." + u.namespace, r11 = "module-" + o11, g = L(this), l5 = L(u.context), p = L(u.scrollContext), h = L(u.boundary), v = u.target ? L(u.target) : g, b = 0, y = !1, x = !1, C = this, w = g.data(r11), k = {
                initialize: function() {
                    k.debug("Initializing", g), k.createID(), k.bind.events(), !k.exists() && u.preserve && k.create(), u.observeChanges && k.observeChanges(), k.instantiate();
                },
                instantiate: function() {
                    k.verbose("Storing instance", k), w = k, g.data(r11, w);
                },
                observeChanges: function() {
                    "MutationObserver" in V && ((e55 = new MutationObserver(k.event.documentChanged)).observe(q, {
                        childList: !0,
                        subtree: !0
                    }), k.debug("Setting up mutation observer", e55));
                },
                refresh: function() {
                    u.popup ? c = L(u.popup).eq(0) : u.inline && (c = v.nextAll(i11.popup).eq(0), u.popup = c), u.popup ? (c.addClass(d.loading), s5 = k.get.offsetParent(), c.removeClass(d.loading), u.movePopup && k.has.popup() && k.get.offsetParent(c)[0] !== s5[0] && (k.debug("Moving popup to the same offset parent as target"), c.detach().appendTo(s5))) : s5 = u.inline ? k.get.offsetParent(v) : k.has.popup() ? k.get.offsetParent(c) : E, s5.is("html") && s5[0] !== E[0] && (k.debug("Setting page as offset parent"), s5 = E), k.get.variation() && k.set.variation();
                },
                reposition: function() {
                    k.refresh(), k.set.position();
                },
                destroy: function() {
                    k.debug("Destroying previous module"), e55 && e55.disconnect(), c && !u.preserve && k.removePopup(), clearTimeout(k.hideTimer), clearTimeout(k.showTimer), k.unbind.close(), k.unbind.events(), g.removeData(r11);
                },
                event: {
                    start: function(e) {
                        var t = L.isPlainObject(u.delay) ? u.delay.show : u.delay;
                        clearTimeout(k.hideTimer), x && !u.addTouchEvents || (k.showTimer = setTimeout(k.show, t));
                    },
                    end: function() {
                        var e = L.isPlainObject(u.delay) ? u.delay.hide : u.delay;
                        clearTimeout(k.showTimer), k.hideTimer = setTimeout(k.hide, e);
                    },
                    touchstart: function(e) {
                        x = !0, u.addTouchEvents && k.show();
                    },
                    resize: function() {
                        k.is.visible() && k.set.position();
                    },
                    documentChanged: function(e56) {
                        [].forEach.call(e56, function(e57) {
                            e57.removedNodes && [].forEach.call(e57.removedNodes, function(e) {
                                (e == C || 0 < L(e).find(C).length) && (k.debug("Element removed from DOM, tearing down events"), k.destroy());
                            });
                        });
                    },
                    hideGracefully: function(e) {
                        var t = L(e.target), n = L.contains(q.documentElement, e.target), t = 0 < t.closest(i11.popup).length;
                        e && !t && n ? (k.debug("Click occurred outside popup hiding popup"), k.hide()) : k.debug("Click was inside popup, keeping popup open");
                    }
                },
                create: function() {
                    var e = k.get.html(), t = k.get.title(), n = k.get.content();
                    e || n || t ? (k.debug("Creating pop-up html"), e = e || u.templates.popup({
                        title: t,
                        content: n
                    }), c = L("<div/>").addClass(d.popup).data(m.activator, g).html(e), u.inline ? (k.verbose("Inserting popup element inline", c), c.insertAfter(g)) : (k.verbose("Appending popup element to body", c), c.appendTo(l5)), k.refresh(), k.set.variation(), u.hoverable && k.bind.popup(), u.onCreate.call(c, C)) : u.popup ? (L(u.popup).data(m.activator, g), k.verbose("Used popup specified in settings"), k.refresh(), u.hoverable && k.bind.popup()) : 0 !== v.next(i11.popup).length ? (k.verbose("Pre-existing popup found"), u.inline = !0, u.popup = v.next(i11.popup).data(m.activator, g), k.refresh(), u.hoverable && k.bind.popup()) : k.debug("No content specified skipping display", C);
                },
                createID: function() {
                    n12 = (Math.random().toString(16) + "000000000").substr(2, 8), t31 = "." + n12, k.verbose("Creating unique id for element", n12);
                },
                toggle: function() {
                    k.debug("Toggling pop-up"), k.is.hidden() ? (k.debug("Popup is hidden, showing pop-up"), k.unbind.close(), k.show()) : (k.debug("Popup is visible, hiding pop-up"), k.hide());
                },
                show: function(e) {
                    e = e || function() {
                    }, k.debug("Showing pop-up", u.transition), !k.is.hidden() || k.is.active() && k.is.dropdown() || (k.exists() || k.create(), !1 !== u.onShow.call(c, C) ? (u.preserve || u.popup || k.refresh(), c && k.set.position() && (k.save.conditions(), u.exclusive && k.hideAll(), k.animate.show(e))) : k.debug("onShow callback returned false, cancelling popup animation"));
                },
                hide: function(e) {
                    e = e || function() {
                    }, (k.is.visible() || k.is.animating()) && (!1 !== u.onHide.call(c, C) ? (k.remove.visible(), k.unbind.close(), k.restore.conditions(), k.animate.hide(e)) : k.debug("onHide callback returned false, cancelling popup animation"));
                },
                hideAll: function() {
                    L(i11.popup).filter("." + d.popupVisible).each(function() {
                        L(this).data(m.activator).popup("hide");
                    });
                },
                exists: function() {
                    return !!c && (u.inline || u.popup ? k.has.popup() : 1 <= c.closest(l5).length);
                },
                removePopup: function() {
                    k.has.popup() && !u.popup && (k.debug("Removing popup", c), c.remove(), c = z, u.onRemove.call(c, C));
                },
                save: {
                    conditions: function() {
                        k.cache = {
                            title: g.attr("title")
                        }, k.cache.title && g.removeAttr("title"), k.verbose("Saving original attributes", k.cache.title);
                    }
                },
                restore: {
                    conditions: function() {
                        return k.cache && k.cache.title && (g.attr("title", k.cache.title), k.verbose("Restoring original attributes", k.cache.title)), !0;
                    }
                },
                supports: {
                    svg: function() {
                        return "undefined" != typeof SVGGraphicsElement;
                    }
                },
                animate: {
                    show: function(e) {
                        e = L.isFunction(e) ? e : function() {
                        }, u.transition && L.fn.transition !== z && g.transition("is supported") ? (k.set.visible(), c.transition({
                            animation: (u.transition.showMethod || u.transition) + " in",
                            queue: !1,
                            debug: u.debug,
                            verbose: u.verbose,
                            duration: u.transition.showDuration || u.duration,
                            onComplete: function() {
                                k.bind.close(), e.call(c, C), u.onVisible.call(c, C);
                            }
                        })) : k.error(f.noTransition);
                    },
                    hide: function(e) {
                        e = L.isFunction(e) ? e : function() {
                        }, k.debug("Hiding pop-up"), u.transition && L.fn.transition !== z && g.transition("is supported") ? c.transition({
                            animation: (u.transition.hideMethod || u.transition) + " out",
                            queue: !1,
                            duration: u.transition.hideDuration || u.duration,
                            debug: u.debug,
                            verbose: u.verbose,
                            onComplete: function() {
                                k.reset(), e.call(c, C), u.onHidden.call(c, C);
                            }
                        }) : k.error(f.noTransition);
                    }
                },
                change: {
                    content: function(e) {
                        c.html(e);
                    }
                },
                get: {
                    html: function() {
                        return g.removeData(m.html), g.data(m.html) || u.html;
                    },
                    title: function() {
                        return g.removeData(m.title), g.data(m.title) || u.title;
                    },
                    content: function() {
                        return g.removeData(m.content), g.data(m.content) || u.content || g.attr("title");
                    },
                    variation: function() {
                        return g.removeData(m.variation), g.data(m.variation) || u.variation;
                    },
                    popup: function() {
                        return c;
                    },
                    popupOffset: function() {
                        return c.offset();
                    },
                    calculations: function() {
                        var e = k.get.offsetParent(c), t = v[0], n = h[0] == V, i = v.offset(), o = u.inline || u.popup && u.movePopup ? v.offsetParent().offset() : {
                            top: 0,
                            left: 0
                        }, a = n ? {
                            top: 0,
                            left: 0
                        } : h.offset(), r = {
                        }, n = n ? {
                            top: A.scrollTop(),
                            left: A.scrollLeft()
                        } : {
                            top: 0,
                            left: 0
                        }, r = {
                            target: {
                                element: v[0],
                                width: v.outerWidth(),
                                height: v.outerHeight(),
                                top: i.top - o.top,
                                left: i.left - o.left,
                                margin: {
                                }
                            },
                            popup: {
                                width: c.outerWidth(),
                                height: c.outerHeight()
                            },
                            parent: {
                                width: s5.outerWidth(),
                                height: s5.outerHeight()
                            },
                            screen: {
                                top: a.top,
                                left: a.left,
                                scroll: {
                                    top: n.top,
                                    left: n.left
                                },
                                width: h.width(),
                                height: h.height()
                            }
                        };
                        return e.get(0) !== s5.get(0) && (n = e.offset(), r.target.top -= n.top, r.target.left -= n.left, r.parent.width = e.outerWidth(), r.parent.height = e.outerHeight()), u.setFluidWidth && k.is.fluid() && (r.container = {
                            width: c.parent().outerWidth()
                        }, r.popup.width = r.container.width), r.target.margin.top = u.inline ? parseInt(V.getComputedStyle(t).getPropertyValue("margin-top"), 10) : 0, r.target.margin.left = u.inline ? k.is.rtl() ? parseInt(V.getComputedStyle(t).getPropertyValue("margin-right"), 10) : parseInt(V.getComputedStyle(t).getPropertyValue("margin-left"), 10) : 0, t = r.screen, r.boundary = {
                            top: t.top + t.scroll.top,
                            bottom: t.top + t.scroll.top + t.height,
                            left: t.left + t.scroll.left,
                            right: t.left + t.scroll.left + t.width
                        }, r;
                    },
                    id: function() {
                        return n12;
                    },
                    startEvent: function() {
                        return "hover" == u.on ? "mouseenter" : "focus" == u.on && "focus";
                    },
                    scrollEvent: function() {
                        return "scroll";
                    },
                    endEvent: function() {
                        return "hover" == u.on ? "mouseleave" : "focus" == u.on && "blur";
                    },
                    distanceFromBoundary: function(e, t) {
                        var n = {
                        }, i = (t = t || k.get.calculations()).popup, t = t.boundary;
                        return e && (n = {
                            top: e.top - t.top,
                            left: e.left - t.left,
                            right: t.right - (e.left + i.width),
                            bottom: t.bottom - (e.top + i.height)
                        }, k.verbose("Distance from boundaries determined", e, n)), n;
                    },
                    offsetParent: function(e) {
                        var t = (e !== z ? e : v)[0].parentNode, n = L(t);
                        if (t) for(var i = "none" === n.css("transform"), o = "static" === n.css("position"), a = n.is("body"); t && !a && o && i;)t = t.parentNode, i = "none" === (n = L(t)).css("transform"), o = "static" === n.css("position"), a = n.is("body");
                        return n && 0 < n.length ? n : L();
                    },
                    positions: function() {
                        return {
                            "top left": !1,
                            "top center": !1,
                            "top right": !1,
                            "bottom left": !1,
                            "bottom center": !1,
                            "bottom right": !1,
                            "left center": !1,
                            "right center": !1
                        };
                    },
                    nextPosition: function(e) {
                        var t = e.split(" "), n = t[0], i = t[1], o = "top" == n || "bottom" == n, a = !1, r = !1, t = !1;
                        return y || (k.verbose("All available positions available"), y = k.get.positions()), k.debug("Recording last position tried", e), y[e] = !0, "opposite" === u.prefer && (t = (t = [
                            {
                                top: "bottom",
                                bottom: "top",
                                left: "right",
                                right: "left"
                            }[n],
                            i
                        ]).join(" "), a = !0 === y[t], k.debug("Trying opposite strategy", t)), "adjacent" === u.prefer && o && (t = (t = [
                            n,
                            {
                                left: "center",
                                center: "right",
                                right: "left"
                            }[i]
                        ]).join(" "), r = !0 === y[t], k.debug("Trying adjacent strategy", t)), (r || a) && (k.debug("Using backup position", t), t = ({
                            "top left": "top center",
                            "top center": "top right",
                            "top right": "right center",
                            "right center": "bottom right",
                            "bottom right": "bottom center",
                            "bottom center": "bottom left",
                            "bottom left": "left center",
                            "left center": "top left"
                        })[e]), t;
                    }
                },
                set: {
                    position: function(e58, t) {
                        if (0 !== v.length && 0 !== c.length) {
                            var n, i, o, a, r, s, l;
                            if (t = t || k.get.calculations(), e58 = e58 || g.data(m.position) || u.position, n = g.data(m.offset) || u.offset, i = u.distanceAway, o = t.target, a = t.popup, r = t.parent, k.should.centerArrow(t) && (k.verbose("Adjusting offset to center arrow on small target element"), "top left" != e58 && "bottom left" != e58 || (n += o.width / 2, n -= u.arrowPixelsFromEdge), "top right" != e58 && "bottom right" != e58 || (n -= o.width / 2, n += u.arrowPixelsFromEdge)), 0 === o.width && 0 === o.height && !k.is.svg(o.element)) return k.debug("Popup target is hidden, no action taken"), !1;
                            switch(u.inline && (k.debug("Adding margin to calculation", o.margin), "left center" == e58 || "right center" == e58 ? (n += o.margin.top, i += -o.margin.left) : "top left" == e58 || "top center" == e58 || "top right" == e58 ? (n += o.margin.left, i -= o.margin.top) : (n += o.margin.left, i += o.margin.top)), k.debug("Determining popup position from calculations", e58, t), k.is.rtl() && (e58 = e58.replace(/left|right/g, function(e) {
                                return "left" == e ? "right" : "left";
                            }), k.debug("RTL: Popup position updated", e58)), e58 = b == u.maxSearchDepth && "string" == typeof u.lastResort ? u.lastResort : e58){
                                case "top left":
                                    s = {
                                        top: "auto",
                                        bottom: r.height - o.top + i,
                                        left: o.left + n,
                                        right: "auto"
                                    };
                                    break;
                                case "top center":
                                    s = {
                                        bottom: r.height - o.top + i,
                                        left: o.left + o.width / 2 - a.width / 2 + n,
                                        top: "auto",
                                        right: "auto"
                                    };
                                    break;
                                case "top right":
                                    s = {
                                        bottom: r.height - o.top + i,
                                        right: r.width - o.left - o.width - n,
                                        top: "auto",
                                        left: "auto"
                                    };
                                    break;
                                case "left center":
                                    s = {
                                        top: o.top + o.height / 2 - a.height / 2 + n,
                                        right: r.width - o.left + i,
                                        left: "auto",
                                        bottom: "auto"
                                    };
                                    break;
                                case "right center":
                                    s = {
                                        top: o.top + o.height / 2 - a.height / 2 + n,
                                        left: o.left + o.width + i,
                                        bottom: "auto",
                                        right: "auto"
                                    };
                                    break;
                                case "bottom left":
                                    s = {
                                        top: o.top + o.height + i,
                                        left: o.left + n,
                                        bottom: "auto",
                                        right: "auto"
                                    };
                                    break;
                                case "bottom center":
                                    s = {
                                        top: o.top + o.height + i,
                                        left: o.left + o.width / 2 - a.width / 2 + n,
                                        bottom: "auto",
                                        right: "auto"
                                    };
                                    break;
                                case "bottom right":
                                    s = {
                                        top: o.top + o.height + i,
                                        right: r.width - o.left - o.width - n,
                                        left: "auto",
                                        bottom: "auto"
                                    };
                            }
                            if (s === z && k.error(f.invalidPosition, e58), k.debug("Calculated popup positioning values", s), c.css(s).removeClass(d.position).addClass(e58).addClass(d.loading), l = k.get.popupOffset(), l = k.get.distanceFromBoundary(l, t), !u.forcePosition && k.is.offstage(l, e58)) {
                                if (k.debug("Position is outside viewport", e58), b < u.maxSearchDepth) return b++, e58 = k.get.nextPosition(e58), k.debug("Trying new position", e58), !!c && k.set.position(e58, t);
                                if (!u.lastResort) return k.debug("Popup could not find a position to display", c), k.error(f.cannotPlace, C), k.remove.attempts(), k.remove.loading(), k.reset(), u.onUnplaceable.call(c, C), !1;
                                k.debug("No position found, showing with last position");
                            }
                            return k.debug("Position is on stage", e58), k.remove.attempts(), k.remove.loading(), u.setFluidWidth && k.is.fluid() && k.set.fluidWidth(t), !0;
                        }
                        k.error(f.notFound);
                    },
                    fluidWidth: function(e) {
                        e = e || k.get.calculations(), k.debug("Automatically setting element width to parent width", e.parent.width), c.css("width", e.container.width);
                    },
                    variation: function(e) {
                        (e = e || k.get.variation()) && k.has.popup() && (k.verbose("Adding variation to popup", e), c.addClass(e));
                    },
                    visible: function() {
                        g.addClass(d.visible);
                    }
                },
                remove: {
                    loading: function() {
                        c.removeClass(d.loading);
                    },
                    variation: function(e) {
                        (e = e || k.get.variation()) && (k.verbose("Removing variation", e), c.removeClass(e));
                    },
                    visible: function() {
                        g.removeClass(d.visible);
                    },
                    attempts: function() {
                        k.verbose("Resetting all searched positions"), b = 0, y = !1;
                    }
                },
                bind: {
                    events: function() {
                        k.debug("Binding popup events to module"), "click" == u.on && g.on(P + a11, k.toggle), "hover" == u.on && g.on("touchstart" + a11, k.event.touchstart), k.get.startEvent() && g.on(k.get.startEvent() + a11, k.event.start).on(k.get.endEvent() + a11, k.event.end), u.target && k.debug("Target set to element", v), A.on("resize" + t31, k.event.resize);
                    },
                    popup: function() {
                        k.verbose("Allowing hover events on popup to prevent closing"), c && k.has.popup() && c.on("mouseenter" + a11, k.event.start).on("mouseleave" + a11, k.event.end);
                    },
                    close: function() {
                        (!0 === u.hideOnScroll || "auto" == u.hideOnScroll && "click" != u.on) && k.bind.closeOnScroll(), k.is.closable() ? k.bind.clickaway() : "hover" == u.on && x && k.bind.touchClose();
                    },
                    closeOnScroll: function() {
                        k.verbose("Binding scroll close event to document"), p.one(k.get.scrollEvent() + t31, k.event.hideGracefully);
                    },
                    touchClose: function() {
                        k.verbose("Binding popup touchclose event to document"), D.on("touchstart" + t31, function(e) {
                            k.verbose("Touched away from popup"), k.event.hideGracefully.call(C, e);
                        });
                    },
                    clickaway: function() {
                        k.verbose("Binding popup close event to document"), D.on(P + t31, function(e) {
                            k.verbose("Clicked away from popup"), k.event.hideGracefully.call(C, e);
                        });
                    }
                },
                unbind: {
                    events: function() {
                        A.off(t31), g.off(a11);
                    },
                    close: function() {
                        D.off(t31), p.off(t31);
                    }
                },
                has: {
                    popup: function() {
                        return c && 0 < c.length;
                    }
                },
                should: {
                    centerArrow: function(e) {
                        return !k.is.basic() && e.target.width <= 2 * u.arrowPixelsFromEdge;
                    }
                },
                is: {
                    closable: function() {
                        return "auto" == u.closable ? "hover" != u.on : u.closable;
                    },
                    offstage: function(e59, n) {
                        var i = [];
                        return L.each(e59, function(e, t) {
                            t < -u.jitter && (k.debug("Position exceeds allowable distance from edge", e, t, n), i.push(e));
                        }), 0 < i.length;
                    },
                    svg: function(e) {
                        return k.supports.svg() && e instanceof SVGGraphicsElement;
                    },
                    basic: function() {
                        return g.hasClass(d.basic);
                    },
                    active: function() {
                        return g.hasClass(d.active);
                    },
                    animating: function() {
                        return c !== z && c.hasClass(d.animating);
                    },
                    fluid: function() {
                        return c !== z && c.hasClass(d.fluid);
                    },
                    visible: function() {
                        return c !== z && c.hasClass(d.popupVisible);
                    },
                    dropdown: function() {
                        return g.hasClass(d.dropdown);
                    },
                    hidden: function() {
                        return !k.is.visible();
                    },
                    rtl: function() {
                        return "rtl" === g.attr("dir") || "rtl" === g.css("direction");
                    }
                },
                reset: function() {
                    k.remove.visible(), u.preserve ? L.fn.transition !== z && c.transition("remove transition") : k.removePopup();
                },
                setting: function(e, t) {
                    if (L.isPlainObject(e)) L.extend(!0, u, e);
                    else {
                        if (t === z) return u[e];
                        u[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (L.isPlainObject(e)) L.extend(!0, k, e);
                    else {
                        if (t === z) return k[e];
                        k[e] = t;
                    }
                },
                debug: function() {
                    !u.silent && u.debug && (u.performance ? k.performance.log(arguments) : (k.debug = Function.prototype.bind.call(console.info, console, u.name + ":"), k.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !u.silent && u.verbose && u.debug && (u.performance ? k.performance.log(arguments) : (k.verbose = Function.prototype.bind.call(console.info, console, u.name + ":"), k.verbose.apply(console, arguments)));
                },
                error: function() {
                    u.silent || (k.error = Function.prototype.bind.call(console.error, console, u.name + ":"), k.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        u.performance && (n = (t = (new Date).getTime()) - (O || t), O = t, R.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: C,
                            "Execution Time": n
                        })), clearTimeout(k.performance.timer), k.performance.timer = setTimeout(k.performance.display, 500);
                    },
                    display: function() {
                        var e = u.name + ":", n = 0;
                        O = !1, clearTimeout(k.performance.timer), L.each(R, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", F && (e += " '" + F + "'"), (console.group !== z || console.table !== z) && 0 < R.length && (console.groupCollapsed(e), console.table ? console.table(R) : L.each(R, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), R = [];
                    }
                },
                invoke: function(i, e60, t32) {
                    var o, a, n, r = w;
                    return e60 = e60 || j, t32 = C || t32, "string" == typeof i && r !== z && (i = i.split(/[\. ]/), o = i.length - 1, L.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (L.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== z) return a = r[n], !1;
                            if (!L.isPlainObject(r[t]) || e == o) return r[t] !== z && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), L.isFunction(a) ? n = a.apply(t32, e60) : a !== z && (n = a), Array.isArray(S) ? S.push(n) : S !== z ? S = [
                        S,
                        n
                    ] : n !== z && (S = n), a;
                }
            };
            I ? (w === z && k.initialize(), k.invoke(M)) : (w !== z && w.invoke("destroy"), k.initialize());
        }), S !== z ? S : this;
    }, L.fn.popup.settings = {
        name: "Popup",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        namespace: "popup",
        observeChanges: !0,
        onCreate: function() {
        },
        onRemove: function() {
        },
        onShow: function() {
        },
        onVisible: function() {
        },
        onHide: function() {
        },
        onUnplaceable: function() {
        },
        onHidden: function() {
        },
        on: "hover",
        boundary: V,
        addTouchEvents: !0,
        position: "top left",
        forcePosition: !1,
        variation: "",
        movePopup: !0,
        target: !1,
        popup: !1,
        inline: !1,
        preserve: !1,
        hoverable: !1,
        content: !1,
        html: !1,
        title: !1,
        closable: !0,
        hideOnScroll: "auto",
        exclusive: !1,
        context: "body",
        scrollContext: V,
        prefer: "opposite",
        lastResort: !1,
        arrowPixelsFromEdge: 20,
        delay: {
            show: 50,
            hide: 70
        },
        setFluidWidth: !0,
        duration: 200,
        transition: "scale",
        distanceAway: 0,
        jitter: 2,
        offset: 0,
        maxSearchDepth: 15,
        error: {
            invalidPosition: "The position you specified is not a valid position",
            cannotPlace: "Popup does not fit within the boundaries of the viewport",
            method: "The method you called is not defined.",
            noTransition: "This module requires ui transitions <https://github.com/Semantic-Org/UI-Transition>",
            notFound: "The target or popup you specified does not exist on the page"
        },
        metadata: {
            activator: "activator",
            content: "content",
            html: "html",
            offset: "offset",
            position: "position",
            title: "title",
            variation: "variation"
        },
        className: {
            active: "active",
            basic: "basic",
            animating: "animating",
            dropdown: "dropdown",
            fluid: "fluid",
            loading: "loading",
            popup: "ui popup",
            position: "top left center bottom right",
            visible: "visible",
            popupVisible: "visible"
        },
        selector: {
            popup: ".ui.popup"
        },
        templates: {
            escape: function(e) {
                var t = {
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                };
                return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                    return t[e];
                }) : e;
            },
            popup: function(e) {
                var t = "", n = L.fn.popup.settings.templates.escape;
                return typeof e !== z && (typeof e.title !== z && e.title && (e.title = n(e.title), t += '<div class="header">' + e.title + "</div>"), typeof e.content !== z && e.content && (e.content = n(e.content), t += '<div class="content">' + e.content + "</div>")), t;
            }
        }
    };
})(jQuery, window, document), (function(T, e61, S, D) {
    "use strict";
    T.isFunction = T.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e61 = void 0 !== e61 && e61.Math == Math ? e61 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), T.fn.progress = function(h) {
        var v, e62 = T(this), b = e62.selector || "", y = (new Date).getTime(), x = [], C = h, w = "string" == typeof C, k = [].slice.call(arguments, 1);
        return e62.each(function() {
            var r12 = T.isPlainObject(h) ? T.extend(!0, {
            }, T.fn.progress.settings, h) : T.extend({
            }, T.fn.progress.settings), n13 = r12.className, t33 = r12.metadata, e63 = r12.namespace, i12 = r12.selector, s6 = r12.error, o12 = "." + e63, a12 = "module-" + e63, c = T(this), u = T(this).find(i12.bar), l6 = T(this).find(i12.progress), d = T(this).find(i12.label), f = this, m = c.data(a12), g = !1, p = {
                helper: {
                    sum: function(e64) {
                        return Array.isArray(e64) ? e64.reduce(function(e, t) {
                            return e + Number(t);
                        }, 0) : 0;
                    },
                    derivePrecision: function(e, t) {
                        for(var n = 0, i = 1, o = e / t; n < 10 && !(1 < (o *= i));)i = Math.pow(10, n++);
                        return i;
                    },
                    forceArray: function(e) {
                        return Array.isArray(e) ? e : isNaN(e) ? "string" == typeof e ? e.split(",") : [] : [
                            e
                        ];
                    }
                },
                initialize: function() {
                    p.set.duration(), p.set.transitionEvent(), p.debug(f), p.read.metadata(), p.read.settings(), p.instantiate();
                },
                instantiate: function() {
                    p.verbose("Storing instance of progress", p), m = p, c.data(a12, p);
                },
                destroy: function() {
                    p.verbose("Destroying previous progress for", c), clearInterval(m.interval), p.remove.state(), c.removeData(a12), m = D;
                },
                reset: function() {
                    p.remove.nextValue(), p.update.progress(0);
                },
                complete: function(e) {
                    (p.percent === D || p.percent < 100) && (p.remove.progressPoll(), !0 !== e && p.set.percent(100));
                },
                read: {
                    metadata: function() {
                        var e = {
                            percent: p.helper.forceArray(c.data(t33.percent)),
                            total: c.data(t33.total),
                            value: p.helper.forceArray(c.data(t33.value))
                        };
                        e.total !== D && (p.debug("Total value set from metadata", e.total), p.set.total(e.total)), 0 < e.value.length && (p.debug("Current value set from metadata", e.value), p.set.value(e.value), p.set.progress(e.value)), 0 < e.percent.length && (p.debug("Current percent value set from metadata", e.percent), p.set.percent(e.percent));
                    },
                    settings: function() {
                        !1 !== r12.total && (p.debug("Current total set in settings", r12.total), p.set.total(r12.total)), !1 !== r12.value && (p.debug("Current value set in settings", r12.value), p.set.value(r12.value), p.set.progress(p.value)), !1 !== r12.percent && (p.debug("Current percent set in settings", r12.percent), p.set.percent(r12.percent));
                    }
                },
                bind: {
                    transitionEnd: function(t) {
                        var e65 = p.get.transitionEnd();
                        u.one(e65 + o12, function(e) {
                            clearTimeout(p.failSafeTimer), t.call(this, e);
                        }), p.failSafeTimer = setTimeout(function() {
                            u.triggerHandler(e65);
                        }, r12.duration + r12.failSafeDelay), p.verbose("Adding fail safe timer", p.timer);
                    }
                },
                increment: function(e) {
                    var t;
                    e = p.has.total() ? (t = p.get.value(), e || 1) : (t = p.get.percent(), e || p.get.randomValue()), p.debug("Incrementing percentage by", t, t = t + e, e), t = p.get.normalizedValue(t), p.set.progress(t);
                },
                decrement: function(e) {
                    var t, n;
                    p.get.total() ? (n = (t = p.get.value()) - (e = e || 1), p.debug("Decrementing value by", e, t)) : (n = (t = p.get.percent()) - (e = e || p.get.randomValue()), p.debug("Decrementing percentage by", e, t)), n = p.get.normalizedValue(n), p.set.progress(n);
                },
                has: {
                    progressPoll: function() {
                        return p.progressPoll;
                    },
                    total: function() {
                        return !1 !== p.get.total();
                    }
                },
                get: {
                    text: function(e, t) {
                        var n = t || 0, i = p.get.value(n), o = p.get.total(), a = g ? p.get.displayPercent(n) : p.get.percent(n), t = !1 !== o ? Math.max(0, o - i) : 100 - a;
                        return e = (e = e || "").replace("{value}", i).replace("{total}", o || 0).replace("{left}", t).replace("{percent}", a).replace("{bar}", r12.text.bars[n] || ""), p.verbose("Adding variables to progress bar text", e), e;
                    },
                    normalizedValue: function(e) {
                        if (e < 0) return p.debug("Value cannot decrement below 0"), 0;
                        if (p.has.total()) {
                            if (e > p.total) return p.debug("Value cannot increment above total", p.total), p.total;
                        } else if (100 < e) return p.debug("Value cannot increment above 100 percent"), 100;
                        return e;
                    },
                    updateInterval: function() {
                        return "auto" == r12.updateInterval ? r12.duration : r12.updateInterval;
                    },
                    randomValue: function() {
                        return p.debug("Generating random increment percentage"), Math.floor(Math.random() * r12.random.max + r12.random.min);
                    },
                    numericValue: function(e) {
                        return "string" == typeof e ? "" !== e.replace(/[^\d.]/g, "") && +e.replace(/[^\d.]/g, "") : e;
                    },
                    transitionEnd: function() {
                        var e, t = S.createElement("element"), n = {
                            transition: "transitionend",
                            OTransition: "oTransitionEnd",
                            MozTransition: "transitionend",
                            WebkitTransition: "webkitTransitionEnd"
                        };
                        for(e in n)if (t.style[e] !== D) return n[e];
                    },
                    displayPercent: function(e) {
                        var t = T(u[e]), n = t.width(), e = c.width(), e = parseInt(t.css("min-width"), 10) < n ? n / e * 100 : p.percent;
                        return 0 < r12.precision ? Math.round(e * (10 * r12.precision)) / (10 * r12.precision) : Math.round(e);
                    },
                    percent: function(e) {
                        return p.percent && p.percent[e || 0] || 0;
                    },
                    value: function(e) {
                        return p.nextValue || p.value && p.value[e || 0] || 0;
                    },
                    total: function() {
                        return p.total !== D && p.total;
                    }
                },
                create: {
                    progressPoll: function() {
                        p.progressPoll = setTimeout(function() {
                            p.update.toNextValue(), p.remove.progressPoll();
                        }, p.get.updateInterval());
                    }
                },
                is: {
                    complete: function() {
                        return p.is.success() || p.is.warning() || p.is.error();
                    },
                    success: function() {
                        return c.hasClass(n13.success);
                    },
                    warning: function() {
                        return c.hasClass(n13.warning);
                    },
                    error: function() {
                        return c.hasClass(n13.error);
                    },
                    active: function() {
                        return c.hasClass(n13.active);
                    },
                    visible: function() {
                        return c.is(":visible");
                    }
                },
                remove: {
                    progressPoll: function() {
                        p.verbose("Removing progress poll timer"), p.progressPoll && (clearTimeout(p.progressPoll), delete p.progressPoll);
                    },
                    nextValue: function() {
                        p.verbose("Removing progress value stored for next update"), delete p.nextValue;
                    },
                    state: function() {
                        p.verbose("Removing stored state"), delete p.total, delete p.percent, delete p.value;
                    },
                    active: function() {
                        p.verbose("Removing active state"), c.removeClass(n13.active);
                    },
                    success: function() {
                        p.verbose("Removing success state"), c.removeClass(n13.success);
                    },
                    warning: function() {
                        p.verbose("Removing warning state"), c.removeClass(n13.warning);
                    },
                    error: function() {
                        p.verbose("Removing error state"), c.removeClass(n13.error);
                    }
                },
                set: {
                    barWidth: function(e66) {
                        p.debug("set bar width with ", e66), e66 = p.helper.forceArray(e66);
                        var o = -1, a = -1, r = p.helper.sum(e66), s = u.length, l = 1 < s, t34 = e66.map(function(e, t) {
                            var n = t === s - 1 && 0 === r, i = T(u[t]);
                            return 0 === e && l && !n ? i.css("display", "none") : (l && n && i.css("background", "transparent"), -1 == o && (o = t), a = t, i.css({
                                display: "block",
                                width: e + "%"
                            })), parseFloat(e);
                        });
                        e66.forEach(function(e, t) {
                            T(u[t]).css({
                                borderTopLeftRadius: t == o ? "" : 0,
                                borderBottomLeftRadius: t == o ? "" : 0,
                                borderTopRightRadius: t == a ? "" : 0,
                                borderBottomRightRadius: t == a ? "" : 0
                            });
                        }), c.attr("data-percent", t34);
                    },
                    duration: function(e) {
                        e = e || r12.duration, p.verbose("Setting progress bar transition duration", e = "number" == typeof e ? e + "ms" : e), u.css({
                            "transition-duration": e
                        });
                    },
                    percent: function(e67) {
                        e67 = p.helper.forceArray(e67).map(function(e) {
                            return e = "string" == typeof e ? +e.replace("%", "") : e, r12.limitValues ? Math.max(0, Math.min(100, e)) : e;
                        });
                        var t, n = p.has.total(), i = p.helper.sum(e67), o = 1 < e67.length && n, a = p.helper.sum(p.helper.forceArray(p.value));
                        o && a > p.total ? p.error(s6.sumExceedsTotal, a, p.total) : !o && 100 < i ? p.error(s6.tooHigh, i) : i < 0 ? p.error(s6.tooLow, i) : (t = 0 < r12.precision ? r12.precision : o ? p.helper.derivePrecision(Math.min.apply(null, p.value), p.total) : 0, o = e67.map(function(e) {
                            return 0 < t ? Math.round(e * (10 * t)) / (10 * t) : Math.round(e);
                        }), p.percent = o, n && (p.value = e67.map(function(e) {
                            return 0 < t ? Math.round(e / 100 * p.total * (10 * t)) / (10 * t) : Math.round(e / 100 * p.total * 10) / 10;
                        })), p.set.barWidth(e67), p.set.labelInterval()), r12.onChange.call(f, e67, p.value, p.total);
                    },
                    labelInterval: function() {
                        clearInterval(p.interval), p.bind.transitionEnd(function() {
                            p.verbose("Bar finished animating, removing continuous label updates"), clearInterval(p.interval), g = !1, p.set.labels();
                        }), g = !0, p.interval = setInterval(function() {
                            T.contains(S.documentElement, f) || (clearInterval(p.interval), g = !1), p.set.labels();
                        }, r12.framerate);
                    },
                    labels: function() {
                        p.verbose("Setting both bar progress and outer label text"), p.set.barLabel(), p.set.state();
                    },
                    label: function(e) {
                        (e = e || "") && (e = p.get.text(e), p.verbose("Setting label to text", e), d.text(e));
                    },
                    state: function(e) {
                        100 === (e = e !== D ? e : p.helper.sum(p.percent)) ? r12.autoSuccess && 1 === u.length && !(p.is.warning() || p.is.error() || p.is.success()) ? (p.set.success(), p.debug("Automatically triggering success at 100%")) : (p.verbose("Reached 100% removing active state"), p.remove.active(), p.remove.progressPoll()) : 0 < e ? (p.verbose("Adjusting active progress bar label", e), p.set.active()) : (p.remove.active(), p.set.label(r12.text.active));
                    },
                    barLabel: function(n) {
                        l6.map(function(e, t) {
                            t = T(t);
                            n !== D ? t.text(p.get.text(n, e)) : "ratio" == r12.label && p.has.total() ? (p.verbose("Adding ratio to bar label"), t.text(p.get.text(r12.text.ratio, e))) : "percent" == r12.label && (p.verbose("Adding percentage to bar label"), t.text(p.get.text(r12.text.percent, e)));
                        });
                    },
                    active: function(e) {
                        e = e || r12.text.active, p.debug("Setting active state"), r12.showActivity && !p.is.active() && c.addClass(n13.active), p.remove.warning(), p.remove.error(), p.remove.success(), (e = r12.onLabelUpdate("active", e, p.value, p.total)) && p.set.label(e), p.bind.transitionEnd(function() {
                            r12.onActive.call(f, p.value, p.total);
                        });
                    },
                    success: function(e, t) {
                        e = e || r12.text.success || r12.text.active, p.debug("Setting success state"), c.addClass(n13.success), p.remove.active(), p.remove.warning(), p.remove.error(), p.complete(t), e = r12.text.success ? r12.onLabelUpdate("success", e, p.value, p.total) : r12.onLabelUpdate("active", e, p.value, p.total), p.set.label(e), p.bind.transitionEnd(function() {
                            r12.onSuccess.call(f, p.total);
                        });
                    },
                    warning: function(e, t) {
                        e = e || r12.text.warning, p.debug("Setting warning state"), c.addClass(n13.warning), p.remove.active(), p.remove.success(), p.remove.error(), p.complete(t), (e = r12.onLabelUpdate("warning", e, p.value, p.total)) && p.set.label(e), p.bind.transitionEnd(function() {
                            r12.onWarning.call(f, p.value, p.total);
                        });
                    },
                    error: function(e, t) {
                        e = e || r12.text.error, p.debug("Setting error state"), c.addClass(n13.error), p.remove.active(), p.remove.success(), p.remove.warning(), p.complete(t), (e = r12.onLabelUpdate("error", e, p.value, p.total)) && p.set.label(e), p.bind.transitionEnd(function() {
                            r12.onError.call(f, p.value, p.total);
                        });
                    },
                    transitionEvent: function() {
                        p.get.transitionEnd();
                    },
                    total: function(e) {
                        p.total = e;
                    },
                    value: function(e) {
                        p.value = p.helper.forceArray(e);
                    },
                    progress: function(e) {
                        p.has.progressPoll() ? (p.debug("Updated within interval, setting next update to use new value", e), p.set.nextValue(e)) : (p.debug("First update in progress update interval, immediately updating", e), p.update.progress(e), p.create.progressPoll());
                    },
                    nextValue: function(e) {
                        p.nextValue = e;
                    }
                },
                update: {
                    toNextValue: function() {
                        var e = p.nextValue;
                        e && (p.debug("Update interval complete using last updated value", e), p.update.progress(e), p.remove.nextValue());
                    },
                    progress: function(e68) {
                        var n = p.has.total();
                        n && p.set.value(e68);
                        e68 = p.helper.forceArray(e68).map(function(e) {
                            var t;
                            return !1 === (e = p.get.numericValue(e)) && p.error(s6.nonNumeric, e), e = p.get.normalizedValue(e), n ? (t = 0 < p.total ? e / p.total * 100 : 100, p.debug("Calculating percent complete from total", t)) : (t = e, p.debug("Setting value to exact percentage value", t)), t;
                        });
                        p.set.percent(e68);
                    }
                },
                setting: function(e, t) {
                    if (p.debug("Changing setting", e, t), T.isPlainObject(e)) T.extend(!0, r12, e);
                    else {
                        if (t === D) return r12[e];
                        T.isPlainObject(r12[e]) ? T.extend(!0, r12[e], t) : r12[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (T.isPlainObject(e)) T.extend(!0, p, e);
                    else {
                        if (t === D) return p[e];
                        p[e] = t;
                    }
                },
                debug: function() {
                    !r12.silent && r12.debug && (r12.performance ? p.performance.log(arguments) : (p.debug = Function.prototype.bind.call(console.info, console, r12.name + ":"), p.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !r12.silent && r12.verbose && r12.debug && (r12.performance ? p.performance.log(arguments) : (p.verbose = Function.prototype.bind.call(console.info, console, r12.name + ":"), p.verbose.apply(console, arguments)));
                },
                error: function() {
                    r12.silent || (p.error = Function.prototype.bind.call(console.error, console, r12.name + ":"), p.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        r12.performance && (n = (t = (new Date).getTime()) - (y || t), y = t, x.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: f,
                            "Execution Time": n
                        })), clearTimeout(p.performance.timer), p.performance.timer = setTimeout(p.performance.display, 500);
                    },
                    display: function() {
                        var e = r12.name + ":", n = 0;
                        y = !1, clearTimeout(p.performance.timer), T.each(x, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", b && (e += " '" + b + "'"), (console.group !== D || console.table !== D) && 0 < x.length && (console.groupCollapsed(e), console.table ? console.table(x) : T.each(x, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), x = [];
                    }
                },
                invoke: function(i, e69, t35) {
                    var o, a, n, r = m;
                    return e69 = e69 || k, t35 = f || t35, "string" == typeof i && r !== D && (i = i.split(/[\. ]/), o = i.length - 1, T.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (T.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== D) return a = r[n], !1;
                            if (!T.isPlainObject(r[t]) || e == o) return r[t] !== D ? a = r[t] : p.error(s6.method, i), !1;
                            r = r[t];
                        }
                    })), T.isFunction(a) ? n = a.apply(t35, e69) : a !== D && (n = a), Array.isArray(v) ? v.push(n) : v !== D ? v = [
                        v,
                        n
                    ] : n !== D && (v = n), a;
                }
            };
            w ? (m === D && p.initialize(), p.invoke(C)) : (m !== D && m.invoke("destroy"), p.initialize());
        }), v !== D ? v : this;
    }, T.fn.progress.settings = {
        name: "Progress",
        namespace: "progress",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        random: {
            min: 2,
            max: 5
        },
        duration: 300,
        updateInterval: "auto",
        autoSuccess: !0,
        showActivity: !0,
        limitValues: !0,
        label: "percent",
        precision: 0,
        framerate: 1000 / 30,
        percent: !1,
        total: !1,
        value: !1,
        failSafeDelay: 100,
        onLabelUpdate: function(e, t, n, i) {
            return t;
        },
        onChange: function(e, t, n) {
        },
        onSuccess: function(e) {
        },
        onActive: function(e, t) {
        },
        onError: function(e, t) {
        },
        onWarning: function(e, t) {
        },
        error: {
            method: "The method you called is not defined.",
            nonNumeric: "Progress value is non numeric",
            tooHigh: "Value specified is above 100%",
            tooLow: "Value specified is below 0%",
            sumExceedsTotal: "Sum of multple values exceed total"
        },
        regExp: {
            variable: /\{\$*[A-z0-9]+\}/g
        },
        metadata: {
            percent: "percent",
            total: "total",
            value: "value"
        },
        selector: {
            bar: "> .bar",
            label: "> .label",
            progress: ".bar > .progress"
        },
        text: {
            active: !1,
            error: !1,
            success: !1,
            warning: !1,
            percent: "{percent}%",
            ratio: "{value} of {total}",
            bars: [
                ""
            ]
        },
        className: {
            active: "active",
            error: "error",
            success: "success",
            warning: "warning"
        }
    };
})(jQuery, window, document), (function(U, t36, B, W) {
    "use strict";
    t36 = void 0 !== t36 && t36.Math == Math ? t36 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), U.fn.slider = function(O) {
        var R, e70 = U(this), M = U(t36), I = e70.selector || "", j = (new Date).getTime(), L = [], V = O, q = "string" == typeof V, z = [].slice.call(arguments, 1), N = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        ], H = 0;
        return e70.each(function() {
            var c, r13, s7, e71, u, l7, t37, d, f, m, g, o13, n14, p, a13, h, v = U.isPlainObject(O) ? U.extend(!0, {
            }, U.fn.slider.settings, O) : U.extend({
            }, U.fn.slider.settings), i13 = v.className, b = v.metadata, y = v.namespace, x = v.error, C = v.keys, w = v.interpretLabel, k = !1, T = "." + y, S = "module-" + y, D = U(this), A = this, E = D.data(S), F = 1, P = {
                initialize: function() {
                    P.debug("Initializing slider", v), h = !0, t37 = H += 1, n14 = P.setup.testOutTouch(), P.setup.layout(), P.setup.labels(), P.is.disabled() || P.bind.events(), P.read.metadata(), P.read.settings(), h = !1, P.instantiate();
                },
                instantiate: function() {
                    P.verbose("Storing instance of slider", P), E = P, D.data(S, P);
                },
                destroy: function() {
                    P.verbose("Destroying previous slider for", D), clearInterval(E.interval), P.unbind.events(), P.unbind.slidingEvents(), D.removeData(S), E = W;
                },
                setup: {
                    layout: function() {
                        D.attr("tabindex") === W && D.attr("tabindex", 0), 0 == D.find(".inner").length && D.append("<div class='inner'><div class='track'></div><div class='track-fill'></div><div class='thumb'></div></div>"), o13 = P.get.precision(), r13 = D.find(".thumb:not(.second)"), c = r13, P.is.range() && (0 == D.find(".thumb.second").length && D.find(".inner").append("<div class='thumb second'></div>"), s7 = D.find(".thumb.second")), e71 = D.find(".track"), u = D.find(".track-fill"), g = r13.width() / 2;
                    },
                    labels: function() {
                        P.is.labeled() && (0 != (l7 = D.find(".labels:not(.auto)")).length ? P.setup.customLabel() : P.setup.autoLabel(), v.showLabelTicks && D.addClass(i13.ticked));
                    },
                    testOutTouch: function() {
                        try {
                            return B.createEvent("TouchEvent"), !0;
                        } catch (e) {
                            return !1;
                        }
                    },
                    customLabel: function() {
                        var n, e72 = l7.find(".label"), i = e72.length, o = P.get.min(), a = P.get.max();
                        e72.each(function(e) {
                            var t = U(this).attr("data-value");
                            n = t ? ((t = a < t ? a : t < o ? o : t) - o) / (a - o) : (e + 1) / (i + 1), P.update.labelPosition(n, U(this));
                        });
                    },
                    autoLabel: function() {
                        0 != (l7 = D.find(".labels")).length ? l7.empty() : l7 = D.append('<ul class="auto labels"></ul>').find(".labels");
                        for(var e = 0, t = P.get.numLabels(); e <= t; e++){
                            var n = P.get.label(e), n = "" !== n ? e % P.get.gapRatio() ? U('<li class="halftick label"></li>') : U('<li class="label">' + n + "</li>") : null;
                            n && (P.update.labelPosition(e / t, n), l7.append(n));
                        }
                    }
                },
                bind: {
                    events: function() {
                        P.bind.globalKeyboardEvents(), P.bind.keyboardEvents(), P.bind.mouseEvents(), P.is.touch() && P.bind.touchEvents(), v.autoAdjustLabels && P.bind.windowEvents();
                    },
                    keyboardEvents: function() {
                        P.verbose("Binding keyboard events"), D.on("keydown" + T, P.event.keydown);
                    },
                    globalKeyboardEvents: function() {
                        U(B).on("keydown" + T + t37, P.event.activateFocus);
                    },
                    mouseEvents: function() {
                        P.verbose("Binding mouse events"), D.find(".track, .thumb, .inner").on("mousedown" + T, function(e) {
                            e.stopImmediatePropagation(), e.preventDefault(), P.event.down(e);
                        }), D.on("mousedown" + T, P.event.down), D.on("mouseenter" + T, function(e) {
                            k = !0;
                        }), D.on("mouseleave" + T, function(e) {
                            k = !1;
                        });
                    },
                    touchEvents: function() {
                        P.verbose("Binding touch events"), D.find(".track, .thumb, .inner").on("touchstart" + T, function(e) {
                            e.stopImmediatePropagation(), e.preventDefault(), P.event.down(e);
                        }), D.on("touchstart" + T, P.event.down);
                    },
                    slidingEvents: function() {
                        P.verbose("Binding page wide events while handle is being draged"), P.is.touch() ? (U(B).on("touchmove" + T, P.event.move), U(B).on("touchend" + T, P.event.up)) : (U(B).on("mousemove" + T, P.event.move), U(B).on("mouseup" + T, P.event.up));
                    },
                    windowEvents: function() {
                        M.on("resize" + T, P.event.resize);
                    }
                },
                unbind: {
                    events: function() {
                        D.find(".track, .thumb, .inner").off("mousedown" + T), D.find(".track, .thumb, .inner").off("touchstart" + T), D.off("mousedown" + T), D.off("mouseenter" + T), D.off("mouseleave" + T), D.off("touchstart" + T), D.off("keydown" + T), D.off("focusout" + T), U(B).off("keydown" + T + t37, P.event.activateFocus), M.off("resize" + T);
                    },
                    slidingEvents: function() {
                        P.is.touch() ? (U(B).off("touchmove" + T), U(B).off("touchend" + T)) : (U(B).off("mousemove" + T), U(B).off("mouseup" + T));
                    }
                },
                event: {
                    down: function(e) {
                        e.preventDefault(), P.is.range() ? (e = P.determine.eventPos(e), e = P.determine.pos(e), c = v.preventCrossover && P.is.range() && P.thumbVal === P.secondThumbVal ? (a13 = e, W) : P.determine.closestThumb(e), p === W && (p = P.get.currentThumbValue())) : p === W && (p = P.get.value()), P.is.disabled() || P.bind.slidingEvents();
                    },
                    move: function(e73) {
                        e73.preventDefault();
                        var t38, n15, i = P.determine.valueFromEvent(e73);
                        c === W && (t38 = P.determine.eventPos(e73), n15 = P.determine.pos(t38), c = n15 < a13 ? r13 : s7), 0 == P.get.step() || P.is.smooth() ? (t38 = P.thumbVal, n15 = P.secondThumbVal, e73 = P.determine.smoothValueFromEvent(e73), c.hasClass("second") ? (v.preventCrossover && P.is.range() && (i = Math.max(t38, i), e73 = Math.max(t38, e73)), n15 = i) : (v.preventCrossover && P.is.range() && (i = Math.min(n15, i), e73 = Math.min(n15, e73)), t38 = i), i = Math.abs(t38 - (n15 || 0)), P.update.position(e73), v.onMove.call(A, i, t38, n15)) : P.update.value(i, function(e, t, n) {
                            v.onMove.call(A, e, t, n);
                        });
                    },
                    up: function(e) {
                        e.preventDefault();
                        e = P.determine.valueFromEvent(e);
                        P.set.value(e), P.unbind.slidingEvents(), p !== W && (p = W);
                    },
                    keydown: function(e, t) {
                        if (v.preventCrossover && P.is.range() && P.thumbVal === P.secondThumbVal && (c = W), P.is.focused() && U(B).trigger(e), t || P.is.focused()) {
                            t = P.determine.keyMovement(e);
                            if (0 != t) switch(e.preventDefault(), t){
                                case 1:
                                    P.takeStep();
                                    break;
                                case 2:
                                    P.takeStep(P.get.multiplier());
                                    break;
                                case -1:
                                    P.backStep();
                                    break;
                                case -2:
                                    P.backStep(P.get.multiplier());
                            }
                        }
                    },
                    activateFocus: function(e) {
                        !P.is.focused() && P.is.hover() && 0 != P.determine.keyMovement(e) && (e.preventDefault(), P.event.keydown(e, !0), D.focus());
                    },
                    resize: function(e) {
                        F != P.get.gapRatio() && (P.setup.labels(), F = P.get.gapRatio());
                    }
                },
                resync: function() {
                    P.verbose("Resyncing thumb position based on value"), P.is.range() && P.update.position(P.secondThumbVal, s7), P.update.position(P.thumbVal, r13), P.setup.labels();
                },
                takeStep: function(e) {
                    var e = e != W ? e : 1, t = P.get.step(), n = P.get.currentThumbValue();
                    P.verbose("Taking a step"), 0 < t ? P.set.value(n + t * e) : 0 == t && (t = P.get.precision(), P.set.value(Math.round((n + e / t) * t) / t));
                },
                backStep: function(e) {
                    var e = e != W ? e : 1, t = P.get.step(), n = P.get.currentThumbValue();
                    P.verbose("Going back a step"), 0 < t ? P.set.value(n - t * e) : 0 == t && (t = P.get.precision(), P.set.value(Math.round((n - e / t) * t) / t));
                },
                is: {
                    range: function() {
                        return D.hasClass(v.className.range);
                    },
                    hover: function() {
                        return k;
                    },
                    focused: function() {
                        return D.is(":focus");
                    },
                    disabled: function() {
                        return D.hasClass(v.className.disabled);
                    },
                    labeled: function() {
                        return D.hasClass(v.className.labeled);
                    },
                    reversed: function() {
                        return D.hasClass(v.className.reversed);
                    },
                    vertical: function() {
                        return D.hasClass(v.className.vertical);
                    },
                    smooth: function() {
                        return v.smooth || D.hasClass(v.className.smooth);
                    },
                    touch: function() {
                        return n14;
                    }
                },
                get: {
                    trackOffset: function() {
                        return P.is.vertical() ? e71.offset().top : e71.offset().left;
                    },
                    trackLength: function() {
                        return P.is.vertical() ? e71.height() : e71.width();
                    },
                    trackLeft: function() {
                        return P.is.vertical() ? e71.position().top : e71.position().left;
                    },
                    trackStartPos: function() {
                        return P.is.reversed() ? P.get.trackLeft() + P.get.trackLength() : P.get.trackLeft();
                    },
                    trackEndPos: function() {
                        return P.is.reversed() ? P.get.trackLeft() : P.get.trackLeft() + P.get.trackLength();
                    },
                    trackStartMargin: function() {
                        var e = P.is.vertical() ? P.is.reversed() ? D.css("padding-bottom") : D.css("padding-top") : P.is.reversed() ? D.css("padding-right") : D.css("padding-left");
                        return e || "0px";
                    },
                    trackEndMargin: function() {
                        var e = P.is.vertical() ? P.is.reversed() ? D.css("padding-top") : D.css("padding-bottom") : P.is.reversed() ? D.css("padding-left") : D.css("padding-right");
                        return e || "0px";
                    },
                    precision: function() {
                        var e = P.get.step();
                        t = 0 != e ? 2 == (t = String(e).split(".")).length ? t[1].length : 0 : v.decimalPlaces;
                        var t = Math.pow(10, t);
                        return P.debug("Precision determined", t), t;
                    },
                    min: function() {
                        return v.min;
                    },
                    max: function() {
                        var e = P.get.step(), t = P.get.min(), n = 0 === e ? 0 : Math.floor((v.max - t) / e);
                        return 0 == (0 === e ? 0 : (v.max - t) % e) ? v.max : t + n * e;
                    },
                    step: function() {
                        return v.step;
                    },
                    numLabels: function() {
                        var e = Math.round((P.get.max() - P.get.min()) / (0 === P.get.step() ? 1 : P.get.step()));
                        return P.debug("Determined that there should be " + e + " labels"), e;
                    },
                    labelType: function() {
                        return v.labelType;
                    },
                    label: function(e) {
                        if (w) return w(e);
                        switch(v.labelType){
                            case v.labelTypes.number:
                                return Math.round((e * (0 === P.get.step() ? 1 : P.get.step()) + P.get.min()) * o13) / o13;
                            case v.labelTypes.letter:
                                return N[e % 26];
                            default:
                                return e;
                        }
                    },
                    value: function() {
                        return d;
                    },
                    currentThumbValue: function() {
                        return c !== W && c.hasClass("second") ? P.secondThumbVal : P.thumbVal;
                    },
                    thumbValue: function(e) {
                        return "second" !== e ? P.thumbVal : P.is.range() ? P.secondThumbVal : void P.error(x.notrange);
                    },
                    multiplier: function() {
                        return v.pageMultiplier;
                    },
                    thumbPosition: function(e) {
                        return "second" !== e ? f : P.is.range() ? m : void P.error(x.notrange);
                    },
                    gapRatio: function() {
                        var e = 1;
                        if (v.autoAdjustLabels) {
                            var t = P.get.numLabels(), n = P.get.trackLength(), i = 1;
                            if (0 < n) for(; n / t * i < v.labelDistance;)t % i || (e = i), i += 1;
                        }
                        return e;
                    }
                },
                determine: {
                    pos: function(e) {
                        return P.is.reversed() ? P.get.trackStartPos() - e + P.get.trackOffset() : e - P.get.trackOffset() - P.get.trackStartPos();
                    },
                    closestThumb: function(e) {
                        var t = parseFloat(P.determine.thumbPos(r13)), n = Math.abs(e - t), t = parseFloat(P.determine.thumbPos(s7)), t = Math.abs(e - t);
                        return (n !== t || P.get.thumbValue() !== P.get.min()) && n <= t ? r13 : s7;
                    },
                    closestThumbPos: function(e) {
                        var t = parseFloat(P.determine.thumbPos(r13)), n = Math.abs(e - t), i = parseFloat(P.determine.thumbPos(s7));
                        return n <= Math.abs(e - i) ? t : i;
                    },
                    thumbPos: function(e) {
                        return P.is.vertical() ? P.is.reversed() ? e.css("bottom") : e.css("top") : P.is.reversed() ? e.css("right") : e.css("left");
                    },
                    positionFromValue: function(e) {
                        var t = P.get.min(), n = P.get.max(), e = n < e ? n : e < t ? t : e, i = P.get.trackLength(), i = Math.round((e - t) / (n - t) * i);
                        return P.verbose("Determined position: " + i + " from value: " + e), i;
                    },
                    positionFromRatio: function(e) {
                        var t = P.get.trackLength(), n = P.get.step(), t = Math.round(e * t);
                        return 0 == n ? t : Math.round(t / n) * n;
                    },
                    valueFromEvent: function(e) {
                        var t = P.determine.eventPos(e), e = P.determine.pos(t), e = t < P.get.trackOffset() ? P.is.reversed() ? P.get.max() : P.get.min() : t > P.get.trackOffset() + P.get.trackLength() ? P.is.reversed() ? P.get.min() : P.get.max() : P.determine.value(e);
                        return e;
                    },
                    smoothValueFromEvent: function(e) {
                        var t = P.get.min(), n = P.get.max(), i = P.get.trackLength(), e = P.determine.eventPos(e) - P.get.trackOffset(), i = (e = e < 0 ? 0 : i < e ? i : e) / i;
                        return (i = P.is.reversed() ? 1 - i : i) * (n - t) + t;
                    },
                    eventPos: function(e) {
                        if (P.is.touch()) {
                            var t = e.changedTouches ? e : e.originalEvent, n = t.changedTouches[0] ? t.changedTouches : t.touches, t = n[0].pageY, n = n[0].pageX;
                            return P.is.vertical() ? t : n;
                        }
                        n = e.pageY || e.originalEvent.pageY, e = e.pageX || e.originalEvent.pageX;
                        return P.is.vertical() ? n : e;
                    },
                    value: function(e) {
                        var t = P.is.reversed() ? P.get.trackEndPos() : P.get.trackStartPos(), n = (e - t) / ((P.is.reversed() ? P.get.trackStartPos() : P.get.trackEndPos()) - t), i = P.get.max() - P.get.min(), t = P.get.step(), i = n * i, t = 0 == t ? i : Math.round(i / t) * t;
                        return P.verbose("Determined value based upon position: " + e + " as: " + i), i != t && P.verbose("Rounding value to closest step: " + t), P.verbose("Cutting off additional decimal places"), Math.round((t + P.get.min()) * o13) / o13;
                    },
                    keyMovement: function(e) {
                        var t = e.which, n = !P.is.vertical() || P.is.reversed() ? C.downArrow : C.upArrow, i = !P.is.vertical() || P.is.reversed() ? C.upArrow : C.downArrow, o = !P.is.vertical() && P.is.reversed() ? C.rightArrow : C.leftArrow, e = !P.is.vertical() && P.is.reversed() ? C.leftArrow : C.rightArrow;
                        return t == n || t == o ? -1 : t == i || t == e ? 1 : t == C.pageDown ? -2 : t == C.pageUp ? 2 : 0;
                    }
                },
                handleNewValuePosition: function(e) {
                    var t = P.get.min(), n = P.get.max();
                    return e <= t ? e = t : n <= e && (e = n), P.determine.positionFromValue(e);
                },
                set: {
                    value: function(i, o) {
                        o = !1 !== o;
                        var a = p === W;
                        p = p === W ? P.get.value() : p, P.update.value(i, function(e, t, n) {
                            h && !v.fireOnInit || !o || (i !== p && v.onChange.call(A, e, t, n), v.onMove.call(A, e, t, n)), a && (p = W);
                        });
                    },
                    rangeValue: function(e, t, n) {
                        var i, o, a;
                        n = !1 !== n, P.is.range() ? (i = P.get.min(), o = P.get.max(), p = (a = p === W) ? P.get.value() : p, e <= i ? e = i : o <= e && (e = o), t <= i ? t = i : o <= t && (t = o), P.thumbVal = e, P.secondThumbVal = t, d = Math.abs(P.thumbVal - P.secondThumbVal), P.update.position(P.thumbVal, r13), P.update.position(P.secondThumbVal, s7), h && !v.fireOnInit || !n || (d !== p && v.onChange.call(A, d, P.thumbVal, P.secondThumbVal), v.onMove.call(A, d, P.thumbVal, P.secondThumbVal)), a && (p = W)) : P.error(x.notrange);
                    },
                    position: function(e, t) {
                        e = P.determine.value(e);
                        "second" === t ? (P.secondThumbVal = e, P.update.position(e, s7)) : (P.thumbVal = e, P.update.position(e, r13)), d = Math.abs(P.thumbVal - (P.secondThumbVal || 0)), P.set.value(d);
                    }
                },
                update: {
                    value: function(e, t) {
                        var n = P.get.min(), i = P.get.max();
                        e <= n ? e = n : i <= e && (e = i), P.is.range() ? ((c = c === W ? e <= P.get.currentThumbValue() ? r13 : s7 : c).hasClass("second") ? (v.preventCrossover && P.is.range() && (e = Math.max(P.thumbVal, e)), P.secondThumbVal = e) : (v.preventCrossover && P.is.range() && (e = Math.min(P.secondThumbVal, e)), P.thumbVal = e), d = Math.abs(P.thumbVal - P.secondThumbVal)) : (d = e, P.thumbVal = d), P.update.position(e), P.debug("Setting slider value to " + d), "function" == typeof t && t(d, P.thumbVal, P.secondThumbVal);
                    },
                    position: function(e, t) {
                        var n = P.handleNewValuePosition(e), i = t != W ? t : c, o = P.thumbVal || P.get.min(), a = P.secondThumbVal || P.get.min();
                        P.is.range() && i.hasClass("second") ? (m = n, a = e) : (f = n, o = e);
                        var r, s = P.get.min(), l = P.get.max(), t = 100 * (e - s) / (l - s), e = 100 * (Math.min(o, a) - s) / (l - s), s = 100 * (1 - (Math.max(o, a) - s) / (l - s)), s = P.is.vertical() ? P.is.reversed() ? (r = {
                            bottom: "calc(" + t + "% - " + g + "px)",
                            top: "auto"
                        }, {
                            bottom: e + "%",
                            top: s + "%"
                        }) : (r = {
                            top: "calc(" + t + "% - " + g + "px)",
                            bottom: "auto"
                        }, {
                            top: e + "%",
                            bottom: s + "%"
                        }) : P.is.reversed() ? (r = {
                            right: "calc(" + t + "% - " + g + "px)",
                            left: "auto"
                        }, {
                            right: e + "%",
                            left: s + "%"
                        }) : (r = {
                            left: "calc(" + t + "% - " + g + "px)",
                            right: "auto"
                        }, {
                            left: e + "%",
                            right: s + "%"
                        });
                        i.css(r), u.css(s), P.debug("Setting slider position to " + n);
                    },
                    labelPosition: function(e, t) {
                        var n = P.get.trackStartMargin(), i = P.get.trackEndMargin(), o = P.is.vertical() ? P.is.reversed() ? "bottom" : "top" : P.is.reversed() ? "right" : "left", a = P.is.reversed() && !P.is.vertical() ? " - " : " + ";
                        t.css(o, "calc(" + ("(100% - " + n + " - " + i + ") * " + e) + a + n + ")");
                    }
                },
                goto: {
                    max: function() {
                        P.set.value(P.get.max());
                    },
                    min: function() {
                        P.set.value(P.get.min());
                    }
                },
                read: {
                    metadata: function() {
                        var e = {
                            thumbVal: D.data(b.thumbVal),
                            secondThumbVal: D.data(b.secondThumbVal)
                        };
                        e.thumbVal && (P.is.range() && e.secondThumbVal ? (P.debug("Current value set from metadata", e.thumbVal, e.secondThumbVal), P.set.rangeValue(e.thumbVal, e.secondThumbVal)) : (P.debug("Current value set from metadata", e.thumbVal), P.set.value(e.thumbVal)));
                    },
                    settings: function() {
                        !1 !== v.start && (P.is.range() ? (P.debug("Start position set from settings", v.start, v.end), P.set.rangeValue(v.start, v.end)) : (P.debug("Start position set from settings", v.start), P.set.value(v.start)));
                    }
                },
                setting: function(e, t) {
                    if (P.debug("Changing setting", e, t), U.isPlainObject(e)) U.extend(!0, v, e);
                    else {
                        if (t === W) return v[e];
                        U.isPlainObject(v[e]) ? U.extend(!0, v[e], t) : v[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (U.isPlainObject(e)) U.extend(!0, P, e);
                    else {
                        if (t === W) return P[e];
                        P[e] = t;
                    }
                },
                debug: function() {
                    !v.silent && v.debug && (v.performance ? P.performance.log(arguments) : (P.debug = Function.prototype.bind.call(console.info, console, v.name + ":"), P.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !v.silent && v.verbose && v.debug && (v.performance ? P.performance.log(arguments) : (P.verbose = Function.prototype.bind.call(console.info, console, v.name + ":"), P.verbose.apply(console, arguments)));
                },
                error: function() {
                    v.silent || (P.error = Function.prototype.bind.call(console.error, console, v.name + ":"), P.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        v.performance && (n = (t = (new Date).getTime()) - (j || t), j = t, L.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: A,
                            "Execution Time": n
                        })), clearTimeout(P.performance.timer), P.performance.timer = setTimeout(P.performance.display, 500);
                    },
                    display: function() {
                        var e = v.name + ":", n = 0;
                        j = !1, clearTimeout(P.performance.timer), U.each(L, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", I && (e += " '" + I + "'"), (console.group !== W || console.table !== W) && 0 < L.length && (console.groupCollapsed(e), console.table ? console.table(L) : U.each(L, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), L = [];
                    }
                },
                invoke: function(i, e74, t39) {
                    var o, a, n, r = E;
                    return e74 = e74 || z, t39 = A || t39, "string" == typeof i && r !== W && (i = i.split(/[\. ]/), o = i.length - 1, U.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (U.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== W) return a = r[n], !1;
                            if (!U.isPlainObject(r[t]) || e == o) return r[t] !== W ? a = r[t] : P.error(x.method, i), !1;
                            r = r[t];
                        }
                    })), U.isFunction(a) ? n = a.apply(t39, e74) : a !== W && (n = a), U.isArray(R) ? R.push(n) : R !== W ? R = [
                        R,
                        n
                    ] : n !== W && (R = n), a;
                }
            };
            q ? (E === W && P.initialize(), P.invoke(V)) : (E !== W && E.invoke("destroy"), P.initialize());
        }), R !== W ? R : this;
    }, U.fn.slider.settings = {
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        name: "Slider",
        namespace: "slider",
        error: {
            method: "The method you called is not defined.",
            notrange: "This slider is not a range slider"
        },
        metadata: {
            thumbVal: "thumbVal",
            secondThumbVal: "secondThumbVal"
        },
        min: 0,
        max: 20,
        step: 1,
        start: 0,
        end: 20,
        labelType: "number",
        showLabelTicks: !1,
        smooth: !1,
        autoAdjustLabels: !0,
        labelDistance: 100,
        preventCrossover: !0,
        fireOnInit: !1,
        interpretLabel: !1,
        decimalPlaces: 2,
        pageMultiplier: 2,
        selector: {
        },
        className: {
            reversed: "reversed",
            disabled: "disabled",
            labeled: "labeled",
            ticked: "ticked",
            vertical: "vertical",
            range: "range",
            smooth: "smooth"
        },
        keys: {
            pageUp: 33,
            pageDown: 34,
            leftArrow: 37,
            upArrow: 38,
            rightArrow: 39,
            downArrow: 40
        },
        labelTypes: {
            number: "number",
            letter: "letter"
        },
        onChange: function(e, t, n) {
        },
        onMove: function(e, t, n) {
        }
    };
})(jQuery, window, document), (function(k, e75, T) {
    "use strict";
    k.isFunction = k.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e75 = void 0 !== e75 && e75.Math == Math ? e75 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), k.fn.rating = function(g) {
        var p, h = k(this), v = h.selector || "", b = (new Date).getTime(), y = [], x = g, C = "string" == typeof x, w = [].slice.call(arguments, 1);
        return h.each(function() {
            var e76, o14 = k.isPlainObject(g) ? k.extend(!0, {
            }, k.fn.rating.settings, g) : k.extend({
            }, k.fn.rating.settings), t40 = o14.namespace, a14 = o14.className, n16 = o14.metadata, i14 = o14.selector, r14 = o14.cssVars, s = "." + t40, l = "module-" + t40, c = this, u = k(this).data(l), d = k(this), f = d.find(i14.icon), m = {
                initialize: function() {
                    m.verbose("Initializing rating module", o14), 0 === f.length && m.setup.layout(), o14.interactive && !m.is.disabled() ? m.enable() : m.disable(), m.set.initialLoad(), m.set.rating(m.get.initialRating()), m.remove.initialLoad(), m.instantiate();
                },
                instantiate: function() {
                    m.verbose("Instantiating module", o14), u = m, d.data(l, m);
                },
                destroy: function() {
                    m.verbose("Destroying previous instance", u), m.remove.events(), d.removeData(l);
                },
                refresh: function() {
                    f = d.find(i14.icon);
                },
                setup: {
                    layout: function() {
                        var e = m.get.maxRating(), t = m.get.icon(), t = k.fn.rating.settings.templates.icon(e, t);
                        m.debug("Generating icon html dynamically"), d.html(t), m.refresh();
                    }
                },
                event: {
                    mouseenter: function() {
                        var e = k(this);
                        e.nextAll().removeClass(a14.selected), d.addClass(a14.selected), e.addClass(a14.selected).prevAll().addClass(a14.selected);
                    },
                    mouseleave: function() {
                        d.removeClass(a14.selected), f.removeClass(a14.selected);
                    },
                    click: function() {
                        var e = k(this), t = m.get.rating(), e = f.index(e) + 1;
                        ("auto" == o14.clearable ? 1 === f.length : o14.clearable) && t == e ? m.clearRating() : m.set.rating(e);
                    }
                },
                clearRating: function() {
                    m.debug("Clearing current rating"), m.set.rating(0);
                },
                bind: {
                    events: function() {
                        m.verbose("Binding events"), d.on("mouseenter" + s, i14.icon, m.event.mouseenter).on("mouseleave" + s, i14.icon, m.event.mouseleave).on("click" + s, i14.icon, m.event.click);
                    }
                },
                remove: {
                    events: function() {
                        m.verbose("Removing events"), d.off(s);
                    },
                    initialLoad: function() {
                        e76 = !1;
                    }
                },
                enable: function() {
                    m.debug("Setting rating to interactive mode"), m.bind.events(), d.removeClass(a14.disabled);
                },
                disable: function() {
                    m.debug("Setting rating to read-only mode"), m.remove.events(), d.addClass(a14.disabled);
                },
                is: {
                    initialLoad: function() {
                        return e76;
                    },
                    disabled: function() {
                        return d.hasClass(a14.disabled);
                    }
                },
                get: {
                    icon: function() {
                        var e = d.data(n16.icon);
                        return e && d.removeData(n16.icon), e || o14.icon;
                    },
                    initialRating: function() {
                        return d.data(n16.rating) !== T ? (d.removeData(n16.rating), d.data(n16.rating)) : o14.initialRating;
                    },
                    maxRating: function() {
                        return d.data(n16.maxRating) !== T ? (d.removeData(n16.maxRating), d.data(n16.maxRating)) : o14.maxRating;
                    },
                    rating: function() {
                        var e = f.filter("." + a14.active).length;
                        return m.verbose("Current rating retrieved", e), e;
                    }
                },
                set: {
                    rating: function(e) {
                        var t = Math.floor(0 <= e - 1 ? e - 1 : 0), n = f.eq(t), i = e <= 1 ? n : n.next(), t = e % 1 * 100;
                        d.removeClass(a14.selected), f.removeClass(a14.selected).removeClass(a14.active).removeClass(a14.partiallyActive), 0 < e && (m.verbose("Setting current rating to", e), n.prevAll().addBack().addClass(a14.active), n.next() && e % 1 != 0 && (i.addClass(a14.partiallyActive).addClass(a14.active), i.css(r14.filledCustomPropName, t + "%"), "transparent" === i.css("backgroundColor") && i.removeClass(a14.partiallyActive).removeClass(a14.active))), m.is.initialLoad() || o14.onRate.call(c, e);
                    },
                    initialLoad: function() {
                        e76 = !0;
                    }
                },
                setting: function(e, t) {
                    if (m.debug("Changing setting", e, t), k.isPlainObject(e)) k.extend(!0, o14, e);
                    else {
                        if (t === T) return o14[e];
                        k.isPlainObject(o14[e]) ? k.extend(!0, o14[e], t) : o14[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (k.isPlainObject(e)) k.extend(!0, m, e);
                    else {
                        if (t === T) return m[e];
                        m[e] = t;
                    }
                },
                debug: function() {
                    !o14.silent && o14.debug && (o14.performance ? m.performance.log(arguments) : (m.debug = Function.prototype.bind.call(console.info, console, o14.name + ":"), m.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !o14.silent && o14.verbose && o14.debug && (o14.performance ? m.performance.log(arguments) : (m.verbose = Function.prototype.bind.call(console.info, console, o14.name + ":"), m.verbose.apply(console, arguments)));
                },
                error: function() {
                    o14.silent || (m.error = Function.prototype.bind.call(console.error, console, o14.name + ":"), m.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        o14.performance && (n = (t = (new Date).getTime()) - (b || t), b = t, y.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: c,
                            "Execution Time": n
                        })), clearTimeout(m.performance.timer), m.performance.timer = setTimeout(m.performance.display, 500);
                    },
                    display: function() {
                        var e = o14.name + ":", n = 0;
                        b = !1, clearTimeout(m.performance.timer), k.each(y, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", v && (e += " '" + v + "'"), 1 < h.length && (e += " (" + h.length + ")"), (console.group !== T || console.table !== T) && 0 < y.length && (console.groupCollapsed(e), console.table ? console.table(y) : k.each(y, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), y = [];
                    }
                },
                invoke: function(i, e77, t41) {
                    var o, a, n, r = u;
                    return e77 = e77 || w, t41 = c || t41, "string" == typeof i && r !== T && (i = i.split(/[\. ]/), o = i.length - 1, k.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (k.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== T) return a = r[n], !1;
                            if (!k.isPlainObject(r[t]) || e == o) return r[t] !== T && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), k.isFunction(a) ? n = a.apply(t41, e77) : a !== T && (n = a), Array.isArray(p) ? p.push(n) : p !== T ? p = [
                        p,
                        n
                    ] : n !== T && (p = n), a;
                }
            };
            C ? (u === T && m.initialize(), m.invoke(x)) : (u !== T && u.invoke("destroy"), m.initialize());
        }), p !== T ? p : this;
    }, k.fn.rating.settings = {
        name: "Rating",
        namespace: "rating",
        icon: "star",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        initialRating: 0,
        interactive: !0,
        maxRating: 4,
        clearable: "auto",
        fireOnInit: !1,
        onRate: function(e) {
        },
        error: {
            method: "The method you called is not defined",
            noMaximum: "No maximum rating specified. Cannot generate HTML automatically"
        },
        metadata: {
            rating: "rating",
            maxRating: "maxRating",
            icon: "icon"
        },
        className: {
            active: "active",
            disabled: "disabled",
            selected: "selected",
            loading: "loading",
            partiallyActive: "partial"
        },
        cssVars: {
            filledCustomPropName: "--full"
        },
        selector: {
            icon: ".icon"
        },
        templates: {
            icon: function(e, t) {
                for(var n = 1, i = ""; n <= e;)i += '<i class="' + t + ' icon"></i>', n++;
                return i;
            }
        }
    };
})(jQuery, window, void 0), (function(F, P, O, R) {
    "use strict";
    F.isFunction = F.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, P = void 0 !== P && P.Math == Math ? P : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), F.fn.search = function(x) {
        var C, w = F(this), k = w.selector || "", T = (new Date).getTime(), S = [], D = x, A = "string" == typeof D, E = [].slice.call(arguments, 1);
        return F(this).each(function() {
            var u5 = F.isPlainObject(x) ? F.extend(!0, {
            }, F.fn.search.settings, x) : F.extend({
            }, F.fn.search.settings), d = u5.className, l8 = u5.metadata, i15 = u5.regExp, a15 = u5.fields, f = u5.selector, m = u5.error, e78 = u5.namespace, o15 = "." + e78, t42 = e78 + "-module", g = F(this), p = g.find(f.prompt), n17 = g.find(f.searchButton), r15 = g.find(f.results), s8 = g.find(f.result), c5 = (g.find(f.category), this), h = g.data(t42), v = !1, b = !1, y = {
                initialize: function() {
                    y.verbose("Initializing module"), y.get.settings(), y.determine.searchFields(), y.bind.events(), y.set.type(), y.create.results(), y.instantiate();
                },
                instantiate: function() {
                    y.verbose("Storing instance of module", y), h = y, g.data(t42, y);
                },
                destroy: function() {
                    y.verbose("Destroying instance"), g.off(o15).removeData(t42);
                },
                refresh: function() {
                    y.debug("Refreshing selector cache"), p = g.find(f.prompt), n17 = g.find(f.searchButton), g.find(f.category), r15 = g.find(f.results), s8 = g.find(f.result);
                },
                refreshResults: function() {
                    r15 = g.find(f.results), s8 = g.find(f.result);
                },
                bind: {
                    events: function() {
                        y.verbose("Binding events to search"), u5.automatic && (g.on(y.get.inputEvent() + o15, f.prompt, y.event.input), p.attr("autocomplete", y.is.chrome() ? "fomantic-search" : "off")), g.on("focus" + o15, f.prompt, y.event.focus).on("blur" + o15, f.prompt, y.event.blur).on("keydown" + o15, f.prompt, y.handleKeyboard).on("click" + o15, f.searchButton, y.query).on("mousedown" + o15, f.results, y.event.result.mousedown).on("mouseup" + o15, f.results, y.event.result.mouseup).on("click" + o15, f.result, y.event.result.click);
                    }
                },
                determine: {
                    searchFields: function() {
                        x && x.searchFields !== R && (u5.searchFields = x.searchFields);
                    }
                },
                event: {
                    input: function() {
                        u5.searchDelay ? (clearTimeout(y.timer), y.timer = setTimeout(function() {
                            y.is.focused() && y.query();
                        }, u5.searchDelay)) : y.query();
                    },
                    focus: function() {
                        y.set.focus(), u5.searchOnFocus && y.has.minimumCharacters() && y.query(function() {
                            y.can.show() && y.showResults();
                        });
                    },
                    blur: function(e79) {
                        function t() {
                            y.cancel.query(), y.remove.focus(), y.timer = setTimeout(y.hideResults, u5.hideDelay);
                        }
                        var n = O.activeElement === this;
                        n || (b = !1, y.resultsClicked ? (y.debug("Determining if user action caused search to close"), g.one("click.close" + o15, f.results, function(e) {
                            y.is.inMessage(e) || v ? p.focus() : (v = !1, y.is.animating() || y.is.hidden() || t());
                        })) : (y.debug("Input blurred without user action, closing results"), t()));
                    },
                    result: {
                        mousedown: function() {
                            y.resultsClicked = !0;
                        },
                        mouseup: function() {
                            y.resultsClicked = !1;
                        },
                        click: function(e) {
                            y.debug("Search result selected");
                            var t = F(this), n = t.find(f.title).eq(0), i = t.is("a[href]") ? t : t.find("a[href]").eq(0), o = i.attr("href") || !1, a = i.attr("target") || !1, r = 0 < n.length && n.text(), s = y.get.results(), n = t.data(l8.result) || y.get.result(r, s), t = y.get.value();
                            if (F.isFunction(u5.onSelect) && !1 === u5.onSelect.call(c5, n, s)) return y.debug("Custom onSelect callback cancelled default select action"), void (v = !0);
                            y.hideResults(), r && y.get.value() === t && y.set.value(r), o && (e.preventDefault(), y.verbose("Opening search link found in result", i), "_blank" == a || e.ctrlKey ? P.open(o) : P.location.href = o);
                        }
                    }
                },
                ensureVisible: function(e) {
                    var t, n, i;
                    0 !== e.length && (n = (t = e.position().top) + e.outerHeight(!0), i = r15.scrollTop(), e = r15.height(), t < 0 ? r15.scrollTop(i + t) : e < n && r15.scrollTop(i + (n - e)));
                },
                handleKeyboard: function(e) {
                    var t, n = g.find(f.result), i = g.find(f.category), o = n.filter("." + d.active), a = n.index(o), r = n.length, s = 0 < o.length, l = e.which, c = 13, u = 38, o = 40;
                    if (l == 27 && (y.verbose("Escape key pressed, blurring search field"), y.hideResults(), b = !0), y.is.visible()) {
                        if (l == c) {
                            if (y.verbose("Enter key pressed, selecting active result"), 0 < n.filter("." + d.active).length) return y.event.result.click.call(n.filter("." + d.active), e), e.preventDefault(), !1;
                        } else l == u && s ? (y.verbose("Up key pressed, changing active result"), t = a - 1 < 0 ? a : a - 1, i.removeClass(d.active), n.removeClass(d.active).eq(t).addClass(d.active).closest(i).addClass(d.active), y.ensureVisible(n.eq(t)), e.preventDefault()) : l == o && (y.verbose("Down key pressed, changing active result"), t = r <= a + 1 ? a : a + 1, i.removeClass(d.active), n.removeClass(d.active).eq(t).addClass(d.active).closest(i).addClass(d.active), y.ensureVisible(n.eq(t)), e.preventDefault());
                    } else l == c && (y.verbose("Enter key pressed, executing query"), y.query(), y.set.buttonPressed(), p.one("keyup", y.remove.buttonFocus));
                },
                setup: {
                    api: function(t, n) {
                        var e80 = {
                            debug: u5.debug,
                            on: !1,
                            cache: u5.cache,
                            action: "search",
                            urlData: {
                                query: t
                            },
                            onSuccess: function(e) {
                                y.parse.response.call(c5, e, t), n();
                            },
                            onFailure: function() {
                                y.displayMessage(m.serverError), n();
                            },
                            onAbort: function(e) {
                            },
                            onError: y.error
                        };
                        F.extend(!0, e80, u5.apiSettings), y.verbose("Setting up API request", e80), g.api(e80);
                    }
                },
                can: {
                    useAPI: function() {
                        return F.fn.api !== R;
                    },
                    show: function() {
                        return y.is.focused() && !y.is.visible() && !y.is.empty();
                    },
                    transition: function() {
                        return u5.transition && F.fn.transition !== R && g.transition("is supported");
                    }
                },
                is: {
                    animating: function() {
                        return r15.hasClass(d.animating);
                    },
                    chrome: function() {
                        return !(!P.chrome || !P.chrome.webstore && !P.chrome.runtime);
                    },
                    hidden: function() {
                        return r15.hasClass(d.hidden);
                    },
                    inMessage: function(e) {
                        if (e.target) {
                            var t = F(e.target);
                            return F.contains(O.documentElement, e.target) && 0 < t.closest(f.message).length;
                        }
                    },
                    empty: function() {
                        return "" === r15.html();
                    },
                    visible: function() {
                        return 0 < r15.filter(":visible").length;
                    },
                    focused: function() {
                        return 0 < p.filter(":focus").length;
                    }
                },
                get: {
                    settings: function() {
                        F.isPlainObject(x) && x.searchFullText && (u5.fullTextSearch = x.searchFullText, y.error(u5.error.oldSearchSyntax, c5)), u5.ignoreDiacritics && !String.prototype.normalize && (u5.ignoreDiacritics = !1, y.error(m.noNormalize, c5));
                    },
                    inputEvent: function() {
                        var e = p[0];
                        return e !== R && e.oninput !== R ? "input" : e !== R && e.onpropertychange !== R ? "propertychange" : "keyup";
                    },
                    value: function() {
                        return p.val();
                    },
                    results: function() {
                        return g.data(l8.results);
                    },
                    result: function(n, e) {
                        var i = !1;
                        return n = n !== R ? n : y.get.value(), e = e !== R ? e : y.get.results(), "category" === u5.type ? (y.debug("Finding result that matches", n), F.each(e, function(e, t) {
                            if (Array.isArray(t.results) && (i = y.search.object(n, t.results)[0])) return !1;
                        })) : (y.debug("Finding result in results object", n), i = y.search.object(n, e)[0]), i || !1;
                    }
                },
                select: {
                    firstResult: function() {
                        y.verbose("Selecting first result"), s8.first().addClass(d.active);
                    }
                },
                set: {
                    focus: function() {
                        g.addClass(d.focus);
                    },
                    loading: function() {
                        g.addClass(d.loading);
                    },
                    value: function(e) {
                        y.verbose("Setting search input value", e), p.val(e);
                    },
                    type: function(e) {
                        e = e || u5.type, "category" == u5.type && g.addClass(u5.type);
                    },
                    buttonPressed: function() {
                        n17.addClass(d.pressed);
                    }
                },
                remove: {
                    loading: function() {
                        g.removeClass(d.loading);
                    },
                    focus: function() {
                        g.removeClass(d.focus);
                    },
                    buttonPressed: function() {
                        n17.removeClass(d.pressed);
                    },
                    diacritics: function(e) {
                        return u5.ignoreDiacritics ? e.normalize("NFD").replace(/[\u0300-\u036f]/g, "") : e;
                    }
                },
                query: function(e) {
                    e = F.isFunction(e) ? e : function() {
                    };
                    var t = y.get.value(), n = y.read.cache(t);
                    e = e || function() {
                    }, y.has.minimumCharacters() ? (n ? (y.debug("Reading result from cache", t), y.save.results(n.results), y.addResults(n.html), y.inject.id(n.results), e()) : (y.debug("Querying for", t), F.isPlainObject(u5.source) || Array.isArray(u5.source) ? (y.search.local(t), e()) : y.can.useAPI() ? y.search.remote(t, e) : (y.error(m.source), e())), u5.onSearchQuery.call(c5, t)) : y.hideResults();
                },
                search: {
                    local: function(e) {
                        var t, n = y.search.object(e, u5.source);
                        y.set.loading(), y.save.results(n), y.debug("Returned full local search results", n), 0 < u5.maxResults && (y.debug("Using specified max results", n), n = n.slice(0, u5.maxResults)), "category" == u5.type && (n = y.create.categoryResults(n)), t = y.generateResults({
                            results: n
                        }), y.remove.loading(), y.addResults(t), y.inject.id(n), y.write.cache(e, {
                            html: t,
                            results: n
                        });
                    },
                    remote: function(e, t) {
                        t = F.isFunction(t) ? t : function() {
                        }, g.api("is loading") && g.api("abort"), y.setup.api(e, t), g.api("query");
                    },
                    object: function(o16, t43, e81) {
                        o16 = y.remove.diacritics(String(o16));
                        function a(e, t) {
                            var n = -1 == F.inArray(t, r), i = -1 == F.inArray(t, l), o = -1 == F.inArray(t, s);
                            n && i && o && e.push(t);
                        }
                        var r = [], s = [], l = [], n18 = o16.replace(i15.escape, "\\$&"), c = new RegExp(i15.beginsWith + n18, "i");
                        return t43 = t43 || u5.source, e81 = e81 !== R ? e81 : u5.searchFields, Array.isArray(e81) || (e81 = [
                            e81
                        ]), t43 === R || !1 === t43 ? (y.error(m.source), []) : (F.each(e81, function(e, i) {
                            F.each(t43, function(e, t) {
                                var n;
                                "string" != typeof t[i] && "number" != typeof t[i] || (-1 !== (n = "string" == typeof t[i] ? y.remove.diacritics(t[i]) : t[i].toString()).search(c) ? a(r, t) : "exact" === u5.fullTextSearch && y.exactSearch(o16, n) ? a(s, t) : 1 == u5.fullTextSearch && y.fuzzySearch(o16, n) && a(l, t));
                            });
                        }), F.merge(s, l), F.merge(r, s), r);
                    }
                },
                exactSearch: function(e, t) {
                    return e = e.toLowerCase(), -1 < (t = t.toLowerCase()).indexOf(e);
                },
                fuzzySearch: function(e, t) {
                    var n = t.length, i = e.length;
                    if ("string" != typeof e) return !1;
                    if (e = e.toLowerCase(), t = t.toLowerCase(), n < i) return !1;
                    if (i === n) return e === t;
                    e: for(var o = 0, a = 0; o < i; o++){
                        for(var r = e.charCodeAt(o); a < n;)if (t.charCodeAt(a++) === r) continue e;
                        return !1;
                    }
                    return !0;
                },
                parse: {
                    response: function(e, t) {
                        Array.isArray(e) && ((n = {
                        })[a15.results] = e, e = n);
                        var n = y.generateResults(e);
                        y.verbose("Parsing server response", e), e !== R && t !== R && e[a15.results] !== R && (y.addResults(n), y.inject.id(e[a15.results]), y.write.cache(t, {
                            html: n,
                            results: e[a15.results]
                        }), y.save.results(e[a15.results]));
                    }
                },
                cancel: {
                    query: function() {
                        y.can.useAPI() && g.api("abort");
                    }
                },
                has: {
                    minimumCharacters: function() {
                        return y.get.value().length >= u5.minCharacters;
                    },
                    results: function() {
                        return 0 !== r15.length && "" != r15.html();
                    }
                },
                clear: {
                    cache: function(e) {
                        var t = g.data(l8.cache);
                        e ? t && t[e] && (y.debug("Removing value from cache", e), delete t[e], g.data(l8.cache, t)) : (y.debug("Clearing cache", e), g.removeData(l8.cache));
                    }
                },
                read: {
                    cache: function(e) {
                        var t = g.data(l8.cache);
                        return !!u5.cache && (y.verbose("Checking cache for generated html for query", e), "object" == typeof t && t[e] !== R && t[e]);
                    }
                },
                create: {
                    categoryResults: function(e) {
                        var n = {
                        };
                        return F.each(e, function(e, t) {
                            t.category && (n[t.category] === R ? (y.verbose("Creating new category of results", t.category), n[t.category] = {
                                name: t.category,
                                results: [
                                    t
                                ]
                            }) : n[t.category].results.push(t));
                        }), n;
                    },
                    id: function(e, t) {
                        var n, e = e + 1;
                        return t !== R ? (n = String.fromCharCode(97 + t), y.verbose("Creating category result id", n = n + e)) : y.verbose("Creating result id", n = e), n;
                    },
                    results: function() {
                        0 === r15.length && (r15 = F("<div />").addClass(d.results).appendTo(g));
                    }
                },
                inject: {
                    result: function(e, t, n) {
                        y.verbose("Injecting result into results");
                        t = (n !== R ? r15.children().eq(n).children(f.results).first() : r15).children(f.result).eq(t);
                        y.verbose("Injecting results metadata", t), t.data(l8.result, e);
                    },
                    id: function(e) {
                        y.debug("Injecting unique ids into results");
                        var n = 0, i = 0;
                        return "category" === u5.type ? F.each(e, function(e, t44) {
                            0 < t44.results.length && (i = 0, F.each(t44.results, function(e, t) {
                                t.id === R && (t.id = y.create.id(i, n)), y.inject.result(t, i, n), i++;
                            }), n++);
                        }) : F.each(e, function(e, t) {
                            t.id === R && (t.id = y.create.id(i)), y.inject.result(t, i), i++;
                        }), e;
                    }
                },
                save: {
                    results: function(e) {
                        y.verbose("Saving current search results to metadata", e), g.data(l8.results, e);
                    }
                },
                write: {
                    cache: function(e, t) {
                        var n = g.data(l8.cache) !== R ? g.data(l8.cache) : {
                        };
                        u5.cache && (y.verbose("Writing generated html to cache", e, t), n[e] = t, g.data(l8.cache, n));
                    }
                },
                addResults: function(e) {
                    if (F.isFunction(u5.onResultsAdd) && !1 === u5.onResultsAdd.call(r15, e)) return y.debug("onResultsAdd callback cancelled default action"), !1;
                    e ? (r15.html(e), y.refreshResults(), u5.selectFirstResult && y.select.firstResult(), y.showResults()) : y.hideResults(function() {
                        r15.empty();
                    });
                },
                showResults: function(e82) {
                    e82 = F.isFunction(e82) ? e82 : function() {
                    }, b || !y.is.visible() && y.has.results() && (y.can.transition() ? (y.debug("Showing results with css animations"), r15.transition({
                        animation: u5.transition + " in",
                        debug: u5.debug,
                        verbose: u5.verbose,
                        duration: u5.duration,
                        onShow: function() {
                            var e = g.find(f.result).eq(0);
                            y.ensureVisible(e);
                        },
                        onComplete: function() {
                            e82();
                        },
                        queue: !0
                    })) : (y.debug("Showing results with javascript"), r15.stop().fadeIn(u5.duration, u5.easing)), u5.onResultsOpen.call(r15));
                },
                hideResults: function(e) {
                    e = F.isFunction(e) ? e : function() {
                    }, y.is.visible() && (y.can.transition() ? (y.debug("Hiding results with css animations"), r15.transition({
                        animation: u5.transition + " out",
                        debug: u5.debug,
                        verbose: u5.verbose,
                        duration: u5.duration,
                        onComplete: function() {
                            e();
                        },
                        queue: !0
                    })) : (y.debug("Hiding results with javascript"), r15.stop().fadeOut(u5.duration, u5.easing)), u5.onResultsClose.call(r15));
                },
                generateResults: function(e) {
                    y.debug("Generating html from response", e);
                    var t = u5.templates[u5.type], n = F.isPlainObject(e[a15.results]) && !F.isEmptyObject(e[a15.results]), i = Array.isArray(e[a15.results]) && 0 < e[a15.results].length, o = "";
                    return n || i ? (0 < u5.maxResults && (n ? "standard" == u5.type && y.error(m.maxResults) : e[a15.results] = e[a15.results].slice(0, u5.maxResults)), F.isFunction(t) ? o = t(e, a15, u5.preserveHTML) : y.error(m.noTemplate, !1)) : u5.showNoResults && (o = y.displayMessage(m.noResults, "empty", m.noResultsHeader)), u5.onResults.call(c5, e), o;
                },
                displayMessage: function(e, t, n) {
                    return y.debug("Displaying message", e, t = t || "standard", n), y.addResults(u5.templates.message(e, t, n)), u5.templates.message(e, t, n);
                },
                setting: function(e, t) {
                    if (F.isPlainObject(e)) F.extend(!0, u5, e);
                    else {
                        if (t === R) return u5[e];
                        u5[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (F.isPlainObject(e)) F.extend(!0, y, e);
                    else {
                        if (t === R) return y[e];
                        y[e] = t;
                    }
                },
                debug: function() {
                    !u5.silent && u5.debug && (u5.performance ? y.performance.log(arguments) : (y.debug = Function.prototype.bind.call(console.info, console, u5.name + ":"), y.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !u5.silent && u5.verbose && u5.debug && (u5.performance ? y.performance.log(arguments) : (y.verbose = Function.prototype.bind.call(console.info, console, u5.name + ":"), y.verbose.apply(console, arguments)));
                },
                error: function() {
                    u5.silent || (y.error = Function.prototype.bind.call(console.error, console, u5.name + ":"), y.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        u5.performance && (n = (t = (new Date).getTime()) - (T || t), T = t, S.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: c5,
                            "Execution Time": n
                        })), clearTimeout(y.performance.timer), y.performance.timer = setTimeout(y.performance.display, 500);
                    },
                    display: function() {
                        var e = u5.name + ":", n = 0;
                        T = !1, clearTimeout(y.performance.timer), F.each(S, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", k && (e += " '" + k + "'"), 1 < w.length && (e += " (" + w.length + ")"), (console.group !== R || console.table !== R) && 0 < S.length && (console.groupCollapsed(e), console.table ? console.table(S) : F.each(S, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), S = [];
                    }
                },
                invoke: function(i, e83, t45) {
                    var o, a, n, r = h;
                    return e83 = e83 || E, t45 = c5 || t45, "string" == typeof i && r !== R && (i = i.split(/[\. ]/), o = i.length - 1, F.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (F.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== R) return a = r[n], !1;
                            if (!F.isPlainObject(r[t]) || e == o) return r[t] !== R && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), F.isFunction(a) ? n = a.apply(t45, e83) : a !== R && (n = a), Array.isArray(C) ? C.push(n) : C !== R ? C = [
                        C,
                        n
                    ] : n !== R && (C = n), a;
                }
            };
            A ? (h === R && y.initialize(), y.invoke(D)) : (h !== R && h.invoke("destroy"), y.initialize());
        }), C !== R ? C : this;
    }, F.fn.search.settings = {
        name: "Search",
        namespace: "search",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        type: "standard",
        minCharacters: 1,
        selectFirstResult: !1,
        apiSettings: !1,
        source: !1,
        searchOnFocus: !0,
        searchFields: [
            "id",
            "title",
            "description"
        ],
        displayField: "",
        fullTextSearch: "exact",
        ignoreDiacritics: !1,
        automatic: !0,
        hideDelay: 0,
        searchDelay: 200,
        maxResults: 7,
        cache: !0,
        showNoResults: !0,
        preserveHTML: !0,
        transition: "scale",
        duration: 200,
        easing: "easeOutExpo",
        onSelect: !1,
        onResultsAdd: !1,
        onSearchQuery: function(e) {
        },
        onResults: function(e) {
        },
        onResultsOpen: function() {
        },
        onResultsClose: function() {
        },
        className: {
            animating: "animating",
            active: "active",
            empty: "empty",
            focus: "focus",
            hidden: "hidden",
            loading: "loading",
            results: "results",
            pressed: "down"
        },
        error: {
            source: "Cannot search. No source used, and Semantic API module was not included",
            noResultsHeader: "No Results",
            noResults: "Your search returned no results",
            logging: "Error in debug logging, exiting.",
            noEndpoint: "No search endpoint was specified",
            noTemplate: "A valid template name was not specified.",
            oldSearchSyntax: "searchFullText setting has been renamed fullTextSearch for consistency, please adjust your settings.",
            serverError: "There was an issue querying the server.",
            maxResults: "Results must be an array to use maxResults setting",
            method: "The method you called is not defined.",
            noNormalize: '"ignoreDiacritics" setting will be ignored. Browser does not support String().normalize(). You may consider including <https://cdn.jsdelivr.net/npm/unorm@1.4.1/lib/unorm.min.js> as a polyfill.'
        },
        metadata: {
            cache: "cache",
            results: "results",
            result: "result"
        },
        regExp: {
            escape: /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,
            beginsWith: "(?:s|^)"
        },
        fields: {
            categories: "results",
            categoryName: "name",
            categoryResults: "results",
            description: "description",
            image: "image",
            price: "price",
            results: "results",
            title: "title",
            url: "url",
            action: "action",
            actionText: "text",
            actionURL: "url"
        },
        selector: {
            prompt: ".prompt",
            searchButton: ".search.button",
            results: ".results",
            message: ".results > .message",
            category: ".category",
            result: ".result",
            title: ".title, .name"
        },
        templates: {
            escape: function(e, t) {
                if (t) return e;
                var n = {
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#x27;",
                    "`": "&#x60;"
                };
                return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                    return n[e];
                }) : e;
            },
            message: function(e, t, n) {
                var i = "";
                return e !== R && t !== R && (i += '<div class="message ' + t + '">', n && (i += '<div class="header">' + n + "</div>"), i += ' <div class="description">' + e + "</div>", i += "</div>"), i;
            },
            category: function(e, n, i) {
                var o = "", a = F.fn.search.settings.templates.escape;
                return e[n.categoryResults] !== R && (F.each(e[n.categoryResults], function(e, t46) {
                    t46[n.results] !== R && 0 < t46.results.length && (o += '<div class="category">', t46[n.categoryName] !== R && (o += '<div class="name">' + a(t46[n.categoryName], i) + "</div>"), o += '<div class="results">', F.each(t46.results, function(e, t) {
                        t[n.url] ? o += '<a class="result" href="' + t[n.url].replace(/"/g, "") + '">' : o += '<a class="result">', t[n.image] !== R && (o += '<div class="image"> <img src="' + t[n.image].replace(/"/g, "") + '"></div>'), o += '<div class="content">', t[n.price] !== R && (o += '<div class="price">' + a(t[n.price], i) + "</div>"), t[n.title] !== R && (o += '<div class="title">' + a(t[n.title], i) + "</div>"), t[n.description] !== R && (o += '<div class="description">' + a(t[n.description], i) + "</div>"), o += "</div>", o += "</a>";
                    }), o += "</div>", o += "</div>");
                }), e[n.action] && (!1 === n.actionURL ? o += '<div class="action">' + a(e[n.action][n.actionText], i) + "</div>" : o += '<a href="' + e[n.action][n.actionURL].replace(/"/g, "") + '" class="action">' + a(e[n.action][n.actionText], i) + "</a>"), o);
            },
            standard: function(e, n, i) {
                var o = "", a = F.fn.search.settings.templates.escape;
                return e[n.results] !== R && (F.each(e[n.results], function(e, t) {
                    t[n.url] ? o += '<a class="result" href="' + t[n.url].replace(/"/g, "") + '">' : o += '<a class="result">', t[n.image] !== R && (o += '<div class="image"> <img src="' + t[n.image].replace(/"/g, "") + '"></div>'), o += '<div class="content">', t[n.price] !== R && (o += '<div class="price">' + a(t[n.price], i) + "</div>"), t[n.title] !== R && (o += '<div class="title">' + a(t[n.title], i) + "</div>"), t[n.description] !== R && (o += '<div class="description">' + a(t[n.description], i) + "</div>"), o += "</div>", o += "</a>";
                }), e[n.action] && (!1 === n.actionURL ? o += '<div class="action">' + a(e[n.action][n.actionText], i) + "</div>" : o += '<a href="' + e[n.action][n.actionURL].replace(/"/g, "") + '" class="action">' + a(e[n.action][n.actionText], i) + "</a>"), o);
            }
        }
    };
})(jQuery, window, document), (function(D, e84, A, E) {
    "use strict";
    D.isFunction = D.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e84 = void 0 !== e84 && e84.Math == Math ? e84 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), D.fn.shape = function(v) {
        var b, y = D(this), x = (new Date).getTime(), C = [], w = v, k = "string" == typeof w, T = [].slice.call(arguments, 1), S = e84.requestAnimationFrame || e84.mozRequestAnimationFrame || e84.webkitRequestAnimationFrame || e84.msRequestAnimationFrame || function(e) {
            setTimeout(e, 0);
        };
        return y.each(function() {
            var n19, i16, t47 = y.selector || "", a16 = D.isPlainObject(v) ? D.extend(!0, {
            }, D.fn.shape.settings, v) : D.extend({
            }, D.fn.shape.settings), e85 = a16.namespace, r16 = a16.selector, o17 = a16.error, s = a16.className, l = "." + e85, c = "module-" + e85, u = D(this), d = u.find(">" + r16.sides), f = d.find(">" + r16.side), m = !1, g = this, p = u.data(c), h = {
                initialize: function() {
                    h.verbose("Initializing module for", g), h.set.defaultSide(), h.instantiate();
                },
                instantiate: function() {
                    h.verbose("Storing instance of module", h), p = h, u.data(c, p);
                },
                destroy: function() {
                    h.verbose("Destroying previous module for", g), u.removeData(c).off(l);
                },
                refresh: function() {
                    h.verbose("Refreshing selector cache for", g), u = D(g), d = D(this).find(r16.sides), f = D(this).find(r16.side);
                },
                repaint: function() {
                    h.verbose("Forcing repaint event");
                    (d[0] || A.createElement("div")).offsetWidth;
                },
                animate: function(e86, t) {
                    h.verbose("Animating box with properties", e86), t = t || function(e) {
                        h.verbose("Executing animation callback"), e !== E && e.stopPropagation(), h.reset(), h.set.active();
                    }, a16.beforeChange.call(i16[0]), h.get.transitionEvent() ? (h.verbose("Starting CSS animation"), u.addClass(s.animating), d.css(e86).one(h.get.transitionEvent(), t), h.set.duration(a16.duration), S(function() {
                        u.addClass(s.animating), n19.addClass(s.hidden);
                    })) : t();
                },
                queue: function(e) {
                    h.debug("Queueing animation of", e), d.one(h.get.transitionEvent(), function() {
                        h.debug("Executing queued animation"), setTimeout(function() {
                            u.shape(e);
                        }, 0);
                    });
                },
                reset: function() {
                    h.verbose("Animating states reset"), u.removeClass(s.animating).attr("style", "").removeAttr("style"), d.attr("style", "").removeAttr("style"), f.attr("style", "").removeAttr("style").removeClass(s.hidden), i16.removeClass(s.animating).attr("style", "").removeAttr("style");
                },
                is: {
                    complete: function() {
                        return f.filter("." + s.active)[0] == i16[0];
                    },
                    animating: function() {
                        return u.hasClass(s.animating);
                    },
                    hidden: function() {
                        return 0 < u.closest(":hidden").length;
                    }
                },
                set: {
                    defaultSide: function() {
                        n19 = f.filter("." + a16.className.active), i16 = 0 < n19.next(r16.side).length ? n19.next(r16.side) : f.first(), m = !1, h.verbose("Active side set to", n19), h.verbose("Next side set to", i16);
                    },
                    duration: function(e) {
                        e = e || a16.duration, h.verbose("Setting animation duration", e = "number" == typeof e ? e + "ms" : e), !a16.duration && 0 !== a16.duration || d.add(f).css({
                            "-webkit-transition-duration": e,
                            "-moz-transition-duration": e,
                            "-ms-transition-duration": e,
                            "-o-transition-duration": e,
                            "transition-duration": e
                        });
                    },
                    currentStageSize: function() {
                        var e = f.filter("." + a16.className.active), t = e.outerWidth(!0), e = e.outerHeight(!0);
                        u.css({
                            width: t,
                            height: e
                        });
                    },
                    stageSize: function() {
                        var e = u.clone().addClass(s.loading), t = e.find(">" + r16.sides + ">" + r16.side), n = t.filter("." + a16.className.active), i = m ? t.eq(m) : 0 < n.next(r16.side).length ? n.next(r16.side) : t.first(), o = "next" === a16.width ? i.outerWidth(!0) : "initial" === a16.width ? u.width() : a16.width, t = "next" === a16.height ? i.outerHeight(!0) : "initial" === a16.height ? u.height() : a16.height;
                        n.removeClass(s.active), i.addClass(s.active), e.insertAfter(u), e.remove(), "auto" !== a16.width && (u.css("width", o + a16.jitter), h.verbose("Specifying width during animation", o)), "auto" !== a16.height && (u.css("height", t + a16.jitter), h.verbose("Specifying height during animation", t));
                    },
                    nextSide: function(e) {
                        m = e, i16 = f.filter(e), m = f.index(i16), 0 === i16.length && (h.set.defaultSide(), h.error(o17.side)), h.verbose("Next side manually set to", i16);
                    },
                    active: function() {
                        h.verbose("Setting new side to active", i16), f.removeClass(s.active), i16.addClass(s.active), a16.onChange.call(i16[0]), h.set.defaultSide();
                    }
                },
                flip: {
                    to: function(e, t) {
                        var n;
                        h.is.hidden() ? h.debug("Module not visible", i16) : !h.is.complete() || h.is.animating() || a16.allowRepeats ? (n = h.get.transform[e](), h.is.animating() ? h.queue("flip " + e) : (h.debug("Flipping " + e, i16), h.set.stageSize(), h.stage[t](), h.animate(n))) : h.debug("Side already visible", i16);
                    },
                    up: function() {
                        h.flip.to("up", "above");
                    },
                    down: function() {
                        h.flip.to("down", "below");
                    },
                    left: function() {
                        h.flip.to("left", "left");
                    },
                    right: function() {
                        h.flip.to("right", "right");
                    },
                    over: function() {
                        h.flip.to("over", "behind");
                    },
                    back: function() {
                        h.flip.to("back", "behind");
                    }
                },
                get: {
                    transform: {
                        up: function() {
                            var e = n19.outerHeight(!0) / 2;
                            return {
                                transform: "translateY(" + (i16.outerHeight(!0) - e) + "px) translateZ(-" + e + "px) rotateX(-90deg)"
                            };
                        },
                        down: function() {
                            var e = n19.outerHeight(!0) / 2;
                            return {
                                transform: "translateY(-" + e + "px) translateZ(-" + e + "px) rotateX(90deg)"
                            };
                        },
                        left: function() {
                            var e = n19.outerWidth(!0) / 2;
                            return {
                                transform: "translateX(" + (i16.outerWidth(!0) - e) + "px) translateZ(-" + e + "px) rotateY(90deg)"
                            };
                        },
                        right: function() {
                            var e = n19.outerWidth(!0) / 2;
                            return {
                                transform: "translateX(-" + e + "px) translateZ(-" + e + "px) rotateY(-90deg)"
                            };
                        },
                        over: function() {
                            return {
                                transform: "translateX(" + -(n19.outerWidth(!0) - i16.outerWidth(!0)) / 2 + "px) rotateY(180deg)"
                            };
                        },
                        back: function() {
                            return {
                                transform: "translateX(" + -(n19.outerWidth(!0) - i16.outerWidth(!0)) / 2 + "px) rotateY(-180deg)"
                            };
                        }
                    },
                    transitionEvent: function() {
                        var e, t = A.createElement("element"), n = {
                            transition: "transitionend",
                            OTransition: "oTransitionEnd",
                            MozTransition: "transitionend",
                            WebkitTransition: "webkitTransitionEnd"
                        };
                        for(e in n)if (t.style[e] !== E) return n[e];
                    },
                    nextSide: function() {
                        return 0 < n19.next(r16.side).length ? n19.next(r16.side) : f.first();
                    }
                },
                stage: {
                    above: function() {
                        var e = {
                            origin: (n19.outerHeight(!0) - i16.outerHeight(!0)) / 2,
                            depth: {
                                active: i16.outerHeight(!0) / 2,
                                next: n19.outerHeight(!0) / 2
                            }
                        };
                        h.verbose("Setting the initial animation position as above", i16, e), n19.css({
                            transform: "rotateX(0deg)"
                        }), i16.addClass(s.animating).css({
                            top: e.origin + "px",
                            transform: "rotateX(90deg) translateZ(" + e.depth.next + "px) translateY(-" + e.depth.active + "px)"
                        });
                    },
                    below: function() {
                        var e = {
                            origin: (n19.outerHeight(!0) - i16.outerHeight(!0)) / 2,
                            depth: {
                                active: i16.outerHeight(!0) / 2,
                                next: n19.outerHeight(!0) / 2
                            }
                        };
                        h.verbose("Setting the initial animation position as below", i16, e), n19.css({
                            transform: "rotateX(0deg)"
                        }), i16.addClass(s.animating).css({
                            top: e.origin + "px",
                            transform: "rotateX(-90deg) translateZ(" + e.depth.next + "px) translateY(" + e.depth.active + "px)"
                        });
                    },
                    left: function() {
                        var e = n19.outerWidth(!0), t = i16.outerWidth(!0), e = {
                            origin: (e - t) / 2,
                            depth: {
                                active: t / 2,
                                next: e / 2
                            }
                        };
                        h.verbose("Setting the initial animation position as left", i16, e), n19.css({
                            transform: "rotateY(0deg)"
                        }), i16.addClass(s.animating).css({
                            left: e.origin + "px",
                            transform: "rotateY(-90deg) translateZ(" + e.depth.next + "px) translateX(-" + e.depth.active + "px)"
                        });
                    },
                    right: function() {
                        var e = n19.outerWidth(!0), t = i16.outerWidth(!0), e = {
                            origin: (e - t) / 2,
                            depth: {
                                active: t / 2,
                                next: e / 2
                            }
                        };
                        h.verbose("Setting the initial animation position as right", i16, e), n19.css({
                            transform: "rotateY(0deg)"
                        }), i16.addClass(s.animating).css({
                            left: e.origin + "px",
                            transform: "rotateY(90deg) translateZ(" + e.depth.next + "px) translateX(" + e.depth.active + "px)"
                        });
                    },
                    behind: function() {
                        var e = n19.outerWidth(!0), t = i16.outerWidth(!0), e = {
                            origin: (e - t) / 2,
                            depth: {
                                active: t / 2,
                                next: e / 2
                            }
                        };
                        h.verbose("Setting the initial animation position as behind", i16, e), n19.css({
                            transform: "rotateY(0deg)"
                        }), i16.addClass(s.animating).css({
                            left: e.origin + "px",
                            transform: "rotateY(-180deg)"
                        });
                    }
                },
                setting: function(e, t) {
                    if (h.debug("Changing setting", e, t), D.isPlainObject(e)) D.extend(!0, a16, e);
                    else {
                        if (t === E) return a16[e];
                        D.isPlainObject(a16[e]) ? D.extend(!0, a16[e], t) : a16[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (D.isPlainObject(e)) D.extend(!0, h, e);
                    else {
                        if (t === E) return h[e];
                        h[e] = t;
                    }
                },
                debug: function() {
                    !a16.silent && a16.debug && (a16.performance ? h.performance.log(arguments) : (h.debug = Function.prototype.bind.call(console.info, console, a16.name + ":"), h.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !a16.silent && a16.verbose && a16.debug && (a16.performance ? h.performance.log(arguments) : (h.verbose = Function.prototype.bind.call(console.info, console, a16.name + ":"), h.verbose.apply(console, arguments)));
                },
                error: function() {
                    a16.silent || (h.error = Function.prototype.bind.call(console.error, console, a16.name + ":"), h.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        a16.performance && (n = (t = (new Date).getTime()) - (x || t), x = t, C.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: g,
                            "Execution Time": n
                        })), clearTimeout(h.performance.timer), h.performance.timer = setTimeout(h.performance.display, 500);
                    },
                    display: function() {
                        var e = a16.name + ":", n = 0;
                        x = !1, clearTimeout(h.performance.timer), D.each(C, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", t47 && (e += " '" + t47 + "'"), 1 < y.length && (e += " (" + y.length + ")"), (console.group !== E || console.table !== E) && 0 < C.length && (console.groupCollapsed(e), console.table ? console.table(C) : D.each(C, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), C = [];
                    }
                },
                invoke: function(i, e87, t48) {
                    var o, a, n, r = p;
                    return e87 = e87 || T, t48 = g || t48, "string" == typeof i && r !== E && (i = i.split(/[\. ]/), o = i.length - 1, D.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (D.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== E) return a = r[n], !1;
                            if (!D.isPlainObject(r[t]) || e == o) return r[t] !== E && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), D.isFunction(a) ? n = a.apply(t48, e87) : a !== E && (n = a), Array.isArray(b) ? b.push(n) : b !== E ? b = [
                        b,
                        n
                    ] : n !== E && (b = n), a;
                }
            };
            k ? (p === E && h.initialize(), 0 < (e85 = u.find("input")).length ? (e85.blur(), setTimeout(function() {
                h.invoke(w);
            }, 150)) : h.invoke(w)) : (p !== E && p.invoke("destroy"), h.initialize());
        }), b !== E ? b : this;
    }, D.fn.shape.settings = {
        name: "Shape",
        silent: !1,
        debug: !1,
        verbose: !1,
        jitter: 0,
        performance: !0,
        namespace: "shape",
        width: "initial",
        height: "initial",
        beforeChange: function() {
        },
        onChange: function() {
        },
        allowRepeats: !1,
        duration: !1,
        error: {
            side: "You tried to switch to a side that does not exist.",
            method: "The method you called is not defined"
        },
        className: {
            animating: "animating",
            hidden: "hidden",
            loading: "loading",
            active: "active"
        },
        selector: {
            sides: ".sides",
            side: ".side"
        }
    };
})(jQuery, window, document), (function(M, I, j, L) {
    "use strict";
    M.isFunction = M.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, I = void 0 !== I && I.Math == Math ? I : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), M.fn.sidebar = function(x) {
        var C, e88 = M(this), w = M(I), k = M(j), T = M("html"), S = M("head"), D = e88.selector || "", A = (new Date).getTime(), E = [], F = x, P = "string" == typeof F, O = [].slice.call(arguments, 1), R = I.requestAnimationFrame || I.mozRequestAnimationFrame || I.webkitRequestAnimationFrame || I.msRequestAnimationFrame || function(e) {
            setTimeout(e, 0);
        };
        return e88.each(function() {
            var o18, a17, e89, t49, r17, s = M.isPlainObject(x) ? M.extend(!0, {
            }, M.fn.sidebar.settings, x) : M.extend({
            }, M.fn.sidebar.settings), n20 = s.selector, l = s.className, i17 = s.namespace, c = s.regExp, u = s.error, d = "." + i17, f = "module-" + i17, m = M(this), g = M(s.context), p = m.children(n20.sidebar), h = (g.children(n20.fixed), g.children(n20.pusher)), v = this, b = m.data(f), y = {
                initialize: function() {
                    y.debug("Initializing sidebar", x), y.create.id(), r17 = y.get.transitionEvent(), s.delaySetup ? R(y.setup.layout) : y.setup.layout(), R(function() {
                        y.setup.cache();
                    }), y.instantiate();
                },
                instantiate: function() {
                    y.verbose("Storing instance of module", y), b = y, m.data(f, y);
                },
                create: {
                    id: function() {
                        e89 = (Math.random().toString(16) + "000000000").substr(2, 8), a17 = "." + e89, y.verbose("Creating unique id for element", e89);
                    }
                },
                destroy: function() {
                    y.verbose("Destroying previous module for", m), m.off(d).removeData(f), y.is.ios() && y.remove.ios(), g.off(a17), w.off(a17), k.off(a17);
                },
                event: {
                    clickaway: function(e) {
                        var t;
                        s.closable && (t = 0 < h.find(e.target).length || h.is(e.target), e = g.is(e.target), t && (y.verbose("User clicked on dimmed page"), y.hide()), e && (y.verbose("User clicked on dimmable context (scaled out page)"), y.hide()));
                    },
                    touch: function(e) {
                    },
                    containScroll: function(e) {
                        v.scrollTop <= 0 && (v.scrollTop = 1), v.scrollTop + v.offsetHeight >= v.scrollHeight && (v.scrollTop = v.scrollHeight - v.offsetHeight - 1);
                    },
                    scroll: function(e) {
                        0 === M(e.target).closest(n20.sidebar).length && e.preventDefault();
                    }
                },
                bind: {
                    clickaway: function() {
                        y.verbose("Adding clickaway events to context", g), g.on("click" + a17, y.event.clickaway).on("touchend" + a17, y.event.clickaway);
                    },
                    scrollLock: function() {
                        s.scrollLock && (y.debug("Disabling page scroll"), w.on("DOMMouseScroll" + a17, y.event.scroll)), y.verbose("Adding events to contain sidebar scroll"), k.on("touchmove" + a17, y.event.touch), m.on("scroll" + d, y.event.containScroll);
                    }
                },
                unbind: {
                    clickaway: function() {
                        y.verbose("Removing clickaway events from context", g), g.off(a17);
                    },
                    scrollLock: function() {
                        y.verbose("Removing scroll lock from page"), k.off(a17), w.off(a17), m.off("scroll" + d);
                    }
                },
                add: {
                    inlineCSS: function() {
                        var e = y.cache.width || m.outerWidth(), t = y.cache.height || m.outerHeight(), n = y.is.rtl(), i = y.get.direction(), t = {
                            left: e,
                            right: -e,
                            top: t,
                            bottom: -t
                        };
                        n && (y.verbose("RTL detected, flipping widths"), t.left = -e, t.right = e), n = "<style>", "left" === i || "right" === i ? (y.debug("Adding CSS rules for animation distance", e), n += " .ui.visible." + i + ".sidebar ~ .fixed, .ui.visible." + i + ".sidebar ~ .pusher {   -webkit-transform: translate3d(" + t[i] + "px, 0, 0);           transform: translate3d(" + t[i] + "px, 0, 0); }") : "top" !== i && "bottom" != i || (n += " .ui.visible." + i + ".sidebar ~ .fixed, .ui.visible." + i + ".sidebar ~ .pusher {   -webkit-transform: translate3d(0, " + t[i] + "px, 0);           transform: translate3d(0, " + t[i] + "px, 0); }"), y.is.ie() && ("left" === i || "right" === i ? (y.debug("Adding CSS rules for animation distance", e), n += " body.pushable > .ui.visible." + i + ".sidebar ~ .pusher:after {   -webkit-transform: translate3d(" + t[i] + "px, 0, 0);           transform: translate3d(" + t[i] + "px, 0, 0); }") : "top" !== i && "bottom" != i || (n += " body.pushable > .ui.visible." + i + ".sidebar ~ .pusher:after {   -webkit-transform: translate3d(0, " + t[i] + "px, 0);           transform: translate3d(0, " + t[i] + "px, 0); }"), n += " body.pushable > .ui.visible.left.sidebar ~ .ui.visible.right.sidebar ~ .pusher:after, body.pushable > .ui.visible.right.sidebar ~ .ui.visible.left.sidebar ~ .pusher:after {   -webkit-transform: translate3d(0, 0, 0);           transform: translate3d(0, 0, 0); }"), o18 = M(n += "</style>").appendTo(S), y.debug("Adding sizing css to head", o18);
                    }
                },
                refresh: function() {
                    y.verbose("Refreshing selector cache"), g = M(s.context), p = g.children(n20.sidebar), h = g.children(n20.pusher), g.children(n20.fixed), y.clear.cache();
                },
                refreshSidebars: function() {
                    y.verbose("Refreshing other sidebars"), p = g.children(n20.sidebar);
                },
                repaint: function() {
                    y.verbose("Forcing repaint event"), v.style.display = "none";
                    v.offsetHeight;
                    v.scrollTop = v.scrollTop, v.style.display = "";
                },
                setup: {
                    cache: function() {
                        y.cache = {
                            width: m.outerWidth(),
                            height: m.outerHeight()
                        };
                    },
                    layout: function() {
                        0 === g.children(n20.pusher).length && (y.debug("Adding wrapper element for sidebar"), y.error(u.pusher), h = M('<div class="pusher" />'), g.children().not(n20.omitted).not(p).wrapAll(h), y.refresh()), 0 !== m.nextAll(n20.pusher).length && m.nextAll(n20.pusher)[0] === h[0] || (y.debug("Moved sidebar to correct parent element"), y.error(u.movedSidebar, v), m.detach().prependTo(g), y.refresh()), y.clear.cache(), y.set.pushable(), y.set.direction();
                    }
                },
                attachEvents: function(e, t) {
                    var n = M(e);
                    t = M.isFunction(y[t]) ? y[t] : y.toggle, 0 < n.length ? (y.debug("Attaching sidebar events to element", e, t), n.on("click" + d, t)) : y.error(u.notFound, e);
                },
                show: function(e) {
                    if (e = M.isFunction(e) ? e : function() {
                    }, y.is.hidden()) {
                        if (y.refreshSidebars(), s.overlay && (y.error(u.overlay), s.transition = "overlay"), y.refresh(), y.othersActive()) {
                            if (y.debug("Other sidebars currently visible"), s.exclusive) {
                                if ("overlay" != s.transition) return void y.hideOthers(y.show);
                                y.hideOthers();
                            } else s.transition = "overlay";
                        }
                        y.pushPage(function() {
                            e.call(v), s.onShow.call(v);
                        }), s.onChange.call(v), s.onVisible.call(v);
                    } else y.debug("Sidebar is already visible");
                },
                hide: function(e) {
                    e = M.isFunction(e) ? e : function() {
                    }, (y.is.visible() || y.is.animating()) && (y.debug("Hiding sidebar", e), y.refreshSidebars(), y.pullPage(function() {
                        e.call(v), s.onHidden.call(v);
                    }), s.onChange.call(v), s.onHide.call(v));
                },
                othersAnimating: function() {
                    return 0 < p.not(m).filter("." + l.animating).length;
                },
                othersVisible: function() {
                    return 0 < p.not(m).filter("." + l.visible).length;
                },
                othersActive: function() {
                    return y.othersVisible() || y.othersAnimating();
                },
                hideOthers: function(e) {
                    var t = p.not(m).filter("." + l.visible), n = t.length, i = 0;
                    e = e || function() {
                    }, t.sidebar("hide", function() {
                        ++i == n && e();
                    });
                },
                toggle: function() {
                    y.verbose("Determining toggled direction"), y.is.hidden() ? y.show() : y.hide();
                },
                pushPage: function(t) {
                    var e90, n, i = y.get.transition(), o = "overlay" === i || y.othersActive() ? m : h;
                    t = M.isFunction(t) ? t : function() {
                    }, "scale down" == s.transition && y.scrollToTop(), y.set.transition(i), y.repaint(), e90 = function() {
                        y.bind.clickaway(), y.add.inlineCSS(), y.set.animating(), y.set.visible();
                    }, i = function() {
                        y.set.dimmed();
                    }, n = function(e) {
                        e.target == o[0] && (o.off(r17 + a17, n), y.remove.animating(), y.bind.scrollLock(), t.call(v));
                    }, o.off(r17 + a17), o.on(r17 + a17, n), R(e90), s.dimPage && !y.othersVisible() && R(i);
                },
                pullPage: function(t) {
                    var e91, n, i = y.get.transition(), o = "overlay" == i || y.othersActive() ? m : h;
                    t = M.isFunction(t) ? t : function() {
                    }, y.verbose("Removing context push state", y.get.direction()), y.unbind.clickaway(), y.unbind.scrollLock(), e91 = function() {
                        y.set.transition(i), y.set.animating(), y.remove.visible(), s.dimPage && !y.othersVisible() && h.removeClass(l.dimmed);
                    }, n = function(e) {
                        e.target == o[0] && (o.off(r17 + a17, n), y.remove.animating(), y.remove.transition(), y.remove.inlineCSS(), ("scale down" == i || s.returnScroll && y.is.mobile()) && y.scrollBack(), t.call(v));
                    }, o.off(r17 + a17), o.on(r17 + a17, n), R(e91);
                },
                scrollToTop: function() {
                    y.verbose("Scrolling to top of page to avoid animation issues"), t49 = M(I).scrollTop(), m.scrollTop(0), I.scrollTo(0, 0);
                },
                scrollBack: function() {
                    y.verbose("Scrolling back to original page position"), I.scrollTo(0, t49);
                },
                clear: {
                    cache: function() {
                        y.verbose("Clearing cached dimensions"), y.cache = {
                        };
                    }
                },
                set: {
                    ios: function() {
                        T.addClass(l.ios);
                    },
                    pushed: function() {
                        g.addClass(l.pushed);
                    },
                    pushable: function() {
                        g.addClass(l.pushable);
                    },
                    dimmed: function() {
                        h.addClass(l.dimmed);
                    },
                    active: function() {
                        m.addClass(l.active);
                    },
                    animating: function() {
                        m.addClass(l.animating);
                    },
                    transition: function(e) {
                        e = e || y.get.transition(), m.addClass(e);
                    },
                    direction: function(e) {
                        e = e || y.get.direction(), m.addClass(l[e]);
                    },
                    visible: function() {
                        m.addClass(l.visible);
                    },
                    overlay: function() {
                        m.addClass(l.overlay);
                    }
                },
                remove: {
                    inlineCSS: function() {
                        y.debug("Removing inline css styles", o18), o18 && 0 < o18.length && o18.remove();
                    },
                    ios: function() {
                        T.removeClass(l.ios);
                    },
                    pushed: function() {
                        g.removeClass(l.pushed);
                    },
                    pushable: function() {
                        g.removeClass(l.pushable);
                    },
                    active: function() {
                        m.removeClass(l.active);
                    },
                    animating: function() {
                        m.removeClass(l.animating);
                    },
                    transition: function(e) {
                        e = e || y.get.transition(), m.removeClass(e);
                    },
                    direction: function(e) {
                        e = e || y.get.direction(), m.removeClass(l[e]);
                    },
                    visible: function() {
                        m.removeClass(l.visible);
                    },
                    overlay: function() {
                        m.removeClass(l.overlay);
                    }
                },
                get: {
                    direction: function() {
                        return m.hasClass(l.top) ? l.top : m.hasClass(l.right) ? l.right : m.hasClass(l.bottom) ? l.bottom : l.left;
                    },
                    transition: function() {
                        var e = y.get.direction(), e = y.is.mobile() ? "auto" == s.mobileTransition ? s.defaultTransition.mobile[e] : s.mobileTransition : "auto" == s.transition ? s.defaultTransition.computer[e] : s.transition;
                        return y.verbose("Determined transition", e), e;
                    },
                    transitionEvent: function() {
                        var e, t = j.createElement("element"), n = {
                            transition: "transitionend",
                            OTransition: "oTransitionEnd",
                            MozTransition: "transitionend",
                            WebkitTransition: "webkitTransitionEnd"
                        };
                        for(e in n)if (t.style[e] !== L) return n[e];
                    }
                },
                is: {
                    ie: function() {
                        var e = !I.ActiveXObject && "ActiveXObject" in I, t = "ActiveXObject" in I;
                        return e || t;
                    },
                    ios: function() {
                        var e = navigator.userAgent, t = e.match(c.ios), n = e.match(c.mobileChrome);
                        return !(!t || n) && (y.verbose("Browser was found to be iOS", e), !0);
                    },
                    mobile: function() {
                        var e = navigator.userAgent;
                        return e.match(c.mobile) ? (y.verbose("Browser was found to be mobile", e), !0) : (y.verbose("Browser is not mobile, using regular transition", e), !1);
                    },
                    hidden: function() {
                        return !y.is.visible();
                    },
                    visible: function() {
                        return m.hasClass(l.visible);
                    },
                    open: function() {
                        return y.is.visible();
                    },
                    closed: function() {
                        return y.is.hidden();
                    },
                    vertical: function() {
                        return m.hasClass(l.top);
                    },
                    animating: function() {
                        return g.hasClass(l.animating);
                    },
                    rtl: function() {
                        return y.cache.rtl === L && (y.cache.rtl = "rtl" === m.attr("dir") || "rtl" === m.css("direction")), y.cache.rtl;
                    }
                },
                setting: function(e, t) {
                    if (y.debug("Changing setting", e, t), M.isPlainObject(e)) M.extend(!0, s, e);
                    else {
                        if (t === L) return s[e];
                        M.isPlainObject(s[e]) ? M.extend(!0, s[e], t) : s[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (M.isPlainObject(e)) M.extend(!0, y, e);
                    else {
                        if (t === L) return y[e];
                        y[e] = t;
                    }
                },
                debug: function() {
                    !s.silent && s.debug && (s.performance ? y.performance.log(arguments) : (y.debug = Function.prototype.bind.call(console.info, console, s.name + ":"), y.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !s.silent && s.verbose && s.debug && (s.performance ? y.performance.log(arguments) : (y.verbose = Function.prototype.bind.call(console.info, console, s.name + ":"), y.verbose.apply(console, arguments)));
                },
                error: function() {
                    s.silent || (y.error = Function.prototype.bind.call(console.error, console, s.name + ":"), y.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        s.performance && (n = (t = (new Date).getTime()) - (A || t), A = t, E.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: v,
                            "Execution Time": n
                        })), clearTimeout(y.performance.timer), y.performance.timer = setTimeout(y.performance.display, 500);
                    },
                    display: function() {
                        var e = s.name + ":", n = 0;
                        A = !1, clearTimeout(y.performance.timer), M.each(E, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", D && (e += " '" + D + "'"), (console.group !== L || console.table !== L) && 0 < E.length && (console.groupCollapsed(e), console.table ? console.table(E) : M.each(E, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), E = [];
                    }
                },
                invoke: function(i, e92, t50) {
                    var o, a, n, r = b;
                    return e92 = e92 || O, t50 = v || t50, "string" == typeof i && r !== L && (i = i.split(/[\. ]/), o = i.length - 1, M.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (M.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== L) return a = r[n], !1;
                            if (!M.isPlainObject(r[t]) || e == o) return r[t] !== L ? a = r[t] : y.error(u.method, i), !1;
                            r = r[t];
                        }
                    })), M.isFunction(a) ? n = a.apply(t50, e92) : a !== L && (n = a), Array.isArray(C) ? C.push(n) : C !== L ? C = [
                        C,
                        n
                    ] : n !== L && (C = n), a;
                }
            };
            P ? (b === L && y.initialize(), y.invoke(F)) : (b !== L && y.invoke("destroy"), y.initialize());
        }), C !== L ? C : this;
    }, M.fn.sidebar.settings = {
        name: "Sidebar",
        namespace: "sidebar",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        transition: "auto",
        mobileTransition: "auto",
        defaultTransition: {
            computer: {
                left: "uncover",
                right: "uncover",
                top: "overlay",
                bottom: "overlay"
            },
            mobile: {
                left: "uncover",
                right: "uncover",
                top: "overlay",
                bottom: "overlay"
            }
        },
        context: "body",
        exclusive: !1,
        closable: !0,
        dimPage: !0,
        scrollLock: !1,
        returnScroll: !1,
        delaySetup: !1,
        duration: 500,
        onChange: function() {
        },
        onShow: function() {
        },
        onHide: function() {
        },
        onHidden: function() {
        },
        onVisible: function() {
        },
        className: {
            active: "active",
            animating: "animating",
            dimmed: "dimmed",
            ios: "ios",
            pushable: "pushable",
            pushed: "pushed",
            right: "right",
            top: "top",
            left: "left",
            bottom: "bottom",
            visible: "visible"
        },
        selector: {
            fixed: ".fixed",
            omitted: "script, link, style, .ui.modal, .ui.dimmer, .ui.nag, .ui.fixed",
            pusher: ".pusher",
            sidebar: ".ui.sidebar"
        },
        regExp: {
            ios: /(iPad|iPhone|iPod)/g,
            mobileChrome: /(CriOS)/g,
            mobile: /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/g
        },
        error: {
            method: "The method you called is not defined.",
            pusher: "Had to add pusher element. For optimal performance make sure body content is inside a pusher element",
            movedSidebar: "Had to move sidebar. For optimal performance make sure sidebar and pusher are direct children of your body tag",
            overlay: "The overlay setting is no longer supported, use animation: overlay",
            notFound: "There were no elements that matched the specified selector"
        }
    };
})(jQuery, window, document), (function(S, D, A, E) {
    "use strict";
    S.isFunction = S.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, D = void 0 !== D && D.Math == Math ? D : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), S.fn.sticky = function(v) {
        var b, e93 = S(this), y = e93.selector || "", x = (new Date).getTime(), C = [], w = v, k = "string" == typeof w, T = [].slice.call(arguments, 1);
        return e93.each(function() {
            var t51, i18, e94, n21, l = S.isPlainObject(v) ? S.extend(!0, {
            }, S.fn.sticky.settings, v) : S.extend({
            }, S.fn.sticky.settings), o19 = l.className, a18 = l.namespace, r18 = l.error, s9 = "." + a18, c = "module-" + a18, u = S(this), d = S(D), f = S(l.scrollContext), m = u.data(c), g = D.requestAnimationFrame || D.mozRequestAnimationFrame || D.webkitRequestAnimationFrame || D.msRequestAnimationFrame || function(e) {
                setTimeout(e, 0);
            }, p = this, h = {
                initialize: function() {
                    h.determineContainer(), h.determineContext(), h.verbose("Initializing sticky", l, t51), h.save.positions(), h.checkErrors(), h.bind.events(), l.observeChanges && h.observeChanges(), h.instantiate();
                },
                instantiate: function() {
                    h.verbose("Storing instance of module", h), m = h, u.data(c, h);
                },
                destroy: function() {
                    h.verbose("Destroying previous instance"), h.reset(), e94 && e94.disconnect(), n21 && n21.disconnect(), d.off("load" + s9, h.event.load).off("resize" + s9, h.event.resize), f.off("scrollchange" + s9, h.event.scrollchange), u.removeData(c);
                },
                observeChanges: function() {
                    "MutationObserver" in D && (e94 = new MutationObserver(h.event.documentChanged), n21 = new MutationObserver(h.event.changed), e94.observe(A, {
                        childList: !0,
                        subtree: !0
                    }), n21.observe(p, {
                        childList: !0,
                        subtree: !0
                    }), n21.observe(i18[0], {
                        childList: !0,
                        subtree: !0
                    }), h.debug("Setting up mutation observer", n21));
                },
                determineContainer: function() {
                    t51 = l.container ? S(l.container) : u.offsetParent();
                },
                determineContext: function() {
                    0 === (i18 = l.context ? S(l.context) : t51).length && h.error(r18.invalidContext, l.context, u);
                },
                checkErrors: function() {
                    h.is.hidden() && h.error(r18.visible, u), h.cache.element.height > h.cache.context.height && (h.reset(), h.error(r18.elementSize, u));
                },
                bind: {
                    events: function() {
                        d.on("load" + s9, h.event.load).on("resize" + s9, h.event.resize), f.off("scroll" + s9).on("scroll" + s9, h.event.scroll).on("scrollchange" + s9, h.event.scrollchange);
                    }
                },
                event: {
                    changed: function(e) {
                        clearTimeout(h.timer), h.timer = setTimeout(function() {
                            h.verbose("DOM tree modified, updating sticky menu", e), h.refresh();
                        }, 100);
                    },
                    documentChanged: function(e95) {
                        [].forEach.call(e95, function(e96) {
                            e96.removedNodes && [].forEach.call(e96.removedNodes, function(e) {
                                (e == p || 0 < S(e).find(p).length) && (h.debug("Element removed from DOM, tearing down events"), h.destroy());
                            });
                        });
                    },
                    load: function() {
                        h.verbose("Page contents finished loading"), g(h.refresh);
                    },
                    resize: function() {
                        h.verbose("Window resized"), g(h.refresh);
                    },
                    scroll: function() {
                        g(function() {
                            f.triggerHandler("scrollchange" + s9, f.scrollTop());
                        });
                    },
                    scrollchange: function(e, t) {
                        h.stick(t), l.onScroll.call(p);
                    }
                },
                refresh: function(e) {
                    h.reset(), l.context || h.determineContext(), e && h.determineContainer(), h.save.positions(), h.stick(), l.onReposition.call(p);
                },
                supports: {
                    sticky: function() {
                        var e = S("<div/>");
                        return e.addClass(o19.supported), e.css("position").match("sticky");
                    }
                },
                save: {
                    lastScroll: function(e) {
                        h.lastScroll = e;
                    },
                    elementScroll: function(e) {
                        h.elementScroll = e;
                    },
                    positions: function() {
                        var e = {
                            height: f.height()
                        }, t = {
                            margin: {
                                top: parseInt(u.css("margin-top"), 10),
                                bottom: parseInt(u.css("margin-bottom"), 10)
                            },
                            offset: u.offset(),
                            width: u.outerWidth(),
                            height: u.outerHeight()
                        }, n = {
                            offset: i18.offset(),
                            height: i18.outerHeight()
                        };
                        h.is.standardScroll() || (h.debug("Non-standard scroll. Removing scroll offset from element offset"), e.top = f.scrollTop(), e.left = f.scrollLeft(), t.offset.top += e.top, n.offset.top += e.top, t.offset.left += e.left, n.offset.left += e.left), h.cache = {
                            fits: t.height + l.offset <= e.height,
                            sameHeight: t.height == n.height,
                            scrollContext: {
                                height: e.height
                            },
                            element: {
                                margin: t.margin,
                                top: t.offset.top - t.margin.top,
                                left: t.offset.left,
                                width: t.width,
                                height: t.height,
                                bottom: t.offset.top + t.height
                            },
                            context: {
                                top: n.offset.top,
                                height: n.height,
                                bottom: n.offset.top + n.height
                            }
                        }, h.set.containerSize(), h.stick(), h.debug("Caching element positions", h.cache);
                    }
                },
                get: {
                    direction: function(e) {
                        var t = "down";
                        return e = e || f.scrollTop(), h.lastScroll !== E && (h.lastScroll < e ? t = "down" : h.lastScroll > e && (t = "up")), t;
                    },
                    scrollChange: function(e) {
                        return e = e || f.scrollTop(), h.lastScroll ? e - h.lastScroll : 0;
                    },
                    currentElementScroll: function() {
                        return h.elementScroll || (h.is.top() ? Math.abs(parseInt(u.css("top"), 10)) || 0 : Math.abs(parseInt(u.css("bottom"), 10)) || 0);
                    },
                    elementScroll: function(e) {
                        e = e || f.scrollTop();
                        var t = h.cache.element, n = h.cache.scrollContext, e = h.get.scrollChange(e), t = t.height - n.height + l.offset, n = h.get.currentElementScroll(), e = n + e;
                        return n = h.cache.fits || e < 0 ? 0 : t < e ? t : e;
                    }
                },
                remove: {
                    lastScroll: function() {
                        delete h.lastScroll;
                    },
                    elementScroll: function(e) {
                        delete h.elementScroll;
                    },
                    minimumSize: function() {
                        t51.css("min-height", "");
                    },
                    offset: function() {
                        u.css("margin-top", "");
                    }
                },
                set: {
                    offset: function() {
                        h.verbose("Setting offset on element", l.offset), u.css("margin-top", l.offset);
                    },
                    containerSize: function() {
                        var e = t51.get(0).tagName;
                        "HTML" === e || "body" == e ? h.determineContainer() : Math.abs(t51.outerHeight() - h.cache.context.height) > l.jitter && (h.debug("Context has padding, specifying exact height for container", h.cache.context.height), t51.css({
                            height: h.cache.context.height
                        }));
                    },
                    minimumSize: function() {
                        var e = h.cache.element;
                        t51.css("min-height", e.height);
                    },
                    scroll: function(e) {
                        h.debug("Setting scroll on element", e), h.elementScroll != e && (h.is.top() && u.css("bottom", "").css("top", -e), h.is.bottom() && u.css("top", "").css("bottom", e));
                    },
                    size: function() {
                        0 !== h.cache.element.height && 0 !== h.cache.element.width && (p.style.setProperty("width", h.cache.element.width + "px", "important"), p.style.setProperty("height", h.cache.element.height + "px", "important"));
                    }
                },
                is: {
                    standardScroll: function() {
                        return f[0] == D;
                    },
                    top: function() {
                        return u.hasClass(o19.top);
                    },
                    bottom: function() {
                        return u.hasClass(o19.bottom);
                    },
                    initialPosition: function() {
                        return !h.is.fixed() && !h.is.bound();
                    },
                    hidden: function() {
                        return !u.is(":visible");
                    },
                    bound: function() {
                        return u.hasClass(o19.bound);
                    },
                    fixed: function() {
                        return u.hasClass(o19.fixed);
                    }
                },
                stick: function(e) {
                    var t = e || f.scrollTop(), n = h.cache, i = n.fits, o = n.sameHeight, a = n.element, r = n.scrollContext, s = n.context, n = h.is.bottom() && l.pushing ? l.bottomOffset : l.offset, e = {
                        top: t + n,
                        bottom: t + n + r.height
                    }, r = i ? 0 : h.get.elementScroll(e.top), i = !i;
                    0 === a.height || o || (h.is.initialPosition() ? e.top >= s.bottom ? (h.debug("Initial element position is bottom of container"), h.bindBottom()) : e.top > a.top && (a.height + e.top - r >= s.bottom ? (h.debug("Initial element position is bottom of container"), h.bindBottom()) : (h.debug("Initial element position is fixed"), h.fixTop())) : h.is.fixed() ? h.is.top() ? e.top <= a.top ? (h.debug("Fixed element reached top of container"), h.setInitialPosition()) : a.height + e.top - r >= s.bottom ? (h.debug("Fixed element reached bottom of container"), h.bindBottom()) : i && (h.set.scroll(r), h.save.lastScroll(e.top), h.save.elementScroll(r)) : h.is.bottom() && (e.bottom - a.height <= a.top ? (h.debug("Bottom fixed rail has reached top of container"), h.setInitialPosition()) : e.bottom >= s.bottom ? (h.debug("Bottom fixed rail has reached bottom of container"), h.bindBottom()) : i && (h.set.scroll(r), h.save.lastScroll(e.top), h.save.elementScroll(r))) : h.is.bottom() && (e.top <= a.top ? (h.debug("Jumped from bottom fixed to top fixed, most likely used home/end button"), h.setInitialPosition()) : l.pushing ? h.is.bound() && e.bottom <= s.bottom && (h.debug("Fixing bottom attached element to bottom of browser."), h.fixBottom()) : h.is.bound() && e.top <= s.bottom - a.height && (h.debug("Fixing bottom attached element to top of browser."), h.fixTop())));
                },
                bindTop: function() {
                    h.debug("Binding element to top of parent container"), h.remove.offset(), u.css({
                        left: "",
                        top: "",
                        marginBottom: ""
                    }).removeClass(o19.fixed).removeClass(o19.bottom).addClass(o19.bound).addClass(o19.top), l.onTop.call(p), l.onUnstick.call(p);
                },
                bindBottom: function() {
                    h.debug("Binding element to bottom of parent container"), h.remove.offset(), u.css({
                        left: "",
                        top: ""
                    }).removeClass(o19.fixed).removeClass(o19.top).addClass(o19.bound).addClass(o19.bottom), l.onBottom.call(p), l.onUnstick.call(p);
                },
                setInitialPosition: function() {
                    h.debug("Returning to initial position"), h.unfix(), h.unbind();
                },
                fixTop: function() {
                    h.debug("Fixing element to top of page"), l.setSize && h.set.size(), h.set.minimumSize(), h.set.offset(), u.css({
                        left: h.cache.element.left,
                        bottom: "",
                        marginBottom: ""
                    }).removeClass(o19.bound).removeClass(o19.bottom).addClass(o19.fixed).addClass(o19.top), l.onStick.call(p);
                },
                fixBottom: function() {
                    h.debug("Sticking element to bottom of page"), l.setSize && h.set.size(), h.set.minimumSize(), h.set.offset(), u.css({
                        left: h.cache.element.left,
                        bottom: "",
                        marginBottom: ""
                    }).removeClass(o19.bound).removeClass(o19.top).addClass(o19.fixed).addClass(o19.bottom), l.onStick.call(p);
                },
                unbind: function() {
                    h.is.bound() && (h.debug("Removing container bound position on element"), h.remove.offset(), u.removeClass(o19.bound).removeClass(o19.top).removeClass(o19.bottom));
                },
                unfix: function() {
                    h.is.fixed() && (h.debug("Removing fixed position on element"), h.remove.minimumSize(), h.remove.offset(), u.removeClass(o19.fixed).removeClass(o19.top).removeClass(o19.bottom), l.onUnstick.call(p));
                },
                reset: function() {
                    h.debug("Resetting elements position"), h.unbind(), h.unfix(), h.resetCSS(), h.remove.offset(), h.remove.lastScroll();
                },
                resetCSS: function() {
                    u.css({
                        width: "",
                        height: ""
                    }), t51.css({
                        height: ""
                    });
                },
                setting: function(e, t) {
                    if (S.isPlainObject(e)) S.extend(!0, l, e);
                    else {
                        if (t === E) return l[e];
                        l[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (S.isPlainObject(e)) S.extend(!0, h, e);
                    else {
                        if (t === E) return h[e];
                        h[e] = t;
                    }
                },
                debug: function() {
                    !l.silent && l.debug && (l.performance ? h.performance.log(arguments) : (h.debug = Function.prototype.bind.call(console.info, console, l.name + ":"), h.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !l.silent && l.verbose && l.debug && (l.performance ? h.performance.log(arguments) : (h.verbose = Function.prototype.bind.call(console.info, console, l.name + ":"), h.verbose.apply(console, arguments)));
                },
                error: function() {
                    l.silent || (h.error = Function.prototype.bind.call(console.error, console, l.name + ":"), h.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        l.performance && (n = (t = (new Date).getTime()) - (x || t), x = t, C.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: p,
                            "Execution Time": n
                        })), clearTimeout(h.performance.timer), h.performance.timer = setTimeout(h.performance.display, 0);
                    },
                    display: function() {
                        var e = l.name + ":", n = 0;
                        x = !1, clearTimeout(h.performance.timer), S.each(C, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", y && (e += " '" + y + "'"), (console.group !== E || console.table !== E) && 0 < C.length && (console.groupCollapsed(e), console.table ? console.table(C) : S.each(C, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), C = [];
                    }
                },
                invoke: function(i, e97, t52) {
                    var o, a, n, r = m;
                    return e97 = e97 || T, t52 = p || t52, "string" == typeof i && r !== E && (i = i.split(/[\. ]/), o = i.length - 1, S.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (S.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== E) return a = r[n], !1;
                            if (!S.isPlainObject(r[t]) || e == o) return r[t] !== E && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), S.isFunction(a) ? n = a.apply(t52, e97) : a !== E && (n = a), Array.isArray(b) ? b.push(n) : b !== E ? b = [
                        b,
                        n
                    ] : n !== E && (b = n), a;
                }
            };
            k ? (m === E && h.initialize(), h.invoke(w)) : (m !== E && m.invoke("destroy"), h.initialize());
        }), b !== E ? b : this;
    }, S.fn.sticky.settings = {
        name: "Sticky",
        namespace: "sticky",
        silent: !1,
        debug: !1,
        verbose: !0,
        performance: !0,
        pushing: !1,
        context: !1,
        container: !1,
        scrollContext: D,
        offset: 0,
        bottomOffset: 0,
        jitter: 5,
        setSize: !0,
        observeChanges: !1,
        onReposition: function() {
        },
        onScroll: function() {
        },
        onStick: function() {
        },
        onUnstick: function() {
        },
        onTop: function() {
        },
        onBottom: function() {
        },
        error: {
            container: "Sticky element must be inside a relative container",
            visible: "Element is hidden, you must call refresh after element becomes visible. Use silent setting to surpress this warning in production.",
            method: "The method you called is not defined.",
            invalidContext: "Context specified does not exist",
            elementSize: "Sticky element is larger than its container, cannot create sticky."
        },
        className: {
            bound: "bound",
            fixed: "fixed",
            supported: "native",
            top: "top",
            bottom: "bottom"
        }
    };
})(jQuery, window, document), (function(P, O, R, M) {
    "use strict";
    P.isWindow = P.isWindow || function(e) {
        return null != e && e === e.window;
    }, P.isFunction = P.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, O = void 0 !== O && O.Math == Math ? O : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), P.fn.tab = function(c6) {
        var u6, d5 = P.isFunction(this) ? P(O) : P(this), f4 = d5.selector || "", T = (new Date).getTime(), S = [], D = c6, A = "string" == typeof D, E = [].slice.call(arguments, 1), F = !1;
        return d5.each(function() {
            var m, o20, g, p, h, v = P.isPlainObject(c6) ? P.extend(!0, {
            }, P.fn.tab.settings, c6) : P.extend({
            }, P.fn.tab.settings), b = v.className, y = v.metadata, t53 = v.selector, x = v.error, n22 = v.regExp, e98 = "." + v.namespace, i19 = "module-" + v.namespace, C = P(this), a19 = {
            }, w = !0, r19 = 0, s10 = this, l9 = C.data(i19), k = {
                initialize: function() {
                    k.debug("Initializing tab menu item", C), k.fix.callbacks(), k.determineTabs(), k.debug("Determining tabs", v.context, o20), v.auto && k.set.auto(), k.bind.events(), v.history && !F && (k.initializeHistory(), F = !0), v.autoTabActivation && l9 === M && null == k.determine.activeTab() && (k.debug("No active tab detected, setting first tab active", k.get.initialPath()), k.changeTab(!0 === v.autoTabActivation ? k.get.initialPath() : v.autoTabActivation)), k.instantiate();
                },
                instantiate: function() {
                    k.verbose("Storing instance of module", k), l9 = k, C.data(i19, k);
                },
                destroy: function() {
                    k.debug("Destroying tabs", C), C.removeData(i19).off(e98);
                },
                bind: {
                    events: function() {
                        P.isWindow(s10) || (k.debug("Attaching tab activation events to element", C), C.on("click" + e98, k.event.click));
                    }
                },
                determineTabs: function() {
                    var e;
                    "parent" === v.context ? (0 < C.closest(t53.ui).length ? (e = C.closest(t53.ui), k.verbose("Using closest UI element as parent", e)) : e = C, m = e.parent(), k.verbose("Determined parent element for creating context", m)) : v.context ? (m = P(v.context), k.verbose("Using selector for tab context", v.context, m)) : m = P("body"), v.childrenOnly ? (o20 = m.children(t53.tabs), k.debug("Searching tab context children for tabs", m, o20)) : (o20 = m.find(t53.tabs), k.debug("Searching tab context for tabs", m, o20));
                },
                fix: {
                    callbacks: function() {
                        P.isPlainObject(c6) && (c6.onTabLoad || c6.onTabInit) && (c6.onTabLoad && (c6.onLoad = c6.onTabLoad, delete c6.onTabLoad, k.error(x.legacyLoad, c6.onLoad)), c6.onTabInit && (c6.onFirstLoad = c6.onTabInit, delete c6.onTabInit, k.error(x.legacyInit, c6.onFirstLoad)), v = P.extend(!0, {
                        }, P.fn.tab.settings, c6));
                    }
                },
                initializeHistory: function() {
                    if (k.debug("Initializing page state"), P.address === M) return k.error(x.state), !1;
                    if ("state" == v.historyType) {
                        if (k.debug("Using HTML5 to manage state"), !1 === v.path) return k.error(x.path), !1;
                        P.address.history(!0).state(v.path);
                    }
                    P.address.bind("change", k.event.history.change);
                },
                event: {
                    click: function(e) {
                        var t = P(this).data(y.tab);
                        t !== M ? (v.history ? (k.verbose("Updating page state", e), P.address.value(t)) : (k.verbose("Changing tab", e), k.changeTab(t)), e.preventDefault()) : k.debug("No tab specified");
                    },
                    history: {
                        change: function(e) {
                            var t = e.pathNames.join("/") || k.get.initialPath(), n = v.templates.determineTitle(t) || !1;
                            k.performance.display(), k.debug("History change event", t, e), h = e, t !== M && k.changeTab(t), n && P.address.title(n);
                        }
                    }
                },
                refresh: function() {
                    g && (k.debug("Refreshing tab", g), k.changeTab(g));
                },
                cache: {
                    read: function(e) {
                        return e !== M && a19[e];
                    },
                    add: function(e, t) {
                        e = e || g, k.debug("Adding cached content for", e), a19[e] = t;
                    },
                    remove: function(e) {
                        e = e || g, k.debug("Removing cached content for", e), delete a19[e];
                    }
                },
                escape: {
                    string: function(e) {
                        return (e = String(e)).replace(n22.escape, "\\$&");
                    }
                },
                set: {
                    auto: function() {
                        var e = "string" == typeof v.path ? v.path.replace(/\/$/, "") + "/{$tab}" : "/{$tab}";
                        k.verbose("Setting up automatic tab retrieval from server", e), P.isPlainObject(v.apiSettings) ? v.apiSettings.url = e : v.apiSettings = {
                            url: e
                        };
                    },
                    loading: function(e) {
                        var t = k.get.tabElement(e);
                        t.hasClass(b.loading) || (k.verbose("Setting loading state for", t), t.addClass(b.loading).siblings(o20).removeClass(b.active + " " + b.loading), 0 < t.length && v.onRequest.call(t[0], e));
                    },
                    state: function(e) {
                        P.address.value(e);
                    }
                },
                changeTab: function(c) {
                    var u = O.history && O.history.pushState && v.ignoreFirstLoad && w, d = v.auto || P.isPlainObject(v.apiSettings), f = d && !u ? k.utilities.pathToArray(c) : k.get.defaultPathArray(c);
                    c = k.utilities.arrayToPath(f), P.each(f, function(e, t) {
                        var n, i, o = f.slice(0, e + 1), a = k.utilities.arrayToPath(o), r = k.is.tab(a), s = e + 1 == f.length, l = k.get.tabElement(a);
                        return k.verbose("Looking for tab", t), r ? (k.verbose("Tab was found", t), g = a, p = k.utilities.filterArray(f, o), s ? i = !0 : (s = f.slice(0, e + 2), e = k.utilities.arrayToPath(s), (i = !k.is.tab(e)) && k.verbose("Tab parameters found", s)), i && d ? (u ? (k.debug("Ignoring remote content on first tab load", a), w = !1, k.cache.add(c, l.html()), k.activate.all(a), v.onFirstLoad.call(l[0], a, p, h), v.onLoad.call(l[0], a, p, h)) : (k.activate.navigation(a), k.fetch.content(a, c)), !1) : (k.debug("Opened local tab", a), k.activate.all(a), k.cache.read(a) || (k.cache.add(a, !0), k.debug("First time tab loaded calling tab init"), v.onFirstLoad.call(l[0], a, p, h)), void v.onLoad.call(l[0], a, p, h))) : -1 != c.search("/") || "" === c ? (k.error(x.missingTab, C, m, a), !1) : (c = k.escape.string(c), a = (n = P("#" + c + ', a[name="' + c + '"]')).closest("[data-tab]").data(y.tab), l = k.get.tabElement(a), n && 0 < n.length && a ? (k.debug("Anchor link used, opening parent tab", l, n), l.hasClass(b.active) || setTimeout(function() {
                            k.scrollTo(n);
                        }, 0), k.activate.all(a), k.cache.read(a) || (k.cache.add(a, !0), k.debug("First time tab loaded calling tab init"), v.onFirstLoad.call(l[0], a, p, h)), v.onLoad.call(l[0], a, p, h), !1) : void 0);
                    });
                },
                scrollTo: function(e) {
                    var t = !!(e && 0 < e.length) && e.offset().top;
                    !1 !== t && (k.debug("Forcing scroll to an in-page link in a hidden tab", t, e), P(R).scrollTop(t));
                },
                update: {
                    content: function(e, t, n) {
                        var i = k.get.tabElement(e), o = i[0];
                        n = n !== M ? n : v.evaluateScripts, "string" == typeof v.cacheType && "dom" == v.cacheType.toLowerCase() && "string" != typeof t ? i.empty().append(P(t).clone(!0)) : n ? (k.debug("Updating HTML and evaluating inline scripts", e, t), i.html(t)) : (k.debug("Updating HTML", e, t), o.innerHTML = t);
                    }
                },
                fetch: {
                    content: function(t, n) {
                        var i = k.get.tabElement(t), e99 = {
                            dataType: "html",
                            encodeParameters: !1,
                            on: "now",
                            cache: v.alwaysRefresh,
                            headers: {
                                "X-Remote": !0
                            },
                            onSuccess: function(e100) {
                                "response" == v.cacheType && k.cache.add(n, e100), k.update.content(t, e100), t == g ? (k.debug("Content loaded", t), k.activate.tab(t)) : k.debug("Content loaded in background", t), v.onFirstLoad.call(i[0], t, p, h), v.onLoad.call(i[0], t, p, h), v.loadOnce ? k.cache.add(n, !0) : "string" == typeof v.cacheType && "dom" == v.cacheType.toLowerCase() && 0 < i.children().length ? setTimeout(function() {
                                    var e = (e = i.children().clone(!0)).not("script");
                                    k.cache.add(n, e);
                                }, 0) : k.cache.add(n, i.html());
                            },
                            urlData: {
                                tab: n
                            }
                        }, o = i.api("get request") || !1, a = o && "pending" === o.state();
                        n = n || t, o = k.cache.read(n), v.cache && o ? (k.activate.tab(t), k.debug("Adding cached content", n), v.loadOnce || ("once" == v.evaluateScripts ? k.update.content(t, o, !1) : k.update.content(t, o)), v.onLoad.call(i[0], t, p, h)) : a ? (k.set.loading(t), k.debug("Content is already loading", n)) : P.api !== M ? (e99 = P.extend(!0, {
                        }, v.apiSettings, e99), k.debug("Retrieving remote content", n, e99), k.set.loading(t), i.api(e99)) : k.error(x.api);
                    }
                },
                activate: {
                    all: function(e) {
                        k.activate.tab(e), k.activate.navigation(e);
                    },
                    tab: function(e) {
                        var t = k.get.tabElement(e), n = "siblings" == v.deactivate ? t.siblings(o20) : o20.not(t), i = t.hasClass(b.active);
                        k.verbose("Showing tab content for", t), i || (t.addClass(b.active), n.removeClass(b.active + " " + b.loading), 0 < t.length && v.onVisible.call(t[0], e));
                    },
                    navigation: function(e) {
                        var t = k.get.navElement(e), n = "siblings" == v.deactivate ? t.siblings(d5) : d5.not(t), i = t.hasClass(b.active);
                        k.verbose("Activating tab navigation for", t, e), i || (t.addClass(b.active), n.removeClass(b.active + " " + b.loading));
                    }
                },
                deactivate: {
                    all: function() {
                        k.deactivate.navigation(), k.deactivate.tabs();
                    },
                    navigation: function() {
                        d5.removeClass(b.active);
                    },
                    tabs: function() {
                        o20.removeClass(b.active + " " + b.loading);
                    }
                },
                is: {
                    tab: function(e) {
                        return e !== M && 0 < k.get.tabElement(e).length;
                    }
                },
                get: {
                    initialPath: function() {
                        return d5.eq(0).data(y.tab) || o20.eq(0).data(y.tab);
                    },
                    path: function() {
                        return P.address.value();
                    },
                    defaultPathArray: function(e) {
                        return k.utilities.pathToArray(k.get.defaultPath(e));
                    },
                    defaultPath: function(e) {
                        var t = d5.filter("[data-" + y.tab + '^="' + k.escape.string(e) + '/"]').eq(0).data(y.tab) || !1;
                        if (t) {
                            if (k.debug("Found default tab", t), r19 < v.maxDepth) return r19++, k.get.defaultPath(t);
                            k.error(x.recursion);
                        } else k.debug("No default tabs found for", e, o20);
                        return r19 = 0, e;
                    },
                    navElement: function(e) {
                        return e = e || g, d5.filter("[data-" + y.tab + '="' + k.escape.string(e) + '"]');
                    },
                    tabElement: function(e) {
                        var t;
                        return e = e || g, t = k.utilities.pathToArray(e), t = k.utilities.last(t), e = o20.filter("[data-" + y.tab + '="' + k.escape.string(e) + '"]'), t = o20.filter("[data-" + y.tab + '="' + k.escape.string(t) + '"]'), 0 < e.length ? e : t;
                    },
                    tab: function() {
                        return g;
                    }
                },
                determine: {
                    activeTab: function() {
                        var n = null;
                        return o20.each(function(e, t) {
                            P(t).hasClass(b.active) && (t = P(this).data(y.tab), d5.filter("[data-" + y.tab + '="' + k.escape.string(t) + '"]').hasClass(b.active) && (n = t));
                        }), n;
                    }
                },
                utilities: {
                    filterArray: function(e101, t) {
                        return P.grep(e101, function(e) {
                            return -1 == P.inArray(e, t);
                        });
                    },
                    last: function(e) {
                        return !!Array.isArray(e) && e[e.length - 1];
                    },
                    pathToArray: function(e) {
                        return "string" == typeof (e = e === M ? g : e) ? e.split("/") : [
                            e
                        ];
                    },
                    arrayToPath: function(e) {
                        return !!Array.isArray(e) && e.join("/");
                    }
                },
                setting: function(e, t) {
                    if (k.debug("Changing setting", e, t), P.isPlainObject(e)) P.extend(!0, v, e);
                    else {
                        if (t === M) return v[e];
                        P.isPlainObject(v[e]) ? P.extend(!0, v[e], t) : v[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (P.isPlainObject(e)) P.extend(!0, k, e);
                    else {
                        if (t === M) return k[e];
                        k[e] = t;
                    }
                },
                debug: function() {
                    !v.silent && v.debug && (v.performance ? k.performance.log(arguments) : (k.debug = Function.prototype.bind.call(console.info, console, v.name + ":"), k.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !v.silent && v.verbose && v.debug && (v.performance ? k.performance.log(arguments) : (k.verbose = Function.prototype.bind.call(console.info, console, v.name + ":"), k.verbose.apply(console, arguments)));
                },
                error: function() {
                    v.silent || (k.error = Function.prototype.bind.call(console.error, console, v.name + ":"), k.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        v.performance && (n = (t = (new Date).getTime()) - (T || t), T = t, S.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: s10,
                            "Execution Time": n
                        })), clearTimeout(k.performance.timer), k.performance.timer = setTimeout(k.performance.display, 500);
                    },
                    display: function() {
                        var e = v.name + ":", n = 0;
                        T = !1, clearTimeout(k.performance.timer), P.each(S, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", f4 && (e += " '" + f4 + "'"), (console.group !== M || console.table !== M) && 0 < S.length && (console.groupCollapsed(e), console.table ? console.table(S) : P.each(S, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), S = [];
                    }
                },
                invoke: function(i, e102, t54) {
                    var o, a, n, r = l9;
                    return e102 = e102 || E, t54 = s10 || t54, "string" == typeof i && r !== M && (i = i.split(/[\. ]/), o = i.length - 1, P.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (P.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== M) return a = r[n], !1;
                            if (!P.isPlainObject(r[t]) || e == o) return r[t] !== M ? a = r[t] : k.error(x.method, i), !1;
                            r = r[t];
                        }
                    })), P.isFunction(a) ? n = a.apply(t54, e102) : a !== M && (n = a), Array.isArray(u6) ? u6.push(n) : u6 !== M ? u6 = [
                        u6,
                        n
                    ] : n !== M && (u6 = n), a;
                }
            };
            A ? (l9 === M && k.initialize(), k.invoke(D)) : (l9 !== M && l9.invoke("destroy"), k.initialize());
        }), u6 !== M ? u6 : this;
    }, P.tab = function() {
        P(O).tab.apply(this, arguments);
    }, P.fn.tab.settings = {
        name: "Tab",
        namespace: "tab",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        auto: !1,
        history: !1,
        historyType: "hash",
        path: !1,
        context: !1,
        childrenOnly: !1,
        maxDepth: 25,
        deactivate: "siblings",
        alwaysRefresh: !1,
        cache: !0,
        loadOnce: !1,
        cacheType: "response",
        ignoreFirstLoad: !1,
        apiSettings: !1,
        evaluateScripts: "once",
        autoTabActivation: !0,
        onFirstLoad: function(e, t, n) {
        },
        onLoad: function(e, t, n) {
        },
        onVisible: function(e, t, n) {
        },
        onRequest: function(e, t, n) {
        },
        templates: {
            determineTitle: function(e) {
            }
        },
        error: {
            api: "You attempted to load content without API module",
            method: "The method you called is not defined",
            missingTab: "Activated tab cannot be found. Tabs are case-sensitive.",
            noContent: "The tab you specified is missing a content url.",
            path: "History enabled, but no path was specified",
            recursion: "Max recursive depth reached",
            legacyInit: "onTabInit has been renamed to onFirstLoad in 2.0, please adjust your code.",
            legacyLoad: "onTabLoad has been renamed to onLoad in 2.0. Please adjust your code",
            state: "History requires Asual's Address library <https://github.com/asual/jquery-address>"
        },
        regExp: {
            escape: /[-[\]{}()*+?.,\\^$|#\s:=@]/g
        },
        metadata: {
            tab: "tab",
            loaded: "loaded",
            promise: "promise"
        },
        className: {
            loading: "loading",
            active: "active"
        },
        selector: {
            tabs: ".ui.tab",
            ui: ".ui"
        }
    };
})(jQuery, window, document), (function(F, e103, P) {
    "use strict";
    F.isFunction = F.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e103 = void 0 !== e103 && e103.Math == Math ? e103 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), F.fn.toast = function(C) {
        var w, e104 = F(this), k = e104.selector || "", T = (new Date).getTime(), S = [], D = C, A = "string" == typeof D, E = [].slice.call(arguments, 1);
        return e104.each(function() {
            var n23, i20, a20, o21, r20, s, l, c = F.isPlainObject(C) ? F.extend(!0, {
            }, F.fn.toast.settings, C) : F.extend({
            }, F.fn.toast.settings), u = c.className, d = c.selector, f = c.error, e105 = c.namespace, m = c.fields, t55 = "." + e105, g = e105 + "-module", p = F(this), h = c.context ? F(c.context) : F("body"), v = p.hasClass("toast") || p.hasClass("message") || p.hasClass("card"), b = this, y = v ? p.data(g) : P, x = {
                initialize: function() {
                    x.verbose("Initializing element"), x.has.container() || x.create.container(), (v || "" !== c.message || "" !== c.title || "" !== x.get.iconClass() || c.showImage || x.has.configActions()) && ("string" == typeof c.showProgress && -1 !== [
                        u.top,
                        u.bottom
                    ].indexOf(c.showProgress) || (c.showProgress = !1), x.create.toast(), c.closeOnClick && (c.closeIcon || 0 < F(i20).find(d.input).length || x.has.configActions()) && (c.closeOnClick = !1), c.closeOnClick || n23.addClass(u.unclickable), x.bind.events()), x.instantiate(), n23 && x.show();
                },
                instantiate: function() {
                    x.verbose("Storing instance of toast"), y = x, p.data(g, y);
                },
                destroy: function() {
                    n23 && (x.debug("Removing toast", n23), x.unbind.events(), n23.remove(), s = i20 = n23 = P, c.onRemove.call(n23, b), l = r20 = o21 = P), p.removeData(g);
                },
                show: function(e) {
                    e = e || function() {
                    }, x.debug("Showing toast"), !1 !== c.onShow.call(n23, b) ? x.animate.show(e) : x.debug("onShow callback returned false, cancelling toast animation");
                },
                close: function(e) {
                    e = e || function() {
                    }, x.remove.visible(), x.unbind.events(), x.animate.close(e);
                },
                create: {
                    container: function() {
                        x.verbose("Creating container"), h.append(F("<div/>", {
                            class: c.position + " " + u.container + " " + (c.horizontal ? u.horizontal : "")
                        }));
                    },
                    toast: function() {
                        n23 = F("<div/>", {
                            class: u.box
                        });
                        var e106, t56 = x.get.iconClass();
                        v ? (i20 = c.cloneModule ? p.clone().removeAttr("id") : p, l = i20.find("> i" + x.helpers.toClass(u.close)), c.closeIcon = 0 < l.length, "" !== t56 && i20.find(d.icon).attr("class", t56 + " " + u.icon), c.showImage && i20.find(d.image).attr("src", c.showImage), "" !== c.title && i20.find(d.title).html(x.helpers.escape(c.title, c.preserveHTML)), "" !== c.message && i20.find(d.message).html(x.helpers.escape(c.message, c.preserveHTML))) : (x.verbose("Creating toast"), i20 = F("<div/>"), e106 = F("<div/>", {
                            class: u.content
                        }), "" !== t56 && i20.append(F("<i/>", {
                            class: t56 + " " + u.icon
                        })), c.showImage && i20.append(F("<img>", {
                            class: u.image + " " + c.classImage,
                            src: c.showImage
                        })), "" !== c.title && e106.append(F("<div/>", {
                            class: u.title,
                            text: c.title
                        })), e106.append(F("<div/>", {
                            class: u.message,
                            html: x.helpers.escape(c.message, c.preserveHTML)
                        })), i20.addClass(c.class + " " + u.toast).append(e106), i20.css("opacity", c.opacity), c.closeIcon && ((l = F("<i/>", {
                            class: u.close + " " + ("string" == typeof c.closeIcon ? c.closeIcon : "")
                        })).hasClass(u.left) ? i20.prepend(l) : i20.append(l))), i20.hasClass(u.compact) && (c.compact = !0), i20.hasClass("card") && (c.compact = !1), a20 = i20.find(".actions"), x.has.configActions() && (0 === a20.length && (a20 = F("<div/>", {
                            class: u.actions + " " + (c.classActions || "")
                        }).appendTo(i20)), i20.hasClass("card") && !a20.hasClass(u.attached) && (a20.addClass(u.extraContent), a20.hasClass(u.vertical) && (a20.removeClass(u.vertical), x.error(f.verticalCard))), c.actions.forEach(function(e) {
                            var t = e[m.icon] ? '<i class="' + x.helpers.deQuote(e[m.icon]) + ' icon"></i>' : "", n = x.helpers.escape(e[m.text] || "", c.preserveHTML), i = x.helpers.deQuote(e[m.class] || ""), o = e[m.click] && F.isFunction(e[m.click]) ? e[m.click] : function() {
                            };
                            a20.append(F("<button/>", {
                                html: t + n,
                                class: u.button + " " + i,
                                click: function() {
                                    !1 !== o.call(b, p) && x.close();
                                }
                            }));
                        })), a20 && a20.hasClass(u.vertical) && i20.addClass(u.vertical), 0 < a20.length && !a20.hasClass(u.attached) && (!a20 || a20.hasClass(u.basic) && !a20.hasClass(u.left) || i20.addClass(u.actions)), "auto" === c.displayTime && (c.displayTime = Math.max(c.minDisplayTime, i20.text().split(" ").length / c.wordsPerMinute * 60000)), n23.append(i20), 0 < a20.length && a20.hasClass(u.attached) && (a20.addClass(u.buttons), a20.detach(), i20.addClass(u.attached), a20.hasClass(u.vertical) ? (i20.wrap(F("<div/>", {
                            class: u.vertical + " " + u.attached + " " + (c.compact ? u.compact : "")
                        })), a20.hasClass(u.left) ? i20.addClass(u.left).parent().addClass(u.left).prepend(a20) : i20.parent().append(a20)) : a20.hasClass(u.top) ? (n23.prepend(a20), i20.addClass(u.bottom)) : (n23.append(a20), i20.addClass(u.top))), p !== i20 && (b = (p = i20)[0]), 0 < c.displayTime && (e106 = u.progressing + " " + (c.pauseOnHover ? u.pausable : ""), c.showProgress && (o21 = F("<div/>", {
                            class: u.progress + " " + (c.classProgress || c.class),
                            "data-percent": ""
                        }), c.classProgress || (i20.hasClass("toast") && !i20.hasClass(u.inverted) ? o21.addClass(u.inverted) : o21.removeClass(u.inverted)), r20 = F("<div/>", {
                            class: "bar " + (c.progressUp ? "up " : "down ") + e106
                        }), o21.addClass(c.showProgress).append(r20), o21.hasClass(u.top) ? n23.prepend(o21) : n23.append(o21), r20.css("animation-duration", c.displayTime / 1000 + "s")), (s = F("<span/>", {
                            class: "wait " + e106
                        })).css("animation-duration", c.displayTime / 1000 + "s"), s.appendTo(i20)), c.compact && (n23.addClass(u.compact), i20.addClass(u.compact), o21 && o21.addClass(u.compact)), c.newestOnTop ? n23.prependTo(x.get.container()) : n23.appendTo(x.get.container());
                    }
                },
                bind: {
                    events: function() {
                        x.debug("Binding events to toast"), (c.closeOnClick || c.closeIcon) && (c.closeIcon ? l : i20).on("click" + t55, x.event.click), s && s.on("animationend" + t55, x.close), n23.on("click" + t55, d.approve, x.event.approve).on("click" + t55, d.deny, x.event.deny);
                    }
                },
                unbind: {
                    events: function() {
                        x.debug("Unbinding events to toast"), (c.closeOnClick || c.closeIcon) && (c.closeIcon ? l : i20).off("click" + t55), s && s.off("animationend" + t55), n23.off("click" + t55);
                    }
                },
                animate: {
                    show: function(e) {
                        e = F.isFunction(e) ? e : function() {
                        }, c.transition && x.can.useElement("transition") && p.transition("is supported") && (x.set.visible(), n23.transition({
                            animation: c.transition.showMethod + " in",
                            queue: !1,
                            debug: c.debug,
                            verbose: c.verbose,
                            duration: c.transition.showDuration,
                            onComplete: function() {
                                e.call(n23, b), c.onVisible.call(n23, b);
                            }
                        }));
                    },
                    close: function(e107) {
                        e107 = F.isFunction(e107) ? e107 : function() {
                        }, x.debug("Closing toast"), !1 !== c.onHide.call(n23, b) ? c.transition && F.fn.transition !== P && p.transition("is supported") ? n23.transition({
                            animation: c.transition.hideMethod + " out",
                            queue: !1,
                            duration: c.transition.hideDuration,
                            debug: c.debug,
                            verbose: c.verbose,
                            interval: 50,
                            onBeforeHide: function(e) {
                                e = F.isFunction(e) ? e : function() {
                                }, "" !== c.transition.closeEasing ? n23 && (n23.css("opacity", 0), n23.wrap("<div/>").parent().hide(c.transition.closeDuration, c.transition.closeEasing, function() {
                                    n23 && (n23.parent().remove(), e.call(n23));
                                })) : e.call(n23);
                            },
                            onComplete: function() {
                                e107.call(n23, b), c.onHidden.call(n23, b), x.destroy();
                            }
                        }) : x.error(f.noTransition) : x.debug("onHide callback returned false, cancelling toast animation");
                    },
                    pause: function() {
                        s.css("animationPlayState", "paused"), r20 && r20.css("animationPlayState", "paused");
                    },
                    continue: function() {
                        s.css("animationPlayState", "running"), r20 && r20.css("animationPlayState", "running");
                    }
                },
                has: {
                    container: function() {
                        return x.verbose("Determining if there is already a container"), 0 < h.find(x.helpers.toClass(c.position) + d.container + (c.horizontal ? x.helpers.toClass(u.horizontal) : "")).length;
                    },
                    toast: function() {
                        return !!x.get.toast();
                    },
                    toasts: function() {
                        return 0 < x.get.toasts().length;
                    },
                    configActions: function() {
                        return Array.isArray(c.actions) && 0 < c.actions.length;
                    }
                },
                get: {
                    container: function() {
                        return h.find(x.helpers.toClass(c.position) + d.container)[0];
                    },
                    toastBox: function() {
                        return n23 || null;
                    },
                    toast: function() {
                        return i20 || null;
                    },
                    toasts: function() {
                        return F(x.get.container()).find(d.box);
                    },
                    iconClass: function() {
                        return "string" == typeof c.showIcon ? c.showIcon : c.showIcon && c.icons[c.class] ? c.icons[c.class] : "";
                    },
                    remainingTime: function() {
                        return s ? s.css("opacity") * c.displayTime : 0;
                    }
                },
                set: {
                    visible: function() {
                        i20.addClass(u.visible);
                    }
                },
                remove: {
                    visible: function() {
                        i20.removeClass(u.visible);
                    }
                },
                event: {
                    click: function(e) {
                        0 === F(e.target).closest("a").length && (c.onClick.call(n23, b), x.close());
                    },
                    approve: function() {
                        !1 !== c.onApprove.call(b, p) ? x.close() : x.verbose("Approve callback returned false cancelling close");
                    },
                    deny: function() {
                        !1 !== c.onDeny.call(b, p) ? x.close() : x.verbose("Deny callback returned false cancelling close");
                    }
                },
                helpers: {
                    toClass: function(e108) {
                        var e108 = e108.split(" "), t = "";
                        return e108.forEach(function(e) {
                            t += "." + e;
                        }), t;
                    },
                    deQuote: function(e) {
                        return String(e).replace(/"/g, "");
                    },
                    escape: function(e, t) {
                        if (t) return e;
                        var n = {
                            "<": "&lt;",
                            ">": "&gt;",
                            '"': "&quot;",
                            "'": "&#x27;",
                            "`": "&#x60;"
                        };
                        return /[&<>"'`]/.test(e) ? (e = e.replace(/&(?![a-z0-9#]{1,6};)/, "&amp;")).replace(/[<>"'`]/g, function(e) {
                            return n[e];
                        }) : e;
                    }
                },
                can: {
                    useElement: function(e) {
                        return F.fn[e] !== P || (x.error(f.noElement.replace("{element}", e)), !1);
                    }
                },
                setting: function(e, t) {
                    if (x.debug("Changing setting", e, t), F.isPlainObject(e)) F.extend(!0, c, e);
                    else {
                        if (t === P) return c[e];
                        F.isPlainObject(c[e]) ? F.extend(!0, c[e], t) : c[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (F.isPlainObject(e)) F.extend(!0, x, e);
                    else {
                        if (t === P) return x[e];
                        x[e] = t;
                    }
                },
                debug: function() {
                    !c.silent && c.debug && (c.performance ? x.performance.log(arguments) : (x.debug = Function.prototype.bind.call(console.info, console, c.name + ":"), x.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !c.silent && c.verbose && c.debug && (c.performance ? x.performance.log(arguments) : (x.verbose = Function.prototype.bind.call(console.info, console, c.name + ":"), x.verbose.apply(console, arguments)));
                },
                error: function() {
                    c.silent || (x.error = Function.prototype.bind.call(console.error, console, c.name + ":"), x.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        c.performance && (n = (t = (new Date).getTime()) - (T || t), T = t, S.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: b,
                            "Execution Time": n
                        })), clearTimeout(x.performance.timer), x.performance.timer = setTimeout(x.performance.display, 500);
                    },
                    display: function() {
                        var e = c.name + ":", n = 0;
                        T = !1, clearTimeout(x.performance.timer), F.each(S, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", k && (e += " '" + k + "'"), (console.group !== P || console.table !== P) && 0 < S.length && (console.groupCollapsed(e), console.table ? console.table(S) : F.each(S, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), S = [];
                    }
                },
                invoke: function(i, e109, t57) {
                    var o, a, n, r = y;
                    return e109 = e109 || E, t57 = b || t57, "string" == typeof i && r !== P && (i = i.split(/[\. ]/), o = i.length - 1, F.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (F.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== P) return a = r[n], !1;
                            if (!F.isPlainObject(r[t]) || e == o) return r[t] !== P ? a = r[t] : x.error(f.method, i), !1;
                            r = r[t];
                        }
                    })), F.isFunction(a) ? n = a.apply(t57, e109) : a !== P && (n = a), Array.isArray(w) ? w.push(n) : w !== P ? w = [
                        w,
                        n
                    ] : n !== P && (w = n), a;
                }
            };
            A ? (y === P && x.initialize(), x.invoke(D)) : (y !== P && y.invoke("destroy"), x.initialize(), w = p);
        }), w !== P ? w : this;
    }, F.fn.toast.settings = {
        name: "Toast",
        namespace: "toast",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        context: "body",
        position: "top right",
        horizontal: !1,
        class: "neutral",
        classProgress: !1,
        classActions: !1,
        classImage: "mini",
        title: "",
        message: "",
        displayTime: 3000,
        minDisplayTime: 1000,
        wordsPerMinute: 120,
        showIcon: !1,
        newestOnTop: !1,
        showProgress: !1,
        pauseOnHover: !0,
        progressUp: !1,
        opacity: 1,
        compact: !0,
        closeIcon: !1,
        closeOnClick: !0,
        cloneModule: !0,
        actions: !1,
        preserveHTML: !0,
        showImage: !1,
        transition: {
            showMethod: "scale",
            showDuration: 500,
            hideMethod: "scale",
            hideDuration: 500,
            closeEasing: "easeOutCubic",
            closeDuration: 500
        },
        error: {
            method: "The method you called is not defined.",
            noElement: "This module requires ui {element}",
            verticalCard: "Vertical but not attached actions are not supported for card layout"
        },
        className: {
            container: "ui toast-container",
            box: "floating toast-box",
            progress: "ui attached active progress",
            toast: "ui toast",
            icon: "centered icon",
            visible: "visible",
            content: "content",
            title: "ui header",
            message: "message",
            actions: "actions",
            extraContent: "extra content",
            button: "ui button",
            buttons: "ui buttons",
            close: "close icon",
            image: "ui image",
            vertical: "vertical",
            horizontal: "horizontal",
            attached: "attached",
            inverted: "inverted",
            compact: "compact",
            pausable: "pausable",
            progressing: "progressing",
            top: "top",
            bottom: "bottom",
            left: "left",
            basic: "basic",
            unclickable: "unclickable"
        },
        icons: {
            info: "info",
            success: "checkmark",
            warning: "warning",
            error: "times"
        },
        selector: {
            container: ".ui.toast-container",
            box: ".toast-box",
            toast: ".ui.toast",
            title: ".header",
            message: ".message:not(.ui)",
            image: "> img.image, > .image > img",
            icon: "> i.icon",
            input: 'input:not([type="hidden"]), textarea, select, button, .ui.button, ui.dropdown',
            approve: ".actions .positive, .actions .approve, .actions .ok",
            deny: ".actions .negative, .actions .deny, .actions .cancel"
        },
        fields: {
            class: "class",
            text: "text",
            icon: "icon",
            click: "click"
        },
        onShow: function() {
        },
        onVisible: function() {
        },
        onClick: function() {
        },
        onHide: function() {
        },
        onHidden: function() {
        },
        onRemove: function() {
        },
        onApprove: function() {
        },
        onDeny: function() {
        }
    }, F.extend(F.easing, {
        easeOutBounce: function(e, t, n, i, o) {
            return (t /= o) < 1 / 2.75 ? i * (7.5625 * t * t) + n : t < 2 / 2.75 ? i * (7.5625 * (t -= 1.5 / 2.75) * t + 0.75) + n : t < 2.5 / 2.75 ? i * (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375) + n : i * (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375) + n;
        },
        easeOutCubic: function(e) {
            return --e * e * e + 1;
        }
    });
})(jQuery, window, void 0), (function(C, e110, w, k) {
    "use strict";
    C.isFunction = C.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e110 = void 0 !== e110 && e110.Math == Math ? e110 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), C.fn.transition = function() {
        var m, r21 = C(this), g = r21.selector || "", p = (new Date).getTime(), h = [], v = arguments, b = v[0], y = [].slice.call(arguments, 1), x = "string" == typeof b;
        return r21.each(function(n24) {
            var l, s11, t58, c, i21, o22, e111, a21, u = C(this), d = this, f = {
                initialize: function() {
                    l = f.get.settings.apply(d, v), c = l.className, t58 = l.error, i21 = l.metadata, a21 = "." + l.namespace, e111 = "module-" + l.namespace, s11 = u.data(e111) || f, o22 = f.get.animationEndEvent(), !1 === (x = x && f.invoke(b)) && (f.verbose("Converted arguments into settings object", l), l.interval ? f.delay(l.animate) : f.animate(), f.instantiate());
                },
                instantiate: function() {
                    f.verbose("Storing instance of module", f), s11 = f, u.data(e111, s11);
                },
                destroy: function() {
                    f.verbose("Destroying previous module for", d), u.removeData(e111);
                },
                refresh: function() {
                    f.verbose("Refreshing display type on next animation"), delete f.displayType;
                },
                forceRepaint: function() {
                    f.verbose("Forcing element repaint");
                    var e = u.parent(), t = u.next();
                    0 === t.length ? u.detach().appendTo(e) : u.detach().insertBefore(t);
                },
                repaint: function() {
                    f.verbose("Repainting element");
                    d.offsetWidth;
                },
                delay: function(e) {
                    var t = (t = f.get.animationDirection()) || (f.can.transition() ? f.get.direction() : "static");
                    e = e !== k ? e : l.interval, t = "auto" == l.reverse && t == c.outward || 1 == l.reverse ? (r21.length - n24) * l.interval : n24 * l.interval, f.debug("Delaying animation by", t), setTimeout(f.animate, t);
                },
                animate: function(e) {
                    if (l = e || l, !f.is.supported()) return f.error(t58.support), !1;
                    if (f.debug("Preparing animation", l.animation), f.is.animating()) {
                        if (l.queue) return !l.allowRepeats && f.has.direction() && f.is.occurring() && !0 !== f.queuing ? f.debug("Animation is currently occurring, preventing queueing same animation", l.animation) : f.queue(l.animation), !1;
                        if (!l.allowRepeats && f.is.occurring()) return f.debug("Animation is already occurring, will not execute repeated animation", l.animation), !1;
                        f.debug("New animation started, completing previous early", l.animation), s11.complete();
                    }
                    f.can.animate() ? f.set.animating(l.animation) : f.error(t58.noAnimation, l.animation, d);
                },
                reset: function() {
                    f.debug("Resetting animation to beginning conditions"), f.remove.animationCallbacks(), f.restore.conditions(), f.remove.animating();
                },
                queue: function(e) {
                    f.debug("Queueing animation of", e), f.queuing = !0, u.one(o22 + ".queue" + a21, function() {
                        f.queuing = !1, f.repaint(), f.animate.apply(this, l);
                    });
                },
                complete: function(e) {
                    e && e.target === d && e.stopPropagation(), f.debug("Animation complete", l.animation), f.remove.completeCallback(), f.remove.failSafe(), f.is.looping() || (f.is.outward() ? (f.verbose("Animation is outward, hiding element"), f.restore.conditions(), f.hide()) : f.is.inward() ? (f.verbose("Animation is outward, showing element"), f.restore.conditions(), f.show()) : (f.verbose("Static animation completed"), f.restore.conditions(), l.onComplete.call(d)));
                },
                force: {
                    visible: function() {
                        var e = u.attr("style"), t = f.get.userStyle(e), n = f.get.displayType(), e = t + "display: " + n + " !important;", t = u[0].style.display;
                        return !n || "none" === t && l.skipInlineHidden || u[0].tagName.match(/(script|link|style)/i) ? (f.remove.transition(), !1) : (f.verbose("Overriding default display to show element", n), u.attr("style", e), !0);
                    },
                    hidden: function() {
                        var e = u.attr("style"), t = u.css("display"), e = e === k || "" === e;
                        "none" === t || f.is.hidden() ? e && u.removeAttr("style") : (f.verbose("Overriding default display to hide element"), u.css("display", "none"));
                    }
                },
                has: {
                    direction: function(e) {
                        var n = !1;
                        return "string" == typeof (e = e || l.animation) && (e = e.split(" "), C.each(e, function(e, t) {
                            t !== c.inward && t !== c.outward || (n = !0);
                        })), n;
                    },
                    inlineDisplay: function() {
                        var e = u.attr("style") || "";
                        return Array.isArray(e.match(/display.*?;/, ""));
                    }
                },
                set: {
                    animating: function(e) {
                        f.remove.completeCallback(), e = e || l.animation;
                        e = f.get.animationClass(e);
                        f.save.animation(e), f.force.visible() && (f.remove.hidden(), f.remove.direction(), f.start.animation(e));
                    },
                    duration: function(e, t) {
                        !(t = "number" == typeof (t = t || l.duration) ? t + "ms" : t) && 0 !== t || (f.verbose("Setting animation duration", t), u.css({
                            "animation-duration": t
                        }));
                    },
                    direction: function(e) {
                        (e = e || f.get.direction()) == c.inward ? f.set.inward() : f.set.outward();
                    },
                    looping: function() {
                        f.debug("Transition set to loop"), u.addClass(c.looping);
                    },
                    hidden: function() {
                        u.addClass(c.transition).addClass(c.hidden);
                    },
                    inward: function() {
                        f.debug("Setting direction to inward"), u.removeClass(c.outward).addClass(c.inward);
                    },
                    outward: function() {
                        f.debug("Setting direction to outward"), u.removeClass(c.inward).addClass(c.outward);
                    },
                    visible: function() {
                        u.addClass(c.transition).addClass(c.visible);
                    }
                },
                start: {
                    animation: function(e) {
                        e = e || f.get.animationClass(), f.debug("Starting tween", e), u.addClass(e).one(o22 + ".complete" + a21, f.complete), l.useFailSafe && f.add.failSafe(), f.set.duration(l.duration), l.onStart.call(d);
                    }
                },
                save: {
                    animation: function(e) {
                        f.cache || (f.cache = {
                        }), f.cache.animation = e;
                    },
                    displayType: function(e) {
                        "none" !== e && u.data(i21.displayType, e);
                    },
                    transitionExists: function(e, t) {
                        C.fn.transition.exists[e] = t, f.verbose("Saving existence of transition", e, t);
                    }
                },
                restore: {
                    conditions: function() {
                        var e = f.get.currentAnimation();
                        e && (u.removeClass(e), f.verbose("Removing animation class", f.cache)), f.remove.duration();
                    }
                },
                add: {
                    failSafe: function() {
                        var e = f.get.duration();
                        f.timer = setTimeout(function() {
                            u.triggerHandler(o22);
                        }, e + l.failSafeDelay), f.verbose("Adding fail safe timer", f.timer);
                    }
                },
                remove: {
                    animating: function() {
                        u.removeClass(c.animating);
                    },
                    animationCallbacks: function() {
                        f.remove.queueCallback(), f.remove.completeCallback();
                    },
                    queueCallback: function() {
                        u.off(".queue" + a21);
                    },
                    completeCallback: function() {
                        u.off(".complete" + a21);
                    },
                    display: function() {
                        u.css("display", "");
                    },
                    direction: function() {
                        u.removeClass(c.inward).removeClass(c.outward);
                    },
                    duration: function() {
                        u.css("animation-duration", "");
                    },
                    failSafe: function() {
                        f.verbose("Removing fail safe timer", f.timer), f.timer && clearTimeout(f.timer);
                    },
                    hidden: function() {
                        u.removeClass(c.hidden);
                    },
                    visible: function() {
                        u.removeClass(c.visible);
                    },
                    looping: function() {
                        f.debug("Transitions are no longer looping"), f.is.looping() && (f.reset(), u.removeClass(c.looping));
                    },
                    transition: function() {
                        u.removeClass(c.transition).removeClass(c.visible).removeClass(c.hidden);
                    }
                },
                get: {
                    settings: function(e, t, n) {
                        return "object" == typeof e ? C.extend(!0, {
                        }, C.fn.transition.settings, e) : "function" == typeof n ? C.extend({
                        }, C.fn.transition.settings, {
                            animation: e,
                            onComplete: n,
                            duration: t
                        }) : "string" == typeof t || "number" == typeof t ? C.extend({
                        }, C.fn.transition.settings, {
                            animation: e,
                            duration: t
                        }) : "object" == typeof t ? C.extend({
                        }, C.fn.transition.settings, t, {
                            animation: e
                        }) : "function" == typeof t ? C.extend({
                        }, C.fn.transition.settings, {
                            animation: e,
                            onComplete: t
                        }) : C.extend({
                        }, C.fn.transition.settings, {
                            animation: e
                        });
                    },
                    animationClass: function(e) {
                        var t = e || l.animation, e = f.can.transition() && !f.has.direction() ? f.get.direction() + " " : "";
                        return c.animating + " " + c.transition + " " + e + t;
                    },
                    currentAnimation: function() {
                        return !(!f.cache || f.cache.animation === k) && f.cache.animation;
                    },
                    currentDirection: function() {
                        return f.is.inward() ? c.inward : c.outward;
                    },
                    direction: function() {
                        return f.is.hidden() || !f.is.visible() ? c.inward : c.outward;
                    },
                    animationDirection: function(e) {
                        var n;
                        return "string" == typeof (e = e || l.animation) && (e = e.split(" "), C.each(e, function(e, t) {
                            t === c.inward ? n = c.inward : t === c.outward && (n = c.outward);
                        })), n || !1;
                    },
                    duration: function(e) {
                        return "string" == typeof (e = !1 === (e = e || l.duration) ? u.css("animation-duration") || 0 : e) ? -1 < e.indexOf("ms") ? parseFloat(e) : 1000 * parseFloat(e) : e;
                    },
                    displayType: function(e) {
                        return l.displayType || ((e = e === k || e) && u.data(i21.displayType) === k && ("" === (e = u.css("display")) || "none" === e ? f.can.transition(!0) : f.save.displayType(e)), u.data(i21.displayType));
                    },
                    userStyle: function(e) {
                        return (e = e || u.attr("style") || "").replace(/display.*?;/, "");
                    },
                    transitionExists: function(e) {
                        return C.fn.transition.exists[e];
                    },
                    animationStartEvent: function() {
                        var e, t = w.createElement("div"), n = {
                            animation: "animationstart",
                            OAnimation: "oAnimationStart",
                            MozAnimation: "mozAnimationStart",
                            WebkitAnimation: "webkitAnimationStart"
                        };
                        for(e in n)if (t.style[e] !== k) return n[e];
                        return !1;
                    },
                    animationEndEvent: function() {
                        var e, t = w.createElement("div"), n = {
                            animation: "animationend",
                            OAnimation: "oAnimationEnd",
                            MozAnimation: "mozAnimationEnd",
                            WebkitAnimation: "webkitAnimationEnd"
                        };
                        for(e in n)if (t.style[e] !== k) return n[e];
                        return !1;
                    }
                },
                can: {
                    transition: function(e) {
                        var t, n, i, o, a = l.animation, r = f.get.transitionExists(a), s = f.get.displayType(!1);
                        if (r === k || e) {
                            if (f.verbose("Determining whether animation exists"), t = u.attr("class"), i = u.prop("tagName"), e = (n = C("<" + i + " />").addClass(t).insertAfter(u)).addClass(a).removeClass(c.inward).removeClass(c.outward).addClass(c.animating).addClass(c.transition).css("animationName"), i = n.addClass(c.inward).css("animationName"), s || (s = n.attr("class", t).removeAttr("style").removeClass(c.hidden).removeClass(c.visible).show().css("display"), f.verbose("Determining final display state", s), f.save.displayType(s)), n.remove(), e != i) f.debug("Direction exists for animation", a), o = !0;
                            else {
                                if ("none" == e || !e) return void f.debug("No animation defined in css", a);
                                f.debug("Static animation found", a, s), o = !1;
                            }
                            f.save.transitionExists(a, o);
                        }
                        return r !== k ? r : o;
                    },
                    animate: function() {
                        return f.can.transition() !== k;
                    }
                },
                is: {
                    animating: function() {
                        return u.hasClass(c.animating);
                    },
                    inward: function() {
                        return u.hasClass(c.inward);
                    },
                    outward: function() {
                        return u.hasClass(c.outward);
                    },
                    looping: function() {
                        return u.hasClass(c.looping);
                    },
                    occurring: function(e) {
                        return e = "." + (e = e || l.animation).replace(" ", "."), 0 < u.filter(e).length;
                    },
                    visible: function() {
                        return u.is(":visible");
                    },
                    hidden: function() {
                        return "hidden" === u.css("visibility");
                    },
                    supported: function() {
                        return !1 !== o22;
                    }
                },
                hide: function() {
                    f.verbose("Hiding element"), f.is.animating() && f.reset(), d.blur(), f.remove.display(), f.remove.visible(), C.isFunction(l.onBeforeHide) ? l.onBeforeHide.call(d, function() {
                        f.hideNow();
                    }) : f.hideNow();
                },
                hideNow: function() {
                    f.set.hidden(), f.force.hidden(), l.onHide.call(d), l.onComplete.call(d);
                },
                show: function(e) {
                    f.verbose("Showing element", e), f.force.visible() && (f.remove.hidden(), f.set.visible(), l.onShow.call(d), l.onComplete.call(d));
                },
                toggle: function() {
                    f.is.visible() ? f.hide() : f.show();
                },
                stop: function() {
                    f.debug("Stopping current animation"), u.triggerHandler(o22);
                },
                stopAll: function() {
                    f.debug("Stopping all animation"), f.remove.queueCallback(), u.triggerHandler(o22);
                },
                clear: {
                    queue: function() {
                        f.debug("Clearing animation queue"), f.remove.queueCallback();
                    }
                },
                enable: function() {
                    f.verbose("Starting animation"), u.removeClass(c.disabled);
                },
                disable: function() {
                    f.debug("Stopping animation"), u.addClass(c.disabled);
                },
                setting: function(e, t) {
                    if (f.debug("Changing setting", e, t), C.isPlainObject(e)) C.extend(!0, l, e);
                    else {
                        if (t === k) return l[e];
                        C.isPlainObject(l[e]) ? C.extend(!0, l[e], t) : l[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (C.isPlainObject(e)) C.extend(!0, f, e);
                    else {
                        if (t === k) return f[e];
                        f[e] = t;
                    }
                },
                debug: function() {
                    !l.silent && l.debug && (l.performance ? f.performance.log(arguments) : (f.debug = Function.prototype.bind.call(console.info, console, l.name + ":"), f.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !l.silent && l.verbose && l.debug && (l.performance ? f.performance.log(arguments) : (f.verbose = Function.prototype.bind.call(console.info, console, l.name + ":"), f.verbose.apply(console, arguments)));
                },
                error: function() {
                    l.silent || (f.error = Function.prototype.bind.call(console.error, console, l.name + ":"), f.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        l.performance && (n = (t = (new Date).getTime()) - (p || t), p = t, h.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: d,
                            "Execution Time": n
                        })), clearTimeout(f.performance.timer), f.performance.timer = setTimeout(f.performance.display, 500);
                    },
                    display: function() {
                        var e = l.name + ":", n = 0;
                        p = !1, clearTimeout(f.performance.timer), C.each(h, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", g && (e += " '" + g + "'"), 1 < r21.length && (e += " (" + r21.length + ")"), (console.group !== k || console.table !== k) && 0 < h.length && (console.groupCollapsed(e), console.table ? console.table(h) : C.each(h, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), h = [];
                    }
                },
                invoke: function(i, e112, t59) {
                    var o, a, n, r = s11;
                    return e112 = e112 || y, t59 = d || t59, "string" == typeof i && r !== k && (i = i.split(/[\. ]/), o = i.length - 1, C.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (C.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== k) return a = r[n], !1;
                            if (!C.isPlainObject(r[t]) || e == o) return r[t] !== k && (a = r[t]), !1;
                            r = r[t];
                        }
                    })), C.isFunction(a) ? n = a.apply(t59, e112) : a !== k && (n = a), Array.isArray(m) ? m.push(n) : m !== k ? m = [
                        m,
                        n
                    ] : n !== k && (m = n), a !== k && a;
                }
            };
            f.initialize();
        }), m !== k ? m : this;
    }, C.fn.transition.exists = {
    }, C.fn.transition.settings = {
        name: "Transition",
        silent: !1,
        debug: !1,
        verbose: !1,
        performance: !0,
        namespace: "transition",
        interval: 0,
        reverse: "auto",
        onStart: function() {
        },
        onComplete: function() {
        },
        onShow: function() {
        },
        onHide: function() {
        },
        useFailSafe: !0,
        failSafeDelay: 100,
        allowRepeats: !1,
        displayType: !1,
        animation: "fade",
        duration: !1,
        queue: !0,
        skipInlineHidden: !1,
        metadata: {
            displayType: "display"
        },
        className: {
            animating: "animating",
            disabled: "disabled",
            hidden: "hidden",
            inward: "in",
            loading: "loading",
            looping: "looping",
            outward: "out",
            transition: "transition",
            visible: "visible"
        },
        error: {
            noAnimation: "Element is no longer attached to DOM. Unable to animate.  Use silent setting to surpress this warning in production.",
            repeated: "That animation is already occurring, cancelling repeated animation",
            method: "The method you called is not defined",
            support: "This browser does not support CSS animations"
        }
    };
})(jQuery, window, document), (function(E, F, P) {
    "use strict";
    E.isWindow = E.isWindow || function(e) {
        return null != e && e === e.window;
    }, F = void 0 !== F && F.Math == Math ? F : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), E.api = E.fn.api = function(x) {
        var C, e113 = E.isFunction(this) ? E(F) : E(this), w = e113.selector || "", k = (new Date).getTime(), T = [], S = x, D = "string" == typeof S, A = [].slice.call(arguments, 1);
        return e113.each(function() {
            var a22, o23, n25, e114, r22, s = E.isPlainObject(x) ? E.extend(!0, {
            }, E.fn.api.settings, x) : E.extend({
            }, E.fn.api.settings), t60 = s.namespace, i22 = s.metadata, l = s.selector, c = s.error, u = s.className, d = "." + t60, f = "module-" + t60, m = E(this), g = m.closest(l.form), p = s.stateContext ? E(s.stateContext) : m, h = this, v = p[0], b = m.data(f), y = {
                initialize: function() {
                    D || y.bind.events(), y.instantiate();
                },
                instantiate: function() {
                    y.verbose("Storing instance of module", y), b = y, m.data(f, b);
                },
                destroy: function() {
                    y.verbose("Destroying previous module for", h), m.removeData(f).off(d);
                },
                bind: {
                    events: function() {
                        var e = y.get.event();
                        e ? (y.verbose("Attaching API events to element", e), m.on(e + d, y.event.trigger)) : "now" == s.on && (y.debug("Querying API endpoint immediately"), y.query());
                    }
                },
                decode: {
                    json: function(e) {
                        if (e !== P && "string" == typeof e) try {
                            e = JSON.parse(e);
                        } catch (e115) {
                        }
                        return e;
                    }
                },
                read: {
                    cachedResponse: function(e) {
                        var t;
                        if (F.Storage !== P) return t = sessionStorage.getItem(e), y.debug("Using cached response", e, t), y.decode.json(t);
                        y.error(c.noStorage);
                    }
                },
                write: {
                    cachedResponse: function(e, t) {
                        t && "" === t ? y.debug("Response empty, not caching", t) : F.Storage !== P ? (E.isPlainObject(t) && (t = JSON.stringify(t)), sessionStorage.setItem(e, t), y.verbose("Storing cached response for url", e, t)) : y.error(c.noStorage);
                    }
                },
                query: function() {
                    if (y.is.disabled()) y.debug("Element is disabled API request aborted");
                    else {
                        if (y.is.loading()) {
                            if (!s.interruptRequests) return void y.debug("Cancelling request, previous request is still pending");
                            y.debug("Interrupting previous request"), y.abort();
                        }
                        if (s.defaultData && E.extend(!0, s.urlData, y.get.defaultData()), s.serializeForm && (s.data = y.add.formData(s.data)), !1 === (o23 = y.get.settings())) return y.cancelled = !0, void y.error(c.beforeSend);
                        if (y.cancelled = !1, (n25 = y.get.templatedURL()) || y.is.mocked()) {
                            if ((n25 = y.add.urlData(n25)) || y.is.mocked()) {
                                if (o23.url = s.base + n25, a22 = E.extend(!0, {
                                }, s, {
                                    type: s.method || s.type,
                                    data: e114,
                                    url: s.base + n25,
                                    beforeSend: s.beforeXHR,
                                    success: function() {
                                    },
                                    failure: function() {
                                    },
                                    complete: function() {
                                    }
                                }), y.debug("Querying URL", a22.url), y.verbose("Using AJAX settings", a22), "local" === s.cache && y.read.cachedResponse(n25)) return y.debug("Response returned from local cache"), y.request = y.create.request(), void y.request.resolveWith(v, [
                                    y.read.cachedResponse(n25)
                                ]);
                                s.throttle ? s.throttleFirstRequest || y.timer ? (y.debug("Throttling request", s.throttle), clearTimeout(y.timer), y.timer = setTimeout(function() {
                                    y.timer && delete y.timer, y.debug("Sending throttled request", e114, a22.method), y.send.request();
                                }, s.throttle)) : (y.debug("Sending request", e114, a22.method), y.send.request(), y.timer = setTimeout(function() {
                                }, s.throttle)) : (y.debug("Sending request", e114, a22.method), y.send.request());
                            }
                        } else y.error(c.missingURL);
                    }
                },
                should: {
                    removeError: function() {
                        return !0 === s.hideError || "auto" === s.hideError && !y.is.form();
                    }
                },
                is: {
                    disabled: function() {
                        return 0 < m.filter(l.disabled).length;
                    },
                    expectingJSON: function() {
                        return "json" === s.dataType || "jsonp" === s.dataType;
                    },
                    form: function() {
                        return m.is("form") || p.is("form");
                    },
                    mocked: function() {
                        return s.mockResponse || s.mockResponseAsync || s.response || s.responseAsync;
                    },
                    input: function() {
                        return m.is("input");
                    },
                    loading: function() {
                        return !!y.request && "pending" == y.request.state();
                    },
                    abortedRequest: function(e) {
                        return e && e.readyState !== P && 0 === e.readyState ? (y.verbose("XHR request determined to be aborted"), !0) : (y.verbose("XHR request was not aborted"), !1);
                    },
                    validResponse: function(e) {
                        return y.is.expectingJSON() && E.isFunction(s.successTest) ? (y.debug("Checking JSON returned success", s.successTest, e), s.successTest(e) ? (y.debug("Response passed success test", e), !0) : (y.debug("Response failed success test", e), !1)) : (y.verbose("Response is not JSON, skipping validation", s.successTest, e), !0);
                    }
                },
                was: {
                    cancelled: function() {
                        return y.cancelled || !1;
                    },
                    succesful: function() {
                        return y.verbose('This behavior will be deleted due to typo. Use "was successful" instead.'), y.was.successful();
                    },
                    successful: function() {
                        return y.request && "resolved" == y.request.state();
                    },
                    failure: function() {
                        return y.request && "rejected" == y.request.state();
                    },
                    complete: function() {
                        return y.request && ("resolved" == y.request.state() || "rejected" == y.request.state());
                    }
                },
                add: {
                    urlData: function(o, a) {
                        var e, t61;
                        return o && (e = o.match(s.regExp.required), t61 = o.match(s.regExp.optional), a = a || s.urlData, e && (y.debug("Looking for required URL variables", e), E.each(e, function(e, t) {
                            var n = -1 !== t.indexOf("$") ? t.substr(2, t.length - 3) : t.substr(1, t.length - 2), i = E.isPlainObject(a) && a[n] !== P ? a[n] : m.data(n) !== P ? m.data(n) : p.data(n) !== P ? p.data(n) : a[n];
                            if (i === P) return y.error(c.requiredParameter, n, o), o = !1;
                            y.verbose("Found required variable", n, i), i = s.encodeParameters ? y.get.urlEncodedValue(i) : i, o = o.replace(t, i);
                        })), t61 && (y.debug("Looking for optional URL variables", e), E.each(t61, function(e, t) {
                            var n = -1 !== t.indexOf("$") ? t.substr(3, t.length - 4) : t.substr(2, t.length - 3), i = E.isPlainObject(a) && a[n] !== P ? a[n] : m.data(n) !== P ? m.data(n) : p.data(n) !== P ? p.data(n) : a[n];
                            o = i !== P ? (y.verbose("Optional variable Found", n, i), o.replace(t, i)) : (y.verbose("Optional variable not found", n), -1 !== o.indexOf("/" + t) ? o.replace("/" + t, "") : o.replace(t, ""));
                        }))), o;
                    },
                    formData: function(e) {
                        var t = E.fn.serializeObject !== P, n = t ? g.serializeObject() : g.serialize();
                        return e = e || s.data, e = E.isPlainObject(e) ? t ? (y.debug("Extending existing data with form data", e, n), E.extend(!0, {
                        }, e, n)) : (y.error(c.missingSerialize), y.debug("Cant extend data. Replacing data with form data", e, n), n) : (y.debug("Adding form data", n), n);
                    }
                },
                send: {
                    request: function() {
                        y.set.loading(), y.request = y.create.request(), y.is.mocked() ? y.mockedXHR = y.create.mockedXHR() : y.xhr = y.create.xhr(), s.onRequest.call(v, y.request, y.xhr);
                    }
                },
                event: {
                    trigger: function(e) {
                        y.query(), "submit" != e.type && "click" != e.type || e.preventDefault();
                    },
                    xhr: {
                        always: function() {
                        },
                        done: function(e, t, n) {
                            var i = this, o = (new Date).getTime() - r22, a = s.loadingDuration - o, o = !!E.isFunction(s.onResponse) && (y.is.expectingJSON() && !s.rawResponse ? s.onResponse.call(i, E.extend(!0, {
                            }, e)) : s.onResponse.call(i, e)), a = 0 < a ? a : 0;
                            o && (y.debug("Modified API response in onResponse callback", s.onResponse, o, e), e = o), 0 < a && y.debug("Response completed early delaying state change by", a), setTimeout(function() {
                                y.is.validResponse(e) ? y.request.resolveWith(i, [
                                    e,
                                    n
                                ]) : y.request.rejectWith(i, [
                                    n,
                                    "invalid"
                                ]);
                            }, a);
                        },
                        fail: function(e, t, n) {
                            var i = this, o = (new Date).getTime() - r22, o = s.loadingDuration - o;
                            0 < (o = 0 < o ? o : 0) && y.debug("Response completed early delaying state change by", o), setTimeout(function() {
                                y.is.abortedRequest(e) ? y.request.rejectWith(i, [
                                    e,
                                    "aborted",
                                    n
                                ]) : y.request.rejectWith(i, [
                                    e,
                                    "error",
                                    t,
                                    n
                                ]);
                            }, o);
                        }
                    },
                    request: {
                        done: function(e, t) {
                            y.debug("Successful API Response", e), "local" === s.cache && n25 && (y.write.cachedResponse(n25, e), y.debug("Saving server response locally", y.cache)), s.onSuccess.call(v, e, m, t);
                        },
                        complete: function(e, t) {
                            var n, i;
                            y.was.successful() ? (i = e, n = t) : i = y.get.responseFromXHR(n = e), y.remove.loading(), s.onComplete.call(v, i, m, n);
                        },
                        fail: function(e, t, n) {
                            var i = y.get.responseFromXHR(e), o = y.get.errorFromRequest(i, t, n);
                            if ("aborted" == t) return y.debug("XHR Aborted (Most likely caused by page navigation or CORS Policy)", t, n), s.onAbort.call(v, t, m, e), !0;
                            "invalid" == t ? y.debug("JSON did not pass success test. A server-side error has most likely occurred", i) : "error" == t && e !== P && (y.debug("XHR produced a server error", t, n), (e.status < 200 || 300 <= e.status) && n !== P && "" !== n && y.error(c.statusMessage + n, a22.url), s.onError.call(v, o, m, e)), s.errorDuration && "aborted" !== t && (y.debug("Adding error state"), y.set.error(), y.should.removeError() && setTimeout(y.remove.error, s.errorDuration)), y.debug("API Request failed", o, e), s.onFailure.call(v, i, m, e);
                        }
                    }
                },
                create: {
                    request: function() {
                        return E.Deferred().always(y.event.request.complete).done(y.event.request.done).fail(y.event.request.fail);
                    },
                    mockedXHR: function() {
                        var e116, t = s.mockResponse || s.response, n = s.mockResponseAsync || s.responseAsync, i = E.Deferred().always(y.event.xhr.complete).done(y.event.xhr.done).fail(y.event.xhr.fail);
                        return t ? (e116 = E.isFunction(t) ? (y.debug("Using specified synchronous callback", t), t.call(v, o23)) : (y.debug("Using settings specified response", t), t), i.resolveWith(v, [
                            e116,
                            !1,
                            {
                                responseText: e116
                            }
                        ])) : E.isFunction(n) && (e116 = function(e) {
                            y.debug("Async callback returned response", e), e ? i.resolveWith(v, [
                                e,
                                !1,
                                {
                                    responseText: e
                                }
                            ]) : i.rejectWith(v, [
                                {
                                    responseText: e
                                },
                                !1,
                                !1
                            ]);
                        }, y.debug("Using specified async response callback", n), n.call(v, o23, e116)), i;
                    },
                    xhr: function() {
                        var e = E.ajax(a22).always(y.event.xhr.always).done(y.event.xhr.done).fail(y.event.xhr.fail);
                        return y.verbose("Created server request", e, a22), e;
                    }
                },
                set: {
                    error: function() {
                        y.verbose("Adding error state to element", p), p.addClass(u.error);
                    },
                    loading: function() {
                        y.verbose("Adding loading state to element", p), p.addClass(u.loading), r22 = (new Date).getTime();
                    }
                },
                remove: {
                    error: function() {
                        y.verbose("Removing error state from element", p), p.removeClass(u.error);
                    },
                    loading: function() {
                        y.verbose("Removing loading state from element", p), p.removeClass(u.loading);
                    }
                },
                get: {
                    responseFromXHR: function(e) {
                        return !!E.isPlainObject(e) && (y.is.expectingJSON() ? y.decode.json(e.responseText) : e.responseText);
                    },
                    errorFromRequest: function(e, t, n) {
                        return E.isPlainObject(e) && e.error !== P ? e.error : s.error[t] !== P ? s.error[t] : n;
                    },
                    request: function() {
                        return y.request || !1;
                    },
                    xhr: function() {
                        return y.xhr || !1;
                    },
                    settings: function() {
                        var e = s.beforeSend.call(m, s);
                        return e && (e.success !== P && (y.debug("Legacy success callback detected", e), y.error(c.legacyParameters, e.success), e.onSuccess = e.success), e.failure !== P && (y.debug("Legacy failure callback detected", e), y.error(c.legacyParameters, e.failure), e.onFailure = e.failure), e.complete !== P && (y.debug("Legacy complete callback detected", e), y.error(c.legacyParameters, e.complete), e.onComplete = e.complete)), e === P && y.error(c.noReturnedValue), !1 === e ? e : e !== P ? E.extend(!0, {
                        }, e) : E.extend(!0, {
                        }, s);
                    },
                    urlEncodedValue: function(e) {
                        var t = F.decodeURIComponent(e), n = F.encodeURIComponent(e);
                        return t !== e ? (y.debug("URL value is already encoded, avoiding double encoding", e), e) : (y.verbose("Encoding value using encodeURIComponent", e, n), n);
                    },
                    defaultData: function() {
                        var e = {
                        };
                        return E.isWindow(h) || (y.is.input() ? e.value = m.val() : y.is.form() || (e.text = m.text())), e;
                    },
                    event: function() {
                        return E.isWindow(h) || "now" == s.on ? (y.debug("API called without element, no events attached"), !1) : "auto" == s.on ? m.is("input") ? h.oninput !== P ? "input" : h.onpropertychange !== P ? "propertychange" : "keyup" : m.is("form") ? "submit" : "click" : s.on;
                    },
                    templatedURL: function(e) {
                        if (e = e || m.data(i22.action) || s.action || !1, n25 = m.data(i22.url) || s.url || !1) return y.debug("Using specified url", n25), n25;
                        if (e) {
                            if (y.debug("Looking up url for action", e, s.api), s.api[e] === P && !y.is.mocked()) return void y.error(c.missingAction, s.action, s.api);
                            n25 = s.api[e];
                        } else y.is.form() && (n25 = m.attr("action") || p.attr("action") || !1, y.debug("No url or action specified, defaulting to form action", n25));
                        return n25;
                    }
                },
                abort: function() {
                    var e = y.get.xhr();
                    e && "resolved" !== e.state() && (y.debug("Cancelling API request"), e.abort());
                },
                reset: function() {
                    y.remove.error(), y.remove.loading();
                },
                setting: function(e, t) {
                    if (y.debug("Changing setting", e, t), E.isPlainObject(e)) E.extend(!0, s, e);
                    else {
                        if (t === P) return s[e];
                        E.isPlainObject(s[e]) ? E.extend(!0, s[e], t) : s[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (E.isPlainObject(e)) E.extend(!0, y, e);
                    else {
                        if (t === P) return y[e];
                        y[e] = t;
                    }
                },
                debug: function() {
                    !s.silent && s.debug && (s.performance ? y.performance.log(arguments) : (y.debug = Function.prototype.bind.call(console.info, console, s.name + ":"), y.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !s.silent && s.verbose && s.debug && (s.performance ? y.performance.log(arguments) : (y.verbose = Function.prototype.bind.call(console.info, console, s.name + ":"), y.verbose.apply(console, arguments)));
                },
                error: function() {
                    s.silent || (y.error = Function.prototype.bind.call(console.error, console, s.name + ":"), y.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        s.performance && (n = (t = (new Date).getTime()) - (k || t), k = t, T.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            "Execution Time": n
                        })), clearTimeout(y.performance.timer), y.performance.timer = setTimeout(y.performance.display, 500);
                    },
                    display: function() {
                        var e = s.name + ":", n = 0;
                        k = !1, clearTimeout(y.performance.timer), E.each(T, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", w && (e += " '" + w + "'"), (console.group !== P || console.table !== P) && 0 < T.length && (console.groupCollapsed(e), console.table ? console.table(T) : E.each(T, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), T = [];
                    }
                },
                invoke: function(i, e117, t62) {
                    var o, a, n, r = b;
                    return e117 = e117 || A, t62 = h || t62, "string" == typeof i && r !== P && (i = i.split(/[\. ]/), o = i.length - 1, E.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (E.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== P) return a = r[n], !1;
                            if (!E.isPlainObject(r[t]) || e == o) return r[t] !== P ? a = r[t] : y.error(c.method, i), !1;
                            r = r[t];
                        }
                    })), E.isFunction(a) ? n = a.apply(t62, e117) : a !== P && (n = a), Array.isArray(C) ? C.push(n) : C !== P ? C = [
                        C,
                        n
                    ] : n !== P && (C = n), a;
                }
            };
            D ? (b === P && y.initialize(), y.invoke(S)) : (b !== P && b.invoke("destroy"), y.initialize());
        }), C !== P ? C : this;
    }, E.api.settings = {
        name: "API",
        namespace: "api",
        debug: !1,
        verbose: !1,
        performance: !0,
        api: {
        },
        cache: !0,
        interruptRequests: !0,
        on: "auto",
        stateContext: !1,
        loadingDuration: 0,
        hideError: "auto",
        errorDuration: 2000,
        encodeParameters: !0,
        action: !1,
        url: !1,
        base: "",
        urlData: {
        },
        defaultData: !0,
        serializeForm: !1,
        throttle: 0,
        throttleFirstRequest: !0,
        method: "get",
        data: {
        },
        dataType: "json",
        mockResponse: !1,
        mockResponseAsync: !1,
        response: !1,
        responseAsync: !1,
        rawResponse: !1,
        beforeSend: function(e) {
            return e;
        },
        beforeXHR: function(e) {
        },
        onRequest: function(e, t) {
        },
        onResponse: !1,
        onSuccess: function(e, t) {
        },
        onComplete: function(e, t) {
        },
        onFailure: function(e, t) {
        },
        onError: function(e, t) {
        },
        onAbort: function(e, t) {
        },
        successTest: !1,
        error: {
            beforeSend: "The before send function has aborted the request",
            error: "There was an error with your request",
            exitConditions: "API Request Aborted. Exit conditions met",
            JSONParse: "JSON could not be parsed during error handling",
            legacyParameters: "You are using legacy API success callback names",
            method: "The method you called is not defined",
            missingAction: "API action used but no url was defined",
            missingSerialize: "jquery-serialize-object is required to add form data to an existing data object",
            missingURL: "No URL specified for api event",
            noReturnedValue: "The beforeSend callback must return a settings object, beforeSend ignored.",
            noStorage: "Caching responses locally requires session storage",
            parseError: "There was an error parsing your request",
            requiredParameter: "Missing a required URL parameter: ",
            statusMessage: "Server gave an error: ",
            timeout: "Your request timed out"
        },
        regExp: {
            required: /\{\$*[A-z0-9]+\}/g,
            optional: /\{\/\$*[A-z0-9]+\}/g
        },
        className: {
            loading: "loading",
            error: "error"
        },
        selector: {
            disabled: ".disabled",
            form: "form"
        },
        metadata: {
            action: "action",
            url: "url"
        }
    };
})(jQuery, window, void 0), (function(w, e118, k) {
    "use strict";
    w.isFunction = w.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, e118 = void 0 !== e118 && e118.Math == Math ? e118 : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), w.fn.state = function(m) {
        var g, p = w(this), h = p.selector || "", v = (new Date).getTime(), b = [], y = m, x = "string" == typeof y, C = [].slice.call(arguments, 1);
        return p.each(function() {
            var o24 = w.isPlainObject(m) ? w.extend(!0, {
            }, w.fn.state.settings, m) : w.extend({
            }, w.fn.state.settings), s = o24.error, n26 = o24.metadata, t63 = o24.className, e119 = o24.namespace, i23 = o24.states, a23 = o24.text, r23 = "." + e119, l = e119 + "-module", c = w(this), u = this, d = c.data(l), f = {
                initialize: function() {
                    f.verbose("Initializing module"), o24.automatic && f.add.defaults(), o24.context && "" !== h ? w(o24.context).on(h, "mouseenter" + r23, f.change.text).on(h, "mouseleave" + r23, f.reset.text).on(h, "click" + r23, f.toggle.state) : c.on("mouseenter" + r23, f.change.text).on("mouseleave" + r23, f.reset.text).on("click" + r23, f.toggle.state), f.instantiate();
                },
                instantiate: function() {
                    f.verbose("Storing instance of module", f), d = f, c.data(l, f);
                },
                destroy: function() {
                    f.verbose("Destroying previous module", d), c.off(r23).removeData(l);
                },
                refresh: function() {
                    f.verbose("Refreshing selector cache"), c = w(u);
                },
                add: {
                    defaults: function() {
                        var n = m && w.isPlainObject(m.states) ? m.states : {
                        };
                        w.each(o24.defaults, function(e, t) {
                            f.is[e] !== k && f.is[e]() && (f.verbose("Adding default states", e, u), w.extend(o24.states, t, n));
                        });
                    }
                },
                is: {
                    active: function() {
                        return c.hasClass(t63.active);
                    },
                    loading: function() {
                        return c.hasClass(t63.loading);
                    },
                    inactive: function() {
                        return !c.hasClass(t63.active);
                    },
                    state: function(e) {
                        return t63[e] !== k && c.hasClass(t63[e]);
                    },
                    enabled: function() {
                        return !c.is(o24.filter.active);
                    },
                    disabled: function() {
                        return c.is(o24.filter.active);
                    },
                    textEnabled: function() {
                        return !c.is(o24.filter.text);
                    },
                    button: function() {
                        return c.is(".button:not(a, .submit)");
                    },
                    input: function() {
                        return c.is("input");
                    },
                    progress: function() {
                        return c.is(".ui.progress");
                    }
                },
                allow: function(e) {
                    f.debug("Now allowing state", e), i23[e] = !0;
                },
                disallow: function(e) {
                    f.debug("No longer allowing", e), i23[e] = !1;
                },
                allows: function(e) {
                    return i23[e] || !1;
                },
                enable: function() {
                    c.removeClass(t63.disabled);
                },
                disable: function() {
                    c.addClass(t63.disabled);
                },
                setState: function(e) {
                    f.allows(e) && c.addClass(t63[e]);
                },
                removeState: function(e) {
                    f.allows(e) && c.removeClass(t63[e]);
                },
                toggle: {
                    state: function() {
                        var e;
                        if (f.allows("active") && f.is.enabled()) {
                            if (f.refresh(), w.fn.api !== k) {
                                if (e = c.api("get request"), c.api("was cancelled")) f.debug("API Request cancelled by beforesend"), o24.activateTest = function() {
                                    return !1;
                                }, o24.deactivateTest = function() {
                                    return !1;
                                };
                                else if (e) return void f.listenTo(e);
                            }
                            f.change.state();
                        }
                    }
                },
                listenTo: function(e) {
                    f.debug("API request detected, waiting for state signal", e), e && (a23.loading && f.update.text(a23.loading), w.when(e).then(function() {
                        "resolved" == e.state() ? (f.debug("API request succeeded"), o24.activateTest = function() {
                            return !0;
                        }, o24.deactivateTest = function() {
                            return !0;
                        }) : (f.debug("API request failed"), o24.activateTest = function() {
                            return !1;
                        }, o24.deactivateTest = function() {
                            return !1;
                        }), f.change.state();
                    }));
                },
                change: {
                    state: function() {
                        f.debug("Determining state change direction"), f.is.inactive() ? f.activate() : f.deactivate(), o24.sync && f.sync(), o24.onChange.call(u);
                    },
                    text: function() {
                        f.is.textEnabled() && (f.is.disabled() ? (f.verbose("Changing text to disabled text", a23.hover), f.update.text(a23.disabled)) : f.is.active() ? a23.hover ? (f.verbose("Changing text to hover text", a23.hover), f.update.text(a23.hover)) : a23.deactivate && (f.verbose("Changing text to deactivating text", a23.deactivate), f.update.text(a23.deactivate)) : a23.hover ? (f.verbose("Changing text to hover text", a23.hover), f.update.text(a23.hover)) : a23.activate && (f.verbose("Changing text to activating text", a23.activate), f.update.text(a23.activate)));
                    }
                },
                activate: function() {
                    o24.activateTest.call(u) && (f.debug("Setting state to active"), c.addClass(t63.active), f.update.text(a23.active), o24.onActivate.call(u));
                },
                deactivate: function() {
                    o24.deactivateTest.call(u) && (f.debug("Setting state to inactive"), c.removeClass(t63.active), f.update.text(a23.inactive), o24.onDeactivate.call(u));
                },
                sync: function() {
                    f.verbose("Syncing other buttons to current state"), f.is.active() ? p.not(c).state("activate") : p.not(c).state("deactivate");
                },
                get: {
                    text: function() {
                        return o24.selector.text ? c.find(o24.selector.text).text() : c.html();
                    },
                    textFor: function(e) {
                        return a23[e] || !1;
                    }
                },
                flash: {
                    text: function(e, t, n) {
                        var i = f.get.text();
                        f.debug("Flashing text message", e, t), e = e || o24.text.flash, t = t || o24.flashDuration, n = n || function() {
                        }, f.update.text(e), setTimeout(function() {
                            f.update.text(i), n.call(u);
                        }, t);
                    }
                },
                reset: {
                    text: function() {
                        var e = a23.active || c.data(n26.storedText), t = a23.inactive || c.data(n26.storedText);
                        f.is.textEnabled() && (f.is.active() && e ? (f.verbose("Resetting active text", e), f.update.text(e)) : t && (f.verbose("Resetting inactive text", e), f.update.text(t)));
                    }
                },
                update: {
                    text: function(e) {
                        var t = f.get.text();
                        e && e !== t ? (f.debug("Updating text", e), o24.selector.text ? c.data(n26.storedText, e).find(o24.selector.text).text(e) : c.data(n26.storedText, e).html(e)) : f.debug("Text is already set, ignoring update", e);
                    }
                },
                setting: function(e, t) {
                    if (f.debug("Changing setting", e, t), w.isPlainObject(e)) w.extend(!0, o24, e);
                    else {
                        if (t === k) return o24[e];
                        w.isPlainObject(o24[e]) ? w.extend(!0, o24[e], t) : o24[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (w.isPlainObject(e)) w.extend(!0, f, e);
                    else {
                        if (t === k) return f[e];
                        f[e] = t;
                    }
                },
                debug: function() {
                    !o24.silent && o24.debug && (o24.performance ? f.performance.log(arguments) : (f.debug = Function.prototype.bind.call(console.info, console, o24.name + ":"), f.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !o24.silent && o24.verbose && o24.debug && (o24.performance ? f.performance.log(arguments) : (f.verbose = Function.prototype.bind.call(console.info, console, o24.name + ":"), f.verbose.apply(console, arguments)));
                },
                error: function() {
                    o24.silent || (f.error = Function.prototype.bind.call(console.error, console, o24.name + ":"), f.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        o24.performance && (n = (t = (new Date).getTime()) - (v || t), v = t, b.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: u,
                            "Execution Time": n
                        })), clearTimeout(f.performance.timer), f.performance.timer = setTimeout(f.performance.display, 500);
                    },
                    display: function() {
                        var e = o24.name + ":", n = 0;
                        v = !1, clearTimeout(f.performance.timer), w.each(b, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", h && (e += " '" + h + "'"), (console.group !== k || console.table !== k) && 0 < b.length && (console.groupCollapsed(e), console.table ? console.table(b) : w.each(b, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), b = [];
                    }
                },
                invoke: function(i, e120, t64) {
                    var o, a, n, r = d;
                    return e120 = e120 || C, t64 = u || t64, "string" == typeof i && r !== k && (i = i.split(/[\. ]/), o = i.length - 1, w.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (w.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== k) return a = r[n], !1;
                            if (!w.isPlainObject(r[t]) || e == o) return r[t] !== k ? a = r[t] : f.error(s.method, i), !1;
                            r = r[t];
                        }
                    })), w.isFunction(a) ? n = a.apply(t64, e120) : a !== k && (n = a), Array.isArray(g) ? g.push(n) : g !== k ? g = [
                        g,
                        n
                    ] : n !== k && (g = n), a;
                }
            };
            x ? (d === k && f.initialize(), f.invoke(y)) : (d !== k && d.invoke("destroy"), f.initialize());
        }), g !== k ? g : this;
    }, w.fn.state.settings = {
        name: "State",
        debug: !1,
        verbose: !1,
        namespace: "state",
        performance: !0,
        onActivate: function() {
        },
        onDeactivate: function() {
        },
        onChange: function() {
        },
        activateTest: function() {
            return !0;
        },
        deactivateTest: function() {
            return !0;
        },
        automatic: !0,
        sync: !1,
        flashDuration: 1000,
        filter: {
            text: ".loading, .disabled",
            active: ".disabled"
        },
        context: !1,
        error: {
            beforeSend: "The before send function has cancelled state change",
            method: "The method you called is not defined."
        },
        metadata: {
            promise: "promise",
            storedText: "stored-text"
        },
        className: {
            active: "active",
            disabled: "disabled",
            error: "error",
            loading: "loading",
            success: "success",
            warning: "warning"
        },
        selector: {
            text: !1
        },
        defaults: {
            input: {
                disabled: !0,
                loading: !0,
                active: !0
            },
            button: {
                disabled: !0,
                loading: !0,
                active: !0
            },
            progress: {
                active: !0,
                success: !0,
                warning: !0,
                error: !0
            }
        },
        states: {
            active: !0,
            disabled: !0,
            error: !0,
            loading: !0,
            success: !0,
            warning: !0
        },
        text: {
            disabled: !1,
            flash: !1,
            hover: !1,
            active: !1,
            inactive: !1,
            activate: !1,
            deactivate: !1
        }
    };
})(jQuery, window, void 0), (function(E, F, P, O) {
    "use strict";
    E.isFunction = E.isFunction || function(e) {
        return "function" == typeof e && "number" != typeof e.nodeType;
    }, F = void 0 !== F && F.Math == Math ? F : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(), E.fn.visibility = function(b) {
        var y, e121 = E(this), x = e121.selector || "", C = (new Date).getTime(), w = [], k = b, T = "string" == typeof k, S = [].slice.call(arguments, 1), D = e121.length, A = 0;
        return e121.each(function() {
            var e122, t65, n27, o25 = E.isPlainObject(b) ? E.extend(!0, {
            }, E.fn.visibility.settings, b) : E.extend({
            }, E.fn.visibility.settings), i24 = o25.className, a24 = o25.namespace, s = o25.error, r24 = o25.metadata, l = "." + a24, c = "module-" + a24, u = E(F), d = E(this), f = E(o25.context), m = d.data(c), g = F.requestAnimationFrame || F.mozRequestAnimationFrame || F.webkitRequestAnimationFrame || F.msRequestAnimationFrame || function(e) {
                setTimeout(e, 0);
            }, p = this, h = !1, v = {
                initialize: function() {
                    v.debug("Initializing", o25), v.setup.cache(), v.should.trackChanges() && ("image" == o25.type && v.setup.image(), "fixed" == o25.type && v.setup.fixed(), o25.observeChanges && v.observeChanges(), v.bind.events()), v.save.position(), v.is.visible() || v.error(s.visible, d), o25.initialCheck && v.checkVisibility(), v.instantiate();
                },
                instantiate: function() {
                    v.debug("Storing instance", v), d.data(c, v), m = v;
                },
                destroy: function() {
                    v.verbose("Destroying previous module"), n27 && n27.disconnect(), t65 && t65.disconnect(), u.off("load" + l, v.event.load).off("resize" + l, v.event.resize), f.off("scroll" + l, v.event.scroll).off("scrollchange" + l, v.event.scrollchange), "fixed" == o25.type && (v.resetFixed(), v.remove.placeholder()), d.off(l).removeData(c);
                },
                observeChanges: function() {
                    "MutationObserver" in F && (t65 = new MutationObserver(v.event.contextChanged), n27 = new MutationObserver(v.event.changed), t65.observe(P, {
                        childList: !0,
                        subtree: !0
                    }), n27.observe(p, {
                        childList: !0,
                        subtree: !0
                    }), v.debug("Setting up mutation observer", n27));
                },
                bind: {
                    events: function() {
                        v.verbose("Binding visibility events to scroll and resize"), o25.refreshOnLoad && u.on("load" + l, v.event.load), u.on("resize" + l, v.event.resize), f.off("scroll" + l).on("scroll" + l, v.event.scroll).on("scrollchange" + l, v.event.scrollchange);
                    }
                },
                event: {
                    changed: function(e) {
                        v.verbose("DOM tree modified, updating visibility calculations"), v.timer = setTimeout(function() {
                            v.verbose("DOM tree modified, updating sticky menu"), v.refresh();
                        }, 100);
                    },
                    contextChanged: function(e123) {
                        [].forEach.call(e123, function(e124) {
                            e124.removedNodes && [].forEach.call(e124.removedNodes, function(e) {
                                (e == p || 0 < E(e).find(p).length) && (v.debug("Element removed from DOM, tearing down events"), v.destroy());
                            });
                        });
                    },
                    resize: function() {
                        v.debug("Window resized"), o25.refreshOnResize && g(v.refresh);
                    },
                    load: function() {
                        v.debug("Page finished loading"), g(v.refresh);
                    },
                    scroll: function() {
                        o25.throttle ? (clearTimeout(v.timer), v.timer = setTimeout(function() {
                            f.triggerHandler("scrollchange" + l, [
                                f.scrollTop()
                            ]);
                        }, o25.throttle)) : g(function() {
                            f.triggerHandler("scrollchange" + l, [
                                f.scrollTop()
                            ]);
                        });
                    },
                    scrollchange: function(e, t) {
                        v.checkVisibility(t);
                    }
                },
                precache: function(e, t) {
                    for(var n = (e = !(e instanceof Array) ? [
                        e
                    ] : e).length, i = 0, o = [], a = P.createElement("img"), r = function() {
                        ++i >= e.length && E.isFunction(t) && t();
                    }; n--;)(a = P.createElement("img")).onload = r, a.onerror = r, a.src = e[n], o.push(a);
                },
                enableCallbacks: function() {
                    v.debug("Allowing callbacks to occur"), h = !1;
                },
                disableCallbacks: function() {
                    v.debug("Disabling all callbacks temporarily"), h = !0;
                },
                should: {
                    trackChanges: function() {
                        return T ? (v.debug("One time query, no need to bind events"), !1) : (v.debug("Callbacks being attached"), !0);
                    }
                },
                setup: {
                    cache: function() {
                        v.cache = {
                            occurred: {
                            },
                            screen: {
                            },
                            element: {
                            }
                        };
                    },
                    image: function() {
                        var e = d.data(r24.src);
                        e && (v.verbose("Lazy loading image", e), o25.once = !0, o25.observeChanges = !1, o25.onOnScreen = function() {
                            v.debug("Image on screen", p), v.precache(e, function() {
                                v.set.image(e, function() {
                                    ++A == D && o25.onAllLoaded.call(this), o25.onLoad.call(this);
                                });
                            });
                        });
                    },
                    fixed: function() {
                        v.debug("Setting up fixed"), o25.once = !1, o25.observeChanges = !1, o25.initialCheck = !0, o25.refreshOnLoad = !0, b.transition || (o25.transition = !1), v.create.placeholder(), v.debug("Added placeholder", e122), o25.onTopPassed = function() {
                            v.debug("Element passed, adding fixed position", d), v.show.placeholder(), v.set.fixed(), o25.transition && E.fn.transition !== O && d.transition(o25.transition, o25.duration);
                        }, o25.onTopPassedReverse = function() {
                            v.debug("Element returned to position, removing fixed", d), v.hide.placeholder(), v.remove.fixed();
                        };
                    }
                },
                create: {
                    placeholder: function() {
                        v.verbose("Creating fixed position placeholder"), e122 = d.clone(!1).css("display", "none").addClass(i24.placeholder).insertAfter(d);
                    }
                },
                show: {
                    placeholder: function() {
                        v.verbose("Showing placeholder"), e122.css("display", "block").css("visibility", "hidden");
                    }
                },
                hide: {
                    placeholder: function() {
                        v.verbose("Hiding placeholder"), e122.css("display", "none").css("visibility", "");
                    }
                },
                set: {
                    fixed: function() {
                        v.verbose("Setting element to fixed position"), d.addClass(i24.fixed).css({
                            position: "fixed",
                            top: o25.offset + "px",
                            left: "auto",
                            zIndex: o25.zIndex
                        }), o25.onFixed.call(p);
                    },
                    image: function(e, t) {
                        d.attr("src", e), o25.transition ? E.fn.transition !== O ? d.hasClass(i24.visible) ? v.debug("Transition already occurred on this image, skipping animation") : d.transition(o25.transition, o25.duration, t) : d.fadeIn(o25.duration, t) : d.show();
                    }
                },
                is: {
                    onScreen: function() {
                        return v.get.elementCalculations().onScreen;
                    },
                    offScreen: function() {
                        return v.get.elementCalculations().offScreen;
                    },
                    visible: function() {
                        return !(!v.cache || !v.cache.element) && !(0 === v.cache.element.width && 0 === v.cache.element.offset.top);
                    },
                    verticallyScrollableContext: function() {
                        var e = f.get(0) !== F && f.css("overflow-y");
                        return "auto" == e || "scroll" == e;
                    },
                    horizontallyScrollableContext: function() {
                        var e = f.get(0) !== F && f.css("overflow-x");
                        return "auto" == e || "scroll" == e;
                    }
                },
                refresh: function() {
                    v.debug("Refreshing constants (width/height)"), "fixed" == o25.type && v.resetFixed(), v.reset(), v.save.position(), o25.checkOnRefresh && v.checkVisibility(), o25.onRefresh.call(p);
                },
                resetFixed: function() {
                    v.remove.fixed(), v.remove.occurred();
                },
                reset: function() {
                    v.verbose("Resetting all cached values"), E.isPlainObject(v.cache) && (v.cache.screen = {
                    }, v.cache.element = {
                    });
                },
                checkVisibility: function(e) {
                    v.verbose("Checking visibility of element", v.cache.element), !h && v.is.visible() && (v.save.scroll(e), v.save.calculations(), v.passed(), v.passingReverse(), v.topVisibleReverse(), v.bottomVisibleReverse(), v.topPassedReverse(), v.bottomPassedReverse(), v.onScreen(), v.offScreen(), v.passing(), v.topVisible(), v.bottomVisible(), v.topPassed(), v.bottomPassed(), o25.onUpdate && o25.onUpdate.call(p, v.get.elementCalculations()));
                },
                passed: function(e125, t66) {
                    var n = v.get.elementCalculations();
                    if (e125 && t66) o25.onPassed[e125] = t66;
                    else {
                        if (e125 !== O) return v.get.pixelsPassed(e125) > n.pixelsPassed;
                        n.passing && E.each(o25.onPassed, function(e, t) {
                            n.bottomVisible || n.pixelsPassed > v.get.pixelsPassed(e) ? v.execute(t, e) : o25.once || v.remove.occurred(t);
                        });
                    }
                },
                onScreen: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onOnScreen;
                    if (e && (v.debug("Adding callback for onScreen", e), o25.onOnScreen = e), t.onScreen ? v.execute(n, "onScreen") : o25.once || v.remove.occurred("onScreen"), e !== O) return t.onOnScreen;
                },
                offScreen: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onOffScreen;
                    if (e && (v.debug("Adding callback for offScreen", e), o25.onOffScreen = e), t.offScreen ? v.execute(n, "offScreen") : o25.once || v.remove.occurred("offScreen"), e !== O) return t.onOffScreen;
                },
                passing: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onPassing;
                    if (e && (v.debug("Adding callback for passing", e), o25.onPassing = e), t.passing ? v.execute(n, "passing") : o25.once || v.remove.occurred("passing"), e !== O) return t.passing;
                },
                topVisible: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onTopVisible, i = "topVisible";
                    if (e && (v.debug("Adding callback for top visible", e), o25.onTopVisible = e), t.topVisible ? v.execute(n, i) : o25.once || v.remove.occurred(i), e === O) return t.topVisible;
                },
                bottomVisible: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onBottomVisible, i = "bottomVisible";
                    if (e && (v.debug("Adding callback for bottom visible", e), o25.onBottomVisible = e), t.bottomVisible ? v.execute(n, i) : o25.once || v.remove.occurred(i), e === O) return t.bottomVisible;
                },
                topPassed: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onTopPassed;
                    if (e && (v.debug("Adding callback for top passed", e), o25.onTopPassed = e), t.topPassed ? v.execute(n, "topPassed") : o25.once || v.remove.occurred("topPassed"), e === O) return t.topPassed;
                },
                bottomPassed: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onBottomPassed, i = "bottomPassed";
                    if (e && (v.debug("Adding callback for bottom passed", e), o25.onBottomPassed = e), t.bottomPassed ? v.execute(n, i) : o25.once || v.remove.occurred(i), e === O) return t.bottomPassed;
                },
                passingReverse: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onPassingReverse, i = "passingReverse";
                    if (e && (v.debug("Adding callback for passing reverse", e), o25.onPassingReverse = e), t.passing ? o25.once || v.remove.occurred(i) : v.get.occurred("passing") && v.execute(n, i), e !== O) return !t.passing;
                },
                topVisibleReverse: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onTopVisibleReverse, i = "topVisibleReverse";
                    if (e && (v.debug("Adding callback for top visible reverse", e), o25.onTopVisibleReverse = e), t.topVisible ? o25.once || v.remove.occurred(i) : v.get.occurred("topVisible") && v.execute(n, i), e === O) return !t.topVisible;
                },
                bottomVisibleReverse: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onBottomVisibleReverse, i = "bottomVisibleReverse";
                    if (e && (v.debug("Adding callback for bottom visible reverse", e), o25.onBottomVisibleReverse = e), t.bottomVisible ? o25.once || v.remove.occurred(i) : v.get.occurred("bottomVisible") && v.execute(n, i), e === O) return !t.bottomVisible;
                },
                topPassedReverse: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onTopPassedReverse, i = "topPassedReverse";
                    if (e && (v.debug("Adding callback for top passed reverse", e), o25.onTopPassedReverse = e), t.topPassed ? o25.once || v.remove.occurred(i) : v.get.occurred("topPassed") && v.execute(n, i), e === O) return !t.onTopPassed;
                },
                bottomPassedReverse: function(e) {
                    var t = v.get.elementCalculations(), n = e || o25.onBottomPassedReverse, i = "bottomPassedReverse";
                    if (e && (v.debug("Adding callback for bottom passed reverse", e), o25.onBottomPassedReverse = e), t.bottomPassed ? o25.once || v.remove.occurred(i) : v.get.occurred("bottomPassed") && v.execute(n, i), e === O) return !t.bottomPassed;
                },
                execute: function(e, t) {
                    var n = v.get.elementCalculations(), i = v.get.screenCalculations();
                    (e = e || !1) && (o25.continuous ? (v.debug("Callback being called continuously", t, n), e.call(p, n, i)) : v.get.occurred(t) || (v.debug("Conditions met", t, n), e.call(p, n, i))), v.save.occurred(t);
                },
                remove: {
                    fixed: function() {
                        v.debug("Removing fixed position"), d.removeClass(i24.fixed).css({
                            position: "",
                            top: "",
                            left: "",
                            zIndex: ""
                        }), o25.onUnfixed.call(p);
                    },
                    placeholder: function() {
                        v.debug("Removing placeholder content"), e122 && e122.remove();
                    },
                    occurred: function(e) {
                        var t;
                        e ? (t = v.cache.occurred)[e] !== O && !0 === t[e] && (v.debug("Callback can now be called again", e), v.cache.occurred[e] = !1) : v.cache.occurred = {
                        };
                    }
                },
                save: {
                    calculations: function() {
                        v.verbose("Saving all calculations necessary to determine positioning"), v.save.direction(), v.save.screenCalculations(), v.save.elementCalculations();
                    },
                    occurred: function(e) {
                        e && (v.cache.occurred[e] !== O && !0 === v.cache.occurred[e] || (v.verbose("Saving callback occurred", e), v.cache.occurred[e] = !0));
                    },
                    scroll: function(e) {
                        e = e + o25.offset || f.scrollTop() + o25.offset, v.cache.scroll = e;
                    },
                    direction: function() {
                        var e = v.get.scroll(), t = v.get.lastScroll(), t = t < e && t ? "down" : e < t && t ? "up" : "static";
                        return v.cache.direction = t, v.cache.direction;
                    },
                    elementPosition: function() {
                        var e = v.cache.element, t = v.get.screenSize();
                        return v.verbose("Saving element position"), e.fits = e.height < t.height, e.offset = d.offset(), e.width = d.outerWidth(), e.height = d.outerHeight(), v.is.verticallyScrollableContext() && (e.offset.top += f.scrollTop() - f.offset().top), v.is.horizontallyScrollableContext() && (e.offset.left += f.scrollLeft() - f.offset().left), v.cache.element = e;
                    },
                    elementCalculations: function() {
                        var e = v.get.screenCalculations(), t = v.get.elementPosition();
                        return o25.includeMargin ? (t.margin = {
                        }, t.margin.top = parseInt(d.css("margin-top"), 10), t.margin.bottom = parseInt(d.css("margin-bottom"), 10), t.top = t.offset.top - t.margin.top, t.bottom = t.offset.top + t.height + t.margin.bottom) : (t.top = t.offset.top, t.bottom = t.offset.top + t.height), t.topPassed = e.top >= t.top, t.bottomPassed = e.top >= t.bottom, t.topVisible = e.bottom >= t.top && !t.topPassed, t.bottomVisible = e.bottom >= t.bottom && !t.bottomPassed, t.pixelsPassed = 0, t.percentagePassed = 0, t.onScreen = (t.topVisible || t.passing) && !t.bottomPassed, t.passing = t.topPassed && !t.bottomPassed, t.offScreen = !t.onScreen, t.passing && (t.pixelsPassed = e.top - t.top, t.percentagePassed = (e.top - t.top) / t.height), v.cache.element = t, v.verbose("Updated element calculations", t), t;
                    },
                    screenCalculations: function() {
                        var e = v.get.scroll();
                        return v.save.direction(), v.cache.screen.top = e, v.cache.screen.bottom = e + v.cache.screen.height, v.cache.screen;
                    },
                    screenSize: function() {
                        v.verbose("Saving window position"), v.cache.screen = {
                            height: f.height()
                        };
                    },
                    position: function() {
                        v.save.screenSize(), v.save.elementPosition();
                    }
                },
                get: {
                    pixelsPassed: function(e) {
                        var t = v.get.elementCalculations();
                        return -1 < e.search("%") ? t.height * (parseInt(e, 10) / 100) : parseInt(e, 10);
                    },
                    occurred: function(e) {
                        return v.cache.occurred !== O && v.cache.occurred[e] || !1;
                    },
                    direction: function() {
                        return v.cache.direction === O && v.save.direction(), v.cache.direction;
                    },
                    elementPosition: function() {
                        return v.cache.element === O && v.save.elementPosition(), v.cache.element;
                    },
                    elementCalculations: function() {
                        return v.cache.element === O && v.save.elementCalculations(), v.cache.element;
                    },
                    screenCalculations: function() {
                        return v.cache.screen === O && v.save.screenCalculations(), v.cache.screen;
                    },
                    screenSize: function() {
                        return v.cache.screen === O && v.save.screenSize(), v.cache.screen;
                    },
                    scroll: function() {
                        return v.cache.scroll === O && v.save.scroll(), v.cache.scroll;
                    },
                    lastScroll: function() {
                        return v.cache.screen === O ? (v.debug("First scroll event, no last scroll could be found"), !1) : v.cache.screen.top;
                    }
                },
                setting: function(e, t) {
                    if (E.isPlainObject(e)) E.extend(!0, o25, e);
                    else {
                        if (t === O) return o25[e];
                        o25[e] = t;
                    }
                },
                internal: function(e, t) {
                    if (E.isPlainObject(e)) E.extend(!0, v, e);
                    else {
                        if (t === O) return v[e];
                        v[e] = t;
                    }
                },
                debug: function() {
                    !o25.silent && o25.debug && (o25.performance ? v.performance.log(arguments) : (v.debug = Function.prototype.bind.call(console.info, console, o25.name + ":"), v.debug.apply(console, arguments)));
                },
                verbose: function() {
                    !o25.silent && o25.verbose && o25.debug && (o25.performance ? v.performance.log(arguments) : (v.verbose = Function.prototype.bind.call(console.info, console, o25.name + ":"), v.verbose.apply(console, arguments)));
                },
                error: function() {
                    o25.silent || (v.error = Function.prototype.bind.call(console.error, console, o25.name + ":"), v.error.apply(console, arguments));
                },
                performance: {
                    log: function(e) {
                        var t, n;
                        o25.performance && (n = (t = (new Date).getTime()) - (C || t), C = t, w.push({
                            Name: e[0],
                            Arguments: [].slice.call(e, 1) || "",
                            Element: p,
                            "Execution Time": n
                        })), clearTimeout(v.performance.timer), v.performance.timer = setTimeout(v.performance.display, 500);
                    },
                    display: function() {
                        var e = o25.name + ":", n = 0;
                        C = !1, clearTimeout(v.performance.timer), E.each(w, function(e, t) {
                            n += t["Execution Time"];
                        }), e += " " + n + "ms", x && (e += " '" + x + "'"), (console.group !== O || console.table !== O) && 0 < w.length && (console.groupCollapsed(e), console.table ? console.table(w) : E.each(w, function(e, t) {
                            console.log(t.Name + ": " + t["Execution Time"] + "ms");
                        }), console.groupEnd()), w = [];
                    }
                },
                invoke: function(i, e126, t67) {
                    var o, a, n, r = m;
                    return e126 = e126 || S, t67 = p || t67, "string" == typeof i && r !== O && (i = i.split(/[\. ]/), o = i.length - 1, E.each(i, function(e, t) {
                        var n = e != o ? t + i[e + 1].charAt(0).toUpperCase() + i[e + 1].slice(1) : i;
                        if (E.isPlainObject(r[n]) && e != o) r = r[n];
                        else {
                            if (r[n] !== O) return a = r[n], !1;
                            if (!E.isPlainObject(r[t]) || e == o) return r[t] !== O ? a = r[t] : v.error(s.method, i), !1;
                            r = r[t];
                        }
                    })), E.isFunction(a) ? n = a.apply(t67, e126) : a !== O && (n = a), Array.isArray(y) ? y.push(n) : y !== O ? y = [
                        y,
                        n
                    ] : n !== O && (y = n), a;
                }
            };
            T ? (m === O && v.initialize(), m.save.scroll(), m.save.calculations(), v.invoke(k)) : (m !== O && m.invoke("destroy"), v.initialize());
        }), y !== O ? y : this;
    }, E.fn.visibility.settings = {
        name: "Visibility",
        namespace: "visibility",
        debug: !1,
        verbose: !1,
        performance: !0,
        observeChanges: !0,
        initialCheck: !0,
        refreshOnLoad: !0,
        refreshOnResize: !0,
        checkOnRefresh: !0,
        once: !0,
        continuous: !1,
        offset: 0,
        includeMargin: !1,
        context: F,
        throttle: !1,
        type: !1,
        zIndex: "10",
        transition: "fade in",
        duration: 1000,
        onPassed: {
        },
        onOnScreen: !1,
        onOffScreen: !1,
        onPassing: !1,
        onTopVisible: !1,
        onBottomVisible: !1,
        onTopPassed: !1,
        onBottomPassed: !1,
        onPassingReverse: !1,
        onTopVisibleReverse: !1,
        onBottomVisibleReverse: !1,
        onTopPassedReverse: !1,
        onBottomPassedReverse: !1,
        onLoad: function() {
        },
        onAllLoaded: function() {
        },
        onFixed: function() {
        },
        onUnfixed: function() {
        },
        onUpdate: !1,
        onRefresh: function() {
        },
        metadata: {
            src: "src"
        },
        className: {
            fixed: "fixed",
            placeholder: "constraint",
            visible: "visible"
        },
        error: {
            method: "The method you called is not defined.",
            visible: "Element is hidden, you must call refresh after element becomes visible"
        }
    };
})(jQuery, window, document);

},{}]},["7h7Y0","7aT0H"], "7aT0H", "parcelRequirea221")

//# sourceMappingURL=index.37d4ab28.js.map
