import {globals} from "../globals.js";
import {generateLinkIdFromLinkObject, generateUndirectedLinkId} from "../utils/utils.js";
import GroupByTransformer from "./groupByTransformer";
import {property} from "lodash";

class GraphData {
    constructor(data, previousGraph) {
        if (previousGraph) {
            this._fromTwoGraphs(data, previousGraph);
        } else {
            this.loadData(data);
        }
    }

    _fromTwoGraphs(newGraphJson, oldGraphNodes) {
        this.loadData(newGraphJson);
        this.nodesRendered.forEach(node => {
            let nodeOld = oldGraphNodes.filter(n => n.id == node.id);
            if (nodeOld.length > 0) {
                node.x = nodeOld[0].x
                node.y = nodeOld[0].y
            }
        })
    }

    loadData(data) {
        this.data = data;
        this.metadata = this.data["metadata"];
        this.attributes = this.metadata["attributes"];
        this.edgeTypeKey = this.metadata["edgeType"];
        this.entityTypeKey = this.metadata["nodeType"];
        this.entityTypes = this.metadata["entityTypes"];
        // this.personType = this.metadata["target_entity_type"];
        // this.documentType = this.metadata["source_entity_type"];
        this.edgeTypes = [];

        this.nodes = data['nodes'];
        this.links = data['links'];
        this.nodesRendered = JSON.parse(JSON.stringify(this.nodes));
        this.linksRendered = JSON.parse(JSON.stringify(this.links));
        this.nodesByTypes = {};

        this.idToNode = null;
        this.createIdToNodeMap();
        this.setSourceTargetsAsReferences();

        this.computeNeighbors();

        this.findEdgeTypes();
        this.computeMetrics();
    }

    documentType() {
        return this.nodeTypes.filter(n => globals.HYPEREDGE_NODE_TYPES.includes(n))[0];
    }

    personType() {
        return this.nodeTypes.filter(n => !globals.HYPEREDGE_NODE_TYPES.includes(n))[0];
    }

    computeMetrics() {
        this.nNodes = this.nodesRendered.length;
        this.nLinks = this.linksRendered.length;
    }

    // Currently undirected
    computeNeighbors() {
        // this.neighbors = Object.fromEntries(this.nodesRendered.map(n => [n.id, []]))
        // this.linksRendered.forEach(l => {
        //     this.neighbors[l.source.id].push(l.target.id);
        //     this.neighbors[l.target.id].push(l.source.id);
        // })

        // Node to linkId to Node
        this.neighbors = Object.fromEntries(this.nodesRendered.map(n => [n.id, {}]))
        this.linksRendered.forEach(l => {
            let linkId = generateLinkIdFromLinkObject(l, this.edgeTypeKey);
            this.neighbors[l.source.id][linkId] = l.target.id;
            this.neighbors[l.target.id][linkId] = l.source.id;
        })
    }

    getNeighbors(nId) {
        return this.neighbors[nId];
    }

    setGlobals() {
        globals.NODETYPE = this.entityTypeKey;
        globals.EDGETYPE = this.edgeTypeKey;
    }

    reset() {
        this.nodesRendered = JSON.parse(JSON.stringify(this.nodes));
        this.linksRendered = JSON.parse(JSON.stringify(this.links));
        this.setSourceTargetsAsReferences();
    }

    entityIds() {
        this.nodesIds = this.nodes.map(n => n.id);
        this.linksIds = this.links.map(l => l.id ? l.id : null);
        return this.nodesIds.concat(this.linksIds);
    }

    nodesIds() {
        return this.nodes.map(n => n.id);
    }

    addNode(node) {
        this.nodes.push(node);
        this.nodesRendered.push(node);
        this.idToNode[node.id] = node;
    }

    removeNode(nodeId, onlyRendering=false) {
        if (!onlyRendering) {
            this.nodes = this.nodes.filter(node => node.id != nodeId);
            this.links = this.links.filter(link => link.source != nodeId && link.target != nodeId)
        }

        this.nodesRendered = this.nodesRendered.filter(node => node.id != nodeId);
        this.linksRendered = this.linksRendered.filter(link => link.source.id != nodeId && link.target.id != nodeId)
    }

    removeNodes(nodesIds, onlyRendering=false) {
        if (!onlyRendering) {
            this.nodes = this.nodes.filter(node => !nodesIds.includes(node));
            this.links = this.links.filter(link => !nodesIds.includes(link.source) && !nodesIds.includes(link.target))
        }

        this.nodesRendered = this.nodesRendered.filter(node => !nodesIds.includes(node.id));
        this.linksRendered = this.linksRendered.filter(link => !(nodesIds.includes(link.source.id)) && !(nodesIds.includes(link.target.id)))
    }

    nodesWithProperty(propertyName) {
        return this.nodesRendered.filter(n => n[propertyName]);
    }

    initPropertiesStats(propertiesStats) {
        this.propertiesStats = {};
        this.nodeTypeToProperties = {};

        propertiesStats.forEach((ps) => {
            let nodeType = ps[globals.NODETYPE_BACKEND];
            // this.propertiesStats[ps.name] = ps;

            if (this.nodeTypeToProperties[nodeType]) {
                this.nodeTypeToProperties[nodeType].push(ps);
            } else {
                this.nodeTypeToProperties[nodeType] = [ps];
            }
        });

        this.nodeTypes = Object.keys(this.nodeTypeToProperties);
        this.mergeCommonNodeTypeProperties()
    }

    allProperties() {
        let allProperties = Object.values(this.nodeTypeToProperties).reduce((a, b) => [...a, ...b]).map(prop => prop.name);
        allProperties = [...new Set(allProperties)];
        return allProperties;
    }

    // Create a category for attributes contained in all node types
    mergeCommonNodeTypeProperties() {
        this.nodeTypeToProperties["ALL"] = []
        // label: for (let property of Object.keys(this.propertiesStats)) {
        label: for (let property of this.allProperties()) {
            for (let nodeType of this.nodeTypes) {
                if (!this.nodeTypeToProperties[nodeType].map(p => p.name).includes(property)) {
                    continue label;
                }
            }

            // this.nodeTypeToProperties["ALL"].push(this.propertiesStats[property]);
            // TODO : HAVE TO UPDATE DOMAIN
            this.nodeTypeToProperties["ALL"].push(this.nodeTypeToProperties[this.nodeTypes[0]].filter(prop => prop.name == property)[0]);
            for (let nodeType of this.nodeTypes) {
                this.nodeTypeToProperties[nodeType] = this.nodeTypeToProperties[nodeType].filter(prop => prop.name != property);
            }
        }

        this.propertiesStats = Object.values(this.nodeTypeToProperties).reduce((a, b) => [...a, ...b]).map(x => [x.name, x]);
        this.propertiesStats = Object.fromEntries(this.propertiesStats);
    }

    getNodesByType(nodeType) {
        let nodes;
        if (nodeType) {
            nodes = this.nodesRendered.filter((n) => n[this.entityTypeKey] == nodeType);
        } else {
            nodes = this.nodesRendered.filter((n) => !(n[this.entityTypeKey]) || n[this.entityTypeKey] == globals.ANY_NODETYPE);
        }

        // if (this.nodesByTypes[nodeType]) {
        //     let nodesByTypeStr = this.nodesByTypes[nodeType].map((n) => JSON.stringify(n));
        //     nodes.forEach((n) => {
        //         if (!(nodesByTypeStr.contains(JSON.stringify(n)))) {
        //             this.nodesByTypes[nodeType].push(n)
        //         }
        //     })
        // }
        //  else {
        //     this.nodesByTypes[nodeType] = nodes;
        // }
        this.nodesByTypes[nodeType] = nodes;
        return this.nodesByTypes[nodeType];
    }

    setSourceTargetsAsReferences() {
        this.linksRendered.forEach(link => {
            link["source"] = this.idToNode[link["source"]];
            link["target"] = this.idToNode[link["target"]];
        })
    }

    createIdToNodeMap() {
        this.idToNode = {};
        this.nodesRendered.forEach((n) => {
            this.idToNode[n.id] = n;
        })
        // this.nodes.forEach((n) => {
        //     this.idToNode[n.id] = n;
        // })
    }

    linkRepetitionCount() {
        this.linkCount = {};
        this.linksRendered.forEach((link) => {
            let linkId = generateUndirectedLinkId(link.source.id, link.target.id);
            if (this.linkCount[linkId] == undefined) {
                this.linkCount[linkId] = 1;
            } else {
                this.linkCount[linkId] += 1;
            }
        })
    }

    createAdj(){
        this.nodeIdsToindex = {}
        let index = 0;
        this.nodes.forEach(node => {
            this.nodeIdsToindex[node.id] = index;
            index += 1;
        })

        this.adj = [];
        this.nodes.forEach(n => {
            this.adj.push([]);
        })

        this.links.forEach((link) => {
            this.adj[this.nodeIdsToindex[link["source"]]][this.nodeIdsToindex[link["target"]]] = 1;
        })
    }

    computeInducedAdj(nodes){
        let inducedAdj = this.adj
            .filter((el, ind) => nodes.includes(ind))
            .map(list => list.filter((el, ind) => nodes.includes(ind)))

        return inducedAdj
    }

    findEdgeTypes(){
        this.links.forEach((link) => {
            if (!this.edgeTypes.includes(link[this.edgeTypeKey])) this.edgeTypes.push(link[this.edgeTypeKey]);
        })
    }

    // groupBy(attribute){
    //     let group = d3.rollup(this.nodesRendered, (v) => v.length, (d) => d[attribute]);
    //     console.log(group);
    // }

    groupByOneValue(attribute, value) {
        let removedNodes = [];
        for (let i = this.nodesRendered.length - 1; i > -1; i-- ){
            if (this.nodesRendered[i][attribute] == value){
                let removedNode = this.nodesRendered.splice(i, 1)[0];
                removedNodes.push(removedNode);
            }
        }

        let removedNodesIds = removedNodes.map((node) => node.id);

        let removedEdges = [];
        for (let i = this.linksRendered.length - 1; i > -1; i-- ){
            if (this.linksRendered[i].source[attribute] == value || this.linksRendered[i].target[attribute] == value) {
                let removedEdge = this.linksRendered.splice(i, 1)[0];
                removedEdges.push(removedEdge);
            }
        }

        let groupNode = {'id': 'group' + value, 'group': value};
        this.nodesRendered.push(groupNode);

        removedEdges.forEach((edge) => {
            let n1 = edge.source
            let n2 = edge.target
            if (removedNodesIds.includes(n1.id) && removedNodesIds.includes(n2.id)) {
            } else if (removedNodesIds.includes(n1.d)) {
                let edgeNodeToGroup = {'source': groupNode.id, 'target': n2, 'value': 1};
                this.linksRendered.push(edgeNodeToGroup);
            } else if (removedNodesIds.includes(n2.id)) {
                let edgeNodeToGroup = {'source': groupNode.id, 'target': n1, 'value': 1};
                this.linksRendered.push(edgeNodeToGroup);
            }
        });
    }

    groupBySubgraphs(subgraphs, nodes, links) {
        let transformer = new GroupByTransformer()
        transformer.run(this, subgraphs, nodes, links);
    }
}


export {GraphData};