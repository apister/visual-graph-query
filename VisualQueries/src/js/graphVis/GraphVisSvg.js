import {globals} from "../globals.js";
import * as d3 from 'd3';
import {ShapeEnum} from "../utils/legend";

class GraphVisSvg {
    constructor(graph, svg, radius, nodeTypeLegend, isDragging, isZooming, isLasso, isMainGraph, linkScale, getEntityPropertyCb) {
        this.graph = graph;
        this.svg = svg;
        this.radius = radius;
        this.nodeTypeLegend = nodeTypeLegend;
        this.simulation = null;
        this.scriptLang = null;
        this.isDragging = isDragging;
        this.isLasso = isLasso;
        this.isMainGraph = isMainGraph;

        this.nodesByTypes = {};

        // d3 selections
        this.gLinks = this.svg.append("g")
            .attr("class", "links")
            .style("z-index", 10)
        this.gNodes = this.svg.append("g")
            .style("z-index", 10)
            .attr("class", "nodes");

        this.nodes = null;
        this.links = null;
        this.circles = null;
        this.text = null;

        if (isZooming) {
            this.svg
                .call(d3.zoom().on("zoom", this.zoomAction));
        }

        this.color = d3.scaleOrdinal(d3.schemePastel1);

        if (linkScale == null) {
            // this.linkScale = d3.scaleOrdinal().domain(this.graph.edgeTypes).range(d3.schemeCategory10);
            this.linkScale = d3.scaleOrdinal().domain(this.graph.linkTypes).range(d3.schemeCategory10);
        } else {
            this.linkScale = linkScale;
        }

        this.getEntityPropertyCb = getEntityPropertyCb;
        this.selections();

        this.idCount = 0;

        this.edgeWidth = 3;
        this.edgeHoverWidth = 10;

        // this.prepareMultipleLinks();
    }

    static calcTranslationApproximate(targetDistance, point0, point1) {
        var x1_x0 = point1.x - point0.x,
            y1_y0 = point1.y - point0.y,
            x2_x0, y2_y0;
        if (targetDistance === 0) {
            x2_x0 = y2_y0 = 0;
        } else if (y1_y0 === 0 || Math.abs(x1_x0 / y1_y0) > 1) {
            y2_y0 = -targetDistance;
            x2_x0 = targetDistance * y1_y0 / x1_x0;
        } else {
            x2_x0 = targetDistance;
            y2_y0 = targetDistance * (-x1_x0) / y1_y0;
        }
        return {
            dx: x2_x0,
            dy: y2_y0
        };
    }

    // lassoInit() {
    //     this.lassoRect = this.svg
    //         .append('svg')
    //
    //     // A rect is needed for the dimensions
    //     this.lassoRect.append("rect")
    //         .attr('width', this.svg.attr('width'))
    //         .attr('height', this.svg.attr('height'))
    //         .attr('opacity', 0)
    //
    //     this.lasso = d3.lasso()
    //         .closePathSelect(true)
    //         .closePathDistance(100)
    //         .items(this.circles)
    //         .targetArea(this.lassoRect)
    //         .on("start", this.lasso_start)
    //         .on("draw", this.lasso_draw)
    //         .on("end", this.lasso_end);
    //     this.lassoRect.call(this.lasso);
    // }

    // TODO : change graph generation to have attribute
    setNodeAttributeToRender(property, colorScale) {
        this.nodePropertyToPlot = property;
        this.nodeColorScale = colorScale;
    }

    selections() {
        if (this.isMainGraph) {
            this.showNamesBox = d3.select("#names-box")
            this.showNamesBox.on("change", () => {
                this.text.style("visibility", (d) => {
                    if (this.showNamesBox.node().checked) {
                        return "visible";
                    } else {
                        return "hidden";
                    }
                });
            })
        }
    }

    /**
     * @description
     * Build an index to help handle the case of multiple links between two nodes
     */
    prepareMultipleLinks() {
        let linksFromNodes = {};
        this.graph.links.forEach((val, idx) => {
            let sid = val.source.id,
                tid = val.target.id,
                key = (sid < tid ? sid + "," + tid : tid + "," + sid);
            if (linksFromNodes[key] === undefined) {
                linksFromNodes[key] = [idx];
                val.multiIdx = 1;
            } else {
                val.multiIdx = linksFromNodes[key].push(idx);
            }
            // Calculate target link distance, from the index in the multiple-links array:
            // 1 -> 0, 2 -> 2, 3-> -2, 4 -> 4, 5 -> -4, ...
            val.targetDistance = (val.multiIdx % 2 === 0 ? val.multiIdx * this.edgeWidth : (-val.multiIdx + 1) * this.edgeWidth);
        });
    }

    render() {
        this.nodes_render();
        this.links_render();
    }

    links_render() {
        this.prepareMultipleLinks();
        // this.links = this.gLinks
        //     .selectAll("line")
        //     .data(this.graph.links)
        //     .join('line')
        //     .attr("stroke-width", "3")
        //     .attr('stroke', (d, i) => {
        //         return this.linkScale(d[this.graph.edgeTypeKey])
        //     });

        this.links = this.gLinks
            .selectAll(".edge")
            .data(this.graph.links)
            .join('g')
            .classed("edge", true)

        // this.dots = this.gLinks
        //     .selectAll(".dot")
        //     .data(this.graph.links)
        //     .join("circle")
        //     .classed("dot", true)
        //     .attr("cx", d =>  {
        //         console.log("l ", d)
        //         return d.source.x
        //     })
        //     .attr("cy", d => d.source.y)
        //     .attr("r", 5);

        this.dots = this.links
            .selectAll(".dot")
            .data(d => {
                let rangeLiteral = d.rangeLiteral;
                if (Array.isArray(rangeLiteral)) {
                    return d3.range(rangeLiteral[1] - 1);
                } else {
                    return d3.range(rangeLiteral - 1);
                }
            })
            .join("circle")
            .classed("dot", true)
            .attr("r", 5);

        // Create one line per edge type
        this.links
            .selectAll("line")
            .data(d => {
                // if (d[this.graph.edgeTypeKey] == globals.ANY_EDGETYPE) return [{edgeType: globals.ANY_EDGETYPE}];
                if (d.type == globals.ANY_EDGETYPE) return [{edgeType: globals.ANY_EDGETYPE}];

                // let nTypes = d[this.graph.edgeTypeKey].length;
                let nTypes = d.type.length;
                let lastType = nTypes == 1 ? null : d.type[nTypes - 1];

                if (Array.isArray(d.type)) {
                    return d.type.map(et => {
                        return {edgeType: et, lastType: lastType}
                    })
                } else {
                    return [{edgeType: d.type, lastType: d.type}]
                }
            })
            .join('line')
            .attr("stroke-width", this.edgeWidth)
            .attr("stroke-dasharray", (d) => {
                if (d.edgeType != globals.ANY_EDGETYPE && d.lastType == d.edgeType) {
                    return "8";
                } else {
                    return "0";
                }
            })
            .attr('stroke', (d, i) => {
                return this.linkScale(d.edgeType)
            });
    }

    nodes_render() {
        this.egos = this.gNodes
            .selectAll(".ego")
            .data(this.graph.nodes.filter(n => n.ego == true))
            .join("circle")
            .attr("r", this.radius * 2)
            .attr("cx", d => {
                return d.x
            })
            .attr("cy", d => d.y)
            .classed("ego", true)
            .style("fill", "red")
            .style("opacity", 0.5)
        this.egos.lower();

        // this.graph.entityTypes.forEach((et) => {
        this.graph.nodeTypes.forEach((et) => {
            let shape = this.nodeTypeLegend.legend[et];
            if (shape == ShapeEnum.CIRCLE) {
                this.circleNodes = this.gNodes
                    .selectAll(".circle")
                    .data(this.graph.getNodesByType(et))
                    .join("circle")
                    .attr("r", this.radius)
                    .attr("fill", d => "white")
                    .attr("stroke", "black")
                    .classed("node", true)
                    .classed("circle", true)
            } else if (shape == ShapeEnum.SQUARE) {
                this.squareNodes = this.gNodes
                    .selectAll("rect")
                    .data(this.graph.getNodesByType(et))
                    .join("rect")
                    .attr("width", this.radius * 2)
                    .attr("height", this.radius * 2)
                    .attr("fill", d => "white")
                    .attr("stroke", "black")
                    .classed("node", true)
            }
        })

        this.starNodes = this.gNodes
            .selectAll("path")
            // .data(this.graph.getNodesByType(globals.ANY_NODETYPE))
            .data(this.graph.getNodesByType(null))
            .join("path")
            .attr("d", d3.symbol().type(d3.symbolStar).size(this.radius * 12))
            .attr("fill", globals.FILLSTYLE)
            .attr("stroke", globals.STROKESTYLE)
            .classed("node", true)

        this.nodes = this.svg.selectAll(".node")
            .attr("stroke-width", globals.LINE_WIDTH)
            .attr("fill", d => {
                return this.getNodeColor(d);
            })

        this.text = this.gNodes.selectAll("text")
            .data(this.graph.nodes)
            .join("text")
            .classed("entity-id-text", true)
            .text((d) => d.id)

        if (this.isDragging) {
            this.nodes.call(d3.drag()
                .on("start", this.dragStart)
                .on("drag", this.dragMove)
                .on("end", this.dragEnd));
        }

        if (this.lasso) {
            this.lassoInit();
        }
    }

    //TODO : handel several values for categorical
    getNodeColor = (d) => {
        this.noValueColor = "white";

        if (this.nodePropertyToPlot) {
            let propValue = this.getEntityPropertyCb(d.id, this.nodePropertyToPlot);
            if (Array.isArray(propValue)) {
                if (isNaN(propValue[0])) {
                    let colors = propValue.map(v => this.nodeColorScale(v));
                    let gradientId = this.createGradient(colors);
                    return `url('#${gradientId}')`
                } else {
                    propValue = (propValue[0] + propValue[1]) / 2;
                }
            } else {
                propValue = propValue;
            }

            if (propValue && propValue != "null") {
                return this.nodeColorScale(propValue);
            } else {
                return this.noValueColor;
            }
        } else {
            return this.noValueColor;
        }
    }

    createGradient(colors) {
        let gradientId = this.gradientId();
        let gradient = this.svg.append("linearGradient")
            .attr("id", gradientId);
        let n_colors = colors.length;

        colors.forEach((color, i) => {
            gradient.append("stop")
                .style("stop-color", color)
                .attr("offset", `${Math.round((100 / n_colors) * (i))}%`)

            gradient.append("stop")
                .style("stop-color", color)
                .attr("offset", `${Math.round((100 / n_colors) * (i + 1))}%`)
        })

        return gradientId;
    }

    gradientId() {
        this.idCount += 1;
        return `gradient-${this.idCount}`;
    }

    edgeSortedCoordinates(source, target) {
        let [x1, x2, y1, y2] = [source.x, target.x, source.y, target.y];

        if (x1 > x2) {
            [x1, x2] = [x2, x1];
        }

        if (y1 > y2) {
            [y1, y2] = [y2, y1];
        }

        return [x1, x2, y1, y2];
    }

    computeEdgeSplitPos(source, target, n) {
        let [x1, x2, y1, y2] = this.edgeSortedCoordinates(source, target);

        let xPositions = [];
        let yPositions = [];

        for (let i of d3.range(n)) {
            let x = x1 + (i + 1) * ((x2 - x1) / (n + 1));
            let y = y1 + (i + 1) * ((y2 - y1) / (n + 1));
            xPositions.push(x);
            yPositions.push(y);
        }

        return [xPositions, yPositions]
    }

    isYInverted(source, target) {
        let [y1, y2] = [source.y, target.y];

        if ((y1 > y2)) {
            return true;
        } else {
            return false;
        }
    }

    isXInverted(source, target) {
        let [x1, x2] = [source.x, target.x];

        if ((x1 > x2)) {
            return true;
        } else {
            return false;
        }
    }

    updateVis = () => {
        this.links.each(function (parentDatum) {
            d3.select(this).selectAll("line")
                .attr("x1", function (d) {
                    return parentDatum["source"].x;
                })
                .attr("y1", function (d) {
                    return parentDatum["source"].y;
                })
                .attr("x2", function (d) {
                    return parentDatum["target"].x;
                })
                .attr("y2", function (d) {
                    return parentDatum["target"].y;
                })
                .attr('transform', (d) => {
                    console.log(parentDatum)
                    let translation = GraphVisSvg.calcTranslationApproximate(parentDatum.targetDistance, parentDatum.source, parentDatum.target);
                    return `translate (${translation.dx}, ${translation.dy})`;
                });
        })

        this.links.each((parentDatum) => {
            this.links.selectAll(".dot")
                .attr("cx", (d) => {
                    let [x1, x2, y1, y2] = this.edgeSortedCoordinates(parentDatum.source, parentDatum.target);

                    let pos = d + 1;
                    if (this.isXInverted(parentDatum.source, parentDatum.target)) {
                        pos = parentDatum.rangeLiteral - (d + 1);
                    }

                    return x1 + pos * ((x2 - x1) / (parentDatum.rangeLiteral));
                })
                .attr("cy", (d) => {
                    let [x1, x2, y1, y2] = this.edgeSortedCoordinates(parentDatum.source, parentDatum.target);

                    let pos = d + 1;
                    if (this.isYInverted(parentDatum.source, parentDatum.target)) {
                        pos = parentDatum.rangeLiteral - (d + 1);
                    }

                    return y1 + pos * ((y2 - y1) / (parentDatum.rangeLiteral));
                })
        })

        // this.dots
        //     .attr("cx", (d) => {
        //         console.log("cx", d)
        //         let [xs, ys] = this.computeEdgeSplitPos(d.source, d.target, 1);
        //         return xs[0]
        //     })
        //     .attr("cy", (d) => {
        //         let [xs, ys] = this.computeEdgeSplitPos(d.source, d.target, 1);
        //         return ys[0]
        //     })

        // this.graph_vis.links.selectAll("line")
        //     .attr("x1", function(d) { return d["source"].x; })
        //     .attr("y1", function(d) { return d["source"].y; })
        //     .attr("x2", function(d) { return d["target"].x; })
        //     .attr("y2", function(d) { return d["target"].y; });

        if (this.circleNodes) {
            this.circleNodes
                .attr("cx", d => d.x)
                .attr("cy", d => d.y)
        }

        if (this.squareNodes) {
            this.squareNodes
                .attr("x", d => d.x - globals.RADIUS)
                .attr("y", d => d.y - globals.RADIUS)
        }

        if (this.starNodes) {
            this.starNodes
                .attr("transform", function (d) {
                    return `translate(${d.x} ,${d.y})`;
                })
        }

        this.text
            .attr("x", d => d.x + 12)
            .attr("y", d => d.y - 12)

        this.egos
            .attr("cx", d => d.x)
            .attr("cy", d => d.y)
    }

    highlightNodes(nodesList) {
        this.nodes
            .style("opacity", (d, i) => {
                if (nodesList.includes(d.id)) {
                    return 1
                } else {
                    return 0.4
                }
            })
    }

    // boldEdges(edges) {
    //     this.links
    //         .style("stroke-width", (d, i) => {
    //             console.log(d);
    //             if (edges.includes(d)) {
    //                 return this.edgeWidth * 2;
    //             } else {
    //                 return this.edgeWidth;
    //             }
    //         });
    // }

    edgeSelectionFromNodeList(nodeList) {
        return this.links.filter((d, i) => nodeList.includes(d.source.id) & nodeList.includes(d.target.id));
    }

    // highlightEdges(subgraphsList) {
    //     this.links
    //         .style("opacity", (d, i) => {
    //             for (let j = 0; j < subgraphsList.length; j++) {
    //                 let subgraph = subgraphsList[j];
    //                 if (subgraph.includes(d.target.id) && subgraph.includes(d.source.id)) {
    //                     return 1;
    //                 }
    //             }
    //             return 0.4;
    //         });
    // }

    dragStart = (d, i) => {
        // this.simulation.restart(1, 0);
        // this.simulation.simulation.stop() // stops the force auto positioning before you start dragging
    }

    dragMove = (e, d, i) => {
        d.px += e.dx;
        d.py += e.dy;
        d.x += e.dx;
        d.y += e.dy;

        // d.x += e.x;
        // d.y += e.y;
        d.fx = d.x;
        d.fy = d.y;
        this.simulation.restart(1, 0);
    }

    dragEnd = (e, d, i) => {
        d.fixed = true; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
        // this.simulation.ticked();
        // this.simulation.restart(1, 0);
    }

    zoomAction = (e, d) => {
        this.gNodes.attr("transform", e.transform);
        this.gLinks.attr("transform", e.transform);
    }

    lasso_start = () => {
        this.lasso.items()
            // .attr("r", this.radius) // reset size
            .classed("not_possible", true)
            .classed("selected", false);
    };

    lasso_draw = () => {
        // Style the possible dots
        this.lasso.possibleItems()
            .classed("not_possible", false)
            .classed("possible", true);

        // Style the not possible dot
        this.lasso.notPossibleItems()
            .classed("not_possible", true)
            .classed("possible", false);
    };

    lasso_end = () => {
        // Reset the color of all dots
        this.lasso.items()
            .classed("not_possible", false)
            .classed("possible", false);

        // Style the selected dots
        let lassoSelectedNodes = [];
        this.lasso.selectedItems()
            .classed("selected", true)
            .each((d) => {
                lassoSelectedNodes.push(d.id);
            })
        // .call((d) => console.log(i, d));
        // .attr("r",(d) => {
        //     console.log(d);
        //     return 7
        // });

        this.scriptLang.graphToScript(this.edgeSelectionFromNodeList(lassoSelectedNodes));
        // this.highlightEdges([lassoSelectedNodes])

        // Reset the style of the not selected dots
        this.lasso.notSelectedItems()
        // .attr("r",3.5);
    };
}

export {GraphVisSvg};