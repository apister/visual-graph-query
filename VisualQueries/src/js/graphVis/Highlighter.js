function highlightNodes(nodesList){
    nodesList.forEach(nodeId => {
        this.highlightNodeTable[nodeId] = true;
    })
}

function highlightLinks(linksList){
    linksList.forEach(linkId => {
        this.highlightLinkTable[linkId] = true;
    })
}


function highlightNodesOnly(nodesList) {
    Object.keys(this.highlightNodeTable).forEach(nId => this.highlightNodeTable[nId] = false);
    nodesList.forEach(nId => {
        this.highlightNodeTable[nId] = true;
    })
}

function selectNodes(nodesList){
    nodesList.forEach(nodeId => {
        this.selectNodeTable[nodeId] = true;
    })
}

function selectNodesOnly(nodesList) {
    Object.keys(this.selectNodeTable).forEach(nId => this.selectNodeTable[nId] = false);
    nodesList.forEach(nId => {
        this.selectNodeTable[nId] = true;
    })
}

function colorNodes(nodesList, color){
    nodesList.forEach(nodeId => {
        this.colorNodeTable[nodeId] = color;
    })
}

function colorNodesOnly(nodesList, color) {
    Object.keys(this.colorNodeTable).forEach(nId => this.colorNodeTable[nId] = false);
    nodesList.forEach(nId => {
        this.colorNodeTable[nId] = color;
    })
}


function highlightLinksOnly(linksIds){
    Object.keys(this.highlightLinkTable).forEach(lId => this.highlightLinkTable[lId] = false);
    linksIds.forEach(linkId => {
        this.highlightLinkTable[linkId] = true;
    })
}

function selectLinksOnly(linksList) {
    Object.keys(this.selectLinkTable).forEach(lId => this.selectLinkTable[lId] = false);
    linksList.forEach(lId => {
        this.selectLinkTable[lId] = true;
    })
}

export {highlightNodes, selectNodes, highlightNodesOnly, highlightLinksOnly, selectNodesOnly, selectLinksOnly, colorNodes, colorNodesOnly, highlightLinks}