import * as d3 from 'd3';
import contextMenu from 'd3-context-menu';
import 'd3-context-menu/css/d3-context-menu.css';

import {globals} from "../globals";
import Tooltip from "../components/Tooltip";


export default class ProvenanceGraphVis {
    static COMPARED = "compared";

    constructor(containerElement, provenanceManager, compareQueryCb, closeCompareQueryCb, clickTreeNodeCb) {
        this.containerElement = containerElement;
        this.provenanceManager = provenanceManager;
        this.provenance = this.provenanceManager.provenance;
        this.compareQueryCb = compareQueryCb;
        this.closeCompareQueryCb = closeCompareQueryCb;
        this.clickTreeNodeCb = clickTreeNodeCb;

        this.nodeRadius = 8;
        this.depthLevelSeparation = 40;
        this.nodeSameDepthSeparation = 100;
        this.depthHeight = this.nodeRadius * 2 + this.depthLevelSeparation;

        this.width = 400;
        this.widthMargin = 15;

        this.height = 1000;
        this.fontSize = 13;
        this.strokeWidth = 3;
        this.nodeColor = "black";
        // this.nodeSelectStrokeColor = globals.SELECTION_COLOR;
        this.nodeSelectStrokeColor = "yellow";

        this.margin = {top: 35, right: 40, bottom: 20, left: 40};
        this.initSvg();
        this.initTooltip();

        this.comparedNode = null;
        this.brushedNodes = [];
    }

    initTooltip() {
        this.tooltip = new Tooltip(d3.select("#provenance-tooltip"));
    }

    initSvg() {
        this.containerElement.html("");
        this.svg = this.containerElement.append("svg")
            .attr("width", this.width)
            .attr("height", this.height)

        this.initBrush();
        this.g = this.svg
            .append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

        this.gLinks = this.g.append("g")
            .attr("fill", "none")
            .attr("stroke", "#555")
            .attr("stroke-opacity", 0.4)
            .attr("stroke-width", 1.5)

        this.gNodes = this.g.append("g")

    }

    initBrush() {
        this.brush = d3.brushY()
            .on("start brush end", this.brushed);

        this.svg
            .append("g")
            .classed("brush-group", true)
            .call(this.brush);
    }

    brushed = ({selection}) => {
        if (selection == null) {
            this.provenanceManager.minimizeButton.classed("disabled", true);
        } else {
            const [y0, y1] = selection;
            this.brushedNodes = [];

            let noneFound = true;
            this.processedTreeDescendants().forEach(d => {
                if ((d.y + this.margin.top) <= y1 && (d.y + this.margin.top) >= y0) {
                    this.brushedNodes.push(d);
                    this.provenanceManager.minimizeButton.classed("disabled", false);
                    noneFound = false;
                }

                if (noneFound) {
                    this.provenanceManager.minimizeButton.classed("disabled", true);
                }
            });
        }
    }

    reduceBrushedNodes() {
        this.brushedNodes.forEach(node => {
            this.reduceNode(node)
        })
    }

    setupContextMenu() {
        // OLD COMPARISON
        // let menuData = [
        //     {
        //         title: "Compare",
        //         action: this.startCompareModeCb
        //     }
        // ]

        // TO reduce nodes
        let menuData = [
            {
                title: "Reduce",
                action: this.reduceNode
            }
        ]

        this.contextMenu = contextMenu(menuData);
    }

    reduceNode = (d) => {
        if (d.parent == null) {
            return; // root case
        }

        if (d.data.isReduced) {
            d.data.isReduced = !d.data.isReduced;
        } else {
            d.data.isReduced = true;
        }

        this.setupTree();
        this.render();
    }

    startCompareModeCb = (d) => {
        let matchingResult = this.provenance.graphIdToMatchingResult[d.data.id];
        let graphMetrics = this.provenance.graphIdToGraphMetrics[d.data.id];

        if (this.comparedNode == d) {
            this.closeCompareMode();
        } else {
            this.openCompareMode(d, matchingResult, graphMetrics);
        }
    }

    openCompareMode(d, matchingResult, graphMetrics) {
        this.comparedNode = d;
        this.compareQueryCb(matchingResult, graphMetrics, d.data.id);
        this.render();
    }

    closeCompareMode() {
        this.comparedNode = null;
        this.closeCompareQueryCb();
        this.render();
    }

    resetComparedNodes() {
        this.tree.descendants().forEach(d => d[ProvenanceGraphVis.COMPARED] = false);
    }

    reset() {
        this.oldTree = null;
        this.latestNode = null;
    }

    setupTree = () => {
        // this.graph = JSON.parse(this.provenance.exportProvenanceGraph());
        this.graph = this.provenance.graph;

        this.root = d3.hierarchy(this.graph.nodes[this.graph.root], d => {
            const filtered = Object.values(this.graph.nodes).filter(
                (val) => d.children.includes(val["id"])
            )
            return filtered
        })

        this.tree = d3.tree()
            .size([this.width - (this.widthMargin * 2), this.depthHeight * (this.root.height)])
            // .nodeSize([this.nodeRadius, this.nodeRadius])
            // .separation((a, b) => a.parent == b.parent ? 1 : 10)
            .separation((a, b) => a.data.isReduced || b.data.isReduced ? 1 : 10)
            (this.root);


        if (this.oldTree) {
            this.computeLatestAddedNode();
        }
        this.oldTree = this.tree;

        this.updatePositions();
    }

    updatePositions() {
        this.reduceNodeReduction = 26;
        this.processedTreeDescendants().forEach(node => {
            let nReducedParent = this.numberOfReducedParents(node);
            let nReducedChain = this.numberOfChanges(node);
            node.y = node.y - (this.reduceNodeReduction * (nReducedParent + nReducedChain));
        })
    }

    numberOfChanges(node) {
        if (node.parent == null) { // Root case
            return 0;
        }

        let nReducedChain = 0;
        let parentNode = node;
        let childNodeReduced = parentNode.data.isReduced;

        let bigToReduced = false;

        while (parentNode) {
            let currentNodeReduced = parentNode.data.isReduced;

            if (!childNodeReduced && currentNodeReduced) {
                bigToReduced = true;
            }

            if ((bigToReduced) && (childNodeReduced && !currentNodeReduced)) {
                nReducedChain += 1;
                bigToReduced = false;
            }

            childNodeReduced = parentNode.data.isReduced;
            parentNode = parentNode.parent;
        }

        return nReducedChain;
    }

    numberOfReducedParents(node) {
        if (node.parent == null) { // Root case
            return 0;
        }

        let nReduced = 0;
        let parentNode = node;
        let childNodeReduced = parentNode.data.isReduced;

        while (parentNode) {
            let currentNodeReduced = parentNode.data.isReduced;
            if (currentNodeReduced) {
                nReduced += 1;
            }

            childNodeReduced = parentNode.data.isReduced;
            parentNode = parentNode.parent;
        }

        return nReduced;
    }

    computeLatestAddedNode() {
        let oldDesc = this.oldTree.descendants()
        let desc = this.tree.descendants()
        this.latestNode = desc.filter(n => !oldDesc.map(node => node.data.id).includes(n.data.id))[0];
    }

    render() {
        let path = d3
            .linkVertical()
            .x(d => d.x)
            .y(d => d.y)

        const t = d3.transition()
            .duration(750)
            .ease(d3.easeLinear);

        this.gLinks
            .selectAll("path")
            .data(this.processedTreeLinks())
            .join((enter) => enter.append("path")
                    .attr("d", path)
                    .attr("stroke-dasharray", (d, i, n) => {
                        let totalLength = n[i].getTotalLength()
                        return totalLength + " " + totalLength
                    })
                    .attr("stroke-dashoffset", (d, i, n) => n[i].getTotalLength())
                    .call(enter => enter.transition(t)
                        .attr("stroke-dashoffset", 0)),
                (update) => update.attr("d", path)
            )

        this.nodes = this.gNodes
            .selectAll('circle')
            .data(this.processedTreeDescendants())
            .join(enter => enter.append("circle")
                    // .on('click', (e, d) => { // Events are to be defined before transition (otherwise the on function is not the same)
                    //     console.log("click")
                    //     this.clickTreeNodeCb();
                    //     this.provenance.goToNode(d.data.id);
                    // })
                    .attr('transform', d => {
                        return `translate(${d.x},${d.y - this.depthHeight})`
                    })
                    .call(enter => enter.transition(t)
                        .attr('transform', d => {
                            return `translate(${d.x},${d.y})`
                        })
                    ),
                update => update.attr('transform', d => {
                    return `translate(${d.x},${d.y})`
                })
            )
            .attr('stroke-width', this.strokeWidth)
            .attr('r', (d) => {
                if (d.data.isReduced) {
                    return this.nodeRadius / 2
                } else {
                    return this.nodeRadius
                }
            })
            .attr('fill', this.nodeColor)
            .attr("stroke", d => {
                if (this.isNodeCurrentNode(d)) {
                    return this.nodeSelectStrokeColor;
                } else if (this.isNodeCompareNode(d)) {
                    return globals.comparisonQueryColor;
                } else {
                    return "black";
                }
            })
            .on('click', (e, d) => { // Events are to be defined before transition (otherwise the on function is not the same)
                console.log("click")
                this.clickTreeNodeCb();
                this.provenance.goToNode(d.data.id);
            })
            .on("mouseover", (e, d) => {
                let thumbnail = this.provenance.graphIdToThumbnail[d.data.id];
                this.tooltip.element.html("");
                this.tooltip.element.append(() => thumbnail)
                this.tooltip.show();
                // this.tooltip.updatePosition(e.clientX, e.clientY);
                this.tooltip.updatePosition(e.clientX, 10);
            })
            .on('mouseout', () => {
                this.tooltip.hide();
            })
            .on("contextmenu", this.contextMenu);

        // this.gNodes
        //     .selectAll('text')
        //     .data(this.processedTreeDescendants())
        //     .join('text')
        //         .attr('text-anchor', 'middle')
        //         .attr('alignment-baseline', 'middle')
        //         .attr('transform', d => `translate(${d.x},${d.y - (this.fontSize * 1.2)})`)
        //         .attr('font-size', this.fontSize)
        //         .text(d => d.data.label);

        this.gNodes
            .selectAll('.label')
            .data(this.processedTreeDescendants())
            .join((enter) => {
                let p = enter
                    .append('foreignObject')
                    .classed("label", true)
                    .attr('x', d => d.x - 20)
                    .attr('y', d => d.y - this.fontSize * 2.2)
                    .attr('width', "200")
                    .attr('height', "20")
                    .append("xhtml:div")
                    .attr("contenteditable", true)
                    .text(d => d.data.label)
            }, (update) => {
                update
                    .attr('x', d => d.x - 20)
                    .attr('y', d => d.y - this.fontSize * 2.2)

                update
                    .filter(d => d.data.isReduced)
                    .text(d => null)
            })

        this.renderComparisonLabels();
    }

    renderComparisonLabels() {
        // Nefore marked buttons
        // if (this.comparedNode) {
        //     this.gNodes
        //         .selectAll('.comparison-text')
        //         .data(this.processedTreeDescendants())
        //         .join('text')
        //         .classed("comparison-text", true)
        //         .attr('text-anchor', 'middle')
        //         .attr('alignment-baseline', 'middle')
        //         .attr('transform', d => `translate(${d.x + 20},${d.y + 3})`)
        //         .attr('font-size', this.fontSize)
        //         .text(d => {
        //             if (this.isNodeCurrentNode(d)) {
        //                 return globals.mainQueryName;
        //             } else if (this.isNodeCompareNode(d)) {
        //                 return globals.comparisonQueryName;
        //             }
        //         })
        //         .attr("fill", (d) => {
        //             if (this.isNodeCurrentNode(d)) {
        //                 return globals.mainQueryColor;
        //             } else if (this.isNodeCompareNode(d)) {
        //                 return globals.comparisonQueryColor;
        //             }
        //         })
        // }

        this.gNodes
            .selectAll('.comparison-text')
            .data(this.processedTreeDescendants())
            .join('text')
            .classed("comparison-text", true)
            .attr('text-anchor', 'middle')
            .attr('alignment-baseline', 'middle')
            .attr('transform', d => `translate(${d.x + 20},${d.y + 3})`)
            .attr('font-size', this.fontSize + 7)
            .attr('font-weight', "bold")
            .text(d => {
                if (d.data.id == this.provenanceManager.nodeMarkedAsA) {
                    return globals.mainQueryName;
                } else if (d.data.id == this.provenanceManager.nodeMarkedAsB) {
                    return globals.comparisonQueryName;
                }
            })
            .attr("fill", (d) => {
                if (d.data.id == this.provenanceManager.nodeMarkedAsA) {
                    return globals.mainQueryColor;
                } else if (d.data.id == this.provenanceManager.nodeMarkedAsB) {
                    return globals.comparisonQueryColor;
                }
            })
    }

    isNodeCurrentNode(d) {
        return d.data.id == this.provenance.current.id;
    }

    isNodeCompareNode(d) {
        return this.comparedNode && d.data.id == this.comparedNode.data.id;
    }

    // We have to process the links and nodes to put the latest ones added the last in the lists. It is for the enter() and update() function to work correctly.
    // (Probably useless Now that provenance.graph is used direcly)
    processedTreeLinks() {
        if (this.latestNode) {
            const latestLink = this.tree.links().find(n => n.target.data.id == this.latestNode.data.id);
            let processedLinks = this.tree.links().filter(n => n.target.data.id != this.latestNode.data.id);
            processedLinks.push(latestLink);
            return processedLinks;
        } else {
            return this.tree.links();
        }
    }

    processedTreeDescendants() {
        if (this.latestNode) {
            let processedDescendants = this.tree.descendants().filter(n => n.data.id != this.latestNode.data.id)
            processedDescendants.push(this.latestNode);
            return processedDescendants;
        } else {
            return this.tree.descendants();
        }
    }

    run() {
        this.setupTree();
        this.setupContextMenu();
        this.render();
    }
}