import * as d3 from "d3";

import {StaticGraphVisCanvas} from "./StaticGraphVisCanvas";
import {Simulation} from "./Simulations/Simulation";
import TopMenuBar from "../components/TopMenuBar";
import {globals} from "../globals";


export default class Visualizer {
    constructor(graphData, canvas, selector) {
        this.graphData = graphData;
        this.canvas = canvas;
        this.canvasDivContainer = d3.select(this.canvas.node().parentNode);
        this.selector = selector;
    }

    setCollection(visualiserCollection) {
        this.visualizerCollection = visualiserCollection;
    }

    toggleRenderLabels = () => {
        this.graphVis.renderLabels = !this.graphVis.renderLabels;
        this.render();
    }

    async init() {
        await this.setupMapRenderer();

        this.simulation = new Simulation(this.graphData, this.canvas, this.mapVis);
        this.graphVis = new StaticGraphVisCanvas(this.graphData, this.canvas, true, true, null, this.mapVis, this.selector);

        this.setupMenu();
    }

    async setupMapRenderer() {
        this.mapVis = null;
        // this.mapVis = new MapVis(this.canvas);
        // await this.mapVis.init();
    }

    setupMenu() {
        this.topMenuBar = new TopMenuBar(this.canvasDivContainer, globals.MENU_COLOR, "100px");
        this.topMenuBar.init();
        this.topMenuBar.addTitle("Node-Link Visualization");
    }

    addSliderOption(title, onMoveCb, min, max, step, start) {
        this.topMenuBar.addSliderOption(title, onMoveCb, min, max, step, start);
    }

    async run() {
        // For small networks, d3 force is better than igraph + jitter
        if (this.graphData.nodes.length < 100) {
            this.simulation.init();
        } else {
            this.simulation.initOnlyJitter();
        }

        // await this.simulation.getPosOrRun(1000);
        await this.simulation.getPosOrRun(300);

        // await this.simulation.run(300);
        this.simulation.stop();

        this.graphVis.isMapLayout = false;
        this.graphVis.render();
    }

    runMapLayout() {
        // this.graphData.nodes.forEach(d => {
        //     if (d["latitude"] && d["longitude"]) {
        //         this.mapVis.transformNodePos(d);
        //     }
        // })

        // this.simulation.stop();
        this.simulation.initMapSim();
        this.simulation.run();

        this.graphVis.isMapLayout = true;
        this.graphVis.render();
    }

    render() {
        this.graphVis.render();
    }
}