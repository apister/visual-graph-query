import * as d3 from 'd3';

import GraphVisCanvas from "./GraphVisCanvas";
import {globals} from "../globals";
import {getNodeLabel} from "./Occlusion";
import {generateLinkIdFromLinkObject, generateReverseLinkIdFromLinkObject} from "../utils/utils";


export default class HyperGraphVisCanvas extends GraphVisCanvas {
    constructor(graph, simulation, documentGraph, canvas, isDragging, isZooming, linkScale, mapVis) {
        super(graph, simulation, canvas, isDragging, isZooming, linkScale, mapVis)
        this.documentGraph = documentGraph;

        this.findNodesLinksWithGeoloc();
        this.highlightLinksOnly([]);
    }

    static rectWidth() {
        return globals.RADIUS * 2;
    }

    static rectHeight() {
        return globals.RADIUS;
    }

    unhighlightOpacity() {
        return 0.1
    }

    findNodesLinksWithGeoloc() {
        this.nodesWithGeoloc = this.graph.nodes.filter(n => this.documentGraph.idToNode[n.document]["latitude"]);
        let nodesIdsWithGeoloc = this.nodesWithGeoloc.map(n => n.id);
        this.linksWithGeoloc = this.graph.links.filter(l => nodesIdsWithGeoloc.includes(l.source.id) && nodesIdsWithGeoloc.includes(l.target.id));
    }

    setMST(mst) {
        this.mst = mst;
        this.mstAllLinks = Object.values(this.mst).reduce((l1s, l2s) => l1s.concat(l2s));
        this.highlightLinksOnly(this.mstAllLinks.map(link => link[0] + link[3]["edgeType"] + link[1]));
    }

    runMapLayout() {
        this.isMapLayout = true;
        this.render();
    }

    renderNodes() {
        this.ctx.lineWidth = 1;

        let nodes;
        if (this.isMapLayout) {
            nodes = this.nodesWithGeoloc;
        } else {
            nodes = this.graph.nodes;
        }

        nodes.forEach((d, i) => {
            this.setNodesRenderingProperties(d);
            this.colorNode(d);
            this.renderNode(d);
        });
    }

    renderNode(d) {
        this.ctx.beginPath();

        let [x, y] = [d.x, d.y];
        this.ctx.rect(x - (HyperGraphVisCanvas.rectWidth() / 2), y - (HyperGraphVisCanvas.rectHeight() / 2), HyperGraphVisCanvas.rectWidth(), HyperGraphVisCanvas.rectHeight())

        this.ctx.fill();
        this.ctx.stroke();

        this.renderNodeLabel(d, x, y);
    }

    colorNode(d) {
        this.ctx.fillStyle = this.linkScale(d.edgeType);
    }

    colorLink(d) {
        this.ctx.strokeStyle = "black";
    }

    renderNodeLabel(d, x, y) {
        this.ctx.fillStyle = "black";
        let label = d.person
        // let label = d.id
        this.ctx.fillText(label, x, y);
    }

    renderLinks() {
        let links = this.isMapLayout ? this.linksWithGeoloc : this.graph.links;

        links.forEach((d) => {
            this.colorLink(d);
            this.setupLinkHighlight(d);
            this.setupLinkSelect(d);
            this.renderLink(d);
        });
    }

    renderLink(link) {
        this.ctx.beginPath();

        let [x1, y1, x2, y2] = [link.source.x, link.source.y, link.target.x, link.target.y];

        let dx = x2 - x1;
        let dy = y2 - y1;
        let angle = Math.atan2(dy, dx);
        // let length=Math.sqrt(dx*dx+dy*dy);

        // to remove the bits overlapping the nodes
        x1 = globals.RADIUS * Math.cos(angle) + x1;
        x2 = x2 - globals.RADIUS * Math.cos(angle);
        y1 = globals.RADIUS * Math.sin(angle) + y1;
        y2 = y2 - globals.RADIUS * Math.sin(angle);

        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);

        // this.renderLinkArrow(x2, y2, angle)

        this.ctx.stroke();
    }

    setupLinkHighlight(link) {
        // For undirected graphs
        let linkId = generateLinkIdFromLinkObject(link, this.graph.edgeTypeKey)
        let linkIdReverse = generateReverseLinkIdFromLinkObject(link, this.graph.edgeTypeKey)

        if (this.highlightLinkTable[linkId] || this.highlightLinkTable[linkIdReverse]) {
            this.ctx.globalAlpha = 1.0;
            this.i += 1;
        } else {
            this.ctx.globalAlpha = this.unhighlightOpacity();
        }
    }

    findNode(x, y) {
        let i;
        for (i = this.graph.nodes.length - 1; i >= 0; --i) {
            const node = this.graph.nodes[i],
                dx = x - node.x,
                dy = y - node.y

            if ((dx * dx < Math.pow(HyperGraphVisCanvas.rectWidth() / 2, 2)) && (dy * dy < Math.pow(HyperGraphVisCanvas.rectHeight() / 2, 2))) {
                return node;
            }
        }
        // No node selected
        return undefined;
    }

    selectPerson = (personId) => {
        console.log("SEL PERSON", personId);
        let personNodes = this.graph.nodes.filter(n => n.person == personId).map(n => n.id);

        if (personNodes.length > 0) {
            let duplicationLinks = personNodes.map(n => Object.keys(this.graph.getNeighbors(n)))
                .reduce((ns1, ns2) => ns1.concat(ns2));

            this.highlightLinksOnly(duplicationLinks);
            this.selectNodes(personNodes);
        }
    }

    deselectPerson = (personId) => {
        let personNodes = this.graph.nodes.filter(n => n.person == personId).map(n => n.id);
        personNodes.forEach(nId => {
            this.selectNodeTable[nId] = false;
        })

        this.highlightLinksOnly([]);

        this.render();
    }
}
