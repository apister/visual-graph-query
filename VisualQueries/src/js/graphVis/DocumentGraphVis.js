import GraphVisCanvas from "./GraphVisCanvas";
import {globals} from "../globals";
import {generateLinkIdFromLinkObject, processFloat} from "../utils/utils";
import * as d3 from "d3";


export default class DocumentGraphVis extends GraphVisCanvas {
    constructor(documentGraph, mainGraph, canvas, isDragging, isZooming, linkScale, mapVis, selector) {
        super(documentGraph, canvas, isDragging, isZooming, linkScale, mapVis, selector);
        this.mainGraph = mainGraph;
        this.NSharedPersonsThreshold = 1000;
    }

    setNSharedPersonsThreshold(value) {
        this.NSharedPersonsThreshold = value;
    }

    init() {
        if (this.isZooming) {
            this.setupZoom();
        }
    }

    setupZoom() {
        this.zoom = d3.zoom()
            .on("zoom", this.zoomAction);

        this.canvas
            .call(this.zoom)
            // .call(this.zoom.transform, d3.zoomIdentity)
            // .call(this.zoom.transform, this.transform
            //     // .translate(this.mapVis.projection.translate()[0], this.mapVis.projection.translate()[1])
            //     // .scale(this.mapVis.projection.scale())
            //     // .translate(this.mapVis.projection([this.mapVis.centerCoordinates[0], this.mapVis.centerCoordinates[1]]))
            //     // .scale(10000)
            //     .translate(this.getWidth() / 2, this.getHeight() / 2)
            //     .scale(1 << 12)
            //     .translate(-this.mapVis.centerCoordinates[0], -this.mapVis.centerCoordinates[1])
            // )
    }

    runMapLayout() {
        this.isMapLayout = true;
        this.render();
    }

    translateAndScale() {
        let transform = this.onlyManualZoomTransform();

        this.ctx.translate(transform.x, transform.y);
        this.ctx.scale(transform.k, transform.k);
    }

    // Nodes are rendered after first inital zoom. Returns the transform equals to only the manual zoom
    onlyManualZoomTransform() {
        let transform = this.transform
            .scale(1/this.mapVis.scaleFactor)
            .translate(-this.mapVis.xTranslate2, -this.mapVis.yTranslate2)
        return transform;
    }

    renderNodes(nodesIds) {
        this.ctx.lineWidth = 1;

        let nodes = nodesIds ? this.graph.nodes.filter(n => nodesIds.includes(n.id)) : this.graph.nodes;
        nodes.forEach((d, i) => {
            this.setNodesRenderingProperties(d);
            this.colorNode(d);
            this.renderNode(d);
        });
    }

    setNodesRenderingProperties(d) {
        super.setNodesRenderingProperties(d);
    }

    renderNode(d) {
        let width = d.get("width") ? d.get("width") : globals.RADIUS * 2
        this.ctx.beginPath();

        let [x, y] = [d.x, d.y];
        this.ctx.rect(x - (width / 2), y - (d.get("height") / 2), width, d.get("height"))

        this.ctx.fill();
        this.ctx.stroke();

        if (this.renderLabels) {
            this.renderNodeLabel(d, x, y);
        }
    }

    renderNodeLabel(d, x, y) {
        this.ctx.fillStyle = "black";
        // let label = d.id
        let label = this.mainGraph.idToNode[d.id].time;
        this.ctx.fillText(label, x, y);
    }

    colorLink(d) {
        this.ctx.strokeStyle = "rgba(14,14,14,0.8)";
    }

    findNode(x, y) {
        let i;
        for (i = this.graph.nodes.length - 1; i >= 0; --i) {
            const node = this.graph.nodes[i],
                dx = x - node.x,
                dy = y - node.y

            if ((dx * dx < Math.pow(node.get("width") / 2, 2)) && (dy * dy < Math.pow(node.get("height") / 2, 2))) {
                return node;
            }
        }

        // No node selected
        return undefined;
    }

    selectPerson = (documentIds) => {
        this.selectNodes(documentIds);
        let linksToHighlight = [];

        if (documentIds.length > 0) {
            documentIds.forEach(nodeId => {
                let neighbors = this.graph.getNeighbors(nodeId);

                let links =
                    [...neighbors]
                        .filter(([k, v]) => documentIds.includes(v.id)).map(v => v[0]);

                linksToHighlight = linksToHighlight.concat(links)
            })

            // TODO : not use highligtht api
            // this.highlightLinksOnly(linksToHighlight.map(l => l.id()));
            // this.highlightLinksOnly([]);
        }

        this.render();
    }

    deselectPerson = () => {
        this.selectNodesOnly([]);
        // this.highlightAllLinks();
        this.render();
    }

    renderLinks(linksIds) {
        let links = linksIds ? this.graph.links.filter(l => linksIds.includes(l.id())) : this.graph.links;

        links.forEach((d) => {
            if (d.get("weight") >= this.NSharedPersonsThreshold) {
                this.colorLink(d);
                this.setupLinkHighlight(d);
                this.setupLinkSelect(d);
                this.renderLink(d);
            }
        });
    }

    setupLinkSelect(link) {
        if (this.selectLinkTable[generateLinkIdFromLinkObject(link)]) {
            this.ctx.lineWidth = globals.LINE_WIDTH_SELECT / (this.manualZoomTransformScale() * 4);
        } else {
            this.ctx.lineWidth = this.getLineWidth() / (this.manualZoomTransformScale() * 8);
        }
    }

    getLineWidth() {
        return super.getLineWidth();
    }

    manualZoomTransformScale() {
        return this.mapVis.scaleManualZoom();
    }

    setupLinkHighlight(link) {
        let linkId = link.id();

        if (this.highlightLinkTable[linkId]) {
            this.ctx.globalAlpha = 1;
            // this.ctx.globalAlpha = 0.8;
        } else {
            this.ctx.globalAlpha = Math.min(this.unighlightOpacity() - 0.05, 0);
        }
    }

    unighlightOpacity() {
        return globals.UNHIGHLIGHT_OPACITY;
    }

    showTooltip(node, e) {
        let html = this.generateHtmlTooltip(node, this.mainGraph.properties);

        this.tooltip
            .style("visibility", "visible")
            .style("top", `${e.clientY + 10}px`)
            .style("left", `${e.clientX + 10}px`)
            .html(html);
    }

    mouseMoveCb(e) {
        // let node = this.findNode(this.transform.invertX(e.offsetX), this.transform.invertY(e.offsetY));
        // let node = this.findNode(this.mapVis.zoomInitialTransform.invertX(e.offsetX), this.mapVis.zoomInitialTransform.invertY(e.offsetY));
        let node = this.findNode(this.onlyManualZoomTransform().invertX(e.offsetX), this.onlyManualZoomTransform().invertY(e.offsetY));

        if (node) {
            this.nodeHover(node, e);
        } else {
            this.hideTooltip();

            if (this.hoveredNode) {
                this.nodeHoverEnd();
            }
        }
    }

    applyNodeZoomLineWidthTransform() {
        if (this.mapVis.scaleManualZoom() < 1) {
            this.ctx.lineWidth = this.ctx.lineWidth / (this.mapVis.scaleManualZoom());
        } else {
            this.ctx.lineWidth = this.ctx.lineWidth;
        }
    }
}