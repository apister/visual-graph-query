import {LassoCanvas} from "canvas-lasso";

const _ = require('lodash');

import {Legend, ShapeEnum} from "../utils/legend";
import * as d3 from 'd3';
import {globals} from "../globals";
import MatrixVis from "./MatrixVis";
import {filterObject, generateLinkIdFromLinkObject, isInt, isPointInRect, processFloat} from '../utils/utils';
import {getNodeLabel, Occlusion} from "./Occlusion";
import AttributeLegend from "../components/AttributeLegend";

import * as Highlighter from "./Highlighter";
import {DocumentNode} from "dynbipgraph/src/Node";


export default class GraphVisCanvas {
    constructor(graph, canvas, isDragging, isZooming, linkScale, mapVis, selector) {
        this.graph = graph;
        this.canvas = canvas;
        this.canvasWrapper = this.canvas.node().parentNode;
        this.mapVis = mapVis;
        this.isDragging = isDragging;
        this.isZooming = isZooming;
        this.selector = selector;

        if (linkScale == null) {
            if (this.graph.linkTypes.length > 8) {
                this.linkScale = d3.scaleOrdinal().domain(this.graph.linkTypes).range(d3.schemeSet1);
            } else {
                this.linkScale = d3.scaleOrdinal().domain(this.graph.linkTypes).range(d3.schemeCategory10);
            }
        } else {
            this.linkScale = linkScale;
        }

        this.setupAttributesTables();
        this.isMapLayout = false;

        this.ctx = canvas.node().getContext('2d');
        this.ctx.font = `${globals.FONT_SIZE}px Arial`;

        if (this.isDragging) {
            this.canvas.call(d3.drag()
                .subject(this.dragSubject)
                .on("start", this.dragStart)
                .on("drag", this.dragMove)
                .on("end", this.dragEnd)
            );
        }
        this.transform = d3.zoomIdentity;
        this.init();

        this.entityTypeLegend = new Legend(this.graph.nodeTypes);
        this.entityTypeLegend.initAutoShapeLegend();

        this.setupMenu();
        this.highlightAllGraph();

        this.occlusion = new Occlusion(this.ctx, this.graph.idToNode, this.highlightNodeTable);

        this.nodePropertyToPlot = null;
        this.nodeColorScale = null;

        this.attributeLegend = new AttributeLegend(d3.select(this.canvasWrapper), null, 30, 30, null, "40%");
        this.attributeLegend.init();

        this.createToolTip();
        this.setupEvents();

        this.persistentSelectedNodes = [];
        this.renderLabels = true;
    }

    getWidth() {
        return this.canvas.attr("width");
    }

    getHeight() {
        return this.canvas.attr("height");
    }

    init() {
        // Lasso has to be setup before zoom event
        this.setupLasso();
        if (this.isZooming) {
            this.setupZoom();
        }
    }

    unighlightOpacity() {
        return globals.UNHIGHLIGHT_OPACITY;
    }

    setSimulation(simulation) {
        this.simulation = simulation;
    }

    setCollection(graphVisCollection) {
        this.graphVisCollection = graphVisCollection;
    }

    setupAttributesTables() {
        this.highlightNodeTable = {};
        this.selectNodeTable = {};
        this.colorNodeTable = {};
        this.highlightLinkTable = {};
        this.selectLinkTable = {};
        this.colorLinkTable = {};
    }

    setNodeAttributeToRender(propertyName, colorScale) {
        this.nodePropertyToPlot = propertyName;
        this.nodeColorScale = colorScale;
    }

    createToolTip() {
        this.tooltip = d3.select(this.canvasWrapper).append("div").classed("tooltip", true)
    }

    toggleMapLayout() {
        this.isMapLayout = !this.isMapLayout;
    }

    removeNodePropertyToRender() {
        this.nodePropertyToPlot = null;
        this.nodeColorScale = null;
    }

    show() {
        this.canvas
            .style("display", "inline-block")

        this.menu.show();
    }

    hide() {
        this.canvas
            .style("display", "none")

        this.menu.hide();
    }

    setupEvents() {
        this.hoveredNode = null;
        this.canvas.on("mousemove", (e) => this.mouseMoveCb(e))
        this.canvas.on("click", (e) => this.mouseClickCb(e))
        this.canvas.on("mouseout", (e) => this.hideTooltip(e))
    }

    mouseMoveCb(e) {
        let node = this.findNode(this.transform.invertX(e.offsetX), this.transform.invertY(e.offsetY));

        if (node) {
            this.nodeHover(node, e);
        } else {
            this.hideTooltip();

            if (this.hoveredNode) {
                this.nodeHoverEnd();
            }
        }
    }

    mouseClickCb(e) {
        if (this.hoveredNode) {
            this.selector.toggleNodeId(this.hoveredNode.id);
        }
    }

    nodeHover(node, e) {
        if (!this.hoveredNode) {
            this.saveState();
            this.hoveredNode = node;

            this.showTooltip(node, e);
            let neighbors = this.graph.getNeighbors(node.id);
            let nodesAdj = Array.from(neighbors.values()).map(n => n.id);
            let linkAdj = Array.from(neighbors.keys()).map(l => l.id());

            let nodeAndAdjacentNodes = nodesAdj.concat([node.id]);

            this.selectNodesOnly(nodeAndAdjacentNodes);
            this.selectLinksOnly(linkAdj);
            this.render();
        }
    }

    nodeHoverEnd() {
        this.hoveredNode = null;
        this.resetState();
        this.render();
    }

    showTooltip(node, e) {
        let html = this.generateHtmlTooltip(node, this.graph.properties);

        this.tooltip
            .style("visibility", "visible")
            .style("top", `${e.clientY + 10}px`)
            .style("left", `${e.clientX + 10}px`)
            .html(html);
    }

    generateHtmlTooltip(node, properties) {
        let html = "";

        if (node.name) {
            html += `<span class="name-tooltip"><strong>${node.name}</strong></span> <br>`
        } else if (node instanceof DocumentNode) {
            html += `<span class="name-tooltip"><strong>${node.id}</strong></span> <br>`
        }

        let attributes = filterObject(node.attributes, (entries) => {
            return Object.keys(properties).includes(entries[0])
        });

        let attributesList = Object.entries(attributes).sort((a, b) => {
            let diff = properties[a[0]].type - properties[b[0]].type;
            if (diff == 0) {
                return a[0].localeCompare(b[0]);
            } else {
                return diff;
            }
        })

        let type0, type1;
        for (let [name, value] of attributesList) {
            type1 = properties[name].type;
            if (type0 && type1 != type0) {
                html += `<br>`;
            }

            if (!globals.ATTRIBUTES_TOOLTIP_FILTERED.includes(name) && name != "name") {
                if (value == "") {
                    //
                } else if (!isNaN(value)) {
                    if (!isInt(value)) {
                        value = processFloat(value);
                    }
                }
                html += `<span class="table-property-key">${name}</span>: ${value} <br>`;
            }

            type0 = type1;
        }
        return html;
    }

    hideTooltip() {
        this.tooltip
            .style("visibility", "hidden")
            .html("")
    }

    deleteCoordinates() {
        this.graph.nodes.forEach((n, i) => {
            delete n.x;
            delete n.y;
        })
    }

    detectResize() {
        let observer = new MutationObserver((mutations) => {
            this.fitToContainer();
            this.render();
        });

        observer.observe(this.canvasWrapper, {attributes: true});
    }

    fitToContainer() {
        this.canvas.node().width = this.canvasWrapper.offsetWidth;
        this.canvas.node().height = this.canvasWrapper.offsetHeight;
    }

    horizontalFitContainer() {
        // Make it visually fill the positioned parent
        this.canvas.node().style.width = '100%';
        // ...then set the internal size to match
        this.canvas.node().width = this.canvas.node().offsetWidth;

        // this.width = this.canvas.node().width;
    }

    setResizableCanvas() {
        setInterval(() => {
            const newWidth = this.canvasWrapper.clientWidth;
            const newHeight = this.canvasWrapper.clientHeight;
            if (newWidth !== this.wrapperWidth || newHeight !== this.wrapperHeight) {
                this.wrapperWidth = newWidth;
                this.wrapperHeight = newHeight;

                this.canvas.attr("width", newWidth);
                this.canvas.attr("height", newHeight);
            }
        }, 100)
    }

    setupMenu() {
        // Abstract method
    }

    setupZoom() {
        this.zoom = d3.zoom()
            .on("zoom", this.zoomAction);

        this.canvas
            .call(this.zoom)
    }

    panOnSelectedNodes() {
        // let selectedNodes = this.selectedNodesIds.map(id => this.graph.idToNode[id]);
        let selectedNodes = Object.keys(this.selectNodeTable).map(id => this.graph.idToNode[id]);
        let [minX, maxX] = d3.extent(selectedNodes.map(n => n.x));
        let [minY, maxY] = d3.extent(selectedNodes.map(n => n.y));

        let dY = maxY - minY;
        let dX = maxX - minX;

        // let ratioX = this.width / dX;
        // let ratioY = this.height / dY;

        let ratioX = this.getWidth() / dX;
        let ratioY = this.getHeight() / dY;
        let k = d3.min([ratioX, ratioY]) * 0.8

        let xTransform = (maxX - minX) / 2 + minX;
        let yTransform = (maxY - minY) / 2 + minY;

        this.canvas.transition().duration(1000).call(
            this.zoom.transform,
            d3.zoomIdentity
                // .translate(this.width / 2, this.height / 2).scale(k)
                .translate(this.getWidth() / 2, this.getHeight() / 2).scale(k)
                .translate(-xTransform, -yTransform)
        )
    }

    setupLasso(lassoSelectItemsCb = (selected) => {
        selected.forEach(s => s.selected = true)
    }) {
        this.lasso = new LassoCanvas();

        let finalCb = (selected) => {
            lassoSelectItemsCb(selected);

            // Deselect lasso feature once used once
            this.lasso.deactivate();
            this.menu.setButtonActive("move")
        }

        this.lasso.init(this.canvas.node(),
            this.graph.nodes,
            finalCb,
            (items) => {
                items.forEach(item => {
                    item.selected = false;
                })
            },
            this.render,
            null,
            this.transform)

        this.lasso.deactivate();
    }

    highlightAllGraph() {
        this.graph.nodes.forEach((d) => {
            this.highlightNodeTable[d.id] = true;
        })
        this.highlightAllLinks();
    }

    highlightAllLinks() {
        this.graph.links.forEach((d) => {
            this.highlightLinkTable[generateLinkIdFromLinkObject(d, this.graph.edgeTypeKey)] = true;
        })
    }

    // TODO : probably better to send the map rendering logic in the Visualizer class
    render(runOcclusion = false, renderMap = true) {
        this.ctx.save();

        if (renderMap) {
            this.ctx.clearRect(0, 0, this.getWidth(), this.getHeight());
        }

        if (this.isMapLayout && renderMap) {
            this.mapVis.render();
        }

        this.translateAndScale();
        if (this.occlusion && runOcclusion) {
            let k = this.transform.k
            this.occlusion.run(k, this.graph.nodes.map(n => n.id));
            // this.occlusion.run(k, this.nodesToRender().map(n => n.id));
        }

        this.renderLinks();
        this.renderNodes();

        this.ctx.restore();

        if (this.isMapLayout && renderMap && this.mapVis.showCities) {
            this.ctx.save();
            this.mapVis.renderPopulatedPlaces();
            this.ctx.restore();
        }
    }

    translateAndScale() {
        this.ctx.translate(this.transform.x, this.transform.y);
        this.ctx.scale(this.transform.k, this.transform.k);
    }

    renderNodes(nodesIds) {
        this.ctx.lineWidth = 1;

        this.nodesToRender().forEach(d => {
            this.setNodesRenderingProperties(d);
            this.colorNode(d);
            this.renderNode(d);
        })
    }

    nodesToRender() {
        return this.graph.nodes.filter(node => isPointInRect(this.transform.applyX(node.x), this.transform.applyY(node.y), this.getWidth(), this.getHeight(), 0, 0))
    }

    setNodesRenderingProperties(d) {
        if (this.highlightNodeTable[d.id]) {
            this.ctx.globalAlpha = 1.0;
        } else {
            this.ctx.globalAlpha = globals.UNHIGHLIGHT_OPACITY;
        }

        if (this.selectNodeTable[d.id] || this.selector.has(d.id)) {
            this.ctx.strokeStyle = globals.SELECTION_COLOR;
            this.ctx.lineWidth = globals.NODE_LINE_WIDTH_SELECT;
        } else {
            this.ctx.strokeStyle = "black";
            this.ctx.lineWidth = 1;
        }

        this.applyNodeZoomLineWidthTransform();
    }

    applyNodeZoomLineWidthTransform() {
        if (this.transform.k < 1) {
            this.ctx.lineWidth = this.ctx.lineWidth / (this.transform.k);
        } else {
            this.ctx.lineWidth = this.ctx.lineWidth;
        }
    }

    colorNode(d) {
        // Currently two different ways of coloring nodes.
        if (this.nodeColorScale) {
            if (d.get(this.nodePropertyToPlot)) {
                this.ctx.fillStyle = this.nodeColorScale(d.get(this.nodePropertyToPlot));
                this.ctx.lineWidth = this.ctx.lineWidth / 3;
            } else {
                this.ctx.fillStyle = "white";
            }
            // Set the linewidth to 1 to better see attribute colors
            // this.ctx.lineWidth = 1;
        } else if (this.colorNodeTable[d.id]) {
            this.ctx.fillStyle = this.colorNodeTable[d.id];
            this.ctx.lineWidth = this.ctx.lineWidth / 3;
        } else {
            this.ctx.fillStyle = "white";
        }
    }

    renderNode(d) {
        this.ctx.beginPath();

        let [x, y] = [d.x, d.y];

        // let radius;
        // if (this.transform.k < 1) {
        //     radius = globals.RADIUS / (this.transform.k * 5);
        // } else {
        //     radius = globals.RADIUS;
        // }

        // let radius = globals.RADIUS * 2 / (this.transform.k);


        let radius = globals.RADIUS;

        if (this.entityTypeLegend.legend[d.type] == ShapeEnum.CIRCLE) {
            this.ctx.arc(x, y, radius, 0, 2 * Math.PI, true);
        } else if (this.entityTypeLegend.legend[d.type] == ShapeEnum.SQUARE) {
            this.ctx.rect(x - radius, y - radius, radius * 2, radius * 2);
        } else { // TODO Currentlu for subgraphs
            // this.ctx.rect(d.x - globals.RADIUS, d.y - globals.RADIUS, globals.RADIUS * 2, globals.RADIUS * 2);
            this.matrixVis = new MatrixVis(this.ctx, radius * 2);
            this.matrixVis.render([[1, 0, 1], [0, 1, 1], [1, 1, 0]], d.x, d.y);
        }

        this.ctx.fill();
        this.ctx.stroke();

        if (this.renderLabels) {
            this.renderNodeLabel(d, x, y);
        }
    }

    renderNodeLabel(d, x, y) {
        if (!this.occlusion.isNodeOccluded(d)) {
            this.ctx.fillStyle = "black";

            x = x + globals.RADIUS;
            y = y - globals.RADIUS;

            let label = getNodeLabel(d);
            if (label) {
                let zoomLevel = d3.zoomTransform(this.canvas.node()).k;
                let size = globals.FONT_SIZE * 1 / zoomLevel;
                this.ctx.font = `${size}px Arial`;
                this.ctx.fillText(label, x, y);
            }
        }
    }

    renderLinks() {
        this.graph.links.forEach((d) => {
            if (isPointInRect(this.transform.applyX(d.source.x), this.transform.applyY(d.source.y), this.getWidth(), this.getHeight(), 0, 0) ||
            isPointInRect(this.transform.applyX(d.target.x), this.transform.applyY(d.target.y), this.getWidth(), this.getHeight(), 0, 0)) {
                this.colorLink(d);
                this.setupLinkHighlight(d);
                this.setupLinkSelect(d);
                this.renderLink(d);
            }
        });
    }

    getLineWidth() {
        return globals.LINE_WIDTH;
    }

    colorLink(d) {
        this.ctx.strokeStyle = this.linkScale ? this.linkScale(d.type) : "black";
    }

    setupLinkHighlight(link) {
        let linkId = generateLinkIdFromLinkObject(link);

        if (this.highlightLinkTable[linkId]) {
            this.ctx.globalAlpha = 1.0;
        } else {
            this.ctx.globalAlpha = this.unighlightOpacity();
            // this.ctx.globalAlpha = 1.0
        }
    }

    setupLinkSelect(link) {
        if (this.selectLinkTable[generateLinkIdFromLinkObject(link)]) {
            this.ctx.lineWidth = globals.LINE_WIDTH_SELECT / (this.manualZoomTransformScale() * 2);;
        } else {
            // this.ctx.lineWidth = this.getLineWidth() / (this.manualZoomTransformScale() * 3);
            this.ctx.lineWidth = this.getLineWidth() / (this.manualZoomTransformScale() * 3.5);
        }
    }

    manualZoomTransformScale() {
        return this.transform.k;
    }

    renderLink(link) {
        this.ctx.beginPath();

        let [x1, y1, x2, y2] = [link.source.x, link.source.y, link.target.x, link.target.y];

        let dx = x2 - x1;
        let dy = y2 - y1;
        let angle = Math.atan2(dy, dx);
        // let length=Math.sqrt(dx*dx+dy*dy);

        // to remove the bits overlapping the nodes
        x1 = globals.RADIUS * Math.cos(angle) + x1;
        x2 = x2 - globals.RADIUS * Math.cos(angle);
        y1 = globals.RADIUS * Math.sin(angle) + y1;
        y2 = y2 - globals.RADIUS * Math.sin(angle);

        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);

        // this.renderLinkArrow(x2, y2, angle)

        this.ctx.stroke();
    }

    renderLinkArrow(x2, y2, angle) {
        this.ctx.lineTo(x2 - globals.ARROW_LENGTH * Math.cos(angle - Math.PI / 6), y2 - globals.ARROW_LENGTH * Math.sin(angle - Math.PI / 6));
        this.ctx.moveTo(x2, y2);
        this.ctx.lineTo(x2 - globals.ARROW_LENGTH * Math.cos(angle + Math.PI / 6), y2 - globals.ARROW_LENGTH * Math.sin(angle + Math.PI / 6));
    }

    saveState() {
        this.previousHighlightNodesTabls = _.cloneDeep(this.highlightNodeTable);
        this.previousHighlightLinkTable = _.cloneDeep(this.highlightLinkTable);

        this.previousColorNodeTable = _.cloneDeep(this.colorNodeTable);
        this.previousSelectLinkTable = _.cloneDeep(this.selectLinkTable);
        this.previousSelectNodeTable = _.cloneDeep(this.selectNodeTable);
    }

    resetState() {
        this.highlightNodeTable = _.cloneDeep(this.previousHighlightNodesTabls);
        this.highlightLinkTable = _.cloneDeep(this.previousHighlightLinkTable);

        this.colorNodeTable = _.cloneDeep(this.previousColorNodeTable);
        this.selectLinkTable = _.cloneDeep(this.previousSelectLinkTable);
        this.selectNodeTable = _.cloneDeep(this.previousSelectNodeTable);
    }

    findNode(x, y) {
        const rSq = globals.RADIUS * globals.RADIUS;
        let i;
        for (i = this.graph.nodes.length - 1; i >= 0; --i) {
            const node = this.graph.nodes[i],
                dx = x - node.x,
                dy = y - node.y,
                distSq = (dx * dx) + (dy * dy);
            if (distSq < rSq) {
                return node;
            }
        }
        // No node selected
        return undefined;
    }

    dragSubject = (e) => {
        let x = this.transform.invertX(e.x);
        let y = this.transform.invertY(e.y);

        let node = this.findNode(x, y);
        if (node) {
            node.x = this.transform.applyX(node.x);
            node.y = this.transform.applyY(node.y);
        }

        return node
    }

    dragStart = (e) => {
        e.subject.x = this.transform.invertX(e.x);
        e.subject.y = this.transform.invertY(e.y);
    }

    dragMove = (e) => {
        e.subject.x = this.transform.invertX(e.x);
        e.subject.y = this.transform.invertY(e.y);
        // this.graphVisCollection.render();
        this.render();
    }

    dragEnd = (e) => {
        e.subject.x = this.transform.invertX(e.x);
        e.subject.y = this.transform.invertY(e.y);
        // this.graphVisCollection.render(true);
        this.render()
    }

    zoomAction = (e) => {
        let isZoom = (e.transform.k != this.transform.k) ? true : false;

        this.transform = e.transform;

        if (this.lasso) {
            this.lasso.transform = e.transform;
        }
        if (this.mapVis) {
            this.mapVis.transform = this.transform;
            this.mapVis.zoomAction(e);
        }

        this.render(isZoom);
        // this.render(true);
    }
}

// Split Class into several files
GraphVisCanvas.prototype.highlightNodes = Highlighter.highlightNodes
GraphVisCanvas.prototype.highlightLinks = Highlighter.highlightLinks
GraphVisCanvas.prototype.selectNodes = Highlighter.selectNodes
GraphVisCanvas.prototype.selectNodesOnly = Highlighter.selectNodesOnly
GraphVisCanvas.prototype.selectLinksOnly = Highlighter.selectLinksOnly
GraphVisCanvas.prototype.highlightNodesOnly = Highlighter.highlightNodesOnly
GraphVisCanvas.prototype.highlightLinksOnly = Highlighter.highlightLinksOnly
GraphVisCanvas.prototype.colorNodes = Highlighter.colorNodes
GraphVisCanvas.prototype.colorNodesOnly = Highlighter.colorNodesOnly

