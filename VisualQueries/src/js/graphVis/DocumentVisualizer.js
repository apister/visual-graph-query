import Visualizer from "./Visualizer";
import DocumentSimulation from "./Simulations/DocumentSimulation";
import HyperGraphVisCanvas from "./HyperGraphVisCanvas";
import {globals} from "../globals";
import DocumentGraphVis from "./DocumentGraphVis";
import MapVis from "./mapVis";
import TopMenuBar from "../components/TopMenuBar";

export default class DocumentVisualizer extends Visualizer {
    constructor(graphData, documentGraphData, repetitionGraphData, canvas, linkScale, selector) {
        super(graphData, canvas, selector);
        this.documentGraphData = documentGraphData;
        this.repetitionGraphData = repetitionGraphData;
        this.linkScale = linkScale;

        this.keepOnlyNodesWithGeoloc();
        this.setWidthHeight();
    }

    setWidthHeight() {
        this.documentGraphData.nodes.forEach(n => {
            let width = HyperGraphVisCanvas.rectWidth();
            n.set("width", width);
            n.set("height", n.get("_degree") ? n.get("_degree") * HyperGraphVisCanvas.rectHeight() : HyperGraphVisCanvas.rectHeight());
        })
    }

    keepOnlyNodesWithGeoloc() {
        this.documentGraphData.nodes = this.documentGraphData.nodes.filter(n => n.get("latitude") && n.get("longitude"));
        let nodesIdsWithGeoloc = this.documentGraphData.nodes.map(n => n.id);
        this.documentGraphData.links = this.documentGraphData.links.filter(l => nodesIdsWithGeoloc.includes(l.source.id) && nodesIdsWithGeoloc.includes(l.target.id));
    }

    // Currently, a first zoom is applied before node simulation and rendering.
    async init() {
        await this.setupMapRenderer();

        // let repetitionSimulation = new RepetitionNodeSimulation(this.repetitionGraphData, this.documentGraphData, this.graphData, this.canvas);
        // repetitionSimulation.init();
        // // repetitionSimulation.run(500);
        // await repetitionSimulation.getPosOrRun(700);
        // repetitionSimulation.stop();

        // repetition node view
        // this.graphVis = new HyperGraphVisCanvas(this.repetitionGraphData, repetitionSimulation, this.documentGraphData, this.canvas, true, true, this.linkScale, this.mapVis);

        // full document view
        this.graphVis = new DocumentGraphVis(this.documentGraphData, this.graphData, this.canvas, true, true, this.linkScale, this.mapVis, this.selector);
        this.mapVis.setZoom(this.graphVis.zoom);
        this.mapVis.zoomInitial();

        let simulation = new DocumentSimulation(this.documentGraphData, this.canvas, this.mapVis);
        simulation.initMapSimJitter();
        await simulation.getPosOrRun(600);
        // await simulation.getPosOrRun(800);
        simulation.stop();

        this.mapVis.zoomInitial2();
        this.setupMenu();
    }

    setupMenu() {
        this.topMenuBar = new TopMenuBar(this.canvasDivContainer, globals.MENU_COLOR, "2px");
        this.topMenuBar.init();
        this.topMenuBar.addTitle("Document Visualization (Each link represents a shared person)");

        this.topMenuBar.addChoiceOption("Show the documents sharing at least", "persons", ["x", 1, 2, 3], this.thresholdPersonsChangeCb);
        this.topMenuBar.addCheckBoxOption("Show Cities", this.showCitiesCb, 0.5);
    }

    thresholdPersonsChangeCb = (value) => {
        if (isNaN(value)) {
            this.graphVis.setNSharedPersonsThreshold(1000);
        } else {
            this.graphVis.setNSharedPersonsThreshold(value);
        }
        this.graphVis.render();
    }

    showCitiesCb = () => {
        this.mapVis.showCities = !this.mapVis.showCities;
        this.render();
    }

    async setupMapRenderer() {
        this.mapVis = new MapVis(this.canvas);
        await this.mapVis.init();
    }

    run() {
        this.graphVis.runMapLayout();
        // this.graphVis.render();

        // this.askMst({
        //     "nodes": this.repetitionGraphData.nodes,
        //     "links": this.repetitionGraphData.links
        // }).then(mst => {
        //     this.graphVis.setMST(mst)
        // })
        this.setAutoRenderer();
    }

    setAutoRenderer() {
        window.setInterval(() => {
            if (!this.mapVis.success) {
                this.render();
            }
        }, 2);
    }

    async askMst(data) {
        let url = globals.route("MST")

        return fetch(url.toString(), {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(r => {
            return r.json()
        }).then(json => {
            return json
        })
    }

    highlightLinksOnly(nodesIds) {
        let linksIds = this.documentGraphData.links.filter(l => {
            return nodesIds.includes(l.source.id) && nodesIds.includes(l.target.id);
        }).map(l => l.id())
        this.graphVis.highlightLinksOnly(linksIds);
    }
}