import GraphVisCanvas from "./GraphVisCanvas.js";
import GraphMenu from "./GraphMenu.js";


class StaticGraphVisCanvas extends GraphVisCanvas {
    constructor(graph, canvas, isDragging, isZooming, linkScale, mapVis, selector) {
        super(graph, canvas, isDragging, isZooming, linkScale, mapVis, selector);

        // this.zoom.scaleBy(this.canvas, 0.2);
    }

    setNodeHoverCbs(nodeHoverCb, nodeHoverEndCb) {
        this.nodeHoverCb = nodeHoverCb;
        this.nodeHoverEndCb = nodeHoverEndCb;
    }

    setupMenu() {
        this.menu = new GraphMenu(this.canvas);
        this.menu.addButton(
            "move",
            (e) => {
                this.lasso.deactivate();
            },
            "hand pointer outline",
            true,
            "Pan and drag"
        )

        this.menu.addButton(
            "lasso",
            (e) => {
                this.lasso.activate();
            },
            "lasso",
            false,
            "Lasso selection: select a pattern in the graph to transform it into a query."
        )
    }

    mouseMoveCb = (e) => {
        let node = this.findNode(this.transform.invertX(e.offsetX), this.transform.invertY(e.offsetY));
        // if (this.nodeHoverCb && node) this.nodeHoverCb([node.id]);
        super.mouseMoveCb(e);
    }

    nodeHover = (node, e) => {
        super.nodeHover(node, e);

        if (this.nodeHoverCb) {
            if (node.type == this.graph.documentType) {
                this.nodeHoverCb([node.id]);
            } else {
                // get documents neighbors
                let neighbors = this.graph.getNeighbors(node.id, true);
                let neighborsIds = neighbors.map(n => n.id);
                this.nodeHoverCb(neighborsIds);
            }
        }
    }

    nodeHoverEnd() {
        // if (this.nodeHoverEndCb) {
        //     if (globals.HYPEREDGE_NODE_TYPES.includes(this.hoveredNode.type)) {
        //         this.nodeHoverEndCb([this.hoveredNode.id]);
        //     } else {
        //         // get documents neighbors
        //         let neighbors = Object.values(this.graph.getNeighbors(this.hoveredNode.id));
        //         this.nodeHoverEndCb(neighbors);
        //     }
        //     // this.nodeHoverEndCb(this.hoveredNode.id);
        // }
        super.nodeHoverEnd();
        this.nodeHoverEndCb();
    }

    render(runOcclusion = false, renderMap = true) {
        super.render(runOcclusion, renderMap);
    }
}

export {StaticGraphVisCanvas};