import * as d3 from "d3";
import * as d3Tile from 'd3-tile';
import * as proj from "d3-geo-projection"

import {globals} from "../globals";

// 10m is best quality
// import * as lakes from '../../../assets/natural-earth-vector-master/geojson/ne_10m_lakes.json';
import * as lakes from '../../../assets/natural-earth-vector-master/geojson/ne_10m_lakes.json';
// import * as rivers from '../../../assets/natural-earth-vector-master/geojson/ne_10m_rivers_europe.json';
import * as rivers from '../../../assets/natural-earth-vector-master/geojson/ne_10m_rivers_europe.json';
// import * as populatedPlaces from '../../../assets/natural-earth-vector-master/geojson/ne_10m_populated_places.json';
import * as populatedPlaces from '../../../assets/natural-earth-vector-master/geojson/ne_50m_populated_places.json';

export default class MapVis {
    static mapFillColor = "rgba(252,190,103,0.35)";
    static mapNodeStrokeColor = "rgba(201,201,201,0.46)";
    static waterColor = "rgb(36,103,227)";
    static mapBoxAccessToken = "pk.eyJ1IjoiYXBpc3RlciIsImEiOiJjbDByeXdyeWEwODBuM2RxbzhyOHByNGpzIn0.v7dMliKqGcvlBEJ3X1UitQ";

    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.node().getContext('2d');

        this.mapFillColor = MapVis.mapFillColor;
        this.centerCoordinates = null;

        // this.scaleFactor = 80000;
        this.scaleFactor = 650000;
        // this.computeContinentsPolygon();

        this.transform = d3.zoomIdentity;
        this.showCities = true;
        // this.backgroundOpacity = 0.40;
        this.backgroundOpacity = 0.75;

        this.count = 0;
    }

    getWidth() {
        return this.canvas.attr("width");
    }

    getHeight() {
        return this.canvas.attr("height");
    }

    setZoom(zoom) {
        this.zoom = zoom;
    }

    zoomInitial() {
        let [x, y] = this.projection(this.centerCoordinates);

        this.xTranslate = this.getWidth() / 2 - x;
        this.yTranslate = this.getHeight() / 2 - y;

        // this.zoom.translateTo(this.canvas, x, y);
        this.zoom.translateBy(this.canvas, this.xTranslate, this.yTranslate);
        this.zoom.scaleBy(this.canvas, this.scaleFactor);

        this.xTranslate2 = this.transform.x;
        this.yTranslate2 = this.transform.y;
    }

    zoomInitial2() {
        this.zoom.scaleBy(this.canvas, 1 / 5);
    }

    zoomAction = () => {
        this.projection
            .scale(this.transform.k / (2 * Math.PI))
            .translate([this.transform.x, this.transform.y]);

        this.path = d3.geoPath(this.projection, this.ctx);
    }

    // return only the manual zoom transform ()
    scaleManualZoom() {
        return this.transform.k / this.scaleFactor;
    }

    geoLocToCanvasCoordinates(lat, long, useCurrentZoom = false) {
        let x, y;

        if (useCurrentZoom) {
            [x, y] = this.projection([long, lat]);
            let transform = d3.zoomTransform(this.canvas.node());
            [x, y] = transform.apply([x, y]);
        } else {
            [x, y] = this.projection([long, lat]);
        }

        return [x, y];
    }

    latLongCanvasPolygon() {
        return this.latLongPolygonFromCanvasBox(0, 0, this.getWidth(), this.getHeight());
    }

    latLongPolygonFromCanvasBox(x1, y1, x2, y2) {
        let [long1, lat1] = this.projection.invert([x1, y1]);
        let [long2, lat2] = this.projection.invert([x2, y1]);
        let [long3, lat3] = this.projection.invert([x2, y2]);
        let [long4, lat4] = this.projection.invert([x1, y2]);

        let polygon = [[long1, lat1], [long2, lat2], [long3, lat3], [long4, lat4]];

        return polygon;
    }

    async init() {
        this.initProjection();
        await this.loadGeoData();
        this.initTiles();
    }

    initProjection() {
        if (globals.dataset == "Nicole") {
            this.centerCoordinates = [2, 46]
        } else {
            this.centerCoordinates = [7.6868565, 45.070312]; // Piemont
        }

        // transform lat.long to canvas space
        // this.projection = d3.geoNaturalEarth1()
        // this.projection = d3.geoMercator()
        //     .center(this.centerCoordinates) // TODO : find This automatically
        //     // .scale(this.scaleFactor)
        //     .translate([this.getWidth() / 2, this.getHeight() / 2])
        //     .precision(.1)

        // working version (Initialize the projection to fit the world in a 1×1 square centered at the origin.)
        this.projection = d3.geoMercator()
            .scale([1 / (2 * Math.PI)])
            .translate([0, 0])

        // this.projection = d3.geoMercator()
        //     .center(this.centerCoordinates)
        //     .scale(Math.pow(2, 21) / (2 * Math.PI))
        //     .translate([this.getWidth() / 2, this.getHeight() / 2])

        // for raster
        // this.projection = d3.geoMercator()
        //     .translate([0, 0])
        //     .scale(1)
        //     .precision(.1)
    }

    async loadGeoData() {
        // this.world = world;
        // // this.filterCountries();
        //
        // this.land = topojson.feature(world, world.objects.land);
        // this.landFiltered = this.land
        //
        // this.countries = topojson.mesh(world, world.objects.countries);
        // // this.countries = topojson.mesh(world, world.objects.countries, (a, b) => a !== b);

        // this.land = land;
        this.lakes = lakes;
        // this.filterTopojson(this.land);
        // this.filterLakesTopojson(this.lakes);
        this.path = d3.geoPath(this.projection, this.ctx);

        this.populatedPlaces = populatedPlaces;

        // console.log("pp ", populatedPlaces);
        // console.log(new Set([...populatedPlaces.features.map(f => f.properties.FEATURECLA)]))

        this.populatedPlaceCategoriesFilter = ["Admin-1 capital", "Admin-1 region capital", "Admin-0 region capital", "Historic place", "Admin-0 capital"];
        this.populatedPlaces.features = this.populatedPlaces.features.filter(place => {
            return this.populatedPlaceCategoriesFilter.includes(place.properties.FEATURECLA);
        })

        // this.loadWorld();
    }

    loadWorld() {
        this.rasterImage = new Image();
        this.rasterImage.src = raster;

        this.bb = this.path.bounds(this.land);

        // scale
        let zoom = 600;
        let width = 1000;
        let height = 1000;

        // let s = 0.99 / Math.max((this.bb[1][0] - this.bb[0][0]) / this.getWidth(), (this.bb[1][1] - this.bb[0][1]) / this.getHeight());
        let s = 0.99 / Math.max((this.bb[1][0] - this.bb[0][0]) / width, (this.bb[1][1] - this.bb[0][1]) / height);
        s = s * zoom;
        this.s = s;

        // transform
        // const t = [(this.getWidth() - s * (this.bb[1][0] + this.bb[0][0])) / 2, (this.getHeight() - s * (this.bb[1][1] + this.bb[0][1])) / 2];
        const t = [(width - s * (this.bb[1][0] + this.bb[0][0])) / 2, (height - s * (this.bb[1][1] + this.bb[0][1])) / 2];
        this.t = t;

        // update projection
        // this.projection
        //     .scale(s)
        //     .translate(t)

        this.raster_width = (this.bb[1][0] - this.bb[0][0]) * s;
        this.raster_height = (this.bb[1][1] - this.bb[0][1]) * s;

        this.rtranslate_x = (this.getWidth() - this.raster_width) / 2;
        this.rtranslate_y = (this.getHeight() - this.raster_height) / 2;
    }

    initTiles() {
        this.tileSize = 256;
        // this.tileSize = 512;

        // this.tile = d3Tile.tile()
        //     .extent([[0, 0], [this.getWidth(), this.getHeight()]])
        //     // .translate(this.projection([0, 0]))
        //     // .translate([this.getWidth() / 2, this.getHeight() / 2])
        //     .translate(t => {
        //         return [t.x * this.getWidth() / 2, t.y * this.getHeight() / 2]
        //     })
        //     .scale(t => {
        //         return t.k  * Math.pow(2, 21) * 2 * Math.PI
        //     })
        //     .clampX(false)

        // Work
        this.tile = d3Tile.tile()
            .tileSize(this.tileSize)
            .extent([[0, 0], [this.getWidth(), this.getHeight()]])
        // .clampX(false)

        this.tileToLakes = new Map();
        this.tileToRivers = new Map();
    }

    tileUrl(x, y, z) {
        // MapBox elevation
        // let url = `https://api.mapbox.com/v4/mapbox.terrain-rgb/${z}/${x}/${y}.pngraw?access_token=${MapVis.mapBoxAccessToken}`;

        // Stamen
        // let url = `https://stamen-tiles-${"abc"[Math.abs(x + y) % 3]}.a.ssl.fastly.net/terrain/${z}/${x}/${y}${devicePixelRatio > 1 ? "@2x" : ""}.png`

        // Own Tile server
        // let url = globals.route(`tilesNE/${z}/${x}/${y}`);
        // let url = globals.route(`tilesSR/${z}/${x}/${y}`);

        let url = `https://api.maptiler.com/tiles/hillshade/${z}/${x}/${y}.webp?key=uxzPNi2ZvKHUGa3jlTl5`
        return url;
    }

    filterTopojson(topojson) {
        let features = topojson.features

        features.forEach((feature, i) => {
            feature.geometry.coordinates.forEach((coordinates, i) => {
                feature.geometry.coordinates[i] = coordinates.filter(coordinate => coordinate.some(point => this.isInEurope(point)))
            })
        })
    }

    filterLakesTopojson(topojson) {
        topojson.features = topojson.features.filter(lake => lake.geometry.coordinates.every(coordinates => coordinates.every(coordinate => {
            return this.isInEurope(coordinate) || this.isInAsia(coordinate) || this.isInAfrica(coordinate);
        })))
    }

    filterCountries() {
        this.world.objects.countries.geometries = this.world.objects.countries.geometries.filter(c => this.countriesToKeep.includes(c.properties.name))
    }

    render() {
        this.ctx.save();

        this.ctx.globalAlpha = this.backgroundOpacity;
        this.renderTiles();
        this.renderRivers();
        this.renderLakes();

        // if (this.showCities) {
        //     this.renderPopulatedPlaces();
        // }
        this.ctx.restore();
    }

    renderRaster() {
        this.ctx.save();
        this.ctx.globalAlpha = 0.2;
        this.ctx.translate(this.rtranslate_x, this.rtranslate_y);
        // // this.ctx.scale(this.scaleFactor, this.scaleFactor);
        this.ctx.drawImage(this.rasterImage, 0, 0, this.raster_width, this.raster_height);
        this.ctx.restore();
    }

    tileToString(tile) {
        let [x, y, z] = tile;
        return `${x}-${y}-${z}`
    }

    computeTileToFeature(tile, k, t) {
        if (!this.tileToLakes.has(this.tileToString(tile))) {
            let [dx, dy, dw, dh] = this.tileToDimensions(tile, k, t);
            let tileLatLongPolygon = this.latLongPolygonFromCanvasBox(dx, dy, dx + dw, dy + dh);

            let lakesFeatures = lakes.features.filter(lake => lake.geometry.coordinates.every(coordinates => coordinates.some(coordinate => {
                return this.isInside(coordinate, tileLatLongPolygon);
            })))

            let lakesFiltered = {
                type: "FeatureCollection",
                crs: this.lakes.crs,
                features: lakesFeatures,
                bbox: this.lakes.bbox
            }

            this.tileToLakes.set(this.tileToString(tile), lakesFiltered);
        }

        if (!this.tileToRivers.has(this.tileToString(tile))) {
            let [dx, dy, dw, dh] = this.tileToDimensions(tile, k, t);
            let tileLatLongPolygon = this.latLongPolygonFromCanvasBox(dx, dy, dx + dw, dy + dh);

            let riversFeatures = rivers.features.filter(river => river.geometry.coordinates.every(coordinates => coordinates.some(coordinate => {
                return this.isInside(coordinate, tileLatLongPolygon);
            })))

            let riversFiltered = {
                type: "FeatureCollection",
                crs: rivers.crs,
                features: riversFeatures,
                bbox: rivers.bbox
            }

            this.tileToRivers.set(this.tileToString(tile), riversFiltered);
        }
    }

    // Using NE tiles, the max zoom is 6. With higher Z values, we zoom on tiles of z=6 (using some hacking with d3-tile)
    renderTiles() {
        this.tiles = this.tile(this.transform);
        let tilesRendered = [];
        this.count++;
        let count = this.count;

        this.success = true;
        for (const tile of this.tiles) {
            let [x, y, z, k, t] = this.processTile(tile);
            if (tilesRendered.includes(this.tileToString([x, y, z]))) {
                continue;
            }

            tilesRendered.push(this.tileToString([x, y, z]));
            this.computeTileToFeature([x, y, z], k, t);

            let image = new Image;
            // image.onerror = reject;
            // image.onload = () => resolve(image);
            // image.src = this.tileUrl(...d3Tile.tileWrap(tile));
            image.src = this.tileUrl(x, y, z);
            // image.crossOrigin = "Anonymous";

            let [dx, dy, dw, dh] = this.tileToDimensions([x, y, z], k, t);

            // To recolor
            // let imageCol = new Image;
            // imageCol.src = this.color(image, dw, dh);

            this.ctx.drawImage(image, dx, dy, dw, dh);
            if (!image.complete) {
                this.success = false;
            }
            // image.onload = () => {
            //     if (!image.complete) {
            //         this.ctx.drawImage(image, dx, dy, dw, dh);
            //     }
            // }
        }
    }

    // Used for NE tiles, where the Z does not goes far.
    processTile(tile) {
        let [newX, newY, newZ, newK, newT] = [tile[0], tile[1], tile[2], this.tiles.scale, this.tiles.translate];
        if (false && newZ > 6) {
            let scaleFactor = newZ - 6;
            newX = Math.floor(newX / (2 ** scaleFactor));
            newY = Math.floor(newY / (2 ** scaleFactor));
            newZ = 6;
            newK = this.tiles.scale * (2 ** scaleFactor);
            newT = [this.tiles.translate[0] / (2 ** scaleFactor), this.tiles.translate[1] / (2 ** scaleFactor)];
        }
        return [newX, newY, newZ, newK, newT];
    }

    tileToDimensions(tile, k, t) {
        let [x, y, z] = tile;

        // Edit translation: make sure the first tile starts at an integer pixel coordinate
        const tx = Math.round(t[0] * k) / k;
        const ty = Math.round(t[1] * k) / k;

        // let dx = (x + this.tiles.translate[0]) * this.tiles.scale
        // let dy = (y + this.tiles.translate[1]) * this.tiles.scale
        let dx = (x + tx) * k;
        let dy = (y + ty) * k;
        let dw = k;
        let dh = k;

        return [dx, dy, dw, dh]
    }

    // processTile(x, y, z, k, t) {
    //     let [newX, newY, newZ, newK, newT] = [x, y, z, k, t];
    //     if (z > 6) {
    //         let scaleFactor = z - 6;
    //         newX = Math.floor(x / (2 ** scaleFactor));
    //         newY = Math.floor(y / (2 ** scaleFactor));
    //         newZ = 6;
    //         newK = k * (2 ** scaleFactor);
    //         newT = [t[0] / (2 ** scaleFactor), t[1] / (2 ** scaleFactor)];
    //     }
    //     return [newX, newY, newZ, newK, newT];
    // }

    color(image, w, h) {
        // let canvasEdited =  document.createElement('canvas');
        let canvasEdited = document.getElementById('canvas-for-image');
        canvasEdited.width = w;
        canvasEdited.height = h;
        let ctxEdited = canvasEdited.getContext('2d');
        ctxEdited.drawImage(image, 0, 0, canvasEdited.width, canvasEdited.height);

        let imageNew = ctxEdited.getImageData(0, 0, canvasEdited.width, canvasEdited.height)
        let imageData = imageNew.data;

        for (let i = 0; i < imageData.length; i += 4) {
            // console.log(imageData[i]);
            // var brightness = 0.34 * imageData[i] + 0.5 * imageData[i + 1] + 0.16 * imageData[i + 2];
            // imageData[i] = brightness;
            // imageData[i + 1] = brightness;
            // imageData[i + 2] = brightness;

            let red = imageData[i]; // red
            let green = imageData[i + 1]; // green
            let blue = imageData[i + 2]; // blue
            // i+3 is alpha (the fourth element)

            // let elevation = -10000 + ((red * 256 * 256 + green * 256 + {blue}) * 0.1)

            imageData[i] = green;
            imageData[i + 1] = green;
            imageData[i + 2] = green;
        }

        ctxEdited.putImageData(imageNew, 0, 0);
        let base64URI = canvasEdited.toDataURL();
        return base64URI;
    }

    renderLand() {
        this.ctx.fillStyle = this.mapFillColor;
        this.ctx.globalAlpha = 0.5;
        this.ctx.beginPath();
        this.path(this.land);
        this.ctx.fill();
    }

    renderLandContour() {
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 1 / this.transform.k;
        this.ctx.beginPath();
        this.path(this.land);
        this.ctx.stroke();
    }

    renderLakes() {
        this.ctx.fillStyle = MapVis.waterColor;

        for (const tile of this.tiles) {
            let tileCorrected = this.processTile(tile);
            let lakes = this.tileToLakes.get(this.tileToString(tileCorrected));

            this.ctx.beginPath();
            this.path(lakes);
            this.ctx.fill();
        }

        // this.ctx.beginPath();
        // this.path(this.lakes);
        // this.ctx.fill();
        // this.ctx.stroke();
    }

    renderPopulatedPlaces() {
        // Populated places
        // this.ctx.fillStyle = "gray";
        // this.ctx.strokeStyle = "gray";

        let color = "rgb(141,76,41)";
        this.ctx.fillStyle = color;
        this.ctx.strokeStyle = color;

        this.ctx.lineWidth = 1;
        // this.ctx.lineWidth = 1 / this.transform.k;
        // this.ctx.lineWidth = 20;

        // 10 is the initial text size
        let textFont = `${(80 * this.scaleManualZoom()) >> 0}px sans-serif`;
        this.ctx.font = textFont;

        this.populatedPlaces.features.forEach(place => {
            const [lat, long] = [place.properties.LATITUDE, place.properties.LONGITUDE];
            const name = place.properties.NAME;
            let [x, y] = this.geoLocToCanvasCoordinates(lat, long);

            this.ctx.beginPath();
            this.ctx.arc(x, y, 2, 0, 2 * Math.PI, true);
            this.ctx.fill();

            this.ctx.fillText(name, x + 2, y + 2);
        })
    }

    renderRivers() {
        this.ctx.strokeStyle = MapVis.waterColor;
        // this.ctx.lineWidth = 7;
         for (const tile of this.tiles) {
            let tileCorrected = this.processTile(tile);
            let rivers = this.tileToRivers.get(this.tileToString(tileCorrected));

            this.ctx.beginPath();
            this.path(rivers);
            this.ctx.stroke();
        }

        // this.ctx.strokeStyle = MapVis.waterColor;
        // this.ctx.lineWidth = 7;
        // this.ctx.beginPath();
        // this.path(rivers);
        // this.ctx.stroke();
    }

    renderPointsAsLine(points) {
        let x0, y0;
        points.forEach((point, i) => {
            let [x, y] = this.projection([point[0], point[1]]);

            if (i == 0) {
                [x0, y0] = [x, y];
                this.ctx.moveTo(x, y);
            } else {
                this.ctx.lineTo(x, y);
            }
        })
        // this.ctx.lineTo(x0, y0);
        // this.ctx.closePath();
    }

    renderPoint(lat, long, color = "black") {
        this.ctx.fillStyle = color;

        let [y, x] = this.projection([lat, long]);

        this.ctx.rect(x, y, globals.RADIUS * 100, globals.RADIUS * 100);
        this.ctx.fill();
    }

    computeContinentsPolygon() {
        let latNAm = [90, 90, 78.13, 57.5, 15, 15, 1.25, 1.25, 51, 60, 60];
        let lonNAm = [-168.75, -10, -10, -37.5, -30, -75, -82.5, -105, -180, -180, -168.75];
        let latNA2 = [51, 51, 60];
        let lonNA2 = [166.6, 180, 180];
        let latSAm = [1.25, 1.25, 15, 15, -60, -60];
        let lonSAm = [-105 - 82.5 - 75 - 30 - 30 - 105];
        let latEur = [90, 90, 42.5, 42.5, 40.79, 41, 40.55, 40.40, 40.05, 39.17, 35.46, 33, 38, 35.42, 28.25, 15, 57.5, 78.13];
        let lonEur = [-10, 77.5, 48.8, 30, 28.81, 29, 27.31, 26.75, 26.36, 25.19, 27.91, 27.5, 10, -10, -13, -30, -37.5, -10];

        let latAfr = [15, 28.25, 35.42, 38, 33, 31.74, 29.54, 27.78, 11.3, 12.5, -60, -60];
        let lonAfr = [-30, -13, -10, 10, 27.5, 34.58, 34.92, 34.46, 44.3, 52, 75, -30];
        let latAus = [-11.88, -10.27, -10, -30, -52.5, -31.88];
        let lonAus = [110, 140, 145, 161.25, 142.5, 110];
        let latAsi = [90, 42.5, 42.5, 40.79, 41, 40.55, 40.4, 40.05, 39.17, 35.46, 33, 31.74, 29.54, 27.78, 11.3, 12.5, -60, -60, -31.88, -11.88, -10.27, 33.13, 51, 60, 90];
        let lonAsi = [77.5, 48.8, 30, 28.81, 29, 27.31, 26.75, 26.36, 25.19, 27.91, 27.5, 34.58, 34.92, 34.46, 44.3, 52, 75, 110, 110, 110, 140, 140, 166.6, 180, 180];
        let latAs2 = [90, 90, 60, 60];
        let lonAs2 = [-180, -168.75, -168.75, -180];
        let latAnt = [-60, -60, -90, -90];
        let lonAnt = [-180, 180, 180, -180];

        this.eurPolygon = this.latLongtoPolygon(latEur, lonEur);
        this.asiaPolygon = this.latLongtoPolygon(latAsi, lonAsi);
        this.asiaPolygon2 = this.latLongtoPolygon(latAs2, lonAs2);
        this.africaPolygon = this.latLongtoPolygon(latAfr, lonAfr);
    }

    latLongtoPolygon(latValues, longValues) {
        let polygon = [];
        for (let i = 0; i < latValues.length; i++) {
            polygon.push([latValues[i], longValues[i]]);
        }
        return polygon;
    }

    isInside(point, vs) {
        // ray-casting algorithm based on
        // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html/pnpoly.html
        var x = point[0], y = point[1];

        var inside = false;
        for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
            var xi = vs[i][0], yi = vs[i][1];
            var xj = vs[j][0], yj = vs[j][1];

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    }

    /**
     * Verify if point of coordinates (longitude, latitude) is polygon of coordinates
     * https://github.com/substack/point-in-polygon/blob/master/index.js
     * @param {number} latitude Latitude
     * @param {number} longitude Longitude
     * @param {array<[number,number]>} polygon Polygon contains arrays of points. One array have the following format: [latitude,longitude]
     */
    isPointInPolygon(latitude, longitude, polygon) {
        // console.log(latitude, longitude)
        // if (typeof latitude !== 'number' || typeof longitude !== 'number') {
        //     throw new TypeError('Invalid latitude or longitude. Numbers are expected')
        // } else if (!polygon || !Array.isArray(polygon)) {
        //     throw new TypeError('Invalid polygon. Array with locations expected')
        // } else if (polygon.length === 0) {
        //     throw new TypeError('Invalid polygon. Non-empty Array expected')
        // }

        const x = latitude;
        const y = longitude

        let inside = false
        for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
            const xi = polygon[i][0];
            const yi = polygon[i][1]
            const xj = polygon[j][0];
            const yj = polygon[j][1]

            const intersect = ((yi > y) !== (yj > y)) &&
                (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
            if (intersect) inside = !inside
        }

        if (inside) {
            console.log(inside);
        }

        return inside
    };

    isInEurope(point) {
        // return this.isInside(point, this.eurPolygon);
        return this.isInside([point[1], point[0]], this.eurPolygon);
        // return this.isPointInPolygon(point[0], point[1], this.eurPolygon);
        // return this.isPointInPolygon(point[1], point[0], this.eurPolygon);
    }

    isInAsia(point) {
        return this.isInside([point[1], point[0]], this.asiaPolygon) || this.isInside(point, this.asiaPolygon2);
        // return this.isInside(point, this.asiaPolygon) || this.isInside(point, this.asiaPolygon2);
    }

    isInAfrica(point) {
        return this.isInside([point[1], point[0]], this.africaPolygon);
        // return this.isInside(point, this.africaPolygon);
    }
}