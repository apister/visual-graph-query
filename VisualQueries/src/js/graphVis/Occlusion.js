import * as d3 from 'd3';
import {globals} from "../globals";
import {DocumentNode} from "dynbipgraph/src/Node";

function getNodeLabel(node) {
    let label;

    if (node instanceof DocumentNode) {
        return null;
    }

    if (node.get("_year")) {
        label = node.get("_year");
    } else if (node.get("date_year")) {
        label = node.get("date_year");
    } else {
        label = node.get("name");
    }
    return label;
}

function getTextCoordinates(node) {
    let x = node.x + globals.RADIUS;
    let y = node.y - globals.RADIUS;
    return [x, y];
}


// Inspired by https://observablehq.com/@cscheid/force-directed-graph-with-occlusion
class Occlusion {
    constructor(ctx, idToNode, highlightNodeTable) {
        this.ctx = ctx;
        this.idToNode = idToNode;
        this.highlightNodeTable = highlightNodeTable;
        this.texts = {};
        this.occlusionTable = {};

        // Width of the letter M give good approximation of the Height of strings
        this.height = this.ctx.measureText('M').width;
        this.computeTextWidths();

        this.margin = 10;
        this.zoomK = 1;
    }

    computeTextWidths() {
        Object.keys(this.idToNode).forEach(nId => {
            let node = this.idToNode[nId];
            let label = getNodeLabel(node);
            if (label) {
                let textWidth = this.ctx.measureText(label).width;
                node["width"] = textWidth;

                let highlightPriority = this.highlightNodeTable[nId] ? 1000 : 0;
                this.texts[nId] = {
                    "label": label,
                    "width": textWidth,
                    "priority": node.get("_degree") + highlightPriority
                }
            }
        })

        this.sort();
    }

    sort() {
        this.textIds = Object.keys(this.texts).sort((a, b) => d3.descending(this.texts[a].priority, this.texts[b].priority));
    }

    updatePriority() {
        for (let [nId, text] of Object.entries(this.texts)) {
            let highlightPriority = this.highlightNodeTable[nId] ? 1000 : 0;
            text.priority = this.idToNode[nId].get("_degree") + highlightPriority
        }
        this.sort();
    }

    run(zoomK, nodesIds) {
        this.zoomK = zoomK;
        let filled = [];

        this.updatePriority();

        this.textIds.forEach(nId => {
            if (nodesIds && nodesIds.includes(nId)) {
                let node = this.idToNode[nId];
                const isOccluded = filled.some(e => this.intersect(node, e));
                if (!isOccluded) filled.push(node);
                this.occlusionTable[nId] = isOccluded;
            }
        })
    }

    intersect(text1, text2) {
        let [x1, y1] = getTextCoordinates(text1);
        let [x2, y2] = getTextCoordinates(text2);

        return !(
            x1 + (text1.width / this.zoomK) + this.margin < x2 ||
            x2 + (text2.width / this.zoomK) + this.margin < x1 ||
            y1 + (this.height / this.zoomK) + this.margin < y2 ||
            y2 + (this.height / this.zoomK) + this.margin < y1
        );
    }

    isNodeOccluded(node) {
        return this.occlusionTable[node.id];
    }
}

export {getNodeLabel, Occlusion};