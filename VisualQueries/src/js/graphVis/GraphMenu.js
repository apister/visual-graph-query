import * as d3 from "d3";

import * as lassoIcon from "../../../icons/lasso.png";

export default class GraphMenu {
    constructor(container) {
        this.container = container.node().parentNode
        this.container = d3.select(this.container);

        this.buttons = [];
        this.setupMenu();
    }

    show() {
        this.container
            .style("display", "inline-block");
    }

    hide() {
        this.container
            .style("display", "none");
    }

    setupMenu() {
        this.menu = this.container
            .append("div")
                .style("position", "absolute")
                .style("top", "10px")
                .style("right", "10px")
                .style("z-index", "10")
                .classed("ui icon buttons", true)
    }

    addButton(name, callback, iconName, active=false, titleTooltip=null) {
        let button = this.menu
            .append("button")
            .attr("id", `button-${name}`)
            .classed("ui button active", true)
            .on("click", (e) => {
                callback(e);
                // this.setAllModesFalse();
                this.setAllButtonsInactive();
                button.classed("active", true);
            });

        if (titleTooltip) {
            button.attr("title", titleTooltip);
        }

        if (iconName == "lasso") {
            button
                .append("i")
                .classed(`icon`, true)
                .append("img")
                .attr("src", lassoIcon)
                .style("max-width", "100%")
                .style("max-height", "100%")
        } else {
            button
                .append("i")
                .classed(`${iconName} icon`, true)
        }

        if (active) {
            button.classed('active', true);
        } else {
            button.classed('active', false);
        }

        this.buttons.push(button);
    }

    setButtonActive(buttonName) {
        this.menu.selectAll("button").classed('active', false);
        d3.select(`#button-${buttonName}`).classed('active', true);
    }

    setAllButtonsInactive() {
        this.buttons.forEach(button => button.classed("active", false));
    }
}