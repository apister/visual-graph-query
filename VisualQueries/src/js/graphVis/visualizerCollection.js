

export default class VisualizerCollection {
    constructor(graphVisList) {
        this.graphVisList = graphVisList;

        this.graphVisList.forEach(gv => {
            gv.setCollection(this);
        })
    }

    setSimulation(simulation) {
        this.graphVisList.forEach(gv => {
            gv.setSimulation(simulation);
        })
    }

    addGraphVis(graphVis) {
        this.graphVisList.push(graphVis);
    }

    render(runOcclusion = false) {
        this.graphVisList.forEach(gv => {
            gv.render(runOcclusion);
        })
    }

    setupLasso(cb) {
        this.graphVisList.forEach(gv => {
            gv.setupLasso(cb);
        })
    }

    synchroniseRenders() {
        this.graphVisList.forEach((gv, i) => {
            let renderFuncBlock = () => {};

            this.graphVisList.forEach((gv2, j) => {
                if (i !== j) {
                    let old = renderFuncBlock;
                    renderFuncBlock = function(){
                        // call the original function with any arguments specified, storing the result
                        old.apply(old, arguments);
                        gv2.render();
                    };
                }
            })

            let old2 = gv.render;
            gv.renderSync = function(){
                // call the original function with any arguments specified, storing the result
                old2.apply(old2, arguments);
                renderFuncBlock();
            };
        })

        this.graphVisList.forEach(gv => {
            gv.render = gv.renderSync;
        })
    }

    // to let iterate over the object directly
    [Symbol.iterator]() {
        return this.graphVisList.values();
    }
}