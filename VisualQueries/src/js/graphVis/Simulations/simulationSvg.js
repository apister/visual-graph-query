import * as d3 from 'd3';

import {globals} from "../../globals.js";
import {generateUndirectedLinkId} from "../../utils/utils.js";

class SimulationSvg {
    constructor(graph, graph_vis, svg) {
        this.graph = graph;
        this.graph_vis = graph_vis;
        this.graph_vis.simulation = this;
        this.svg = svg;
        this.width = this.svg.attr("width");
        this.height = this.svg.attr("height");

        this.simulation = null;
    }

    init(){
        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                .id(function(d) { return d.id; })
                .strength(1)
            )
            .force("charge", d3.forceManyBody().strength(-5))
            .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 2))
            .force("center", d3.forceCenter(this.svg.attr('width') / 2, this.svg.attr('height') / 2))
            .force("bounding", (alpha) => {
                for (let i = 0, n = this.graph.nodes.length; i < n; ++i) {
                    let node = this.graph.nodes[i];
                    if (node.x <= globals.RADIUS) {
                        node.x = globals.RADIUS;
                    } else if (node.x >= (this.width - globals.RADIUS - 2)) {
                        node.x = (this.width - globals.RADIUS - 2);
                    }
                    
                    if (node.y <= globals.RADIUS) {
                        node.y = globals.RADIUS;
                    } else if (node.y >= (this.height - globals.RADIUS - 2)) {
                        node.y = (this.height - globals.RADIUS - 2);
                    }
                }
        })
    }

    start(){
        this.simulation
            .nodes(this.graph.nodes)
        this.simulation.force("link")
            .links(this.graph.links);
    }

    runAuto() {
        this.simulation
            .on("tick", () => {
                    this.ticked();
                })
    }

    run(n) {
        this.stop();
        for (let i in d3.range(n)){
            this.simulation.tick();
        }
        this.ticked();
    }

    stop(){
        this.simulation.stop();
    }

    restart(alpha = 1, alphaTarget= 0){
        this.simulation.alphaTarget(alphaTarget).alpha(alpha).restart();
    }

    findEdgePos(d, axis, node="source"){
        // let linkId = d.source.id.toString() + "-" + d.target.id.toString();
        let linkId = generateUndirectedLinkId(d.source.id, d.target.id);
        if (this.graph.linkCount[linkId] > 1) {
            if (this.linkOccurences[linkId] === undefined) {
                this.linkOccurences[linkId] = 1;
                return d[node][axis] + 3;
            }
            else if (this.linkOccurences[linkId] == 1) {
                this.linkOccurences[linkId] += 1;
                return d[node][axis] - 3;
            }
        } else {
            return d[node][axis];
        }
    }

    ticked = () => {
        this.graph_vis.updateVis();
    }

    addTickAction(func) {
        this.oldTicked = this.ticked;
        this.ticked = () => {
            this.oldTicked();
            func();
        }
    }
}

export {SimulationSvg};