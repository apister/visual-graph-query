import * as d3 from 'd3';

import {generateUndirectedLinkId} from '../../utils/utils.js';
import {globals} from '../../globals.js'
import {Simulation} from "./simulation";
import HyperGraphVisCanvas from "../HyperGraphVisCanvas";


export default class RepetitionNodeSimulation extends Simulation {
    constructor(graph, documentGraph, mainGraph, canvas) {
        super(graph, canvas);
        this.documentGraph = documentGraph;
        this.mainGraph = mainGraph;

        this.documentForce = 1;

        this.askLayoutRoute = globals.route("askD3ForceLayout/repetitionNodes");
        this.saveLayoutRoute = globals.route("saveD3ForceLayout/repetitionNodes");
    }

    init(){
        this.forceX();

        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                .id(function(d) { return d.id; })
                .strength(1)
                .distance(100)
            )
            // .force("collision", d3.forceCollide().radius(() => globals.RADIUS))
            .force("collision", d3.forceCollide().radius(() => HyperGraphVisCanvas.rectHeight() / 2))
            .force("boundingDocument", () => {
                if (this.counter > 300) {
                    this.simulation.force("link", null);
                }

                for (let node of this.graph.nodes) {
                    let document = this.documentGraph.idToNode[node.document];

                    let [documentWidth, documentHeight] = [document.width, document.height];

                    node.y = Math.max(document.y - (documentHeight / 2) + (HyperGraphVisCanvas.rectHeight() / 2), Math.min(document.y + (documentHeight / 2) - (HyperGraphVisCanvas.rectHeight() / 2), node.y));
                    // node.y = Math.max(document.y - (documentHeight / 2) + globals.RADIUS, Math.min(document.y + (documentHeight / 2) - globals.RADIUS, node.y));
                }
            })

        // this.simulation.force("link")
        //     .links(this.graph.linksRendered);

        this.simulation
            .nodes(this.graph.nodes)
    }

    forceX() {
        this.graph.nodes.forEach(n => {
            n.fx = this.documentGraph.idToNode[n.document].x;
            // n.fy = this.documentGraph.idToNode[n.document].y;
        })
    }

    // computeBoundingBox(document) {
    //     console.log("compute")
    //     let documentWidth = globals.RADIUS * globals.RECTANGLE_FACTOR * 2;
    //     let nActors = this.mainGraph.idToNode[document.id].Degree
    //     let documentHeight = nActors ? nActors * globals.RADIUS * 2 : globals.RADIUS * 2;
    //     document.width = documentWidth;
    //     document.height = documentHeight;
    //     return [documentWidth, documentHeight];
    // }
}