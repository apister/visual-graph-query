// import * as d3 from 'd3-3';
import * as setcola from '../../../libs/setcola.js';


window.d3 = require('d3');
import * as cola from 'webcola';

import {generateUndirectedLinkId} from '../../utils/utils.js';
import {globals} from '../../globals.js'

export default class SimulationCola {
    constructor(graph, graph_vis, canvas) {
        this.graph = graph;
        this.graph_vis = graph_vis;
        this.graph_vis.setSimulation(this);
        this.canvas = canvas;

        this.simulation = null;
    }

    init() {
        // this.setColaResults = setcola
        //   .nodes(this.graph.nodes)        // Set the graph nodes
        //   .links(this.graph.links)        // Set the graph links
        //   // .groups(groups)            // (Optional) Set any predefined groups in the graph
        //   // .guides(guides)            // (Optional) Define any guides that are used by the SetCoLa layout
        //   // .constraints(setcolaSpec)  // Set the constraints
        //   // .gap(this.gap)                  // The default gap size to use for generating the constraints (if not specified in the SetCoLa spec)
        //   .layout();                 // Run the layout to convert the SetCoLa constraints to WebCoLa constraints

        // console.log("cola ", webcola, result);

        // var d3cola = cola.d3adaptor()
        //     .nodes(this.setColaResults.nodes)
        //     .links(this.setColaResults.links)
        //     .constraints(this.setColaResults.constraints)
        //     .avoidOverlaps(true)
        //     .start(10,15,20);
        // // .linkDistance(30)
        // // .size([width, height]);
        console.log("start cola")

        this.simulation = cola.d3adaptor()
            .nodes(this.graph.nodesRendered)
            .links(this.graph.linksRendered)
            .linkDistance(50)
            // .constraints(this.setColaResults.constraints)
            .avoidOverlaps(true)
            .on("tick", () => {
                this.ticked();
            })
            .start(10, 40, 50);

        console.log("end cola")
    }

    ticked = () => {
        this.graph_vis.render();
    }
}