import * as d3 from 'd3';
import {bboxCollide} from "d3-bboxCollide";

import {globals} from '../../globals.js'
import {Simulation} from "./Simulation.js";
import {rectCollide2, rectCollide3} from "./rectCollide";


export default class DocumentSimulation extends Simulation {
    constructor(graph, canvas, mapVis) {
        super(graph, canvas);
        this.mapVis = mapVis;

        this.askLayoutRoute = globals.route("askD3ForceLayout/document");
        this.saveLayoutRoute = globals.route("saveD3ForceLayout/document");
    }

    init() {
        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                .id(function (d) {
                    return d.id;
                })
                .strength(0.5)
                .distance(200)
            )
            .force("charge", d3.forceManyBody().strength(-500))
            // .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 10))
            .force("collision", rectCollide2())
            .force("center", d3.forceCenter(this.canvas.attr('width') / 2, this.canvas.attr('height') / 2))
            .force("placeholder", () => { // rectCollide is in fight with other forces
                if (this.counter >= 200) {
                    this.simulation.force("link", null);
                    this.simulation.force("center", null);
                    this.simulation.force("charge", null);
                }
            })


        this.simulation.force("link")
            .links(this.graph.links);

        this.simulation
            .nodes(this.graph.nodes)
    }

    initMapSim() {
        // this force position in exact geolocalisation. We have to do it differently for jitter
        this.forceXY();

        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                .id(function (d) {
                    return d.id;
                })
                .strength(1)
                .distance(100)
            )
    }

    initMapSimJitter() {
        // this force position in exact geolocalisation. We have to do it differently for jitter
        // this.forceXY();

        let margin = 2;
        const collide = bboxCollide((d,i) => {
            // let [x, y] = this.mapVis.geoLocToCanvasCoordinates(d.get("latitude"), d.get("longitude"));
            // let topLeftBottomRight = [[[x - d.get("width") / 2, y - d.get("height") / 2]], [x + d.get("width") / 2, y + d.get("height") / 2]];
            let topLeftBottomRight = [[-d.get("width") / 2 - margin,-d.get("height") / 2 - margin], [d.get("width") / 2 + margin, d.get("height") / 2 + margin]];
            return topLeftBottomRight
            // return [[-d.value * 10, -d.value * 5],[d.value * 10, d.value * 5]]
          }).strength(0.02)

        this.simulation = d3.forceSimulation(this.graph.nodes)
            // .force("link", d3.forceLink()
            //     .id(function (d) {
            //         return d.id;
            //     })
            //     .strength(0.001)
            //     .distance(200)
            // )
            // .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 4))
            // .force('collision', rectCollide2()) // WORKS MORE OR LESS
            .force('collision', collide)
            // .force('collision', rectCollide3().size(function (d) { return [d.get("width"), d.get("height")] }))
            .force("x", d3.forceX((d) => { // TODO : custom x and y function, returning null cause weird behaviours (centering around 0 I think)
                    return this.mapVis.geoLocToCanvasCoordinates(d.get("latitude"), d.get("longitude"))[0];
                })
                .strength(3)
            )
            .force("y", d3.forceY((d) => {
                    return this.mapVis.geoLocToCanvasCoordinates(d.get("latitude"), d.get("longitude"))[1];
                })
                .strength(3)
            )
            // .force("placeholder", () => { // rectCollide is in fight with other forces
            //     if (this.counter == 300) {
            //         this.simulation.force("x", null);
            //         this.simulation.force("y", null);
            //     }
            // })
    }

    forceXY() {
        this.graph.nodes.forEach(n => {
            [n.fx, n.fy] = this.mapVis.geoLocToCanvasCoordinates(n["latitude"], n["longitude"]);
        })
    }

    async getPosOrRun(n) {
        await this.loadFromBackend(n);
    }

    checkComputeLayout(pos) {
        return (("empty" in pos) || globals.RUN_LAYOUT || globals.RUN_MAP_LAYOUT);
    }
}