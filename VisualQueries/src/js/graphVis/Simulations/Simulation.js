import * as d3 from 'd3';

import {generateUndirectedLinkId} from '../../utils/utils.js';
import {globals} from '../../globals.js'

class Simulation {
    constructor(graph, canvas, mapVis) {
        this.graph = graph;
        this.canvas = canvas;
        this.mapVis = mapVis;

        this.simulation = null;
        this.counter = 0;

        this.askLayoutRoute = globals.route("askD3ForceLayout/mainGraph");
        this.saveLayoutRoute = globals.route("saveD3ForceLayout/mainGraph");
    }

    init() {
        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                .id(function (d) {
                    return d.id;
                })
                .strength(1)
                .distance(100)
            )
            .force("charge", d3.forceManyBody().strength(-300))
            .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 1.5))
            .force("center", d3.forceCenter(this.canvas.attr('width') / 2, this.canvas.attr('height') / 2))

        this.simulation.force("link")
            .links(this.graph.links);

        this.simulation
            .nodes(this.graph.nodes)
    }

    initOnlyJitter() {
        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("charge", d3.forceManyBody().strength(-15))
            .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 1.5))
    }

    initMapSim() {
        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("link", d3.forceLink()
                    .id(function (d) {
                        return d.id;
                    })
                    .strength(0.1)
                // .distance(100)
            )
            .force("charge", d3.forceManyBody().strength(-500))
            .force("collision", d3.forceCollide().radius(() => globals.RADIUS * 2))
            .force("x", d3.forceX((d) => { // TODO : custom x and y function, returning null cause weird behaviours (centering around 0 I think)
                    if (d["latitude"] && d["longitude"]) {
                        return this.mapVis.geoLocToCanvasCoordinates(d["latitude"], d["longitude"])[0];
                    } else {
                        return null;
                    }
                }).strength(10)
            )
            .force("y", d3.forceY((d) => {
                    if (d["latitude"] && d["longitude"]) {
                        return this.mapVis.geoLocToCanvasCoordinates(d["latitude"], d["longitude"])[1];
                    } else {
                        return null;
                    }
                }).strength(10)
            )

        // .force("center", (alpha) => {
        //     for (let node of this.graph.nodes) {
        //         if (node["latitude"] && node["longitude"]) {
        //             node.vx -= node.x * k;
        //         }
        //     }
        // })

        this.simulation.force("link")
            .links(this.graph.links);

        this.simulation
            .nodes(this.graph.nodes)
    }

    start() {
        let i = 0;
        this.simulation
            .on("tick", () => {
                i++;
                this.ticked();
            })
    }

    run(n = 200) {
        for (let i in d3.range(n)) {
            this.simulation.tick();
            this.counter += 1;
        }
        console.log("d3 simulation end");
    }

    stop() {
        this.simulation.stop();
    }

    hasAlreadyCoordinates() {
        return (this.graph.nodes[0]["x"] && this.graph.nodes[0]["y"])
    }

    async getPosOrRun(n) {
        await this.loadFromBackend(n);
    }

    positions() {
        let pos = {}
        this.graph.nodes.forEach(node => {
            pos[node.id] = {x: node.x, y: node.y};
        })
        return pos;
    }

    saveToBackend(url = this.saveLayoutRoute) {
        fetch(url, {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.positions())
        }).catch(err => {
            console.log(err);
        })
    }

    async loadFromBackend(n) {
        await fetch(this.askLayoutRoute, {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: this.graph.nodes.length
        }).then(response => {
            return response.json()
        }).then(pos => {
            if (this.checkComputeLayout(pos)) {
                console.log("Compute d3 layout");
                this.run(n);
                this.saveToBackend();
            } else {
                console.log("Get d3 layout from backend");
                this.savePosOnNodes(pos);
            }
        }).catch(err => {
            console.log(err);
        })
    }

    checkComputeLayout(pos) {
        return (("empty" in pos) || globals.RUN_LAYOUT);
    }

    savePosOnNodes(positions) {
        this.graph.nodes.forEach(node => {
            let nodeId = node.id;
            let pos = positions[nodeId];
            node.x = pos.x;
            node.y = pos.y;
        })
    }

    restart(alpha = 1, alphaTarget = 0) {
        this.simulation.alphaTarget(alphaTarget).alpha(alpha).restart();
    }

    ticked = () => {
        // this.graphVisCollection.render(true);
    }

    findEdgePos(d, axis, node = "source") {
        // let linkId = d.source.id.toString() + "-" + d.target.id.toString();
        let linkId = generateUndirectedLinkId(d.source.id, d.target.id);
        if (this.graph.linkCount[linkId] > 1) {
            if (this.linkOccurences[linkId] === undefined) {
                this.linkOccurences[linkId] = 1;
                return d[node][axis] + 3;
            } else if (this.linkOccurences[linkId] == 1) {
                this.linkOccurences[linkId] += 1;
                return d[node][axis] - 3;
            }
        } else {
            return d[node][axis];
        }
    }

    // ticked = () => {
    //     this.graph.linkRepetitionCount();
    //     this.linkOccurences = {};
    //
    //     this.graph_vis.links
    //         .attr("x1", (d) => {
    //             return this.findEdgePos(d, "x", "source");
    //         })
    //     this.linkOccurences = {};
    //     this.graph_vis.links
    //         .attr("x2", (d) => {
    //             return this.findEdgePos(d, "x", "target");
    //         })
    //     this.linkOccurences = {};
    //     this.graph_vis.links
    //         .attr("y1", (d) => {
    //             return this.findEdgePos(d, "y", "source");
    //         })
    //     this.linkOccurences = {};
    //     this.graph_vis.links
    //         .attr("y2", (d) => {
    //             return this.findEdgePos(d, 'y', "target");
    //         })
    //
    //         // .attr("y1", function(d) { return d["source"].y; })
    //         // .attr("x2", function(d) { return d["target"].x; })
    //         // .attr("y2", function(d) { return d["target"].y; });
    //
    //     this.graph_vis.nodes
    //         .attr("transform", function(d) {
    //             return "translate(" + d.x + "," + d.y + ")";
    //         })
    // }

}

export {Simulation};