import * as d3 from 'd3';


// from https://bl.ocks.org/cmgiven/547658968d365bcc324f3e62e175709b
export function rectCollide3() {
    var nodes, sizes, masses
    var size = constant([0, 0])
    var strength = 1
    var iterations = 1

    function force() {
        var node, size, mass, xi, yi
        var i = -1
        while (++i < iterations) { iterate() }

        function iterate() {
            var j = -1
            var tree = d3.quadtree(nodes, xCenter, yCenter).visitAfter(prepare)

            while (++j < nodes.length) {
                node = nodes[j]
                size = sizes[j]
                mass = masses[j]
                xi = xCenter(node)
                yi = yCenter(node)

                tree.visit(apply)
            }
        }

        function apply(quad, x0, y0, x1, y1) {
            var data = quad.data
            var xSize = (size[0] + quad.size[0]) / 2
            var ySize = (size[1] + quad.size[1]) / 2
            if (data) {
                if (data.index <= node.index) { return }

                var x = xi - xCenter(data)
                var y = yi - yCenter(data)
                var xd = Math.abs(x) - xSize
                var yd = Math.abs(y) - ySize

                if (xd < 0 && yd < 0) {
                    var l = Math.sqrt(x * x + y * y)
                    var m = masses[data.index] / (mass + masses[data.index])

                    if (Math.abs(xd) < Math.abs(yd)) {
                        node.vx -= (x *= xd / l * strength) * m
                        data.vx += x * (1 - m)
                    } else {
                        node.vy -= (y *= yd / l * strength) * m
                        data.vy += y * (1 - m)
                    }
                }
            }

            return x0 > xi + xSize || y0 > yi + ySize ||
                   x1 < xi - xSize || y1 < yi - ySize
        }

        function prepare(quad) {
            if (quad.data) {
                quad.size = sizes[quad.data.index]
            } else {
                quad.size = [0, 0]
                var i = -1
                while (++i < 4) {
                    if (quad[i] && quad[i].size) {
                        quad.size[0] = Math.max(quad.size[0], quad[i].size[0])
                        quad.size[1] = Math.max(quad.size[1], quad[i].size[1])
                    }
                }
            }
        }
    }

    function xCenter(d) { return d.x + d.vx + sizes[d.index][0] / 2 }
    function yCenter(d) { return d.y + d.vy + sizes[d.index][1] / 2 }

    force.initialize = function (_) {
        sizes = (nodes = _).map(size)
        masses = sizes.map(function (d) { return d[0] * d[1] })
    }

    force.size = function (_) {
        return (arguments.length
             ? (size = typeof _ === 'function' ? _ : constant(_), force)
             : size)
    }

    force.strength = function (_) {
        return (arguments.length ? (strength = +_, force) : strength)
    }

    force.iterations = function (_) {
        return (arguments.length ? (iterations = +_, force) : iterations)
    }

    return force
}

function constant(_) {
    return function () { return _ }
}






export function rectCollide2() {
    let nodes;
    let padding = 10;

    function force(alpha) {
        const quad = d3.quadtree(nodes, d => d.x, d => d.y);
        for (const d of nodes) {
            quad.visit((q, x1, y1, x2, y2) => {
                let updated = false;
                if (q.data && q.data !== d) {
                    let xSpacing = padding + (q.data.get("width") + d.get("width")) / 2;
                    let ySpacing = padding + (q.data.get("height") + d.get("height")) / 2;

                    let x = d.x - q.data.x,
                        y = d.y - q.data.y,
                        // xSpacing = padding + (q.data.width + d.width) / 2,
                        // ySpacing = padding + (q.data.height + d.height) / 2,
                        absX = Math.abs(x),
                        absY = Math.abs(y),
                        l,
                        lx,
                        ly;

                    if (absX < xSpacing && absY < ySpacing) {
                        l = Math.sqrt(x * x + y * y);

                        lx = (absX - xSpacing) / l;
                        ly = (absY - ySpacing) / l;

                        // the one that's barely within the bounds probably triggered the collision
                        if (Math.abs(lx) > Math.abs(ly)) {
                            lx = 0;
                        } else {
                            ly = 0;
                        }
                        d.x -= x *= lx;
                        d.y -= y *= ly;
                        q.data.x += x;
                        q.data.y += y;

                        updated = true;
                    }
                }
                return updated;
            });
        }
    }

    force.initialize = _ => nodes = _;

    return force;
}


// Code from https://gist.github.com/lvngd/f4e9d42d2348a71f109680aea90b7c6f
export function rectCollide() {
    var nodes, sizes, masses;
    var strength = 1;
    var iterations = 1;
    var nodeCenterX;
    var nodeMass;
    var nodeCenterY;

    function force() {
        var node;
        var i = -1;
        while (++i < iterations) {
            iterate();
        }

        function iterate() {
            var quadtree = d3.quadtree(nodes, xCenter, yCenter);

            var j = -1

            while (++j < nodes.length) {
                node = nodes[j];

                nodeMass = masses[j];
                nodeCenterX = xCenter(node);
                nodeCenterY = yCenter(node);
                quadtree.visit(collisionDetection);
            }
        }


        function collisionDetection(quad, x0, y0, x1, y1) {
            var updated = false;
            var data = quad.data;
            if (data) {
                if (data.index > node.index) {


                    let xSize = (node.width + data.width) / 2;
                    let ySize = (node.height + data.height) / 2;
                    let dataCenterX = xCenter(data);
                    let dataCenterY = yCenter(data);
                    let dx = nodeCenterX - dataCenterX;
                    let dy = nodeCenterY - dataCenterY;
                    let absX = Math.abs(dx);
                    let absY = Math.abs(dy);
                    let xDiff = absX - xSize;
                    let yDiff = absY - ySize;

                    if (xDiff < 0 && yDiff < 0) {

                        //collision has occurred


                        //separation vector
                        let sx = xSize - absX;
                        let sy = ySize - absY;
                        if (sx < sy) {
                            if (sx > 0) {
                                sy = 0;
                            }
                        } else {
                            if (sy > 0) {
                                sx = 0;
                            }
                        }
                        if (dx < 0) {
                            sx = -sx;
                        }
                        if (dy < 0) {
                            sy = -sy;
                        }

                        let distance = Math.sqrt(sx * sx + sy * sy);
                        let vCollisionNorm = {x: sx / distance, y: sy / distance};
                        let vRelativeVelocity = {x: data.vx - node.vx, y: data.vy - node.vy};
                        let speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;
                        if (speed < 0) {
                            //negative speed = rectangles moving away
                        } else {
                            var collisionImpulse = 2 * speed / (masses[data.index] + masses[node.index]);

                            if (Math.abs(xDiff) < Math.abs(yDiff)) {
                                //x overlap is less

                                data.vx -= (collisionImpulse * masses[node.index] * vCollisionNorm.x);
                                data.x += data.vx;
                                // console.log(data.vx);
                                node.vx += (collisionImpulse * masses[data.index] * vCollisionNorm.x);
                                node.x += node.vx;
                                // console.log(node.vx);
                            } else {
                                //y overlap is less
                                data.vy -= (collisionImpulse * masses[node.index] * vCollisionNorm.y);
                                data.y += data.vy;
                                node.vy += (collisionImpulse * masses[data.index] * vCollisionNorm.y);
                                node.y += node.vy;
                            }

                            updated = true;
                        }
                    }
                }
            }
            return updated
        }
    }//end force

    function xCenter(d) {
        return d.x + d.vx + sizes[d.index][0] / 2
    }

    function yCenter(d) {
        return d.y + d.vy + sizes[d.index][1] / 2
    }

    force.initialize = function (_) {
        sizes = (nodes = _).map(function (d) {
            return [d.width, d.height]
        })
        masses = sizes.map(function (d) {
            return d[0] * d[1]
        })
        console.log("F INIT", sizes, masses);
    }

    force.size = function (_) {
        return (arguments.length
            ? (size = typeof _ === 'function' ? _ : constant(_), force)
            : size)
    }

    force.strength = function (_) {
        return (arguments.length ? (strength = +_, force) : strength)
    }

    force.iterations = function (_) {
        return (arguments.length ? (iterations = +_, force) : iterations)
    }

    return force
}