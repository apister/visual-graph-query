class Globals {
    constructor() {
        console.log("glob", process.env.NODE_ENV);

        if (process.env.NODE_ENV == "dev") {
            this.LOCALHOST = true;
        } else if (process.env.NODE_ENV == "prod")  {
            this.LOCALHOST = false;
        }

        this.parseURLParameters();
        this.computeUrl();

        // keys used in the backend
        this.NODETYPE_BACKEND = "nodeType";
        this.EDGETYPE_BACKEND = "edgeType";

        this.ANY_EDGETYPE = "Any";
        this.ANY_NODETYPE = "Any";
        this.ANY_EDGETYPE_COLOR = "gray";
        this.SUBGRAPH_NODE_TYPE = "subgraph";

        this.FONT_SIZE = 14;
        this.RADIUS = 12;
        this.ARROW_LENGTH = 10;
        this.LINE_WIDTH = 2;
        // this.NODE_LINE_WIDTH_SELECT = 8;
        this.NODE_LINE_WIDTH_SELECT = 5;
        this.LINE_WIDTH_SELECT = 9;
        this.STROKESTYLE = "black";
        this.FILLSTYLE = "white";
        this.SHAPES = ["circle", "square"];

        this.MENU_COLOR = "rgb(241,241,241)";
        // this.SELECTION_COLOR = "#78afe5";
        this.SELECTION_COLOR = "rgb(120, 175, 229)";
        this.UNHIGHLIGHT_OPACITY = 0.20;

        this.FORCE_LAYOUT = true;

        this.matchedAttribute = "matched";

        // COMPARISON
        this.mainQueryName = "A";
        this.comparisonQueryName = "B";
        this.mainQueryColor = this.SELECTION_COLOR;
        // this.comparisonQueryColor = "red";
        this.comparisonQueryColor = "rgb(219, 40, 40)";
        this.mainQueryColorLowOpacity = "rgb(120, 175, 229, 0.2)";
        this.comparisonQueryColorLowOpacity = "rgb(219, 40, 40, 0.2)";

        this.ALL_NODETYPE = "ALL";
        // this.ATTRIBUTES_FILTERED = ["id", "name", "label", "documents", "latitude", "longitude"];
        this.ATTRIBUTES_FILTERED = ["name", "label", "documents", "latitude", "longitude"];
        this.ATTRIBUTES_TOOLTIP_FILTERED = ["label", "documents", "latitude", "longitude"];

        this.HYPEREDGE_NODE_TYPES = ["DOCUMENT", "MARRIAGE_ACT", "CONTRACT"];

        // To make small plots, specifically for publication
        if (process.env.NODE_ENV == "dev") {
            // this.SHORT_PLOTS = false;
            this.SHORT_PLOTS = true;
        } else {
            this.SHORT_PLOTS = false;
        }
    }

    route(routeName) {
        return this.URL_BACKEND.toString() + "/" + routeName;
        // return this.URL_BACKEND.toString() + routeName;
    }

    computeUrl() {
        if (this.dataset == "Zacarias") {
            this.URL_BACKEND = new URL("https://historia2.lri.fr/vq/");
        } else if (this.dataset == "Nicole"){
            this.URL_BACKEND = new URL("https://historia1.lri.fr/vq");
        } else if (this.dataset == "Pascal") {
            this.URL_BACKEND = new URL("https://apister.lri.fr/vq");
        } else if (this.dataset == "Thesis") {
            this.URL_BACKEND = new URL("https://historia3.lri.fr/vq");
        } else {
            if (this.LOCALHOST) {
                this.URL_BACKEND = new URL("http://127.0.0.1:10090");
                // this.URL_BACKEND = new URL("http://backend-service:10090");
            } else {
                // this.URL_BACKEND = new URL("https://historia1.lri.fr/vq");
                this.URL_BACKEND = new URL("https://apister.lri.fr/vq");
                // throw "Backend URL Error"
            }
        }
        console.log(this.URL_BACKEND);
    }

    parseURLParameters() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        this.dataset = urlParams.get('dataset');
        this.RUN_LAYOUT = urlParams.get('runlayout');
        this.RUN_MAP_LAYOUT = urlParams.get('runmaplayout');

        console.log("dataset ", this.dataset);
        console.log("runlayout", this.RUN_LAYOUT);
        console.log("runmaplayout ", this.RUN_MAP_LAYOUT);
    }

    matchedAttributeName(queryEntityId) {
        return `matched_${queryEntityId}`;
    }
}

const globals = new Globals();
export {globals};

