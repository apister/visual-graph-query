// const jquery = require("jquery");
// window.$ = window.jQuery = jquery;
// import "../../semantic/dist/semantic.min.js";
// import "../../semantic/dist/semantic.min.css";

// Imports have to be in this order
import {globals} from "./globals";
import {VisualQueries} from "./visualQueries.js";

import * as d3 from 'd3';


async function main(){
    $('#menu-accordion')
      .accordion({
          exclusive: false
      });

    setGlobalCss();

    let visualQueries = new VisualQueries();
    await visualQueries.loadDataFromNeo4j();
    // await visualQueries.loadHypergraph();
    // visualQueries.initQueryManager();
    // visualQueries.fitBothCanvas();
    // visualQueries.linkTwoViews();
}

function setGlobalCss() {
    d3.select("#menu-accordion")
        .style("background-color", `${globals.MENU_COLOR}`);
}


if (module.hot) { module.hot.accept(function () { location.reload(); }); }
main();

