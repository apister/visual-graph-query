
// SHared between graph views, tables and plots
export default class Selector {
    constructor(updateCb) {
        this.selectedNodesIds = [];
        this.selectedLinksIds = [];
        this.updateCb = updateCb
    }

    addNodeId(nodeId) {
        this.selectedNodesIds.push(nodeId);
    }

    removeNodeId(nodeId) {
        this.selectedNodesIds = this.selectedNodesIds.filter(node => node != nodeId);
    }

    toggleNodeId(nodeId) {
        if (this.selectedNodesIds.includes(nodeId)) {
            this.removeNodeId(nodeId);
        } else {
            this.addNodeId(nodeId)
        }

        this.updateCb();
    }

    has(nodeId) {
        return this.selectedNodesIds.includes(nodeId);
    }
}