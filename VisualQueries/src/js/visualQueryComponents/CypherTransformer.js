import * as cypherlib from "cypher-compiler-js";

export default class CypherTransformer {
    constructor(cypher) {
        this.cypher = cypher;
    }

    run() {
        // const tree = cypherlib.parseCypher(this.cypher, console.log("error"));
        const tree = cypherlib.parseCypher(this.cypher);
        this.ast = cypherlib.parsePatterns(tree);

        if (this.hasEgo()) {
            Object.keys(this.ast.scopeGlobal.variables).forEach(v => {
                this.ast.return.addVariable(v);
            })
            this.ast.return.addVariable(["nodes", "relationships"]);

        } else {
            this.ast.return.toStar();
        }

        return this.ast.toCypher();
    }

    hasEgo() {
        return Object.values(this.ast.scopeGlobal.variables).some(v => v["ego"]);
    }
}