import ConstraintWidget from "./constraintWidget";

class CypherConstraintWidget extends ConstraintWidget {
    constructor(constraint, domain=null, updateCb, removeCb) {
        super(constraint, domain, updateCb, removeCb);
        this.mainDiv.classed("cypher", true)

        console.log("init cypher widget")
    }

    cypher() {
        return this.constraint.treeNode.toCypher();
    }

    initWidget() {
        super.initWidget();

        this.mainDiv
            .append("span")
            .html(this.cypher());
    }

    update() {
        // super.update();
        // this.constraint.update(this.selectedValues);
        // this.updateCb(this.constraint);
    }
}

export {CypherConstraintWidget};