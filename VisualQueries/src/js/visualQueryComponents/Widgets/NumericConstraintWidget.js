import * as d3 from 'd3';
import * as cypherlib from 'cypher-compiler-js';

import ConstraintWidget from "./constraintWidget.js";

class numericConstraintWidget extends ConstraintWidget {
    constructor(constraint, domain, updateCb, removeCb) {
        super(constraint, domain, updateCb, removeCb);
        this.mainDiv.classed("numeric", true);
    }

    initWidget() {
        super.initWidget();
        this.getInitValues();
        this.initBoundsElements();

        this.sliderId = `slider-${this.entityId}-${this.property}`;

        this.slider = document.createElement("div");
        this.slider.id = this.sliderId;
        this.slider.className = "ui range slider";

        this.mainDiv.node().appendChild(this.slider);
    }

    initBoundsElements() {
        this.lowerBoundElement = d3.create("span")
            .html(this.lowerInit)

        this.upperBoundElement = d3.create("span")
            .html(this.upperInit)

        this.mainDiv.append(() => this.lowerBoundElement.node());
        this.mainDiv.append("span").html(" - ");
        this.mainDiv.append(() => this.upperBoundElement.node());
    }

    initEvents() {
        this.computeStep();
        $(`#${this.sliderId}`).slider({
            min: this.domain[0],
            max: this.domain[1],
            start: this.lowerInit,
            end: this.upperInit,
            // step: 1,
            step: this.step,
            onChange: () => {
                this.updateCb(this.constraint, true);
            },
            onMove: ( e, d, d2) => {
                d = Number(d.toFixed(3));
                d2 = Number(d2.toFixed(3));

                this.lowerBoundElement.node().innerHTML = d;
                this.upperBoundElement.node().innerHTML = d2;
                this.update(d, d2);
            }
        });
    }

    computeStep() {
        let diff = (this.domain[1] - this.domain[0]);
        if (diff > 2) {
            this.step = 1;
        } else {
            this.step = (this.domain[1] - this.domain[0]) / 10;
        }
    }

    getInitValues() {
        if (this.constraint instanceof cypherlib.IntervalConstraint) {
            this.lowerInit = this.constraint.value[0];
            this.upperInit = this.constraint.value[1];
        } else if (this.constraint.value == null) {
            this.lowerInit = this.domain[0];
            this.upperInit = this.domain[1];
        } else if (this.constraint.op == "<") {
            this.lowerInit = this.domain[0];
            this.upperInit = this.constraint.value;
        } else if (this.constraint == ">") {
            this.lowerInit = this.constraint.value;
            this.upperInit = this.domain[0];
        }

        if (this.lowerInit < this.domain[0]) {
            this.lowerInit = this.domain[0]
        }
        if (this.upperInit > this.domain[1]) {
            this.upperInit = this.domain[1]
        }
    }

    update(lower, upper) {
        super.update();

        // console.log("UPDATE", this.constraint);
        this.constraint.update(upper, lower);
        this.updateCb(this.constraint, false);
    }
}

export {numericConstraintWidget}