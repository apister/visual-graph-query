import * as d3 from 'd3';

import ConstraintWidget from "./constraintWidget.js";

export default class CategoricalConstraintWidget extends ConstraintWidget {
    static noNullValue = "NONULL";

    constructor(constraint, domain, updateCb, removeCb) {
        super(constraint, domain, updateCb, removeCb)
        this.mainDiv.classed("categorical", true);
    }

    initWidget() {
        // this.selectedValues = this.constraint.value ? (Array.isArray(this.constraint.value) ? this.constraint.value : [this.constraint.value]) : [];
        if (this.constraint.value && this.constraint.op == "<>") {
            this.selectedValues = [CategoricalConstraintWidget.noNullValue];
        } else if (this.constraint.value) {
            if (Array.isArray(this.constraint.value)) {
                this.selectedValues = this.constraint.value;
            } else {
                this.selectedValues = [this.constraint.value];
            }
        } else {
            this.selectedValues = [];
        }

        super.initWidget();
        this.initNotNullValueOption()
        this.initCheckboxes();
    }

    initNotNullValueOption() {
        let text = "No Null Value";

        let checkbox = this.mainDiv.append("div")
            .append("div")
            .classed("ui checkbox", true)

        let input = checkbox.append("input")
            .attr("type", "checkbox")
            .attr("value", text)

        if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
            checkbox.classed("checked", true);
            input.attr("checked", "");
        }

        checkbox.append("label")
            .html(text)

        checkbox.on("click", (e) => {
            if (this.selectedValues.includes(CategoricalConstraintWidget.noNullValue)) {
                this.selectedValues = [];
            } else {
                this.selectedValues = [CategoricalConstraintWidget.noNullValue];
            }
            this.update();
            // this.setNoNullValue();
        })
    }

    initCheckboxes() {
        this.checkBoxesContainer = this.mainDiv

        this.domain.sort();
        this.domain.forEach((c) => {
            let checkbox = this.checkBoxesContainer
                .append("div")
                .classed("ui checkbox", true)

            let input = checkbox.append("input")
                .attr("type", "checkbox")
                .attr("value", c)

            if (this.selectedValues.includes(c)) {
                checkbox.classed("checked", true);
                input.attr("checked", "");
            }

            checkbox.append("label").html(c);
            this.checkBoxesContainer.append("span")
                .html("  ")
                .style("margin-right", "10px")

            checkbox.on("click", (e) => {
                let domainValue = e.target.value;
                if (this.selectedValues.includes(domainValue)) {
                    this.selectedValues = this.selectedValues.filter(v => v != domainValue);
                } else {
                    if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
                        this.selectedValues = [domainValue];
                    } else {
                        this.selectedValues.push(domainValue);
                    }
                }
                this.update();
            });
        })
    }

    // TODO : refactor
    setNoNullValue() {
        super.update();
        // this.constraint.update(this.selectedValues);

        this.updateCb(this.constraint);
    }

    update() {
        super.update();

        if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
            this.constraint.op = "<>";
            this.constraint.right = "null";
            this.constraint.treeNode.op = "<>";
            this.constraint.treeNode.right = "null"
        } else {
            this.constraint.update(this.selectedValues);
        }

        this.updateCb(this.constraint);
    }
}

export {CategoricalConstraintWidget};
