import * as d3 from 'd3';

import {CypherWidgets, EntityWidgets} from "./EntityWidgets.js";
import {globals} from "../../globals.js";

// Highest order widget class
export default class WidgetsCollection {
    constructor(element, graph, graphPaint, astController, provenanceManager) {
        this.containerElement = element;
        this.mainGraph = graph;
        this.graphPaint = graphPaint;
        this.astController = astController;
        this.provenanceManager = provenanceManager;

        this.entityWidgetsList = [];
        this.InputPlaceHolder = "Add Node Attribute Constraint";
    }

    entityWidgetsIds() {
        return this.entityWidgetsList.map(e => e.entityId);
    }

    initEvents() {
        this.entityWidgetsList.forEach(ew => ew.initEvents());
    }

    initFromConstraints(constraintsCollection) {
        this.entityWidgetsList = [];

        this.constraintsCollection = constraintsCollection;
        for (const entityId in this.constraintsCollection.entityToConstraints) {
            if (entityId != "undefined") {
                let constraints =  this.constraintsCollection.entityToConstraints[entityId];
                let entity = this.astController.getEntityById(entityId);
                let entityWidgets = new EntityWidgets(entity, constraints, this.mainGraph, this.astController, this.provenanceManager);
                this.entityWidgetsList.push(entityWidgets);
            }
        }

        if (this.constraintsCollection.entityToConstraints["undefined"]) {
            let undefinedConstraints = this.constraintsCollection.entityToConstraints["undefined"];
            let entityWidgets = new CypherWidgets(undefinedConstraints, this.mainGraph, this.astController, this.provenanceManager);
            this.entityWidgetsList.push(entityWidgets);
        }
    }

    initNewEntityWidgetArea() {
        let html = `<select>`

        html += `<option value='Entity'>${this.InputPlaceHolder}</option>`
        // for (let entityId of this.graphPaint.graph.nodesIds()) {
        for (let entityId of this.graphPaint.graph.getNodes(false)) {
        // for (let entityId of this.graphPaint.graph.entityIds()) {
            if (!(this.entityWidgetsIds().includes(entityId))) {
                html += ` <option value="${entityId}">${entityId}</option>`
            }
        }
        html += "</select>";

        this.entityWidgetsCreationDiv = d3.create("div")
            .classed("entity-widgets-creation", true)
            .classed("widget", true)
            .classed("ui segment", true)
            .style("background-color", globals.MENU_COLOR)
            .html(html)
            .on("change", this.newEntityWidgetsCb)
    }

    newEntityWidgetsCb = (e) => {
        let entityId = e.target.selectedOptions[0].label;
        let entity = this.astController.getEntityById(entityId);
        if (entityId != "Entity") {
            let entityWidgets = new EntityWidgets(entity, [], this.mainGraph, this.astController, this.provenanceManager);
            this.entityWidgetsList.push(entityWidgets);
        }

        this.render();
    }

    render() {
        this.containerElement.html("");

        this.nestedContainerElement = this.containerElement
            .append("div")
            .classed("ui segments", true)

        this.entityWidgetsList.forEach((entityWidgets) => {
            entityWidgets.init();
            this.nestedContainerElement.append(() => entityWidgets.containerElement.node());
            // entityWidgets.render();
            // entityWidgets.init();
            // this.nestedContainerElement.append("br")
        });

        this.initEvents();

        this.initNewEntityWidgetArea();
        this.nestedContainerElement.append(() => this.entityWidgetsCreationDiv.node());
    }
}