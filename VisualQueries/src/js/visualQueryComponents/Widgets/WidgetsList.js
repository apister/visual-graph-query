import * as d3 from 'd3';

import {EntityWidgets} from "./EntityWidgets";
import {globals} from "../../globals";


export default class WidgetsList {
    static entityWidgetsClass(entityId) {
        return `entity-widgets-${entityId}`;
    }

    constructor(constraints, mainGraph, astController, provenanceManager) {
        this.constraints = constraints;
        this.mainGraph = mainGraph;
        this.astController = astController;
        this.provenanceManager = provenanceManager;

        this.widgets = [];

        this.containerElement = null;

        this.attributePlaceHolder = "Choose an attribute";
    }

    init() {
        this.initContainer();
        this.initHeader();
        this.initWidgetsFromConstraints();
        this.initNewWidgetArea();

        this.render();
    }

    initEvents() {
        this.widgets.forEach(w => w.initEvents());
    }

    initContainer() {
        this.containerElement = d3.create("div")
            .classed("ui segments", true)
            .classed("widget", true)
            .style("background-color", globals.MENU_COLOR)
    }

    initHeader() {
        this.headerElement = d3.create("div")
            .classed("widget-header ui segment", true)
            .classed("widget", true)
            .style("background-color", globals.MENU_COLOR)
            .style("font-size", "130%")
            .html(`<p>${this.entityId}</p>`)
    }

    initWidgetsFromConstraints() {
        //
    }

    initNewWidgetArea() {
        //
    }

    createNewConstraintWidget = (e) => {
        let attribute = e.target.selectedOptions[0].label;
        if (attribute != "Attribute") {
            let attributeDomain = this.mainGraph.propertiesStats[attribute].domain;
            let type = this.mainGraph.propertiesStats[attribute].type;

            let constraint;
            if (type == "INTEGER" || type == "FLOAT") {
                constraint = new cypherlib.NumericSimpleConstraint(this.entityId, attribute, null, null, null, null);
            } else {
                constraint = new cypherlib.SimpleConstraint(this.entityId, attribute, null, null, null, null);
            }

            this.astController.ast.addSimpleConstraint(constraint, true);
            this.addWidget(constraint, type, attributeDomain);
        }
        this.render();
    }

    updateWidgetCb = (constraint, trueChange=true) => {
        if (trueChange) {
            let label = `${this.entityId}.${constraint.property} change`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(label)(this.astController.ast.toCypher()));
        } else {
            this.astController.constraintModification(false);
        }
    }

    removeWidget = (widget) => {
        this.widgets = this.widgets.filter(w => w != widget);
        this.astController.removeConstraint(widget.constraint);
        this.render();
        // this.astController.constraintModification();
    }

    render() {
        this.containerElement
            .html("")
        this.containerElement.append(() => this.headerElement.node());

        this.nestedElementsContainer = this.containerElement
            .append("div")
            .classed(EntityWidgets.entityWidgetsClass(this.entityId), true)
            .classed("ui segments", true)

        this.widgets.forEach((widget) => {
            this.nestedElementsContainer.append(() => widget.mainDiv.node());
            widget.initEvents();
        });

        this.initNewWidgetArea();
        this.nestedElementsContainer.append(() => this.widgetCreationDiv.node());
    }
}

export {EntityWidgets};