import * as d3 from 'd3';
import * as cypherlib from 'cypher-compiler-js';

import {nominalConstraintWidget} from "./NominalConstraintWidget.js";
import CategoricalConstraintWidget from "./categoricalConstraintWidget.js";
import {numericConstraintWidget} from "./NumericConstraintWidget.js";
import {globals} from "../../globals";
import WidgetsList from "./WidgetsList";
import {CypherConstraintWidget} from "./CypherConstraintWidget";

import {Property, PropertyTypesEnum} from "dynbipgraph"

export function getPropertyType(property, categorical_threshold=30) {
    // const categorical_threshold = 30;
    let type = property.type;
    let domain = property.domain;
    if (type == "STRING") {
        if (domain.length > categorical_threshold) {
            return "nominal"
        } else {
            return "categorical"
        }
    } else if (type == 'INTEGER' || type == 'FLOAT') {
        return "numeric"
    }
}

class CypherWidgets extends WidgetsList {
    constructor(constraints, mainGraph, astController, provenanceManager) {
        super(constraints, mainGraph, astController, provenanceManager);
        this.entityId = "cypherBlock"
    }

    initWidgetsFromConstraints() {
        this.widgets = [];
        this.constraints.forEach(constraint => {
            this.addWidget(constraint)
        })
    }

    // initNewWidgetArea() {
    //     // It's best to write directly in the cypher editor
    //     this.widgetCreationDiv = d3.create("i")
    // }

    initNewWidgetArea() {
        let html = `<button class="ui button">
            <i class="play icon"></i>
          </button>`


         this.widgetCreationDiv = d3.create("span")
            // .classed("widget-creation", true)
            // .classed("widget", true)
            // .classed("ui segment", true)

        // this.widgetCreationDiv = d3.create("div")
        //     .classed("widget-creation", true)
        //     .classed("widget", true)
        //     .classed("ui segment", true)
            // .html(html);


        // this.widgetCreationDiv = d3.create("div")
        //     .classed("widget-creation", true)
        //     .classed("widget", true)
        //     .classed("ui segment", true)
        //     .style("background-color", globals.MENU_COLOR)
        //     .html(html)
        //     .on("change", this.createNewConstraintWidget)
    }

    addWidget(constraint) {
        let newWidget = new CypherConstraintWidget(constraint, null, this.updateWidgetCb, this.removeWidget);
        this.widgets.push(newWidget);
    }
}

class EntityWidgets extends WidgetsList {
    static entityWidgetsClass(entityId) {
        return `entity-widgets-${entityId}`;
    }

    constructor(entity, constraints, mainGraph, astController, provenanceManager) {
        super(constraints, mainGraph, astController, provenanceManager);

        this.entity = entity;
        this.entityId = this.entity.id;

        this.findAttributes();
    }

    findAttributes() {
        // if (this.entity["label"]) {
        //     this.attributes = this.mainGraph.nodeTypeToProperties[this.entity["label"]].concat(this.attributes = this.mainGraph.nodeTypeToProperties["ALL"]);
        // } else {
        //     this.attributes = this.mainGraph.nodeTypeToProperties["ALL"];
        // }
        let nodeTypeToProperties = this.mainGraph.propertiesByNodeType();

        if (this.entity["label"]) {
            this.attributes = nodeTypeToProperties[this.entity["label"]];
        } else {
            this.attributes = nodeTypeToProperties[Property.NODETYPE_ALL];
        }

        // if (this.astController.ast.scopeGlobal.nodeIds().includes(this.entityId)) {
        //     let labels = this.astController.ast.findNodeLabels(this.entityId);
        //     if (labels.length == 0) {
        //         this.attributes = this.mainGraph.attributes;
        //     } else {
        //         this.attributes = [];
        //         labels.forEach(label => {
        //             let attributes = this.mainGraph.nodeTypeToProperties[label];
        //             this.attributes = this.attributes.concat(attributes);
        //         })
        //     }
        // }
    }

    initContainer() {
        this.containerElement = d3.create("div")
            .classed("ui segments", true)
            .classed("widget", true)
            .style("background-color", globals.MENU_COLOR)
    }

    initWidgetsFromConstraints() {
        this.widgets = [];
        this.constraints.forEach(constraint => {
            // const propertyStats = this.mainGraph.propertiesStats[constraint.property];
            const property = this.mainGraph.properties[constraint.property];
            this.addWidget(constraint, property.type, property.domain)
            // this.addWidget(constraint, propertyStats.type, propertyStats.domain)
        })
    }

    initNewWidgetArea() {
        let html = `<select>`
        html += ` <option value="Attribute">${this.attributePlaceHolder}</option>`
        for (let attribute of this.attributes) {
            html += ` <option value="${attribute.name}">${attribute.name}</option>`
        }
        html += "</select>";

        this.widgetCreationDiv = d3.create("div")
            .classed("widget-creation", true)
            .classed("widget", true)
            .classed("ui segment", true)
            .style("background-color", globals.MENU_COLOR)
            .html(html)
            .on("change", this.createNewConstraintWidget)
    }

    createNewConstraintWidget = (e) => {
        let attribute = e.target.selectedOptions[0].label;
        if (attribute != this.attributePlaceHolder) {
            // let property = this.mainGraph.getProperty(attribute);
            let property = this.mainGraph.properties[attribute];
            let attributeDomain = property.domain;
            let type = property.type;

            // let attributeDomain = this.mainGraph.propertiesStats[attribute].domain;
            // let type = this.mainGraph.propertiesStats[attribute].type;

            let constraint;
            if (type == PropertyTypesEnum.NUMERIC) {
                constraint = new cypherlib.NumericSimpleConstraint(this.entityId, attribute, null, null, null, null);
            } else {
                constraint = new cypherlib.SimpleConstraint(this.entityId, attribute, null, null, null, null);
            }

            this.astController.ast.addSimpleConstraint(constraint, true);
            this.addWidget(constraint, type, attributeDomain);
        }
        this.render();
    }

    addWidget(constraint, type, domain) {
        let newWidget;
        if (type == PropertyTypesEnum.NOMIMAL) {
            newWidget = new nominalConstraintWidget(constraint, domain, this.updateWidgetCb, this.removeWidget);
        } else if (type == PropertyTypesEnum.CATEGORICAL) {
            newWidget = new CategoricalConstraintWidget(constraint, domain, this.updateWidgetCb, this.removeWidget);
        } else if (type == PropertyTypesEnum.NUMERIC) {
            newWidget = new numericConstraintWidget(constraint, domain, this.updateWidgetCb, this.removeWidget);
        }
        this.widgets.push(newWidget);
    }

    updateWidgetCb = (constraint, trueChange = true) => {
        if (trueChange) {
            let label = `${this.entityId}.${constraint.property} change`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(label)(this.astController.ast.toCypher()));
        } else {
            this.astController.constraintModification(false);
        }
    }

    render() {
        this.containerElement
            .html("")
        this.containerElement.append(() => this.headerElement.node());

        this.nestedElementsContainer = this.containerElement
            .append("div")
            .classed(EntityWidgets.entityWidgetsClass(this.entityId), true)
            .classed("ui segments", true)

        this.widgets.forEach((widget) => {
            this.nestedElementsContainer.append(() => widget.mainDiv.node());
            widget.initEvents();
        });

        this.initNewWidgetArea();
        this.nestedElementsContainer.append(() => this.widgetCreationDiv.node());
    }
}

export {EntityWidgets, CypherWidgets};
