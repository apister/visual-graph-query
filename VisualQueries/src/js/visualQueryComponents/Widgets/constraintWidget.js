import * as d3 from 'd3';
import * as cypherlib from 'cypher-compiler-js';
import {globals} from "../../globals";

export default class ConstraintWidget {
    constructor(constraint, domain, updateCb, removeCb) {
        this.constraint = constraint;
        this.domain = domain;
        this.property = this.constraint.property;
        this.entityId = this.constraint.entityId;
        this.updateCb = updateCb;
        this.removeCb = removeCb;

        this.mainDiv = d3.create("div")
            .classed("widget ui segment", true)
            // .style("padding-top", "4px")
            // .style("padding-bottom", "4px")

        this.initWidget();
        this.initCross();
    }

    static widgetClass(entityId, property) {
        return `widget-${entityId}-${property}`;
    }

    widgetClass() {
        return ConstraintWidget.widgetClass(this.constraint.entityId, this.constraint.property);
    }

    initWidget() {
        this.mainDiv
            .classed(ConstraintWidget.widgetClass(this.entityId, this.property), true)
            .style("background-color", globals.MENU_COLOR)
            .style("padding-right", "50px")
            .append("p")
            .style("font-size", "130%")
                .html(this.property)
    }

    initCross() {
        this.closeButton = this.mainDiv
            .append("i")
            .classed("window close outline icon", true)
                .style("position", "absolute")
                .style("right", "10px")
                .style("top", "50%")
                .on("click", (e) => {
                    this.removeCb(this);
                });
    }

    initEvents() {
        //
    }

    update() {
        if (this.constraint instanceof cypherlib.SimpleConstraint) {
            this.constraint.treeNode.disabled = false;
        } else if (this.constraint instanceof cypherlib.IntervalConstraint) {
            this.constraint.treeNode1.disabled = false;
            this.constraint.treeNode2.disabled = false;
        }
    }
}