import * as d3 from 'd3';

import ConstraintWidget from "./constraintWidget.js";
import CategoricalConstraintWidget from "./categoricalConstraintWidget.js";

class nominalConstraintWidget extends ConstraintWidget {
    constructor(constraint, domain, updateCb, removeCb) {
        super(constraint, domain, updateCb, removeCb);
        this.mainDiv.classed("nominal", true)
        // this.selectedValues = constraint.value ? (Array.isArray(constraint.value) ? constraint.value : [constraint.value]) : [];
        this.fillWithSelectedValue();
    }

    initWidget() {
        // this.selectedValues = this.constraint.value ? (Array.isArray(this.constraint.value) ? this.constraint.value : [this.constraint.value]) : [];
        if (this.constraint.value && this.constraint.op == "<>") {
            this.selectedValues = [CategoricalConstraintWidget.noNullValue];
        } else if (this.constraint.value) {
            if (Array.isArray(this.constraint.value)) {
                this.selectedValues = this.constraint.value;
            } else {
                this.selectedValues = [this.constraint.value];
            }
        } else {
            this.selectedValues = [];
        }

        super.initWidget();
        this.initNotNullValueOption();

        this.input = this.mainDiv
            .append("input")
            .attr("list", this.widgetClass())
            .attr("size", "5")
                .on("change", (e) => {
                    let newValue = e.target.value;
                    if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
                        this.selectedValues = [newValue];
                    } else if (!this.selectedValues.includes(newValue)) {
                        // this.addNewValue(newValue);
                        this.selectedValues.push(newValue);
                    }
                    this.update();
                })

        this.datalist = this.input
            .append("datalist")
            .attr("id", this.widgetClass());

        this.domain.sort();
        this.domain.forEach(v => {
            this.datalist.append("option")
                .attr("value", v);
        })

        this.mainDiv.append("span").html("  ");

        this.enteredValueDiv = this.mainDiv
            .append("div")
            .style("display", "inline")
    }

    initNotNullValueOption() {
        let text = "No Null Value";

        let checkbox = this.mainDiv.append("div").append("div")
            .classed("ui checkbox", true)

        let input = checkbox.append("input")
            .attr("type", "checkbox")
            .attr("value", text)

        if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
            checkbox.classed("checked", true);
            input.attr("checked", "");
        }
        checkbox.append("label").html(text);

        checkbox.on("click", (e) => {
            if (this.selectedValues.includes(CategoricalConstraintWidget.noNullValue)) {
                this.selectedValues = [];
            } else {
                this.selectedValues = [CategoricalConstraintWidget.noNullValue];
            }
            this.update();
            // this.setNoNullValue();
        })
    }

    addNewValue(newValue) {
        this.selectedValues.push(`${newValue}`);
        this.valueToDiv(newValue);
        this.update();
    }

    valueToDiv(value) {
        this.enteredValueDiv
            .append("div")
                .html(value)
            .classed("ui label", true)
            .style("margin", '3px 5px');

        this.enteredValueDiv
            .append("i")
            .classed("window close outline icon", true)
                // .style("position", "absolute")
                // .style("right", "10px")
                // .style("top", "50%")
                .on("click", (e) => {
                    this.selectedValues = this.selectedValues.filter(v => v != value);
                    // this.removeCb(this);
                    this.update();
                });

    }

    fillWithSelectedValue() {
        if (this.selectedValues.length > 0) {
            this.selectedValues.forEach(v => {
                if (v != CategoricalConstraintWidget.noNullValue) {
                    this.valueToDiv(v);
                }
            })
        }
    }

    update() {
        super.update();

        if (this.selectedValues[0] == CategoricalConstraintWidget.noNullValue) {
            this.constraint.op = "<>";
            this.constraint.right = "null";
            this.constraint.treeNode.op = "<>";
            this.constraint.treeNode.right = "null"
        } else {
            this.constraint.update(this.selectedValues);
        }

        // this.constraint.update(this.selectedValues);
        this.updateCb(this.constraint);
    }
}

export {nominalConstraintWidget}


