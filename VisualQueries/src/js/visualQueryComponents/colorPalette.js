import * as d3 from 'd3';

import {globals} from '../globals.js';

class colorPalette {
    constructor(divElement, d3Scale, docTypeToRoles, width) {
        this.element = divElement;
        this.docTypeToRoles = docTypeToRoles;
        this.width = width;

        this.roleHeight = 15;
        this.offsetTop = 10;
        this.rectWidth = 20;
        this.rectHeight = 10;
        this.fontSize = "14px";
        this.fontSizeSelected = "18px";

        this.colorScale = d3Scale;
        this.addAnyColor();
        this.attributeDomain = this.colorScale.domain();

        this.computeHeight();
        this.initElement();

        this.rects = null;
        this.initPalette();

        this.selectedEdgeType = this.attributeDomain[0];

        if (this.docTypeToRoles) {
            this.highlight(null, [this.selectedEdgeType, 0]);
        } else {
            this.highlight(null, this.selectedEdgeType);
        }

        this.selectedEdgeTypeColor = this.colorScale(this.selectedEdgeType);
        this.setupClickEvents();
    }

    computeHeight() {
        if (this.docTypeToRoles) {
            this.height = this.attributeDomain.length * this.roleHeight + Object.keys(this.docTypeToRoles).length * this.roleHeight + this.offsetTop;
        } else {
            this.height = this.attributeDomain.length * (this.roleHeight+ 2) + this.offsetTop * 2 + 20;
        }
    }

    initElement() {
        this.element.html("");

        this.element.append("div")
            .classed("legend-title", true)
            .text("Link Types")
            .style("font-size", `${globals.FONT_SIZE + 3}px`)

        this.svg = this.element
            .append("div")
            .append("svg")
            .attr("width", this.width)
            .attr("height", this.height)
    }

    addAnyColor() {
        // this.colorScale.domain([globals.ANY_EDGETYPE, ...this.colorScale.domain()]);
        // this.colorScale.range([globals.ANY_EDGETYPE_COLOR, ...this.colorScale.range()]);

        this.colorScale.domain([...this.colorScale.domain(), globals.ANY_EDGETYPE]);
        let domain = this.colorScale.domain();
        let range = this.colorScale.range();
        range[domain.length - 1] = globals.ANY_EDGETYPE_COLOR;
        this.colorScale.range(range);

        if (this.docTypeToRoles) {
            this.docTypeToRoles[globals.ANY_EDGETYPE] = [globals.ANY_EDGETYPE];
        }
    }

    initPalette() {
        if (!this.docTypeToRoles) {
            this.rects = this.svg
                .selectAll("rect")
                .data(this.attributeDomain)
                .join("rect")
                .attr("width", 20)
                .attr("height", 10)
                .attr("x", 0)
                // .attr("y", (d, i) => i * 20 + 2)
                .attr("y", (d, i) => i * 20 + this.offsetTop)
                .attr("fill", (d, i) => this.colorScale(d))
                // .attr("stroke", globals.SELECTION_COLOR)
                .attr("stroke", "black")
                .attr("stroke-width", 0)
                .style("cursor", "pointer")
                .on("mouseover", (e, d) => {
                    this.highlight(e, d)
                })
                .on("mouseout", (e, d) => {
                    if (this.selectedEdgeType != d) this.unHighlight(e, d);
                })

            this.texts = this.svg
                .selectAll("text")
                .data(this.attributeDomain)
                .join("text")
                .attr("x", 22)
                // .attr("y", (d, i) => (i * 20) + 10 + 2)
                .attr("y", (d, i) => (i * 20) + 10 + this.offsetTop)
                .style("font-size", globals.FONT_SIZE)
                .style("cursor", "pointer")
                .text(d => d)
                .on("mouseover", (e, d) => {
                    this.highlight(e, d)
                })
                .on("mouseout", (e, d) => {
                    if (this.selectedEdgeType != d) this.unHighlight(e, d);
                })
        } else {
            this.svg
            .selectAll(".docType")
            .data(Object.entries(this.docTypeToRoles))
            .join((enter) => {
                let group = enter.append("g")
                    .classed("docType", true)

                group.append("text")
                    .text(d => d[0])
                    .attr("x", 0)
                    .attr("y", (d, i) => {
                        let nRolesBefore = this.numberOfRolesBefore(i);
                        return (nRolesBefore + i) * this.roleHeight + this.offsetTop;
                    })
                    .style("font-style", "italic")

                return group
            })
            .selectAll(".roles-group")
            .data((d, i) => {
                return d[1].map(role => [role, i])
            })
            .join((enter) => {
                let group = enter.append("g")
                    .classed("roles-group", true)

                group.append("rect")
                    .attr("width", 20)
                    .attr("height", this.roleHeight - 5)
                    .attr("x", 0)
                    .attr("y", (d, i) => {
                        let [role, index] = d;
                        let nRolesBefore = this.numberOfRolesBeforeValue(role);

                        return (nRolesBefore) * this.roleHeight + (index * this.roleHeight) + this.offsetTop + 5;
                    })
                    .attr("fill", (d, i) => this.colorScale(d[0]))
                    .attr("stroke", "black")
                    .attr("stroke-width", 0)
                    .on("mouseover", (e, d) => {
                        this.highlight(e, d)
                    })
                    .on("mouseout", (e, d) => {
                        if (this.selectedEdgeType != d[0]) this.unHighlight(e, d);
                    })

                group
                    .append("text")
                    .classed("role-text", true)
                    .attr("x", 22)
                    .attr("y", (d, i) => {
                        let [role, index] = d;
                        let nRolesBefore = this.numberOfRolesBeforeValue(role);

                        return (nRolesBefore) * this.roleHeight + (index * this.roleHeight) + this.offsetTop + this.roleHeight;
                    })
                    .style("font-size", globals.FONT_SIZE)
                    .style("cursor", "pointer")
                    .text(d => d[0])
                    .on("mouseover", (e, d) => {
                        this.highlight(e, d)
                    })
                    .on("mouseout", (e, d) => {
                        if (this.selectedEdgeType != d[0]) this.unHighlight(e, d);
                    })

                return group
            })

            this.rects = this.svg.selectAll("rect")
            this.texts = this.svg.selectAll(".role-text")
        }
    }

    numberOfRolesBefore(i) {
        if (i == 0) return 0;
        let roles = Object.values(this.docTypeToRoles);
        roles.splice(i);
        roles = roles.reduce((a, b) => a.concat(b));
        return roles.length;
    }

    numberOfRolesBeforeValue(role) {
        let roles = Object.values(this.docTypeToRoles).reduce((a, b) => a.concat(b));
        let index = roles.indexOf(role);
        return index;
    }

    highlight = (ev, datum) => {
        this.rects.filter((d) => JSON.stringify(d) == JSON.stringify(datum))
            .style("stroke-width", 3)

        this.texts.filter((d) => JSON.stringify(d) == JSON.stringify(datum))
            .style("font-size", this.fontSizeSelected)
            .attr("font-weight", "bold");
    }

    unHighlight = (ev, datum) => {
        this.rects.filter((d) => JSON.stringify(d) == JSON.stringify(datum))
            .style("stroke-width", 0)

        this.texts.filter((d) => JSON.stringify(d) == JSON.stringify(datum))
            .style("font-size", this.fontSize)
            .attr("font-weight", "normal");
    }

    unHighlightAll() {
        this.rects
            .style("stroke-width", 0)

        this.texts
            .style("font-size", this.fontSize)
            .attr("font-weight", "normal");
    }

    setupClickEvents() {
        this.rects.on("click", (e, d) => {
            let role;
            if (this.docTypeToRoles) {
                role = d[0];
            } else {
                role = d;
            }
            // this.unHighlight(e, this.selectedEdgeType);
            this.unHighlightAll();
            this.highlight(e, d);
            this.selectedEdgeType = role;
            this.selectedEdgeTypeColor = this.colorScale(this.selectedEdgeType);
        })

        this.texts.on("click", (e, d) => {
            let role;
            if (this.docTypeToRoles) {
                role = d[0];
            } else {
                role = d;
            }
            // this.unHighlight(e, this.selectedEdgeType);
            this.unHighlightAll();
            this.highlight(e, d);
            this.selectedEdgeType = role;
            this.selectedEdgeTypeColor = this.colorScale(this.selectedEdgeType);
        })
    }
}

export {colorPalette};