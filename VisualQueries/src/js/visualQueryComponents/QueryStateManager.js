import * as d3 from 'd3';
import '../../../node_modules/codemirror/mode/cypher/cypher.js'
import "../../../node_modules/codemirror/addon/search/searchcursor.js";

import {globals} from "../globals.js";
import CypherEditor from "./CypherEditor";
import WidgetsCollection from "./Widgets/WidgetsCollection.js";
import {QueryGraphPainter} from "./queryGraphPainter";
import AstController from "./AstController.js";
import ProvenanceManager from "./ProvenanceManager.js";
import SubgraphToAstTransformer from "./subgraphToAstTransformer";
import QueryResultsComponent from "./QueryResultsComponent";
import {generateLinkIdFromLinkList} from "../utils/utils";
import {ComparisonManager} from "./comparisonManager";
import {Property} from "dynbipgraph";


class MatchingResult {
    constructor() {
        this.matchOccurences = [];
        this.nodeIds = [];
        this.linkIds = [];
    }

    isEmpty() {
        return this.matchOccurences.length == 0;
    }

    addOccurrence(occurrence) {
        this.matchOccurences.push(occurrence);
    }

    addNode(nodeId) {
        this.nodeIds.push(nodeId);
    }

    addNodes(nodeIds) {
        this.nodeIds.push(...nodeIds);
    }

    addLinks(linkIds) {
        this.linkIds.push(...linkIds);
    }

    addLink(linkId) {
        this.linkIds.push(linkId);
    }

    getNodesByType(type, mainGraph) {
        return this.nodeIds.map(nId => mainGraph.idToNode[nId]).filter(node => node.type == type);
    }
}


class QueryStateManager {
    constructor(mainGraph, graphVisualizer, documentGraphVisualizer, selector) {
        this.mainGraph = mainGraph;
        this.visualizer = graphVisualizer;
        this.mainGraphVis = this.visualizer.graphVis;
        this.documentGraphVisualizer = documentGraphVisualizer;
        this.mainGraphVis.setupLasso(this.lassoSelectItemsCb);
        this.selector = selector;

        this.initSelections();
        this.astController = new AstController(null, this.updateCypherFromAst, this.updateAstFromCypher, this.errorAst, this.updateVisualQuery);
        this.cypherEditor = new CypherEditor(this.astController, this.requestDatabaseProgressive);

        this.provenance = new ProvenanceManager(this.astController, this.cypherEditor);
        this.astController.setProvenanceManager(this.provenance);

        this.graphPaint = new QueryGraphPainter(this.paintSvg, this.mainGraph, this.mainGraphVis, this);
        this.widgetsCollection = new WidgetsCollection(this.widgetsCollectionsSelection, this.mainGraph, this.graphPaint, this.astController, this.provenance);

        this.queryResultMain = new QueryResultsComponent(d3.select("#results-tabs-main"), this.mainGraph, this.mainGraphVis, "", "#query-result-datatable", "#div-results", "#tab-menu-main", this.selectAttribute, this.deselectAttribute, this.selector);

        this.comparaisonManager = new ComparisonManager(d3.select("#comparison-div"), this.mainGraphVis, this.documentGraphVisualizer, this.queryResultMain, null, this.provenance, this.selectAttribute, this.deselectAttribute);
        this.comparaisonManager.init();

        this.cypherEditor.start(this.cypherFirstRequest());

        // TODO: merge functions
        this.provenance.init();
        this.provenance.setupGraphVis(this.comparaisonManager.initCompareOneCanvas, this.comparaisonManager.closeCompareOneCanvas, this.clickProvenanceTreeCb);

        this.provenance.setComparisonManager(this.comparaisonManager);

        this.initGroupByEvent();
        this.initButtons();
        this.setupUndoEventDetection();

        this.loaderMainGraph = d3.select("#main-graph-loader");
    }

    clickProvenanceTreeCb = () => {
        this.graphPaint.unconnectedNodes = [];
    }

    cypherFirstRequest() {
        let personId = this.graphPaint.generateId(this.mainGraph.personType);
        let documentId = this.graphPaint.generateId(this.mainGraph.documentType);

        return `MATCH (${personId}:${this.mainGraph.personType})-[l]-(${documentId}:${this.mainGraph.documentType}) RETURN *`;
    }

    initSelections() {
        this.widgetsCollectionsSelection = d3.select("#div-widgets-collection");
        this.paintSvg = d3.select('#svg-paint');
        this.groupByButton = d3.select('#btn-groupby');
    }

    initButtons() {
        this.buttonsContainer = d3.select("#visual-query-buttons");
        this.buttonsContainer.html("");
        this.buttons = this.buttonsContainer
            .append("div")
            .classed("ui buttons", true)

        // this.initRunButton();
        this.initUndoButton();
        this.initExampleButton();

        // this.cypherEditor.initImportExportButtons(this.buttons);
        // this.initProvenanceButtons()
        this.provenance.initButtons();
    }

    initExampleButton() {
        let button = this.buttonsContainer
            .append("div")
            .classed("ui tiny button", true)
            .style("float", "right")
            .on("click", () => this.startExample2())
        // button.node().innerHTML += "Example2";
        button.node().innerHTML += this.example2Name();

        let button2 = this.buttonsContainer
            .append("div")
            .classed("ui tiny button", true)
            .style("float", "right")
            .on("click", () => this.startExample1())
        // button2.node().innerHTML += "Example1";
        button2.node().innerHTML += this.example1Name();
    }

    startExample1() {
        let example;
        if (globals.dataset == "Nicole") {
            example = "MATCH (per1:PERSON)-[l:father|mother]-(doc1:DOCUMENT)-[l3:child]-(per2:PERSON) RETURN *"
        } else {
            example = `MATCH (doc2:${this.mainGraph.documentType})-[l3:Approbator]-(per1:${this.mainGraph.personType})-[l:Guarantor]-(doc1:${this.mainGraph.documentType}) RETURN *`
        }

        this.provenance.apply(this.provenance.customCypherAction("Example")(example));
    }

    startExample2() {
        let example
        if (globals.dataset == "Nicole") {
            example = `MATCH (doc2:DOCUMENT)-[l3:witness]-(per1:PERSON)-[l:child]-(doc1:DOCUMENT), (doc2) WHERE doc1.departement <> "null" AND doc2.departement <> "null" RETURN *`;
        } else {
            example = `MATCH (doc2:${this.mainGraph.documentType})-[l5]-(per1:${this.mainGraph.personType})-[l:Guarantor]-(doc1:${this.mainGraph.documentType})-[l3]-(per2:${this.mainGraph.personType})-[l6:Guarantor]-(doc2) RETURN *`
        }
        this.provenance.apply(this.provenance.customCypherAction("Example")(example));
    }

    example1Name() {
        if (globals.dataset == "Nicole") {
            return "One Family";
        } else {
            // return "Approbator and Guarantor";
            return "2 roles";
        }
    }

    example2Name() {
        if (globals.dataset == "Nicole") {
            return "Localized birth and death";
        } else {
            return "Mutual Guarantors";
        }
    }

    initRunButton() {
        let button = this.buttons
            .append("div")
            .classed("ui button", true)
            .style("margin-right", "20px")
            .on("click", () => this.runCb())
        button.node().innerHTML += "Run";
    }

    runCb() {
        this.provenance.apply(this.provenance.customCypherAction("test")(this.astController.ast.toCypher()));
    }

    initUndoButton() {
        let button = this.buttons
            .append("div")
            .classed("ui labeled icon button", true)
            .attr("title", "CTRL+Z")
            .on("click", () => this.undo())
        button
            .append("i")
            .classed("undo icon", true)
        button.node().innerHTML += "Undo";
    }

    setupUndoEventDetection() {
        document.addEventListener('keydown', (event) => {
            if (event.ctrlKey && event.key === 'z') {
                this.undo();
            }
        });
    }

    undo() {
        if (this.graphPaint.haveUnconnectedNodes()) {
            this.graphPaint.removeUnconnectedNode();
        } else {
            this.provenance.undo();
        }
    }

    initGroupByEvent() {
        this.groupByButton
            .on('click', () => {
                this.mainGraph.groupBySubgraphs(this.matchingResult, this.matchedNodes, this.matchedLinks);
                this.mainGraphVis.simulation.init();
                this.mainGraphVis.simulation.start();
                this.mainGraphVis.simulation.restart();
                // this.mainGraphVis.render();
            })
    }

    updateCypherFromAst = (request = true) => {
        this.astController.isManualChange = false;
        let cypherHTML = this.astController.ast.toCypherHTML();
        this.cypherEditor.codeMirror.getDoc().setValue(cypherHTML);

        if (request) this.requestDatabase();
    }

    updateVisualQuery = () => {
        this.ast = this.astController.ast;
        this.subgraph = this.astController.ast.scopes[0].graph();
        this.processAstGraph();
        this.graphPaint.init(this.subgraph);

        // Visual constraints creation
        if (!(this.astController.isWidgetChange)) {
            this.updateWidgets();
        }
    }

    updateAstFromCypher = (provenanceCall) => {
        this.ast = this.astController.ast;

        if (provenanceCall && !(this.astController.isManualChange)) {
            this.updateCypherFromAst(false);
        }

        // graph paint creation
        this.subgraph = this.astController.ast.scopes[0].graph();
        this.processAstGraph();
        this.graphPaint.init(this.subgraph);

        // Visual constraints creation
        if (!(this.astController.isWidgetChange)) {
            this.updateWidgets();
        }

        // Database request
        this.requestDatabase();
    }

    async requestDatabase() {
        this.loaderMainGraph.classed("active", true);

        let cypherTransformed = this.astController.preRequestTransform();
        // this.requestDatabaseProgressive(this.astController.cypher);
        // this.requestDatabaseOneshot(this.astController.cypher);
        // this.requestDatabaseTwoTimes(this.astController.cypher);

        this.requestDatabaseTwoTimes(cypherTransformed)
            .then(_ => {
                this.loaderMainGraph.classed("active", false);
            });
    }

    errorAst = (err) => {
        this.astController.error = true;
        this.cypherEditor.errorSelection.node().textContent = err;
    }

    updateWidgets() {
        this.astController.ast.transformWheres();
        this.widgetsCollection.initFromConstraints(this.ast.constraintsCollection);
        this.widgetsCollection.render();
        // this.widgetsCollection.render();
    }

    processAstGraph() {
        this.subgraph.links = this.subgraph.links.map(l => {
            if (l.label == null) {
                l.label = globals.ANY_EDGETYPE;
            }
            return l
        })

        // this.subgraph['metadata'] = {
        //     "attributes": this.mainGraph.attributes,
        //     "nodeType": "label",
        //     "edgeType": "label"
        // };

        this.subgraph['metadata'] = {
            "attributes": this.mainGraph.attributes,
            "entityType": "label",
            "edgeType": "label"
        };
    }

    lassoSelectItemsCb = (selectedNodes) => {
        selectedNodes.forEach(item => item.selected = true);
        const selectedNodesIds = selectedNodes.map(n => `"${n.id}"`);

        fetch(globals.URL_BACKEND.toString() + '/cypher', {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: `MATCH (n)-[r]->(m) WHERE n.id in [${selectedNodesIds}] AND m.id in [${selectedNodesIds}] RETURN DISTINCT *`
        }).then(
            response => response.json()
        ).then(match => {
            let links = match.map(match => match["r"])
            this.subgraphToAst(selectedNodes, links);
        })
        //     .catch((error) => {
        //     console.log(error);
        // })
    }

    subgraphToAst(nodes, links) {
        let transformer = new SubgraphToAstTransformer(this.astController);
        transformer.run(nodes, links);
        this.provenance.apply(this.provenance.customCypherAction("lasso")(this.astController.ast.toCypher()));
    }

    requestDatabaseOneshot = async (input) => {
        this.queryResultMain.setLoader();

        this.mainGraphVis.highlightNodesOnly([]);
        this.mainGraphVis.highlightLinksOnly([]);

        fetch(globals.route("cypherMatch"), {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "mainGraph": this.mainGraph.data,
                "scriptRequest": input,
                "edgeTypes": this.mainGraph.edgeTypes,
                "edgeTypeKey": this.mainGraph.edgeTypeKey
            })
        }).then((response) => {
            if (response.status !== 200) {
                // if (this.errorDiv.html() == "") {
                //     this.errorDiv
                //         .html(response.status)
                //     this.queryResultTable.removeLoader();
                // }
            } else {
                response.json().then(
                    this.processMatching
                )
            }
        }).catch((error) => {
            if (this.cypherEditor.errorDiv.html() == "") {
                this.cypherEditor.errorDiv
                    .html(error)
                this.queryResultMain.removeLoader();
            }
        });
    }

    requestDatabaseTwoTimes = async (input) => {
        this.queryResultMain.setLoader();

        this.mainGraphVis.highlightNodesOnly([]);
        this.mainGraphVis.highlightLinksOnly([]);
        this.matchingResult = new MatchingResult();

        const response = await fetch(globals.URL_BACKEND.toString() + "/cypherMatchTwoTimes", {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "scriptRequest": input,
            })
        })

        const reader = response.body.getReader();

        this.processTwoTimesRequestOutput(reader)
            .then(() => {
                // console.timeEnd("Request")
            })
    }

    async processTwoTimesRequestOutput(reader) {
        this.lastValue = null;

        let i = 0;
        while (true) {
            const {value, done} = await reader.read();
            if (done) {
                break;
            }
            let chunk = this.decodeRequestChunk(value);
            if (chunk) {
                this.processTwoTimeChunk(chunk);
            }
            i++;
        }

        console.log(`finish reading stream after ${i} loops`);
        this.afterStreamProcessing();
    }

    async processTwoTimeChunk(chunk) {
        for (let v of chunk) {
            // console.log("v ", chunk)
            if (this.isRequestEntryGraphMetrics(v)) {
                console.log("process metric chunk")
                this.graphMetrics = v;
                this.queryResultMain.updateGraphMetrics(v);
            } else {
                console.log("process matched items")
                this.processMatchedItems(v);
                console.log("end process matched items")
            }
        }
    }

    processMatching = (matchingResponse) => {
        this.processMatchedItems(matchingResponse);
        this.processGraphMetrics(matchingResponse);

        if (this.comparaisonManager.isCompareMode()) {
            this.comparaisonManager.update();
        } else {
            this.queryResultMain.update(this.matchingResult, this.astController.nodes());
        }
    }

    processMatchedItems(matchingResponse) {
        let matching = matchingResponse["matching"];
        this.matchingResult = new MatchingResult();
        this.matchingResult.matchOccurences = matching["matching_list"];
        this.matchingResult.addNodes(matching["matched_nodes"]);
        this.matchingResult.addLinks(matching["matched_links"].map(l => generateLinkIdFromLinkList(l)));

        this.mainGraphVis.highlightNodes(this.matchingResult.nodeIds);
        this.mainGraphVis.highlightLinks(this.matchingResult.linkIds);
        this.documentGraphVisualizer.graphVis.highlightNodesOnly(this.matchingResult.nodeIds);
        this.documentGraphVisualizer.highlightLinksOnly(this.matchingResult.nodeIds);
        this.mainGraphVis.render();
        this.documentGraphVisualizer.render();

        this.provenance.setMatchingResult(this.provenance.getCurrentId(), this.matchingResult);
        this.comparaisonManager.setMatchingResult(this.matchingResult);
    }

    processGraphMetrics(matchingResponse) {
        this.graphMetrics = JSON.parse(matchingResponse["metrics"]);
        this.queryResultMain.updateGraphMetrics(this.graphMetrics);

        this.provenance.setGraphMetrics(this.provenance.getCurrentId(), this.graphMetrics);
        this.comparaisonManager.setGraphMetrics(this.graphMetrics);
    }

    requestDatabaseProgressive = async (input) => {
        this.queryResultMain.setLoader();

        this.mainGraphVis.highlightNodesOnly([]);
        this.mainGraphVis.highlightLinksOnly([]);

        this.matchingResult = new MatchingResult();

        const response = await fetch(globals.URL_BACKEND.toString() + "/cypherMatchStream", {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "mainGraph": this.mainGraph.data,
                "scriptRequest": input,
                "edgeTypes": this.mainGraph.edgeTypes,
                "edgeTypeKey": this.mainGraph.edgeTypeKey
            })
        })

        const reader = response.body.getReader();
        this.processStreamRequestOutput(reader)
            .then(() => {
                console.timeEnd("Request")
            })
    }

    async processStreamRequestOutput(reader) {
        this.lastValue = null;

        let i = 0;
        while (true) {
            const {value, done} = await reader.read();
            if (done) {
                break;
            }

            let chunk = this.decodeRequestChunk(value);
            if (chunk) {
                await this.processChunk(chunk);
            }

            i++;
        }

        // let id = setInterval(async () => {
        //     const {value, done} = await reader.read();
        //
        //     if (done) {clearInterval(id)}
        //
        //     let chunk = this.decodeRequestChunk(value);
        //     // console.log("chunk ", chunk);
        //     if (chunk) {
        //         await this.processChunk(chunk);
        //     }
        // }, 2000)

        console.log(`finish reading stream after ${i} loops`);
        this.afterStreamProcessing();
    }

    async afterStreamProcessing() {
        console.log("after stream")
        this.provenance.setMatchingResult(this.provenance.getCurrentId(), this.matchingResult);
        this.provenance.setGraphMetrics(this.provenance.getCurrentId(), this.graphMetrics);

        // console.log("MATCHING RESULT", this.matchingResult);
        console.log("after stream 2");
        this.comparaisonManager.setMatchingResult(this.matchingResult);
        this.comparaisonManager.setGraphMetrics(this.graphMetrics);

        console.log("after stream 3");
        if (this.comparaisonManager.isCompareMode()) {
            this.comparaisonManager.update()
        } else {
            this.queryResultMain.update(this.matchingResult, this.astController.nodes());
        }
        console.log("end after stream")
    }

    // TODO : in some rare case, the chunks are not correctly cut
    decodeRequestChunk(chunk) {
        let value = new TextDecoder("utf-8").decode(chunk);
        // console.log("V ", value);
        try {
            if (this.lastValue) {
                // console.log("merge ", value, this.lastValue);
                value = this.lastValue + value;
                this.lastValue = null;
            }
            // console.log("V ", value);
            let valueArray = value.split("\n").filter(v => v != "").map(v => JSON.parse(v));
            // console.log("V2 ", valueArray);
            return valueArray;
        } catch (e) {
            // console.log('err ', e);
            this.lastValue = value;
            return null
        }
    }

    async processChunk(chunk) {
        let i = 0;
        for (let v of chunk) {
            // try {
            //     if (i == 0) {
            //         if (v.length > 0) {
            //             // this.mainGraphVis.highlightNodes(v);
            //             this.matchingResult.addNodes(v);
            //         }
            //         i++;
            //     } else if (i == 1) {
            //         // console.log("links", v)
            //         if (v.length > 0) {
            //             let links = v.map(l => generateLinkIdFromLinkList(l));
            //             // this.mainGraphVis.highlightLinks(links);
            //             this.matchingResult.addLinks(links);
            //         }
            //         i--;
            //     }
            // } catch (e) {
            //
            // }

            if (this.isRequestEntryGraphMetrics(v)) {
                this.graphMetrics = v;
                this.queryResultMain.updateGraphMetrics(v);
            } else if (this.isRequestEntryMatchingResults(v)) {
                // console.log("END ", v);
                this.matchingResult.matchOccurences = v;
            } else {
                if (i == 0) {
                    // console.log("nodes", v)
                    if (v.length > 0) {
                        this.mainGraphVis.highlightNodes(v);
                        this.matchingResult.addNodes(v);
                    }
                    i++;
                } else if (i == 1) {
                    // console.log("links", v)
                    if (v.length > 0) {
                        let links = v.map(l => generateLinkIdFromLinkList(l));
                        this.mainGraphVis.highlightLinks(links);
                        this.matchingResult.addLinks(links);
                    }
                    i--;
                }
                // if (i == 0) {
                //     this.matchingResult.addOccurrence(v);
                // } else if (i == 1 && v.length > 0) {
                //     this.mainGraphVis.highlightNodes(v);
                //     this.matchingResult.addNodes(v);
                // } else if (i == 2) {
                //     // console.log("l ", v)
                //     if (v.length > 0) {
                //         let links = v.map(l => generateLinkIdFromLinkList(l));
                //         this.mainGraphVis.highlightLinks(links);
                //         this.matchingResult.addLinks(links);
                //     }
                //     i = -1;
                // }
                // i++
                // await sleep(0);
            }
        }
    }

    isRequestEntryMatchingResults(entry) {
        if (Array.isArray(entry) && typeof entry[0] === 'object' && !Array.isArray(entry[0]) && entry[0] !== null) {
            return true;
        } else {
            return false;
        }
    }

    isRequestEntryGraphMetrics(entry) {
        if ('Number_of_links' in entry) {
            return true;
        } else {
            return false;
        }
    }

    requestGraph = () => {
        fetch(globals.URL_ADJACENCY.toString(), {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "graph": this.graphPaint.graph
            })
        }).then((response) => {
            if (response.status !== 200) {
                //
            } else {
                response.json().then((json) => {
                        // console.log(json)
                    }
                )
            }
        })
    }

    setMatchedAttributes() {
        for (let node of this.mainGraph.nodes) {
            for (let queryEntityId of this.astController.nodesIds()) {
                node[this.matchedAttributeName(queryEntityId)] = false;
            }
        }

        this.matchingResult.matchOccurences.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (this.astController.nodesIds().includes(queryEntityId)) {
                    let entityId = matchedEntity.id;
                    this.setNodeMatchedAttribute(queryEntityId, entityId)
                }
            }
        })
    }

    setNodeMatchedAttribute(queryEntityId, graphEntityId) {
        this.mainGraph.idToNode[graphEntityId][this.matchedAttributeName(queryEntityId)] = true;
    }

    highlight(entityId) {
        this.highlightQueryEntity(entityId);
        this.highlightCypherPart(entityId);
    }

    unhighlight(entityId) {
        this.unhighlightQueryEntity(entityId);
        this.unhighlightCypherPart(entityId);
    }

    highlightCypherPart(entityId) {
        d3.select(".CodeMirror")
            .style("background", "rgba(0, 0, 0, 0.2)");

        this.cypherEditor.highlightCypherPart(entityId);
    }

    unhighlightCypherPart() {
        d3.select(".CodeMirror")
            .style("background", "rgba(0, 0, 0, 0)");
        this.cypherEditor.unhighlightCypherPart();
    }

    // TODO : works only for node, todo for links
    highlightQueryEntity(entityId) {
        if (this.matchingResult && !this.matchingResult.isEmpty() && this.matchingResult.matchOccurences[0][entityId]) {
            this.selectedEntityIds = [];
            this.matchingResult.matchOccurences.forEach((match) => {
                this.selectedEntityIds.push(match[entityId]["id"]);
            })
            this.mainGraphVis.selectNodesOnly(this.selectedEntityIds);
        }
        this.mainGraphVis.render();
    }

    unhighlightQueryEntity(entityId) {
        this.mainGraphVis.selectNodesOnly([]);
        this.mainGraphVis.render();
    }

    selectAttribute = (property) => {
        this.applyNodeColorScale(property);

        if (property.domain.length < 40) {
            this.mainGraphVis.attributeLegend.fillLegendPanel(property);
        }
    }

    deselectAttribute = (property, stillSelectedAttribute = null) => {
        this.applyNodeColorScale(stillSelectedAttribute);
        this.mainGraphVis.attributeLegend.fillLegendPanel(stillSelectedAttribute);
    }

    applyNodeColorScale = (property) => {
        let propertyName = property ? property.name : null;
        let colorScale = property ? property.colorScale : null;

        this.mainGraphVis.setNodeAttributeToRender(propertyName, colorScale);
        this.documentGraphVisualizer.graphVis.setNodeAttributeToRender(propertyName, colorScale);
        this.graphPaint.setNodeAttributeToRender(propertyName, colorScale);

        this.mainGraphVis.render();
        this.documentGraphVisualizer.graphVis.render();
        this.graphPaint.graphVis.render()
    }
}

export {QueryStateManager}