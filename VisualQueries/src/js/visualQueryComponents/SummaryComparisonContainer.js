import * as vega from 'vega';
import * as vegaLite from 'vega-lite';
import * as vl from 'vega-lite-api';
import 'vega-tooltip';
import embed from "vega-embed";
import * as d3 from 'd3';
import $ from "jquery";

import {Table, GraphMetricsTable} from "../components/Table";
import SummaryContainer from "./SummaryContainer";
import SankeyVis from "./sankeyVis";
import {globals} from "../globals";


export default class SummaryComparisonContainer extends SummaryContainer {
    constructor(element, graphData, properties, plotIdsSuffixe = "", selectAttributeCb, deselectAttributeCb) {
        super(element, graphData, properties, plotIdsSuffixe, selectAttributeCb, deselectAttributeCb);
        this.gridWidth = ["five", "five", "six"];
    }

    setMatchingResultCompare(matchingResult) {
        this.matchingResultCompare = matchingResult;
    }

    // plotsContainerWidth() {
    //     return this.plotsContainer.node().clientWidth - this.plotsContainer.node().clientHeight * 0.12;
    // }
    //
    // plotsContainerHeight() {
    //     return this.plotsContainer.node().clientHeight - this.plotsContainer.node().clientHeight * 0.12;
    // }

    initGraphMetrics() {
        this.title = this.graphMetricsContainer
            .append("div")
            .classed("title", true)
        this.title.append("span")
            .style("font-size", "150%")
            .html("Graph Measures");

        this.graphMetricsContainer = this.graphMetricsContainer
            .append("div")
            .classed("content", true)

        this.graphMetricsTable = new GraphMetricsTable(this.graphMetricsContainer);
    }

    initUniquePlotsButtons(queryNodes, queryNodesB, nodesData) {
        this.queryNodes = queryNodes;
        this.queryNodesIds = queryNodes.map(n => n.id);

        this.queryNodesB = queryNodesB;
        this.queryNodesBIds = queryNodesB.map(n => n.id);

        this.nodesData = nodesData;

        this.attributeSelecionContainer.html("");
        this.plotsContainer.html("");
        this.attributeMenu.init();
    }

    renderGraphMetrics(graphMetrics, graphMetricsCompare) {
        this.graphMetricsTable.render([graphMetrics, graphMetricsCompare]);
    }

    generateNumberUniquePlot(property, data, nodeType) {
        let xVega;
        if (this.isNumericPropertyInt(property)) {
            xVega = vl.x().fieldO(property.name).title(property.name);
            if (property.domain[1] - property.domain[0] > 30) {
                xVega = xVega.bin(true);
            }
        } else {
            xVega = vl.x().fieldQ(property.name).title(property.name).bin(true);
        }
        xVega = xVega.axis({labelAngle: 0});

        let chartSpec;
        if (nodeType == "ALL" || nodeType == "All") { // TODO
            chartSpec = this.numberUniquePlotTransforms(data, property, this.attributeMenu.showGlobalDistribution, true)
            .encode(
                xVega,
                vl.y().count().title("count"),
                vl.row().title(null).fieldN("matched"),
                vl.column().title(null).fieldN("type"),
                // vl.color().fieldN(property.name).scale(colorScale),
                vl.tooltip().count()
            )
            .width(this.numberPlotWidth() / 2)
            .height(this.numberPlotHeight() / 2)
        } else {
            chartSpec = this.numberUniquePlotTransforms(data, property, this.attributeMenu.showGlobalDistribution, false)
            .encode(
                xVega,
                vl.y().count().title("count"),
                vl.row().title(null).fieldN("matched"),
                vl.tooltip().count()
            )
            .width(this.numberPlotWidth())
            .height(this.numberPlotHeight())
        }

        chartSpec = JSON.parse(chartSpec);

        const divId = this.generateNewDivPlot('', property.name);
        embed(`#${divId}`, chartSpec);
    }

    generateCategoricalUniquePlot(property, data) {
        let colorScale = this.processColorScale(property);

        if (this.attributeMenu.plotSankey) {
            try {
                let sankeyA = new SankeyVis(this.matchingResult.matchOccurences, this.queryNodes, this.plotsContainerWidth() * 0.8, this.categoricalPlotHeight() / 2, globals.mainQueryColorLowOpacity);
                let sankeyB = new SankeyVis(this.matchingResultCompare.matchOccurences, this.queryNodesB, this.plotsContainerWidth() * 0.8, this.categoricalPlotHeight() / 2, globals.comparisonQueryColorLowOpacity);

                sankeyA.processData(property);
                sankeyB.processData(property);
                let sankeySvgA = sankeyA.render();
                let sankeySvgB = sankeyB.render();

                const divId = this.generateNewDivPlot('', property.name);
                let plotDiv = d3.select(`#${divId}`)

                plotDiv
                    .append(() => sankeySvgA.node());
                plotDiv
                    .append("span")
                    .style("color", globals.mainQueryColor)
                    .style("font-size", "130%")
                    .style("padding", "10px")
                    .html("A")

                plotDiv
                    .append(() => sankeySvgB.node());
                plotDiv
                    .append("span")
                    .style("color", globals.comparisonQueryColor)
                    .style("font-size", "130%")
                    .style("padding", "10px")
                    .html("B")

            } catch (e) {
                this.generateCategoricalBarChart(data, property, colorScale);
            }
        } else {
            this.generateCategoricalBarChart(data, property, colorScale);
        }
    }

    generateCategoricalBarChart(data, property, colorScale) {
        let chartSpec = this.categoricalUniquePlot(data, property, colorScale);

        chartSpec = JSON.parse(chartSpec);
        const divId = this.generateNewDivPlot('', property.name);

        embed(`#${divId}`, chartSpec);
    }

    categoricalUniquePlot(data, property, colorScale) {
        return this.categoricalUniquePlotTransforms(data, this.attributeMenu.showGlobalDistribution)
            .encode(
                vl.x().title(null).fieldN("matched").sort(["all graph", "A", "B"]).axis({labelAngle: 0}),
                vl.y().count().stack("normalize").title("count"),
                // vl.y().title(null).count(),
                vl.color().fieldN(property.name).scale(colorScale),
                vl.tooltip().count()
            )
            .width(this.categoricalPlotWidth())
            .height(this.categoricalPlotHeight())
    }
}