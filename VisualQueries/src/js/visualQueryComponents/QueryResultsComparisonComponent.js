import * as d3 from 'd3';

import {QueryResultTable} from "./QueryResultTable";
import {globals} from "../globals.js";
import SummaryComparisonContainer from "./SummaryComparisonContainer";
import _ from "lodash";
import {DocumentsTable, PersonsTable, PersonsComparisonTable, DocumentsComparisonTable} from "../components/Table";


export default class QueryResultsComparisonComponent {
    constructor(mainGraphVis, matchingResult, matchingResultCompare, selectAttributeCb, deselectAttributeCb) {
        this.mainGraphVis = mainGraphVis;
        this.mainGraph = this.mainGraphVis.graph;
        this.selector = this.mainGraphVis.selector;
        this.matchingResult = matchingResult;
        this.matchingResultCompare = matchingResultCompare;
        this.selectAttributeCb = selectAttributeCb;
        this.deselectAttributeCb = deselectAttributeCb;

        this.container = d3.select("#result-tabs-comparison");

        this.initTables();

        this.properties = this.mainGraph.propertiesByNodeType();
        this.plotIdsSuffixe = "comparison";
        this.summaryContainerId = `#div-results-comparison`;
        this.summaryContainer = new SummaryComparisonContainer(this.summaryContainerId, this.mainGraph, this.properties, this.plotIdsSuffixe, this.selectAttributeCb, this.deselectAttributeCb);
        this.summaryContainer.initContainer();

        this.tabMenuSelector = "#tab-menu-comparison";
        this.graphMetrics = null
    }

    show() {
        this.container
            .style("display", "block")
    }

    remove() {
        this.container
            .style("display", "none")
    }

    initTables() {
        this.personProperties = this.mainGraph.getPropertiesByNodeType(this.mainGraph.personType);
        this.documentProperties = this.mainGraph.getPropertiesByNodeType(this.mainGraph.documentType);

        this.personTable = new PersonsComparisonTable(d3.select("#persons-table-container-comp"), this.personProperties, "person-comp-table", this.selector);
        this.documentTable = new DocumentsComparisonTable(d3.select("#documents-table-container-comp"), this.documentProperties, "document-comp-table", this.selector);
    }

    update = (matchingResultA, matchingResultB, astNodes, astNodesCompare, graphMetricsA, graphMetricsB ) => {
        this.matchingResult = matchingResultA;
        this.graphMetrics = graphMetricsA;
        this.matchingResultCompare = matchingResultB;
        this.graphMetricsCompare = graphMetricsB;

        this.astNodes = astNodes;
        this.astNodesCompare = astNodesCompare;
        this.astNodesIds = astNodes.map(n => n.id);
        this.astNodesIdsCompare = astNodesCompare.map(n => n.id);

        this.summaryContainer.setMatchingResult(this.matchingResult);
        this.summaryContainer.setMatchingResultCompare(this.matchingResultCompare);

        this.render();
    }

    render() {
        this.renderTables();

        $(this.tabMenuSelector).find('.item').tab("change tab", "second-comparison");
        this.summaryContainer.renderGraphMetrics(this.graphMetrics, this.graphMetricsCompare);

        this.initSummaryResults();
    }

    renderTables() {
        // Render persons and documents table
        let personsA = JSON.parse(JSON.stringify(this.matchingResult.getNodesByType(this.mainGraph.personType, this.mainGraph)));
        let personsAIds = personsA.map(p => p.id);
        let personsB = JSON.parse(JSON.stringify(this.matchingResultCompare.getNodesByType(this.mainGraph.personType, this.mainGraph)));
        console.log("PP", personsA, personsB);
        personsA.forEach(p => {
            p["A"] = true;
        })
        personsB.forEach(p => {
            if (personsAIds.includes(p.id)) {
                personsA.filter(pA => pA.id == p.id)[0]["B"] = true;
            } else {
                p["B"] = true;
                p["A"] = false;
                personsA.push(p)
            }
        })
        this.personTable.render(personsA);

        let documentsA = JSON.parse(JSON.stringify(this.matchingResult.getNodesByType(this.mainGraph.documentType, this.mainGraph)));
        let documentsAIds = documentsA.map(p => p.id);
        let documentsB = JSON.parse(JSON.stringify(this.matchingResultCompare.getNodesByType(this.mainGraph.documentType, this.mainGraph)));
        documentsA.forEach(p => {
            p["A"] = true;
        })
        documentsB.forEach(p => {
            if (documentsAIds.includes(p.id)) {
                documentsA.filter(pA => pA.id == p.id)[0]["B"] = true;
            } else {
                p["B"] = true;
                p["A"] = false;
                documentsA.push(p)
            }
        })
        this.documentTable.render(documentsA);
    }

    initSummaryResults() {
        this.vegaData = this.initVegaData();
        this.summaryContainer.initUniquePlotsButtons(this.astNodes, this.astNodesCompare, this.vegaData);
    }

    initVegaData(matchedAttribute = "matched") {
        // Each node can be matched several time. vegaData list each node with repetitions when a node appear in different matches.
        let vegaData = JSON.parse(JSON.stringify(this.mainGraph.nodes));
        vegaData = vegaData.map(node => {
            node = this.processNodeForVega(node)
            node[matchedAttribute] = "all graph"
            return node;
        })

        this.matchingResult.matchOccurences.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (this.astNodesIds.includes(queryEntityId)) {
                    let nodeId = matchedEntity.id;
                    let matchedNode = JSON.parse(JSON.stringify(this.mainGraph.idToNode[nodeId]));
                    matchedNode = this.processNodeForVega(matchedNode);
                    matchedNode[matchedAttribute] = globals.mainQueryName;

                    if (vegaData.filter(n => n.id == nodeId && n[matchedAttribute] == globals.mainQueryName).length == 0) {
                        vegaData.push(matchedNode);
                    }
                }
            }
        })

        this.matchingResultCompare.matchOccurences.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (this.astNodesIdsCompare.includes(queryEntityId)) {
                    let nodeId = matchedEntity.id;
                    let matchedNode = JSON.parse(JSON.stringify(this.mainGraph.idToNode[nodeId]));
                    matchedNode = this.processNodeForVega(matchedNode);
                    matchedNode[matchedAttribute] = globals.comparisonQueryName;

                    if (vegaData.filter(n => n.id == nodeId && n[matchedAttribute] == globals.comparisonQueryName).length == 0) {
                        vegaData.push(matchedNode);
                    }
                }
            }
        })

        return vegaData;
    }

    processNodeForVega(node) {
        return {...node.attributes, ..._.omit(node, "attributes")}
    }

    setMatchedAttributes() {
        for (let node of this.mainGraph.nodes) {
            for (let queryEntityId of this.astController.nodesIds()) {
                node[this.matchedAttributeName(queryEntityId)] = false;
            }
        }

        this.matchingResult.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (this.astController.nodesIds().includes(queryEntityId)) {
                    let entityId = matchedEntity.id;
                    this.setNodeMatchedAttribute(queryEntityId, entityId)
                }
            }
        })
    }

    matchedAttributeName(queryEntityId) {
        return `matched_${queryEntityId}`;
    }

    setNodeMatchedAttribute(queryEntityId, graphEntityId) {
        this.mainGraph.idToNode[graphEntityId][this.matchedAttributeName(queryEntityId)] = true;
    }
}