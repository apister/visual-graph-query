import * as d3 from 'd3';

import {globals} from "../globals.js";
import {ShapeEnum} from "../utils/legend";

export default class EntityTypeLegendVis {
    constructor(element, selectNodeTypeCb, width) {
        this.element = element;
        this.selectedNodeTypeCb = selectNodeTypeCb;
        this.width = width;

        this.fontSize = "14px";
        this.fontSizeSelected = "18px";

        d3.select(this.element).style("width", `${this.width}px`);
        this.svgShapePadding = 1;
        this.svgSize = globals.RADIUS * 2 + 2 * this.svgShapePadding;
    }

    initHeader() {
        this.headerElement = document.createElement("div");
        this.headerElement.classList.add("legend-title");
        this.headerElement.appendChild(document.createTextNode("Node Types"));
        this.headerElement.style.fontSize = `${globals.FONT_SIZE + 3}px`;
        
        this.element.appendChild(this.headerElement);
    }

    init(entityTypesLegend) {
        this.element.innerHTML = "";
        this.initHeader();
        this.entityTypesLegend = entityTypesLegend;
        this.legendElement = document.createElement("div");
        this.element.appendChild(this.legendElement);

        // We delete ANY node type for now
        this.deleteAnyType()

        let i = 0;
        for (const [nodeType, shape] of Object.entries(entityTypesLegend)) {
            let entityTypeLegend = document.createElement("div");
            entityTypeLegend.style.cursor = "pointer";

            let svgShape;
            if (shape == ShapeEnum.CIRCLE) {
                svgShape = this.circleSvg();
            } else if (shape == ShapeEnum.SQUARE) {
                svgShape = this.rectSvg();
            } else if (shape == ShapeEnum.STAR) {
                svgShape = this.starSvg();
            }

            svgShape = svgShape.node();

            svgShape.style.marginBottom = "-4px";
            entityTypeLegend.appendChild(svgShape);

            let textLegend = document.createTextNode(nodeType);
            entityTypeLegend.appendChild(textLegend);
            entityTypeLegend.style.fontSize = `${globals.FONT_SIZE}px`;

            this.legendElement.appendChild(entityTypeLegend);

            entityTypeLegend.onclick = () => {
                this.unhighlightAll();
                this.selectedNodeType = nodeType;
                this.highlight(entityTypeLegend);

                this.selectedNodeTypeCb();
            }

            entityTypeLegend.onmouseover = () => {
                this.highlight(entityTypeLegend);
            }

            entityTypeLegend.onmouseout = () => {
                if (nodeType != this.selectedNodeType) this.unhighlight(entityTypeLegend);
            }
            
            // first selection
            if (i == 0) {
                this.selectedNodeType = nodeType;
                this.highlight(entityTypeLegend);
            }
            i++;
        }
    }

    deleteAnyType() {
        delete this.entityTypesLegend[globals.ANY_NODETYPE];
    }

    highlight = (entityTypeLegend) => {
        // entityTypeLegend.style.color = globals.SELECTION_COLOR;
        // entityTypeLegend.querySelector("svg").childNodes[0].style.stroke = globals.SELECTION_COLOR;
        entityTypeLegend.style.fontWeight = "bold";
        entityTypeLegend.style.fontSize = this.fontSizeSelected;
        entityTypeLegend.querySelector("svg").childNodes[0].style.strokeWidth = "4";
    }

    unhighlight = (entityTypeLegend) => {
        // entityTypeLegend.style.color = "black";
        // entityTypeLegend.querySelector("svg").childNodes[0].style.stroke = "black";
        entityTypeLegend.style.fontWeight = "normal";
        entityTypeLegend.style.fontSize = this.fontSize;
        entityTypeLegend.querySelector("svg").childNodes[0].style.strokeWidth = "1";
    }

    unhighlightAll() {
        this.legendElement.childNodes.forEach(div => {
            this.unhighlight(div);
        })
    }

    currentlySelectedSvgShape() {
        let selectedNodeType = this.selectedNodeType;
        let shapeType = this.entityTypesLegend[selectedNodeType];

        let shape;
        if (shapeType == ShapeEnum.CIRCLE) {
            shape = this.circleSvg();
        } else if (shapeType == ShapeEnum.SQUARE) {
            shape = this.rectSvg();
        } else if (shapeType == ShapeEnum.STAR) {
            shape = this.starSvg();
        }

        return shape;
    }

    circleSvg() {
        let circleSvg = d3.create("svg")
            .attr("width", this.svgShapeSize())
            .attr("height", this.svgShapeSize())
        circleSvg
            .append("circle")
            .attr("r", globals.RADIUS)
            .attr("cx", globals.RADIUS + this.svgShapePadding)
            .attr("cy", globals.RADIUS + this.svgShapePadding)
            .style("fill", globals.FILLSTYLE)
            .style("stroke", globals.STROKESTYLE)
            .style("z-index", -10)

        return circleSvg;
    }

    rectSvg() {
        let rectSvg = d3.create("svg")
            .attr("width", this.svgShapeSize())
            .attr("height", this.svgShapeSize())
        rectSvg
            .append("rect")
            .attr("width", globals.RADIUS * 2)
            .attr("height", globals.RADIUS * 2)
            .style("fill", globals.FILLSTYLE)
            .style("stroke", globals.STROKESTYLE)
        return rectSvg;
    }

    starSvg() {
        let starSvg = d3.create("svg")
            .attr("width", this.svgShapeSize())
            .attr("height", this.svgShapeSize())
        starSvg
            .append("path")
            .attr("d", d3.symbol().type(d3.symbolStar).size(globals.RADIUS * 12))
            .attr("transform", `translate(${globals.RADIUS}, ${globals.RADIUS})`)
            .style("fill", globals.FILLSTYLE)
            .style("stroke", globals.STROKESTYLE)

        return starSvg;
    }

    svgShapeSize() {
        return this.svgSize + 2 * this.svgShapePadding;
    }

    static withTopSvg(svgShape) {
        let svgSel = d3.create("svg");
        svgSel.append(() => svgShape.node());
        return svgSel;
    }

    svgRect(color) {
        let svg = d3.create("svg")
            .attr("width", this.svgSize)
            .attr("height", this.svgSize)
        svg
            .append("rect")
            .attr("width", globals.RADIUS * 2)
            .attr("height", globals.RADIUS * 2)
            .style("fill", color)
            .style("stroke", globals.STROKESTYLE)

        return svg.node();
    }
}