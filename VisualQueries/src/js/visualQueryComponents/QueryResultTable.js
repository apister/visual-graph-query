import * as d3 from 'd3';

// Semantic Ui Style is not working
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';

// Parcel with datatable seem to work better with require
var $ = require('jquery');
require('datatables.net')(window, $)
require('jszip')(window, $)
require('datatables.net-dt')(window, $)
require('datatables.net-buttons-dt')(window, $)
require('datatables.net-buttons/js/buttons.html5.js')(window, $)
require('datatables.net-buttons/js/buttons.print.js')(window, $)


class QueryResultTable {
    static dataTable;

    constructor(mainGraphVis, tableElementSelector) {
        this.mainGraphVis = mainGraphVis;
        this.queryResult = null;
        this.queryEntitiesIds = [];
        this.queryNodesIds = null;
        this.nNodes = null;

        this.attributes = null;
        this.attributeSortBy = null;

        // the width specification of DataTable do no seem to work, this hack works for now
        this.detailsColumnWidth = "50px";
        this.detailsColumnTrueWidth = "85px";

        this.tableElementSelector = tableElementSelector;
        this.tableContainer = d3.select(d3.select(tableElementSelector).node().parentNode);
        this.attributeOrderDiv = this.tableContainer.insert("div")
    }

    hide() {
        d3.select(this.tableElementSelector).node().parentNode.parentNode.parentNode.style.display = "none";
    }

    initQueryResult(queryResult) {
        this.queryResult = queryResult;
        this.processQueryResult();
        if (this.queryResult.length > 0) {
            this.queryEntitiesIds = Object.keys(this.queryResult[0]);
            this.findQueryNodesIds();
            this.nNodes = this.queryNodesIds.length;
            this.widthPercent = 100 / this.queryEntitiesIds.length;
        }
        this.computeTableLabels();
    }

    processQueryResult() {
        let keyToChange = "[nodes, relationships]";
        let keyChange = "nodes, relationships";
        this.queryResult.forEach(n => {
            if (n[keyToChange]) {
                n[ keyChange ] = n[ keyToChange ];
                delete n[ keyToChange ];
            }
        })
    }

    computeTableLabels() {
        this.queryResult.forEach(row => {
            for (let key in row) {
                let label;
                let item = row[key]
                if (item["relation_type"]) {
                    label = item.relation_type;
                } else if (item["type"] == 'subgraph') {
                    label = item["type"];
                } else if (item["name"]) {
                    label = item.name;
                } else if (item["labels"]) {
                    label = item.labels[0];
                }

                let id = item["id"] ? ` (${item.id})` : ""
                label = label += id;

                row[key]["label"] = label;
            }
        })
    }

    findQueryNodesIds() {
        this.queryNodesIds = [];
        Object.entries(this.queryResult[0]).forEach((d) => {
            const [entityId, match] = d;
            if (!("relation_type" in match)) {
                this.queryNodesIds.push(entityId);
            }
        })
    }

    initAttributeOrderButton() {
        this.attributes = this.mainGraphVis.graph.properties;

        let dropdown = `<div id="table-attribute-order" class="ui dropdown">
          <div class="text"></div>
          <i class="dropdown icon"></i>
        </div>`

        this.attributeOrderDiv.html("")
        this.attributeOrderDiv
            .insert("div", ":first-child")
            .html(dropdown)

        $('#table-attribute-order.ui.dropdown')
            .dropdown({
                placeholder: "Order by",
                values: Object.keys(this.attributes).map(attr => {
                    return {
                        name: this.attributes[attr].name,
                        value: this.attributes[attr].name
                    }
                }),
                onChange : (e, d) => {this.changeAttributeOrderBy(e)}
            })
        ;
    }

    changeAttributeOrderBy(attributeName) {
        this.attributeSortBy = attributeName;
        this.initTable();
        // this.dataTable.draw();
    }

    queryResultToTableColumns() {
        let columns = this.queryEntitiesIds.map((entityId) => {
            return {
                title: entityId,
                data: entityId,
                // data: '',
                width: this.widthPercent + "%",
                render: {
                    _: "label",
                    // sort: "Degree"
                    sort: this.attributeSortBy
                }
                // render: (data, type, full, meta) => {
                //     // console.log("DD ", data)
                //     const properties = full[entityId]
                //     let cellContent;
                //     if (properties["relation_type"]) {
                //         cellContent = full[entityId].relation_type;
                //     } else if (properties["type"] == 'subgraph') {
                //         cellContent = properties["type"];
                //     } else if (properties["name"]) {
                //         cellContent = full[entityId].name;
                //     } else if (properties["labels"]) {
                //         cellContent = full[entityId].labels[0];
                //     }
                //
                //     let id = full[entityId]["id"] ? ` (${full[entityId].id})` : ""
                //     cellContent = cellContent += id;
                //     return cellContent
                // }
            }
        })

        // detail button column
        columns.unshift({
            "width": this.detailsColumnWidth,
            "className": "details-control",
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "title": "Option",
            "render": () => {return this.detailButton()}
        });

        return columns
    }

    detailButton() {
        return '<i style="margin: 0 auto;" class="plus square outline icon"></i>'
    }

    setLoader() {
        d3.select(this.tableElementSelector).node().innerHTML += "<div class='custom-loader'></div>";
        d3.select(this.tableElementSelector).style("opacity", 0.5);
    }

    removeLoader() {
        d3.select(this.tableElementSelector)
            .html("");
        d3.select(this.tableElementSelector).style("opacity", 1);
    }

    init() {
        this.initTable();
        this.initAttributeOrderButton();
    }

    initTable() {
        if (QueryResultTable.dataTable) {
            QueryResultTable.dataTable.destroy();
            $(this.tableElementSelector).empty();
        }

        let columnsDataTable = this.queryResultToTableColumns();
        QueryResultTable.dataTable = $(this.tableElementSelector).DataTable({
            autoWidth: true,
            paging: true,
            ordering: true,
            info: true,
            dom: 'Bfrtip',
            buttons: ['csv',
                'copy',
                {
                    text: 'Unselect',
                    action: (e, dt, node, config) => {
                        QueryResultTable.dataTable.$(".selected").toggleClass('selected');
                        this.mainGraphVis.selectNodesOnly([]);
                    }
                },
                {
                    text: "Resize",
                    action: (e, dt, node, config) => {
                        this.mainGraphVis.panOnSelectedNodes();
                    }
                }],
            data: this.queryResult,
            columns: columnsDataTable
        })

        $(".details-control")
            .css('cssText', `width: ${this.detailsColumnWidth} !important`)
            .css("max-width", this.detailsColumnWidth)
            .css("word-break", "break-all")
            .css("white-space", "pre-line")

        this.initRowSelection();
        this.initRowClickEvent();

        $("td.details-control").css("text-align", "center");
    }

    initRowSelection() {
        $(this.tableElementSelector + ' tbody').on('click', 'tr', (e) => {
            $(e.currentTarget).toggleClass('selected');
            let subgraphsSelected = QueryResultTable.dataTable.rows('.selected').data().toArray();

            let [nodesIds, linksIds] = this.subgraphsToNodesLinksIds(subgraphsSelected);

            this.mainGraphVis.selectNodesOnly(nodesIds);
            // this.mainGraphVis.highlightLinks(linksIds);
        });
    }

    initRowClickEvent() {
        $(this.tableElementSelector + ' tbody').on('click', 'td.details-control', (e) => {
            e.stopPropagation();
            let tr = $(e.currentTarget).closest('tr');
            let row = QueryResultTable.dataTable.row(tr);

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(this.formatDetails(row.data())).show();
                tr.addClass('shown');
            }
        });
    }

    formatDetails(d) {
        let widthPercent = 100 / this.queryEntitiesIds.length;
        let html = '<div style="display: flex">'
        html += `<div style='width: ${this.detailsColumnTrueWidth};'></div>`
        html += "<div style='flex-grow: 1'>";
        for (const [entity, properties] of Object.entries(d)) {
            html += `<div style='display: inline-block; width: ${widthPercent}%; vertical-align: top;'>`
            for (const [name, value] of Object.entries(properties)) {
                html += `<span class="table-property-key">${name}</span>: ${value} <br>`;
            }
            html += "</div>"
        }
        html += "</div></div>";
        return html;
    }

    subgraphsToNodesLinksIds(subgraphs) {
        let nodesIds = [];
        let linksIds = [];
        subgraphs.forEach((match, i) => {
            this.queryEntitiesIds.forEach((queryEntityId) => {
                if (this.queryNodesIds.includes(queryEntityId)) {
                    let nodeMatch = match[queryEntityId]
                    let id = nodeMatch["id"];
                    if (!(nodesIds.includes(id))) {
                        nodesIds.push(id);
                    }
                } else {
                    let linkId = this.processLink(match[queryEntityId]);

                    if (!(linksIds.includes(linkId))) {
                        linksIds.push(linkId);
                    }
                }
            })
        })
        return [nodesIds, linksIds];
    }

    processLink(link) {
        return link.source.toString() + link['relation_type'] + link.target.toString();
    }
}

export {QueryResultTable};