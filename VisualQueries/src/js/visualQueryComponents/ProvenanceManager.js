import * as d3 from 'd3';
import html2canvas from 'html2canvas';

import * as cypherlib from "cypher-compiler-js";

import {
  initProvenance, createAction,
} from '@visdesignlab/trrack';
import ProvenanceGraphVis from "../graphVis/ProvenanceGraphVis";
import {saveData} from "../utils/utils";

class NodeState {
    constructor(ast, cypher) {
        this.ast = ast;
        this.cypher = cypher;
    }
}

// TODO : Currently at each new state, the ast is recreated from the cypher.
export default class ProvenanceManager {
    constructor(astController, cypherEditor) {
        this.astController = astController;
        this.cypherEditor = cypherEditor;

        this.started = false;
        this.comparisonManager = null;

        this.nodeMarkedAsA = null;
        this.nodeMarkedAsB = null;

        this.queryRepresentationSel = d3.select("#query-thumbnail");
    }

    getCurrentId() {
        return this.provenance.graph.current;
    }

    getAst(graphId) {
        return this.provenance.graphIdToAst[graphId];
    }

    getMatchingResult(graphId) {
        return this.provenance.graphIdToMatchingResult[graphId];
    }

    getGraphMetrics(graphId) {
        return this.provenance.graphIdToGraphMetrics[graphId];
    }

    setComparisonManager(comparisonManager) {
        this.comparisonManager = comparisonManager;
    }

    setAst(graphId, value) {
        this.provenance.graphIdToAst[graphId] = value;
    }

    setMatchingResult(graphId, value) {
        this.provenance.graphIdToMatchingResult[graphId] = value;
    }

    setGraphMetrics(graphId, value) {
        this.provenance.graphIdToGraphMetrics[graphId] = value;
    }

    markNodeAsA() {
        let currentNode = this.provenance.graph.current;
        if (currentNode == this.nodeMarkedAsB) {
            this.nodeMarkedAsB = null;
        }
        this.nodeMarkedAsA = currentNode;
        this.checkCompareButtonEnabled();
    }

    markNodeAsB() {
        let currentNode = this.provenance.graph.current;
        if (currentNode == this.nodeMarkedAsA) {
            this.nodeMarkedAsA = null;
        }
        this.nodeMarkedAsB = currentNode;
        this.checkCompareButtonEnabled();
    }

    checkCompareButtonEnabled() {
        if (this.nodeMarkedAsA && this.nodeMarkedAsB) {
            this.compareButton
                .classed("disabled", false);
        } else {
            this.compareButton
                .classed("disabled", true);
        }
    }

    init() {
        this.provenance = this.setupProvenance();
        this.initGraphAttributes();

        this.graph = this.provenance.graph;
        this.setupObservers();
        this.provenance.done();
        this.started = true;
    }

    // each state has some linked attributes. I keep them separate as states object to not visualize them in the tree vis.
    initGraphAttributes() {
        this.provenance.graphIdToThumbnail = {};
        this.provenance.graphIdToMatchingResult = {};
        this.provenance.graphIdToGraphMetrics = {};
        this.provenance.graphIdToAst = {};
    }

    setupProvenance() {
        this.initialState = this.initState();
        const provenance = initProvenance(this.initialState);

        // URL saving does not work
        // const provenance = initProvenance(this.initialState, {loadFromUrl: true});

        return provenance;
    }

    initState() {
        return new NodeState(null, this.astController.cypher);
        // return new NodeState(stringify(this.astController.ast), this.astController.cypher);
    }

    apply(action) {
        console.log("Provenance Apply");

        if (this.comparisonManager.isCompareMode()) {
            this.provenanceVis.closeCompareMode();
        }

        this.provenance.apply(action);
    }

    changeCypherAction = createAction((state, cypher) => {
        state.cypher = cypher;
    }).setLabel("Cypher Change");

    customCypherAction = (label) => {
        return createAction((state, cypher) => {
            state.cypher = cypher;
        }).setLabel(label)
    }

    generateLinkCreationLabel(sourceId, targetId, type) {
        let typeStr = type ? `${type}` : ""
        return `Link ${sourceId}-${typeStr}-${targetId}`;

        // TODO : not enough space to show big sentences
        // return `New Link ${sourceId}-${typeStr}-${targetId}`;
    }

    // TODO : AST is currently not JSON serializable because of the Scopes and Constraints. So it cannot be used as a state variable.
    setupObservers() {
        this.provenance.addObserver(
            (state) => {
                this.saveThumbnail();

                if (!this.provenance.graphIdToAst[this.provenance.graph.current]) {
                    this.saveAstFromCypher(state.cypher);
                }

                return state.cypher;
            },
            (cypher) => {
                console.log("provenance new node");
                console.log(this);
                this.provenanceVis.run();

                if (!this.astController.isManualChange) {
                    this.astController.isWidgetChange = false;
                    this.astController.isManualChange = false;
                    this.astController.setAstFromCypher(cypher, true);
                }
            }
        )
    }

    saveAstFromCypher = (cypher) => {
        const tree = cypherlib.parseCypher(cypher);
        this.provenance.graphIdToAst[this.provenance.graph.current] = cypherlib.parsePatterns(tree);
    }

    saveThumbnail = () => {
        // Timeout is to wait for the final layout of the subgraph
        setTimeout(() => {
            if (!this.provenance.graphIdToThumbnail[this.provenance.graph.current]) {
                html2canvas(this.queryRepresentationSel.node(), {scrollY: -window.scrollY}).then((canvas) => {
                    canvas.className = "query-canvas";
                    this.provenance.graphIdToThumbnail[this.provenance.graph.current] = canvas;
                })
            }
        }, 500);
    }

    isFinalNode() {
        let allNodes = Object.keys(this.graph.nodes);
        // return this.graph.nodes[this.graph.current] == allNodes[allNodes.length - 1];
        return this.graph.current == allNodes[allNodes.length - 1];

        // if (this.graph.nodes[this.graph.current].children.length == 0) {
        //     return true;
        // } else {
        //     return false;
        // }
    }

    setupGraphVis(compareQueryCb, closeCompareQueryCb, clickTreeNodeCb) {
        this.launchCompareCb = compareQueryCb;
        this.closeCompareCb = closeCompareQueryCb;

        this.provenanceVis = new ProvenanceGraphVis(d3.select('#provenance-tree'), this, compareQueryCb, closeCompareQueryCb, clickTreeNodeCb);
        this.provenanceVis.run();
    }

    renderVis() {
        this.provenanceVis.run();
    }

    undo() {
        this.provenance.undo();
    }

    initButtons() {
        let buttonsContainer = d3.select("#provenance-buttons");
        buttonsContainer.html("");

        this.provenanceTextExplication = `You can go to a previous query state by clicking on a node of the tree. To compare two query states, mark their nodes as A and B, then click the compare button.`

        buttonsContainer.append("p")
            .html(this.provenanceTextExplication)

        this.importExportButtons = buttonsContainer
            .append("div")
            // .classed("ui buttons", true)

        let button = this.importExportButtons
            .append("div")
            .style("display", "inline-block")

        button.append("label")
            .attr("for", "hidden-new-file")
            .classed("ui tiny button", true)
            .html("Import")

        button.append("input")
            .attr("type", "file")
            .attr("id", "hidden-new-file")
            .style("display", "none")
            .on("change", this.handleFileSelect)

        this.importExportButtons
            .append("div")
            .classed("ui tiny button", true)
            .html("Export")
            .on("click", () => {
                saveData(this.provenance.exportProvenanceGraph(), "provenance.json");
            })

        this.provenanceButtons = buttonsContainer
            .append("div")
            .style("margin-top", "6px")

        this.provenanceButtons
            .append("div")
                .classed("ui tiny blue button", true)
                .html("Mark state as A")
                .on("click", () => {
                    this.markNodeAsA();
                    this.renderVis();
                })

        this.provenanceButtons
            .append("div")
                .classed("ui tiny red button", true)
                .html("Mark state as B")
                .on("click", () => {
                    this.markNodeAsB();
                    this.renderVis();
                })

        this.compareButton = this.provenanceButtons
            .append("div")
                .classed("ui small disabled button", true)
                .html("Compare A and B")
                .on("click", () => {
                    this.launchCompareCb(this.nodeMarkedAsA, this.nodeMarkedAsB);
                })

        this.otherButtons = buttonsContainer
            .append("div")
            .style("margin-top", "6px")

        this.minimizeButton = this.otherButtons
            .append("div")
                .classed("ui small disabled button", true)
                .html("Minimize")
                .on("click", () => {
                    this.provenanceVis.reduceBrushedNodes();
                })
    }

    handleFileSelect = (evt) => {
        let files = evt.target.files; // FileList object
        // use the 1st file from the list
        let f = files[0];
        let reader = new FileReader();

        reader.onload = (e) => {
            let treeRepresentation = e.target.result;
            console.log("TREE ", treeRepresentation);
            this.provenanceVis.reset();
            this.provenance.importProvenanceGraph(treeRepresentation);
        }

        reader.readAsText(f);
    }
}