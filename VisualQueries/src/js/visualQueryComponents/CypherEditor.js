import * as d3 from 'd3';
import CodeMirror from "codemirror";
import '../../../node_modules/codemirror/mode/cypher/cypher.js'
import "../../../node_modules/codemirror/addon/search/searchcursor.js";

import {requestFile, saveData} from "../utils/utils";

export default class CypherEditor {
    static codeMirrorSaved = null;

    constructor(astController) {
        this.astController = astController;

        this.error = false;
        this.errorDiv = d3.select("#cypher-error");

        this.textArea = document.getElementById("script-request-input");
        this.container = d3.select(this.textArea.parentNode.parentNode.parentNode.parentNode);

        this.cypher = this.textArea.textContent;
        this.astController.cypher = this.cypher;

        if (CypherEditor.codeMirrorSaved) {
            this.codeMirror = CypherEditor.codeMirrorSaved
        } else {
            this.codeMirror = CodeMirror.fromTextArea(this.textArea, {
                lineNumbers: true,
                lineWrapping: true,
                viewportMargin: 50,
                mode: "cypher",
                height: "auto"
            });
            CypherEditor.codeMirrorSaved = this.codeMirror
        }

        // this.initImportExportButtons();
        this.codeMirrorMarks = [];

        this.errorSelection = d3.select("#cypher-error");
        this.setupEvents();

        this.cypherChangeDelay = 1600;
    }

    initImportExportButtons(buttons) {
        let button = buttons
            .append("div")

        button.append("label")
            .attr("for", "hidden-new-file")
            .classed("ui button", true)
            .style("margin", "0px 10px")
            .html("Import")

        button.append("input")
            .attr("type", "file")
            .attr("id", "hidden-new-file")
            .style("display", "none")
            .on("change", this.handleFileSelect)

        buttons
            .append("div")
            .classed("ui button", true)
            .html("Export")
            .on("click", () => {
                saveData(this.astController.cypher, "download.json");
            })
    }

    handleFileSelect = (evt) => {
        let files = evt.target.files; // FileList object
        // use the 1st file from the list
        let f = files[0];
        let reader = new FileReader();

        reader.onload = (e) => {
            let cypher = e.target.result;
            this.astController.loadCypherFromFile(cypher);
        }

        reader.readAsText(f);
    }

    start(cypher) {
        this.cypher = cypher;
        this.astController.cypher = this.cypher;
        this.changeCypher(this.cypher);
        this.astController.isManualChange = true;
        this.cypherChangeCb(false, 0);
    }

    changeCypher(cypher) {
        this.astController.isManualChange = false;
        this.codeMirror.getDoc().setValue(cypher);
    }

    setupEvents() {
        this.codeMirror.on("change", (e, d) => {
            this.astController.isManualChange = this.isManualChange(d);

            if (this.astController.isManualChange) {
                this.cypherChangeCb();
            }
        })

        $("#menu-accordion").accordion({
            exclusive: false,
            onOpen: () => {
                this.codeMirror.refresh();
            }
        })
    }

    isManualChange(d) {
        return d.origin == "setValue" ? false : true;
    }

    cypherChangeCb = (provenance = true, delay=this.cypherChangeDelay) => {
        this.errorSelection.node().textContent = "";
        let cypher = this.codeMirror.getValue();
        this.cypher = cypher;

        setTimeout(() => this.cypherToRequest(cypher, provenance), delay);
    }

    cypherToRequest(cypher, provenance = true) {
        if (cypher != this.cypher) {
            return;
        }

        if (this.astController.isManualChange) {
            // try {
                this.astController.setAstFromCypher(this.cypher, provenance);
            // } catch (err) {
            //     console.log(err)
            //     this.astController.error = true;
            //     if (err instanceof TypeError) {
            //         this.errorDiv
            //             .html("Error in Ast processing")
            //
            //         this.requestDatabaseCb(this.cypher);
            //     } else {
            //         console.log("TODO");
            //     }
            // }
        }
    }

    highlightCypherPart(entityId) {
        let regexStr = `\\(${entityId}\\)|\\(${entityId}[:{][^)]*\\)`;
        let regex = new RegExp(regexStr);
        let mark;
        let cursor = this.codeMirror.getSearchCursor(regex);
        while (cursor.findNext()) {
            mark = this.codeMirror.markText(
                cursor.from(),
                cursor.to(),
                {className: "highlight"}
            );
            this.codeMirrorMarks.push(mark);
        }
    }

    unhighlightCypherPart() {
        this.codeMirrorMarks.forEach(mark => mark.clear());
    }
}