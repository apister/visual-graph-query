import * as cypherlib from "cypher-compiler-js";

import {globals} from "../globals";

export default class SubgraphToAstTransformer {
    constructor(astController, nodeTypeKey) {
        this.astController = astController;
        this.nodeTypeKey = nodeTypeKey;
    }

    initAst() {
        this.astController.resetAst()
    }

    run(nodes, links, addNames = true) {
        this.initAst();

        this.nodes = nodes;
        this.links = links;

        // this.addNodes();
        this.addLinks();

        // Ast is initiated with a node n
        this.astController.removeNode("n", false);

        if (addNames) {
            nodes.forEach(n => {
                if (n.name) {
                    let nameConstraint = new cypherlib.SimpleConstraint(n.id, "name", null, "=", n.name, null);
                    console.log("c ", nameConstraint);
                    this.astController.addConstraint(nameConstraint);
                }
            })
        }

        // this.astController.isManualChange = false;
        // this.astController.setAstFromCypher(this.astController.ast.toCypher(), false);
    }

    // TODO : take nodeType and relation_types from the globals
    addLinks() {
        this.links.forEach(l => {
            let sourceTypes = this.nodes.filter(n => n.id == l.source)[0].type;
            let targetTypes = this.nodes.filter(n => n.id == l.target)[0].type;
            this.astController.addLink(l.source, l.target, l.relation_type, sourceTypes, targetTypes, false);
        })
    }

    addNodes() {
        this.nodes.forEach(node => {
            this.astController.addNode(node.id);
        })
    }
}