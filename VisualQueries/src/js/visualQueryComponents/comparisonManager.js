import * as d3 from 'd3';
import AstController from "./AstController";
import QueryResultsComparisonComponent from "./QueryResultsComparisonComponent";
import _ from "lodash";
import {globals} from '../globals.js';


class ComparisonManager {
    matchingA;
    matchingB;
    metricsA;
    metricsB;
    astA;
    astB;

    constructor(container, graphVis, documentGraphVisualizer, queryResultMain, matchingResultCompare, provenance, selectAttributeCb, deselectAttributeCb) {
        this.container = container;
        this.mainGraphVis = graphVis;
        this.documentGraphVisualizer = documentGraphVisualizer;

        this.queryResultMain = queryResultMain;
        this.matchingResultCompare = matchingResultCompare;
        // this.queryResultCompare = queryResultCompare;

        this.provenance = provenance;
        this.selectAttributeCb = selectAttributeCb;
        this.deselectAttributeCb = deselectAttributeCb;

        this.operators = {
            "A": array1,
            "B": array2,
            "A - B": difference,
            "B - A": inverseDifference,
            "A &#8745; B": intersection,
            "A &#8746; B": union
        }

        this.vegaName = {
            "A": "A",
            "B": "B",
            "A - B": "A - B",
            "B - A": "B - A",
            "A &#8745; B": "A Intersection B",
            "A &#8746; B": "A Union B"
        }

        this.initColors();
        this.compareMode = false;

        this.queryResultComparisonComponent = new QueryResultsComparisonComponent(this.mainGraphVis, queryResultMain.matchingResult, matchingResultCompare, this.selectAttributeCb, this.deselectAttributeCb);
    }

    setMatchingResult(matching) {
        this.queryResultMain.matchingResult = matching;
        this.queryResultComparisonComponent.matchingResult = matching;
    }

    setGraphMetrics(graphMetrics) {
        this.queryResultComparisonComponent.graphMetrics = graphMetrics;
    }

    setMatchingResultCompare(matching) {
        this.matchingResultCompare = matching;
        // this.queryResultCompare.matchingResult = matching;
        this.queryResultComparisonComponent.matchingResultCompare = matching;
    }

    initColors() {
        this.intersectionColor = "#93e0a1";
        this.diffColor = "#e09393";
        // this.unionColor = "#93abe0";
        this.unionColor = "#93e0a1";
    }

    isCompareMode() {
        return this.compareMode;
    }

    toggleComparisonMode() {
        this.compareMode = !this.compareMode;
        d3.select("#comparison-title")
            .classed("disabled", !this.compareMode);

        d3.select("#comparison-title")
            .classed("active", this.compareMode);
        d3.select("#comparison-content")
            .classed("active", this.compareMode);
    }

    // before mark buttons
    // initCompareOneCanvas = (matchingCompare, graphMetrics, provenanceGraphId) => {
    //     this.toggleComparisonMode();
    //
    //     // this.queryResultCompare.update(matching, AstController.astNodesIds(this.provenance.getAst(provenanceGraphId)), graphMetrics);
    //     this.queryResultMain.hide();
    //     // this.queryResultCompare.hide();
    //
    //     this.queryResultComparisonComponent.show();
    //     this.setMatchingResultCompare(matchingCompare);
    //     this.queryResultComparisonComponent.update(matchingCompare, this.provenance.astController.nodesIds(), AstController.astNodesIds(this.provenance.getAst(provenanceGraphId)), graphMetrics);
    // }

    initCompareOneCanvas = (provenanceNodeIdA, provenanceNodeIdB) => {
        this.toggleComparisonMode();
        this.queryResultMain.hide();
        this.queryResultComparisonComponent.show();

        this.matchingA = this.provenance.provenance.graphIdToMatchingResult[provenanceNodeIdA];
        this.matchingB = this.provenance.provenance.graphIdToMatchingResult[provenanceNodeIdB];
        this.metricsA = this.provenance.provenance.graphIdToGraphMetrics[provenanceNodeIdA];
        this.metricsB = this.provenance.provenance.graphIdToGraphMetrics[provenanceNodeIdB];
        this.astA = this.provenance.provenance.graphIdToAst[provenanceNodeIdA];
        this.astB = this.provenance.provenance.graphIdToAst[provenanceNodeIdB];

        this.queryResultComparisonComponent.update(this.matchingA, this.matchingB, AstController.astNodes(this.astA), AstController.astNodes(this.astB), this.metricsA, this.metricsB);
    }

    closeCompareOneCanvas = () => {
        this.toggleComparisonMode();

        this.queryResultMain.show();
        this.queryResultComparisonComponent.remove();
    }

    comparisonOperatorCb = (operationName, operatorFunction, color, updateVegaData=false) => {
        // let operatorNodesRes = operatorFunction(this.queryResultMain.matchingResult.nodeIds, this.matchingResultCompare.nodeIds);
        // let operatorLinksRes = operatorFunction(this.queryResultMain.matchingResult.linkIds, this.matchingResultCompare.linkIds);

        let operatorNodesRes = operatorFunction(this.matchingA.nodeIds, this.matchingB.nodeIds);
        let operatorLinksRes = operatorFunction(this.matchingA.linkIds, this.matchingB.linkIds);

        this.mainGraphVis.saveState();

        // this.mainGraphVis.colorNodesOnly(operatorNodesRes, color);
        // this.mainGraphVis.selectLinksOnly(operatorLinksRes);

        if (updateVegaData) {
            this.addOperatorVegaData(operationName, operatorFunction, operatorNodesRes);
        }
        // this.queryResultComparisonComponent.initSummaryResults();

        this.mainGraphVis.highlightNodesOnly(operatorNodesRes);
        this.mainGraphVis.highlightLinksOnly(operatorLinksRes);

        this.documentGraphVisualizer.graphVis.highlightNodesOnly(operatorNodesRes);

        this.mainGraphVis.render();
        this.documentGraphVisualizer.render();
    }

    addOperatorVegaData(operationName, operatorFunction, nodesOperatorIds) {
        if (!this.canonVegaData) {
            this.saveVegaData();
        }
        this.restoreVegaData();

        this.newVegaNodes = nodesOperatorIds.map(nId => {
            let node = this.mainGraphVis.graph.idToNode[nId];
            node = this.queryResultComparisonComponent.processNodeForVega(node);
            node[globals.matchedAttribute] = this.vegaName[operationName];
            return node;
            // return {..._.cloneDeep(this.mainGraphVis.graph.idToNode[nId]), "matched": this.vegaName[operationName]}
        })

        let newVegaData = [...this.queryResultComparisonComponent.vegaData, ...this.newVegaNodes];

        this.queryResultComparisonComponent.vegaData = newVegaData;
        this.queryResultComparisonComponent.summaryContainer.nodesData = newVegaData;
    }

    saveVegaData() {
        this.canonVegaData = _.cloneDeep(this.queryResultComparisonComponent.vegaData);
    }

    restoreVegaData() {
        this.queryResultComparisonComponent.vegaData = this.canonVegaData;
        this.queryResultComparisonComponent.summaryContainer.nodesData = this.canonVegaData;
    }

    comparisonOperatorEndCb = () => {
        this.mainGraphVis.resetState();
        this.mainGraphVis.render();
    }

    init() {
        this.container.html("");
        this.buttons = this.container
            .selectAll(".operator")
            .data(Object.keys(this.operators))
            .join("div")
            .classed("ui button", true)
            .classed("red", d => {
                return (d == "B") ? true : false;
            })
            .classed("blue", d => {
                return (d == "A") ? true : false;
            })
            .style("width", "60%")
            .style("display", "block")
            .style("margin", "0 auto")
            .html(d => d)
            .classed("active", d => d == Object.keys(this.operators)[0])
            .on("mouseover", (e, d) => {
                let color;
                if (d == "Intersection") {
                    color = this.intersectionColor;
                } else if (d == "Union") {
                    color = this.unionColor;
                } else {
                    color = this.diffColor;
                }
                this.comparisonOperatorCb(d, this.operators[d], color);
            })
            .on("mouseout", (e, d) => {
                this.comparisonOperatorEndCb();
            })
            .on("click", (e, d) => {
                this.selectedOperator = this.operators[d];
                this.buttons.classed("active", false);
                e.target.classList.add("active");
                this.comparisonOperatorCb(d, this.operators[d], null, true);
            })
    }
}


function array1(array1, array2) {
    return array1;
}


function array2(array1, array2) {
    return array2;
}

function intersection(array1, array2) {
    const intersectionArray = array1.filter(value => array2.includes(value));
    return intersectionArray;
}

function difference(array1, array2) {
    const diffArray = array1.filter(value => !array2.includes(value));
    return diffArray;
}

function inverseDifference(array1, array2) {
    const diffArray = array2.filter(value => !array1.includes(value));
    return diffArray;
}

function union(array1, array2) {
    const unionArray = [...new Set([...array1, ...array2])];
    return unionArray
}

export {ComparisonManager};