import * as vl from 'vega-lite-api';
import 'vega-tooltip';
import embed from "vega-embed";
import * as d3 from 'd3';

import {globals} from "../globals.js";
import AttributesMenuSummary from "../components/AttributesMenuSummary";
import SankeyVis from "./sankeyVis";
import {PropertyTypesEnum} from "dynbipgraph";


export default class SummaryContainer {
    constructor(element, graphData, properties, plotIdsSuffixe = "", selectAttributeCb, deselectAttributeCb) {
        this.elementSelector = element;
        this.graph = graphData;
        this.properties = properties;
        this.plotIdsSuffixe = plotIdsSuffixe;
        this.selectAttributeCb = selectAttributeCb;
        this.deselectAttributeCb = deselectAttributeCb;

        this.container = d3.select(this.elementSelector);
        this.gridWidth = ["four", "five", "seven"];

        this.attributesFiltered = globals.ATTRIBUTES_FILTERED;
        this.matchingResult = null;
        this.attributeMenu = null;
    }

    static prettifyGraphValue(v) {
        let value;
        if (Number.isInteger(v)) {
            value = v;
        } else {
            value = v.toFixed(2);
        }
        return value;
    }

    setMatchingResult(matchingResult) {
        this.matchingResult = matchingResult;
    }

    selectAttribute = (property, nodeType) => {
        this.selectAttributeCb(property);

        if (property) {
            // if (!this.doPLotExists(property)) {
            //     this.initPropertyPlot(property, nodeType);
            // } else {
            //     this.showPlot(property);
            // }

            this.initPropertyPlot(property, nodeType);
        }
    }

    deselectAttribute = (property, stillSelectedAttribute) => {
        this.deselectAttributeCb(property, stillSelectedAttribute);

        if (property) {
            this.hidePlot(property);
        }
    }

    initContainer() {
        this.container.html("");

        this.container = this.container
            .append("div")
            .classed("ui three column grid", true)
            .style("height", "100%")

        this.graphMetricsContainer = this.container
            .append("div")
            .classed(`${this.gridWidth[0]} wide column summary-column`, true)

        this.attributeContainer = this.container
            .append("div")
            .classed(`${this.gridWidth[1]} wide column summary-column`, true)

        this.initGraphMetrics();
        this.initPropertiesSelection();

        this.plotsContainer = this.container
            .append("div")
            .style("text-align", "center")
            .classed(`${this.gridWidth[2]} wide column summary-column`, true)

        this.attributeMenu = new AttributesMenuSummary(this.properties, this.attributeSelecionContainer, this.selectAttribute, this.deselectAttribute);
    }

    plotsContainerWidth() {
        // return this.plotsContainer.node().clientWidth - this.plotsContainer.node().clientHeight * 0.12;
        return this.plotsContainer.node().clientWidth;
    }

    plotsContainerHeight() {
        // return this.plotsContainer.node().clientHeight - this.plotsContainer.node().clientHeight * 0.12;
        return this.plotsContainer.node().clientHeight;
    }

    categoricalPlotWidth() {
        if (globals.SHORT_PLOTS) {
            return {"step": 50};
        } else {
            return this.plotsContainerWidth() - 150;
        }
    }

    categoricalPlotHeight() {
        return Math.min(this.plotsContainerHeight() - 120, 300);
    }

    numberPlotWidth() {
        return this.plotsContainerWidth() - 150;
    }

    numberPlotHeight() {
        return this.plotsContainerHeight() / 5;
    }

    initPropertiesSelection() {
        this.title = this.attributeContainer
            .append("div")
            .classed("title", true)
        this.title.append("span")
            .style("font-size", "150%")
            .html("Properties");

        this.attributeSelecionContainer = this.attributeContainer
            .append("div")
            .append("div")
    }

    initGraphMetrics() {
        this.title = this.graphMetricsContainer
            .append("div")
            .classed("title", true)

        this.title.append("span")
            .style("font-size", "150%")
            .html("Graph Measures");

        this.metricsListEl = this.graphMetricsContainer
            .append("div")
            .classed("content", true)
            .append("ul")
    }

    updateGraphMetrics(metrics) {
        this.metricsListEl
            .selectAll("li")
            .data(Object.keys(metrics))
            .join("li")
            .html((d) => {
                let value = SummaryContainer.prettifyGraphValue(metrics[d]);
                if (d == `Number_of_${this.graph.personType}`) {
                    return `${d} : <strong>${metrics[d]}</strong> (/${this.graph.nPersons} total)`
                } else if (d == `Number_of_${this.graph.documentType}`) {
                    return `${d} : <strong>${metrics[d]}</strong> (/${this.graph.nDocuments} total)`
                } else {
                    return `${d} : <strong>${value}</strong>`
                }
            })
    }

    initUniquePlotsButtons(queryNodes, nodesData) {
        this.queryNodes = queryNodes;
        this.queryNodesIds = queryNodes.map(n => n.id);
        this.nodesData = nodesData;

        this.attributeSelecionContainer.html("");
        this.plotsContainer.html("");
        this.attributeMenu.init();
    }

    initPropertyPlot = (property, nodeType) => {
        let nodeTypeNodes;
        if (nodeType != globals.ALL_NODETYPE && nodeType != "All") { // TODO: fix this
            nodeTypeNodes = this.nodesData.filter(n => n.type == nodeType);
        } else {
            nodeTypeNodes = this.nodesData;
        }

        if (property.type == PropertyTypesEnum.NUMERIC && !this.attributesFiltered.includes(property.name)) {
            this.generateNumberUniquePlot(property, nodeTypeNodes, nodeType);
            // } else if (property.type == PropertyTypesEnum.CATEGORICAL && !this.attributesFiltered.includes(property.name)) {
        } else if (property.type == PropertyTypesEnum.CATEGORICAL || property.type == PropertyTypesEnum.NOMIMAL) {
            this.generateCategoricalUniquePlot(property, nodeTypeNodes);
        }
    }

    doPLotExists(property) {
        const divId = this.plotDivId(property.name, '');
        if ($(`#${divId}`).length > 0) {
            return true;
        } else {
            return false;
        }
    }

    generateNumberUniquePlot(property, data, nodeType) {
        let xVega;
        if (this.isNumericPropertyInt(property)) {
            xVega = vl.x().fieldO(property.name).title(property.name);
            if (property.domain[1] - property.domain[0] > 30) {
                xVega = xVega.bin(true);
            }
        } else {
            xVega = vl.x().fieldQ(property.name).title(property.name).bin(true);
        }
        xVega = xVega.axis({labelAngle: 0});

        let chartSpec;
        if (nodeType == "All") { // TODO: remove hard coding
            chartSpec = this.numberUniquePlotTransforms(data, property, this.attributeMenu.showGlobalDistribution, false)
                .encode(
                    xVega,
                    vl.y().count().title("count"),
                    vl.row().title(null).fieldN("matched"),
                    vl.column().title(null).fieldN("type"),
                    vl.tooltip().count()
                )
                .width(this.numberPlotWidth() / 2)
                .height(this.numberPlotHeight() / 2)
        } else {
            chartSpec = this.numberUniquePlotTransforms(data, property, this.attributeMenu.showGlobalDistribution, false)
                .encode(
                    xVega,
                    vl.y().count().title("count"),
                    // vl.x().fieldQ("value").title(property.name),
                    // vl.y().fieldQ('density'),
                    vl.row().title(null).fieldN("matched"),
                    vl.tooltip().count()
                )
                .width(this.numberPlotWidth())
                .height(this.numberPlotHeight())
        }

        chartSpec = JSON.parse(chartSpec);

        const divId = this.generateNewDivPlot('', property.name);
        embed(`#${divId}`, chartSpec);
    }

    // TODO : differentiate Int and float properties in the Property class
    isNumericPropertyInt(property) {
        let domain = property.domain;
        if (domain[0] == 0 && domain[1] == 1) {
            return false;
        }
        return Number.isInteger(property.domain[0]) && Number.isInteger(property.domain[1]);
    }

    numberUniquePlotTransforms(data, property, showGlobal, twoGroupBy) {
        if (showGlobal) {
            return vl
                // .markArea()
                .markBar()
                .data(data)
            // .transform(
            //     // vl.density("matched")
            //     // vl.density(property.name).bandwidth(0.5).extent([0, 20])
            //     // vl.density(property.name).groupby("matched")
            //     twoGroupBy ? vl.density(property.name).groupby(["matched", "label"]) : vl.density(property.name).groupby("matched")
            //     // vl.density(property.name).groupby(["matched", "label"])
            //     // vl.density().as([property.name, "density"])
            // )
        } else {
            return vl
                // .markLine()
                .markBar()
                .data(data)
                .transform(
                    vl.filter('datum.matched != "all graph"')
                )
        }
    }

    generateCategoricalUniquePlot(property, data) {
        let colorScale = this.processColorScale(property);

        if (this.attributeMenu.plotSankey) {
            try {
                // let sankeyRenderer = new SankeyVis(this.matchingResult.matchOccurences, this.queryNodes, this.categoricalPlotWidth(), this.categoricalPlotHeight());
                let sankeyRenderer = new SankeyVis(this.matchingResult.matchOccurences, this.queryNodes, this.plotsContainerWidth() - 60, this.plotsContainerHeight() - 60);
                sankeyRenderer.processData(property);
                let sankeySvg = sankeyRenderer.render();

                const divId = this.generateNewDivPlot('', property.name);

                d3.select(`#${divId}`)
                    .html("")
                    .append(() => sankeySvg.node());
            } catch (e) {
                console.log("Sankey canceled");
                this.generateCategoricalBarChart(data, property, colorScale);

            }
        } else {
            this.generateCategoricalBarChart(data, property, colorScale);
        }
    }

    generateCategoricalBarChart(data, property, colorScale) {
        let chartSpec = this.categoricalUniquePlot(data, property, colorScale);

        chartSpec = JSON.parse(chartSpec);
        const divId = this.generateNewDivPlot('', property.name);

        embed(`#${divId}`, chartSpec);
    }

    categoricalUniquePlot(data, property, colorScale) {
        return this.categoricalUniquePlotTransforms(data, this.attributeMenu.showGlobalDistribution)
            .encode(
                vl.x().title(null).fieldN("matched").axis({labelAngle: 0}),
                vl.y().title("Proportion").count().stack("normalize"),
                vl.color().fieldN(property.name).scale(colorScale),
                vl.tooltip().count()
            )
            .width(this.categoricalPlotWidth())
            .height(this.categoricalPlotHeight())
    }


    processColorScale(property) {
        let colorScale = {domain: [], range: []};
        property.colorScale.domain().forEach(v => {
            colorScale.domain.push(v)
            colorScale.range.push(property.colorScale(v));
        })
        return colorScale;
    }

    categoricalUniquePlotTransforms(data, showGlobal) {
        if (showGlobal) {
            return vl
                .markBar()
                .data(data)
        } else {
            return vl
                .markBar()
                .data(data)
                .transform(
                    vl.filter('datum.matched != "all graph"')
                )
        }
    }

    generateNewDivPlot(queryNodeId = "", propertyName) {
        const divId = this.plotDivId(propertyName, queryNodeId);

        $(`#${divId}`).remove();

        this.plotsContainer
            .append("div")
            .classed("plot-vega", true)
            .attr("id", divId)
            .style("width", '100%')
            .style("height", '100%')

        return divId;
    }

    plotDivId(propertyName, queryNodeId = '') {
        return `div-plot-${queryNodeId}-${propertyName}-${this.plotIdsSuffixe}`;
    }

    hidePlots() {
        this.plotsContainer.selectAll(".plot-vega")
            .classed("hidden", true)
    }

    showPlot(property) {
        this.plotsContainer.select(`#${this.plotDivId(property.name)}`)
            .classed("hidden", false)
    }

    hidePlot(property) {
        this.plotsContainer.select(`#${this.plotDivId(property.name)}`)
            .classed("hidden", true)
    }
}