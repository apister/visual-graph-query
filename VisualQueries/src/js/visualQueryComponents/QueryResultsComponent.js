import * as d3 from 'd3';
import '../../../node_modules/codemirror/mode/cypher/cypher.js'
import "../../../node_modules/codemirror/addon/search/searchcursor.js";
import _ from "lodash";

import {QueryResultTable} from "./QueryResultTable.js";
import SummaryContainer from "./SummaryContainer.js";
import {DocumentsTable, PersonsTable} from "../components/Table";
import {globals} from "../globals";


export default class QueryResultsComponent {
    constructor(element, graphData, graphVis, plotIdsSuffixe="", tableElementSelector, summaryElementSelector, tabMenuSelector, selectAttributeCb, deselectAttributeCb, selector) {
        this.element = element;
        this.graphData = graphData;
        this.graphVis = graphVis;
        this.plotIdsSuffixe = plotIdsSuffixe;
        this.tableElementSelector = tableElementSelector;
        this.summaryElementSelector = summaryElementSelector;
        this.tabMenuSelector = tabMenuSelector;
        this.selectAttributeCb = selectAttributeCb;
        this.deselectAttributeCb = deselectAttributeCb;
        this.selector = selector;

        this.initTables();
        this.initTablesTab();

        this.properties = this.graphData.propertiesByNodeType();
        this.summaryContainer = new SummaryContainer(this.summaryElementSelector, this.graphData, this.properties, this.plotIdsSuffixe, this.selectAttributeCb, this.deselectAttributeCb);
        this.summaryContainer.initContainer();
    }

    initTables() {
        this.personProperties = this.graphData.getPropertiesByNodeType(this.graphData.personType);
        this.documentProperties = this.graphData.getPropertiesByNodeType(this.graphData.documentType);
        this.allProperties = this.graphData.getPropertiesByNodeType("All");

        this.tableContainer = new QueryResultTable(this.graphVis, this.tableElementSelector);
        this.personTable = new PersonsTable(d3.select("#persons-table-container"), [...this.personProperties, ...this.allProperties], "table-person", this.selector);
        this.documentTables = new DocumentsTable(d3.select("#documents-table-container"), [...this.documentProperties, ...this.allProperties], "table-document", this.selector);
    }

    initTablesTab() {
        $('.menu .item')
          .tab()
        ;
    }

    show() {
        this.element
            .style("display", "block")
    }

    hide() {
        this.element
            .style("display", "none")
    }

    update = (matchingResult, astNodes, graphMetrics=null) => {
        this.matchingResult = matchingResult;
        this.summaryContainer.setMatchingResult(this.matchingResult);

        console.log("render table")
        this.renderTables();
        console.log("render summary")
        this.initSummaryResults(astNodes);

        if (graphMetrics) {
            console.log("render metrics")
            this.updateGraphMetrics(graphMetrics);
        }
        // this.highlightGraphVis();
        console.log("render end")
    }

    renderTables() {
        this.tableContainer.initQueryResult(this.matchingResult.matchOccurences);
        this.tableContainer.removeLoader();

        let persons = this.matchingResult.getNodesByType(this.graphData.personType, this.graphData);
        let documents = this.matchingResult.getNodesByType(this.graphData.documentType, this.graphData);

        // We have to switch to the table tab however if it is hidden Datatable won't initialize correctly
        this.goToTableTab();
        this.personTable.render(persons);
        this.goToDocumentTab();
        this.documentTables.render(documents);
        this.goToOccurenceTab();
        this.tableContainer.init();
        this.goToSummaryTable();
    }

    updateTables() {
        this.personTable.update();
        this.documentTables.update();
    }

    goToTableTab() {
        $(this.tabMenuSelector).find('.item').tab("change tab", "first");
    }

    goToSummaryTable() {
        $(this.tabMenuSelector).find('.item').tab("change tab", "second");
    }

    goToPersonTab() {
        $("#tab-menu-tables").find('.item').tab("change tab", "a");
    }

    goToDocumentTab() {
        $("#tab-menu-tables").find('.item').tab("change tab", "b");
    }

    goToOccurenceTab() {
        $("#tab-menu-tables").find('.item').tab("change tab", "c");
    }

    updateGraphMetrics(graphMetrics) {
        this.summaryContainer.updateGraphMetrics(graphMetrics);
    }

    initSummaryResults(astNodes) {
        let astNodesIds = astNodes.map(n => n.id);

        console.log("init vega data")
        this.vegaData = this.initVegaData(astNodesIds);
        // this.summaryContainer.initUniquePlotsAccordion(astNodesIds, this.vegaData);
        console.log("end vega data")

        this.summaryContainer.initUniquePlotsButtons(astNodes, this.vegaData);
    }

    setLoader() {
        this.tableContainer.setLoader();
    }

    removeLoader() {
        this.tableContainer.removeLoader();
    }

    // TODO : Remove repetitions of one node appearing several times in the same role.
    initVegaData(astNodesIds, matchedAttribute = "matched") {
        // Each node can be matched several time. vegaData list each node with repetitions when a node appear in different matches.
        let vegaData = _.cloneDeep(this.graphData.nodes);
        vegaData = vegaData.map(node => {
            node = this.processNodeForVega(node)
            node[matchedAttribute] = "all graph"
            return node;
        })

        let nodeIdToMatchedEntities = {};
        this.matchingResult.matchOccurences.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (astNodesIds.includes(queryEntityId)) {
                    let nodeId = matchedEntity.id;
                    // let matchedNode = JSON.parse(JSON.stringify(this.graphData.idToNode[nodeId]));
                    let matchedNode = _.cloneDeep(this.graphData.idToNode[nodeId]);
                    matchedNode = this.processNodeForVega(matchedNode);
                    matchedNode[matchedAttribute] = queryEntityId;

                    // if (vegaData.filter(n => n.id == nodeId && n[matchedAttribute] == queryEntityId).length == 0) {
                    //     vegaData.push(matchedNode);
                    // }

                    if (nodeIdToMatchedEntities[nodeId]) {
                        if (!nodeIdToMatchedEntities[nodeId].includes(queryEntityId)) {
                            vegaData.push(matchedNode);
                            nodeIdToMatchedEntities[nodeId].push(queryEntityId);
                        }
                    } else {
                        vegaData.push(matchedNode);
                        nodeIdToMatchedEntities[nodeId] = [queryEntityId];
                    }

                    // if (nodeIdToMatchedEntities[nodeId] && !nodeIdToMatchedEntities[nodeId].includes(queryEntityId)) {
                    //     nodeIdToMatchedEntities[nodeId].push(queryEntityId);
                    // } else {
                    //     nodeIdToMatchedEntities[nodeId] = [queryEntityId];
                    // }
                }
            }
        })

        // TODO
        // let vegaLinkData = JSON.parse(JSON.stringify(this.graphData.links));
        // let vegaLinkData = _.cloneDeep(this.graphData.links);
        // this.matchingResult.linkIds
        // vegaLinkData.forEach(link => link[matchedAttribute] = "all graph");

        return vegaData;
    }

    processNodeForVega(node) {
        return {...node.attributes, ..._.omit(node, "attributes")}
    }

    setMatchedAttributes() {
        for (let node of this.mainGraph.nodes) {
            for (let queryEntityId of this.astController.nodesIds()) {
                node[this.matchedAttributeName(queryEntityId)] = false;
            }
        }

        this.matchingResult.forEach(subgraph => {
            for (const [queryEntityId, matchedEntity] of Object.entries(subgraph)) {
                if (this.astController.nodesIds().includes(queryEntityId)) {
                    let entityId = matchedEntity.id;
                    this.setNodeMatchedAttribute(queryEntityId, entityId)
                }
            }
        })
    }

    matchedAttributeName(queryEntityId) {
        return `matched_${queryEntityId}`;
    }

    setNodeMatchedAttribute(queryEntityId, graphEntityId) {
        this.mainGraph.idToNode[graphEntityId][this.matchedAttributeName(queryEntityId)] = true;
    }
}