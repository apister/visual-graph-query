import * as d3 from 'd3';
import {sankey, sankeyLinkHorizontal} from "d3-sankey";


export default class SankeyVis {
    constructor(data, queryNodes, width, height, backgroundColor="white") {
        this.data = data;
        this.queryNodes = queryNodes;
        this.width = width;
        this.height = height;
        this.color = "gray";
        this.backgroundColor = backgroundColor;
    }

    processData(attribute) {
        this.attribute = attribute;
        this.attributeName = this.attribute.name;

        let queryNodesFilter = this.queryNodes.filter(n => n.label == attribute.nodeType);

        // Sort nodes alphabetically
        queryNodesFilter.sort((a, b) => a.id === b.id ? 0 : a.id < b.id ? -1 : 1);

        let sankeyNodes = [];
        let sankeyLinks = [];
        queryNodesFilter.forEach((node, i) => {
            if (i < queryNodesFilter.length - 1) {
                let [nodes, links] = this.groupByNodePair(node.id, queryNodesFilter[i + 1].id);
                sankeyNodes = [...new Set([...nodes, ...sankeyNodes])]
                sankeyLinks = sankeyLinks.concat(links);
            }
        })

        sankeyNodes = sankeyNodes.map(n => {
            return {"name": n}
        });

        this.sankeyData = {
            "nodes": sankeyNodes,
            "links": sankeyLinks
        }
    }

    groupByNodePair(nodeId1, nodeId2) {
        let processedData = new Map();
        let sankeyNodes = [];
        let linkToNodesIds = {}

        this.data.forEach(match => {
            let key = [match[nodeId1][this.attributeName], match[nodeId2][this.attributeName]];
            let keyStr = key.toString();
            linkToNodesIds[keyStr] = key;

            let [sankeyNode1, sankeyNode2] = [this.sankeyNodeId(nodeId1, key[0]), this.sankeyNodeId(nodeId2, key[1])];
            if (!sankeyNodes.includes(sankeyNode1)) sankeyNodes.push(sankeyNode1);
            if (!sankeyNodes.includes(sankeyNode2)) sankeyNodes.push(sankeyNode2);

            if (processedData.has(keyStr)) {
                processedData.set(keyStr, processedData.get(keyStr) + 1);
            } else {
                processedData.set(keyStr, 1);
            }
        })

        let sankeyLinks = []
        for (let [k, v] of processedData) {
            sankeyLinks.push({
                source: this.sankeyNodeId(nodeId1, linkToNodesIds[k][0]),
                target: this.sankeyNodeId(nodeId2, linkToNodesIds[k][1]),
                value: v
            })
        }

        return [sankeyNodes, sankeyLinks];
    }

    sankeyNodeId(queryNodeId, attributeValue) {
        return `${attributeValue}-${queryNodeId}`;
    }

    initSankey() {
        this.sankey = sankey()
            .nodeId(d => d.name)
            // .nodeAlign(d3[`sankey${align[0].toUpperCase()}${align.slice(1)}`])
            // .nodeSort(inputOrder ? null : undefined)
            .nodeWidth(15)
            .nodePadding(10)
            .extent([[0, 5], [this.width, this.height - 5]])
    }

    render() {
        this.initSankey();
        this.svg = d3.create("svg")
            .style("background", this.backgroundColor)
            .style("width", this.width)
            .style("height", this.height)

        const {nodes, links} = this.sankey({
            nodes: this.sankeyData.nodes.map(d => Object.assign({}, d)),
            links: this.sankeyData.links.map(d => Object.assign({}, d))
        });

        console.log(nodes, links);

        this.svg.append("g")
            .selectAll("rect")
            .data(nodes)
            .join("rect")
            .attr("x", d => d.x0 + 1)
            .attr("y", d => d.y0)
            .attr("height", d => d.y1 - d.y0)
            .attr("width", d => d.x1 - d.x0 - 2)
            .attr("fill", d => {
                // let attributeValue = d.name.split('-')[0]
                let lastIndex = d.name.lastIndexOf('-');
                let attributeValue = d.name.substr(0, lastIndex);
                return this.attribute.colorScale(attributeValue);
            })
            .append("title")
            .text(d => `${d.name}\n${d.value.toLocaleString()}`);

        const link = this.svg.append("g")
            .attr("fill", "none")
            .selectAll("g")
            .data(links)
            .join("g")
            .attr("stroke", d => d3.color(d.color) || this.color)
            .style("mix-blend-mode", "multiply");

        link.append("path")
            .attr("d", sankeyLinkHorizontal())
            .attr("stroke-width", d => Math.max(1, d.width));

        link.append("title")
            .text(d => `${d.source.name} → ${d.target.name}\n${d.value.toLocaleString()}`);

        this.svg.append("g")
            .style("font", "9px sans-serif")
            .selectAll("text")
            .data(nodes)
            .join("text")
            .attr("x", d => d.x0 < this.width / 2 ? d.x1 + 6 : d.x0 - 6)
            .attr("y", d => (d.y1 + d.y0) / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", d => d.x0 < this.width / 2 ? "start" : "end")
            .text(d => d.name)
            .append("tspan")
            .attr("fill-opacity", 0.7)
            .text(d => ` ${d.value.toLocaleString()}`);

        return this.svg;
    }
}