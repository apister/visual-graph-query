import * as d3 from "d3";

import {GraphData} from "../graph/graphData";
import {Graph} from "dynbipgraph";
import {SimulationSvg} from "../graphVis/Simulations/simulationSvg";
import GraphMenu from "../graphVis/GraphMenu";

export default class GraphCreator {
    constructor(svg) {
        this.svg = svg;

        try {
            this.svgContainer = d3.select(this.svg.node().parentNode);
        } catch (e) {
            console.log(e);
        }

        this.graph = null;
        this.graphVis = null;
        this.isLinkDragged = false;
        this.draggedNode = null;

        this.initDragLine();
        this.setupMenu();
    }

    fitToContainer() {
        this.svg.attr("width", this.svg.node().parentNode.offsetWidth)
        this.svg.attr("height", this.svg.node().parentNode.offsetHeight)
    }

    initDragLine() {
        this.dragLine = this.svg
            .append("path")
            .attr("stroke", "black")
            .attr("class", "dragLine")
            .attr("d", "M0,0L20,20")
            .style("visibility", "hidden");
    }

    init(graphJson) {
        this.empty();
        this.initGraph(graphJson);
        this.initGraphVis();
        this.initSimulation();

        this.setupEvents();
    }

    empty() {
        $("#" + this.svg.attr("id").toString()).children(":not(.dragLine)").remove();
    }

    initGraph(graphJson) {
        this.graph = new Graph(graphJson);
    }

    initGraphVis() {
        throw new Error("Abstract method");
    }

    initSimulation() {
        if (this.simulation) this.simulation.stop();

        this.simulation = new SimulationSvg(this.graph, this.graphVis, this.svg);
        this.simulation.init();
        this.simulation.run(20);
    }

    updateVis() {
        this.simulation.start();
        this.graphVis.render();
        this.simulation.ticked();
    }

    setupEvents() {
        this.setupMouseEvents();
    }

    setupMenu() {
        this.menu = new GraphMenu(this.svg);
        this.menu.addButton(
            "select",
            (e) => {
                this.setAllModesFalse();
                this.selectMode = true;
                e.target.classList.add("active");
                this.svg.style("cursor", "hand");
            },
            "hand pointer outline",
            false,
            "Move nodes around by clicking and dragging."
        )

        this.menu.addButton(
            "edit",
            (e) => {
                this.setAllModesFalse();
                this.editMode = true;
                e.target.classList.add("active")

                this.svg.style("cursor", "crosshair");
            },
            "edit",
            true,
            "Create nodes by left clicking on the canvas. Create links by clicking on an existing node and dragging to another node. The legend at the right lets you choose node and edge types. Right clicking on a node or link allows to change its type."
        )

        this.menu.addButton(
            "erase",
            (e) => {
                this.setAllModesFalse();
                this.eraseMode = true;
                e.target.classList.add("active");

                // this.svg.style("cursor", 'url("https://i.stack.imgur.com/bUGV0.png"), auto');
                this.svg.style("cursor", "crosshair");
            },
            "eraser",
            false,
            "Left clicking delete the node."
        )

        this.setAllModesFalse();
        this.editMode = true;
        this.svg.style("cursor", "hand");
    }

    setAllModesFalse() {
        this.eraseMode = false;
        this.selectMode = false;
        this.editMode = false;
    }

    setupMouseEvents() {
        this.setupSvgEvents();
        this.setupGraphEvents();
    }

    setupSvgEvents() {
        this.svg
            .on("mousemove", (e, d) => {
                if (this.editMode) this.updateDragLine(e, d);
                if (this.selectMode && this.draggedNode) this.dragMove(e, d);
            })
            .on("mouseup", (e, d) => {
                if (this.editMode && !this.isContextMenuOpen() && !this.isLinkDragged) {
                    this.createNewNode(d3.pointer(e)[0], d3.pointer(e)[1]);
                }
                if (this.selectMode && this.draggedNode) this.dragEnd(e, d);
                this.hideDragLine();
            })
            .on("mousedown", (e) => {
                if (this.editMode) {
                    if (this.isLinkDragged) {
                        this.isLinkDragged = false;
                        return;
                    }
                }
            })
            // .on("mouseout", () => {
            //     this.dragEnd()
            // })
            // .on("click", (e) => {
            //     console.log("clicl svg");
            //     if (this.editMode) {
            //         this.createNewNode(d3.pointer(e)[0], d3.pointer(e)[1]);
            //     }
            // })
    }

    isContextMenuOpen() {
        throw "Abstract Method"
    }

    setupGraphEvents() {
        this.graphVis.nodes
            .on("mousedown", (e, d) => {
                if (this.editMode) {
                    this.beginDragLine(e, d);
                } else if (this.selectMode) {
                    this.dragStart(e, d);
                }
            })
            .on("mouseup", (e, d) => {
                if (this.editMode) {
                    e.stopPropagation();
                    if (this.isLinkDragged) {
                        if (this.mousedownNode.id != d.id) this.createNewLink(this.mousedownNode, d);
                    }
                    this.isLinkDragged = false;
                } else if (this.selectMode) {
                    this.dragEnd(e, d);
                }
                this.hideDragLine();
            })
            .on("click", (e, d) => {
                if (this.eraseMode) {
                    this.removeNode(d);
                }
                e.stopPropagation();
            })

        this.graphVis.links
            .on("mouseover", (e, d) => {
                d3.select(e.target)
                    .attr("title", "wjefwie")
                    .style("stroke-width", this.graphVis.edgeHoverWidth);
            })
            .on("mouseout", (e, d) => {
                d3.select(e.target)
                    .style("stroke-width", this.graphVis.edgeWidth);
            })
            .on("mouseup", (e, d) => {
                e.stopPropagation();
            })
            .on("click", (e, d) => {
                if (this.eraseMode) {
                    this.removeLink(d);
                }
                e.stopPropagation();
            })

    }

    dragStart = (e, d) => {
        this.draggedNode = d;

        let [x, y] = d3.pointer(e);
        this.draggedNode.x = x;
        this.draggedNode.y = y;
    }

    dragMove = (e, d) => {
        let [x, y] = d3.pointer(e);
        this.draggedNode.x = x;
        this.draggedNode.y = y;

        this.graphVis.updateVis();
    }

    dragEnd = (e, d, i) => {
        this.draggedNode = null;
        this.hideDragLine();
    }

    beginDragLine(e, d) {
        e.stopPropagation();
        this.mousedownNode = d;
        this.isLinkDragged = true;

        this.dragLine
            .style("visibility", "visible")
            .attr(
                "d",
                "M" +
                (this.mousedownNode.x) +
                "," +
                (this.mousedownNode.y) +
                "L" +
                (this.mousedownNode.x) +
                "," +
                (this.mousedownNode.y)
            );
    }

    updateDragLine(e) {
        let coords = d3.pointer(e);
        if (!this.mousedownNode) return;
        this.dragLine
            .attr(
                "d",
                "M" +
                this.mousedownNode.x +
                "," +
                this.mousedownNode.y +
                "L" +
                (coords[0] - 2) +
                "," +
                (coords[1] - 2)
            )
    }

    hideDragLine() {
        this.dragLine
            .style("visibility", "hidden");
        this.isLinkDragged = false;
        this.mousedownNode = null;
    }
}