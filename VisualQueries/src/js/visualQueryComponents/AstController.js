import * as cypherlib from "cypher-compiler-js";
import {globals} from "../globals";
import CypherTransformer from "./CypherTransformer";

export default class AstController {
    constructor(ast, updateCypherFromAstCb, updateAstFromCypherCb, errorAstCb, updateVisualQueryCb) {
        this.ast = ast;
        this.updateCypherFromAstCb = updateCypherFromAstCb;
        this.updateAstFromCypherCb = updateAstFromCypherCb;
        this.errorAstCb = errorAstCb;
        this.updateVisualQueryCb = updateVisualQueryCb;

        this.isManualChange = true;
        this.isWidgetChange = false;
        this.error = false;

        this.provenanceManager = null;
    }

    static astNodesIds(ast) {
        return ast.scopes[0].nodes().map(n => n.id);
    }

    static astNodes(ast) {
        return ast.scopes[0].nodes();
    }

    getEntityById(entityId) {
        return this.ast.scopes[0].entities().filter(n => n.id == entityId)[0];
    }

    getEntityProperty = (entityId, property) => {
        return this.ast.getEntityProperty(entityId, property);
    }

    resetAst() {
        const tree = cypherlib.parseCypher("MATCH (n) RETURN *", this.errorAstCb);
        this.ast = cypherlib.parsePatterns(tree);

        // this.setAstFromCypher("MATCH () RETURN *", false);
    }

    nodesIds() {
        return AstController.astNodesIds(this.ast);
    }

    nodes() {
        return AstController.astNodes(this.ast);
    }

    setProvenanceManager(provenanceManager) {
        this.provenanceManager = provenanceManager;
    }

    setAstFromCypher(cypher, provenanceCall=false) {
        console.log("set AST from cypher !");
        this.cypher = cypher;

        const tree = cypherlib.parseCypher(this.cypher, this.errorAstCb);
        this.ast = cypherlib.parsePatterns(tree);

        if (!(this.error) && this.provenanceManager.started && this.isManualChange && provenanceCall) {
            this.provenanceManager.apply(this.provenanceManager.customCypherAction("Cypher Change")(this.cypher));
        }

        this.updateAstFromCypherCb(provenanceCall);
        this.resetFlags();
    }

    parseCypherErrorCb = (err) => {
        this.error = true;
        this.errorAstCb(err);
    }

    constraintModification(performRequest = true) {
        this.isWidgetChange = true;
        this.cypher = this.ast.toCypher();
        this.updateCypherFromAstCb(performRequest);
    }

    resetFlags() {
        this.error = false;
        this.isManualChange = false;
        this.isWidgetChange = false;
    }

    addNode(nodeId) {
        this.ast.addNode(nodeId);
    }

    addLink(sourceId, targetId, edgeType, sourceType, targetType, track= true) {
        this.ast.addLink(sourceId, targetId, edgeType, sourceType, targetType);
        // this.graphModification();

        // track = false;

        if (track) {
            let actionLabel = this.provenanceManager.generateLinkCreationLabel(sourceId, targetId, edgeType);
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(actionLabel)(this.ast.toCypher()));
            // this.provenanceManager.apply(this.provenanceManager.changeCypherAction(this.ast.toCypher()));
        }
    }

    toggleLinkLabel(linkId, edgeType, track=true) {
        if (edgeType == globals.ANY_NODETYPE) {
            this.ast.toggleLabelToLink(linkId, null);
        } else {
            this.ast.toggleLabelToLink(linkId, edgeType);
        }


        // let track = false;

        if (track) {
            const provenanceLabel = `${linkId}:${edgeType}`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(provenanceLabel)(this.ast.toCypher()));
        }
    }

    changeNodeLabel(nodeId, newLabel, track=true) {
        this.ast.eraseLabels(nodeId);
        if (newLabel != globals.ANY_NODETYPE) {
            this.ast.addLabelToNode(nodeId, newLabel);
        }

        // this.updateVisualQueryCb();

        // let track = false;

        if (track) {
            const provenanceLabel = `${nodeId}:${newLabel}`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(provenanceLabel)(this.ast.toCypher()));
        }
    }

    hasNode(nodeId) {
        return this.ast.scopeGlobal.variables[nodeId];
    }

    removeNode(nodeId, track = true) {
        let isNodeRemoved = this.ast.removeNode(nodeId);

        if (this.ast.scopeGlobal.variables[nodeId]["ego"]) {
            this.ast.removeSubgraphProcedureCall(nodeId);
        }

        // this.graphModification();

        // track = false;
        if (track && isNodeRemoved) {
            let actionLabel = `Remove Node ${nodeId}`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(actionLabel)(this.ast.toCypher()));
        }
    }

    removeLink(linkId, track = true) {
        this.ast.removeLink(linkId);

        // track = false;
        if (track) {
            let actionLabel = `Remove Link ${linkId}`;
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(actionLabel)(this.ast.toCypher()));
        }
    }

    addSubgraphAllProcedure(nodeId, minLevel=0, maxLevel=1, track=true) {
        console.log("add Subgraph Ego")
        this.ast.addSubgraphAllProcedureCall(nodeId, minLevel, maxLevel);

        // track = false;
        if (track) {
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(`${nodeId} Ego`)(this.ast.toCypher()));
        }
    }

    removeSubgraphAllProcedure(nodeId, track=true) {
        this.ast.removeSubgraphProcedureCall(nodeId);

        // track = false;
        if (track) {
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(`Remove ${nodeId} Ego`)(this.ast.toCypher()));
        }
    }

    removeConstraint(constraint, track=true) {
        this.ast.removeConstraint(constraint);

        // let track  = false;
        if (track) {
            let label = `Remove ${constraint.entityId}.${constraint.property} constraint`
            this.provenanceManager.apply(this.provenanceManager.customCypherAction(label)(this.ast.toCypher()));
        }
    }

    addConstraint(constraint) {
        this.ast.addSimpleConstraint(constraint);
    }

    loadCypherFromFile(cypher) {
        this.provenanceManager.apply(this.provenanceManager.customCypherAction("Import query")(cypher));
    }

    preRequestTransform() {
        let astTransformer = new CypherTransformer(this.cypher);
        let cypherTransform = astTransformer.run()
        return cypherTransform;
    }
}