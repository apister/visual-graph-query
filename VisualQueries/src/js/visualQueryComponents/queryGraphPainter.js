import * as d3 from 'd3';

import contextMenu from 'd3-context-menu';
import 'd3-context-menu/css/d3-context-menu.css';
import {Node, Link} from "dynbipgraph";

import {Graph} from "dynbipgraph";
import {GraphVisSvg} from "../graphVis/GraphVisSvg.js";
import {colorPalette} from "./colorPalette.js";
import EntityTypeLegendVis from "./EntityTypeLegendVis.js";
import {globals} from "../globals.js";
import GraphCreator from "./GraphCreator.js";
import {ShapeEnum} from "../utils/legend";


class QueryGraphPainter extends GraphCreator {
    constructor(svg, mainGraph, mainGraphVis, queryStateManager) {
        super(svg);
        this.mainGraph = mainGraph;
        this.mainGraphVis = mainGraphVis;
        this.queryStateManager = queryStateManager;
        this.astController = this.queryStateManager.astController;
        this.provenanceManager = this.queryStateManager.provenanceManager;

        this.computeWidths();
        this.setHeight();

        this.colorPalette = new colorPalette(d3.select("#color-palette"), this.mainGraphVis.linkScale, this.mainGraph.documentTypeToRoles, this.legendWidth);
        this.entityTypeLegendVis = new EntityTypeLegendVis(document.querySelector("#legend-entityType"), this.selectNodeTypeCb, this.legendWidth);
        this.entityTypeLegendVis.init(this.mainGraphVis.entityTypeLegend.legend);

        this.unconnectedNodes = [];

        this.contextNodeOpen = false;
        this.contextLinkOpen = false;

        this.errorDiv = d3.select("#visual-query-error");
    }

    computeWidths() {
        let width = this.svg.node().parentNode.parentNode.offsetWidth;
        this.svgWidth = parseInt(width * 0.58);
        this.legendWidth = parseInt(width * 0.31);
        this.svg.attr("width", this.svgWidth);
    }

    setHeight() {
        if (globals.SHORT_PLOTS && globals.dataset != "Nicole" && this.mainGraph.linkTypes.length < 7) {
            this.svg.attr("height", 280);
        } else {
            this.svg.attr("height", 350);
        }
    }

    init(graphJson) {
        super.init(graphJson);

        this.initCursors();
        this.resetHover();
    }

    initGraph(graphJson) {
        if (this.graph) {
            // Take coordinates of previous graph for same node ids.
            let newGraph = new Graph(graphJson, this.mainGraph);
            this.loadOldXPositions(newGraph);
            this.graph = newGraph;
        } else {
            this.graph = new Graph(graphJson, this.mainGraph);
        }

        if (this.haveUnconnectedNodes()) {
            this.unconnectedNodes.forEach(node => {
                this.graph.addNode(node);
            })
        }
    }

    empty() {
        super.empty();
        this.emptyErrorDiv();
    }

    loadOldXPositions(newGraph) {
        newGraph.nodes.forEach(node => {
            let nodeOld = this.graph.nodes.filter(n => n.id == node.id);
            if (nodeOld.length > 0) {
                node.x = nodeOld[0].x
                node.y = nodeOld[0].y
            }
        })
        return newGraph;
    }

    initGraphVis() {
        this.graphVis = new GraphVisSvg(this.graph, this.svg, globals.RADIUS, this.mainGraphVis.entityTypeLegend, false, false, false, false, this.mainGraphVis.linkScale, this.astController.getEntityProperty);
        this.graphVis.render();
    }

    initCursors() {
        this.cursorGroup = this.svg.insert("g")
            .attr("id", "cursor-creation")
            .style("opacity", 0.5)
            .style("pointer-events", "none");
        this.hideCursor();
        this.updateCursorShape();
        // this.selectNodeTypeCb();
    }

    showCursor() {
        this.cursorGroup
            .style("opacity", 0.5)
    }

    hideCursor = () => {
        this.cursorGroup
            .style("opacity", 0)
    }

    haveUnconnectedNodes() {
        return this.unconnectedNodes.length > 0;
    }

    removeUnconnectedNode() {
        let node = this.unconnectedNodes.pop();

        this.graph.removeNode(node.id);
        this.updateVis();
    }

    selectNodeTypeCb = () => {
        this.updateCursorShape();
        this.editMode = true;
        this.menu.setButtonActive("edit");
    }

    updateCursorShape() {
        this.cursorGroup.html("");
        this.cursorGroup.append(_ => this.entityTypeLegendVis.currentlySelectedSvgShape().node().children[0]);
    }

    updateCursorPosition(e) {
        let coords = d3.pointer(e);
        this.cursorGroup.selectAll("circle")
            .attr("cx", coords[0])
            .attr("cy", coords[1])
            .style("z-index", -10)

        this.cursorGroup.selectAll("rect")
            .attr("x", coords[0] - globals.RADIUS)
            .attr("y", coords[1] - globals.RADIUS)

        this.cursorGroup.selectAll("path")
            .attr("transform", function (d) {
                return `translate(${coords[0]} ,${coords[1]})`;
            })
    }

    takeMetadataFromMainGraph() {
        // this.graph.entityTypes = this.mainGraph.entityTypes;
        // this.graph.attributes = this.mainGraph.attributes;

        this.graph.nodeTypes = this.mainGraph.attributes;
        this.graph.linkTypes = this.mainGraph.linkTypes;
        this.graph.properties = this.mainGraph.attributes;
    }

    setupEvents() {
        super.setupEvents();
        this.setupHover();
        this.setupContextMenu();
    }

    setupSvgEvents() {
        super.setupSvgEvents();

        this.svg
            .on("mousemove.cursor", (e, d) => {
                if (this.editMode) {
                    this.updateCursorPosition(e);
                }
            })
            .on("mouseout.cursor", (e, d) => {
                this.hideCursor();
            })
            .on("mouseover.cursor", (e, d) => {
                if (this.editMode) {
                    this.showCursor();
                }
            })
    }

    setupGraphEvents() {
        super.setupGraphEvents();
        this.graphVis.nodes
            .on("mouseover.cursor", (e, d) => {
                e.stopPropagation();
                this.hideCursor();
            })
            .on("mouseout.cursor", (e, d) => {
                if (this.editMode && !this.isLinkDragged) {
                    this.showCursor();
                }
            })

        this.graphVis.links
            .on("mouseover.cursor", (e, d) => {
                e.stopPropagation();
                this.hideCursor();
            })
            .on("mouseout.cursor", (e, d) => {
                this.showCursor();
            })
    }

    setupHover() {
        this.graphVis.nodes
            .on("mouseover", (e, d) => {
                this.queryStateManager.highlightCypherPart(d.id);
                this.queryStateManager.highlightQueryEntity(d.id);
                this.isMouseOnNode = true;

                d3.select(e.target)
                    .attr("stroke", globals.SELECTION_COLOR)
                    .attr("stroke-width", globals.NODE_LINE_WIDTH_SELECT)
            }).on("mouseout", (e, d) => {
            this.isMouseOnNode = false;

            d3.select(e.target)
                .attr("stroke", "black")
                .attr("stroke-width", globals.LINE_WIDTH)

            // Delay is for allowing dragging fast nodes
            setTimeout(() => {
                if (!this.isMouseOnNode) {
                    this.queryStateManager.unhighlightCypherPart();
                    this.queryStateManager.unhighlightQueryEntity();
                }
            }, 100)
        })
    }

    resetHover() {
        this.queryStateManager.unhighlightCypherPart();
        this.queryStateManager.unhighlightQueryEntity();
    }

    setupContextMenu() {
        this.setupNodesContextMenu();
        this.setupLinkContextMenu();
    }

    setupNodesContextMenu() {
        let menuData = Object.entries(this.entityTypeLegendVis.entityTypesLegend)
            .map(([nodeType, shape]) => {
                let svgShape;
                if (shape == ShapeEnum.CIRCLE) {
                    svgShape = this.entityTypeLegendVis.circleSvg();
                } else if (shape == ShapeEnum.SQUARE) {
                    svgShape = this.entityTypeLegendVis.rectSvg();
                } else if (shape == ShapeEnum.STAR) {
                    svgShape = this.entityTypeLegendVis.starSvg();
                }
                return {
                    title: svgShape.node().outerHTML,
                    action: (d) => {
                        this.astController.changeNodeLabel(d.id, nodeType);
                    },
                    disabled: false // optional, defaults to false
                }
            });

        // TODO: EGO
        // menuData.unshift(
        //     {
        //         title: "Turn Into Ego",
        //         action: (d) => this.turnNodeIntoEgo(d)
        //     },
        //     {divider: true}
        // )

        this.nodesContextMenu = contextMenu(menuData, {
            onOpen: () => {
                this.contextNodeOpen = true
            },
            onClose: () => {
                setTimeout(() => {
                    this.contextNodeOpen = false;
                }, 400)
            }
        })

        this.graphVis.nodes
            .on("contextmenu", this.nodesContextMenu)

        // this.graphVis.nodes
        //     .on("click.contextmenu", this.nodesContextMenu);
    }

    isContextMenuOpen() {
        return this.contextNodeOpen || this.contextLinkOpen;
        // return !d3.select(".d3-context-menu").empty();
    }

    turnNodeIntoEgo = (d) => {
        if (d.ego) {
            this.astController.removeSubgraphAllProcedure(d.id);
        } else {
            d.ego = true;
            this.astController.addSubgraphAllProcedure(d.id);
        }
        // this.graphVis.render();
    }

    processRoleValue(rect) {
        if (this.colorPalette.docTypeToRoles) {
            return rect.__data__[0];
        } else {
            return rect.__data__;
        }
    }

    setupLinkContextMenu() {
        let linkMenuData = Array.from(this.colorPalette.rects._groups[0])
            .map((rect, i) => {
                let newRect = rect.cloneNode(true);
                newRect.setAttribute("x", 0);
                newRect.setAttribute("y", 0);

                let roleValue = this.processRoleValue(rect);

                return {
                    title: (d) => {
                        this.currentLink = d;
                        return this.linkMenuValue(d, roleValue, newRect);
                    },
                    disabled: false // optional, defaults to false
                }
            });

        this.contextMenu = contextMenu(linkMenuData, {
            onOpen: () => {
                this.contextLinkOpen = true
                this.generateLinkCheckboxEvents();
            }, onClose: () => {
                setTimeout(() => {
                    this.contextLinkOpen = false;
                }, 400)
            }
        })

        this.graphVis.links
            .on("contextmenu", this.contextMenu);

        this.graphVis.links
            .on("click.contextmenu", (e) => {
                if (!this.eraseMode) {
                    this.contextMenu
                }
            });
    }

    linkMenuValue(d, i, newRect) {
        // return this.initCheckbox(d, this.colorPalette.attributeDomain[i]).outerHTML + `<svg width="${this.colorPalette.rectWidth}" height="${this.colorPalette.rectHeight}">${newRect.outerHTML}</svg>`
        return this.initCheckbox(d, i).outerHTML + `<svg width="${this.colorPalette.rectWidth}" height="${this.colorPalette.rectHeight}">${newRect.outerHTML}</svg>`
    }

    generateLinkCheckboxEvents() {
        // The checking of the checkbox has to be done here. It does not work at the initialization of the checkbox html
        // [globals.ANY_EDGETYPE, ...this.mainGraph.edgeTypes].forEach(edgeType => {
        //     let checked = false;
        //
        //     if (this.currentLink.label == globals.ANY_NODETYPE) {
        //         if (this.currentLink.label == edgeType) {
        //             checked = true;
        //         }
        //     } else {
        //         if (this.currentLink.label && this.currentLink.label.includes(edgeType)) {
        //             checked = true;
        //         }
        //     }
        //
        //     let behaviour = checked ? 'check' : 'uncheck';
        //
        //     // Had to call twice or it does not work
        //     $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
        //         .checkbox(behaviour)
        //     $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
        //         .checkbox({
        //             onChange: () => this.clickEdgeTypeCheckboxCb(edgeType)
        //         });
        // })

        [globals.ANY_EDGETYPE, ...this.mainGraph.linkTypes].forEach(edgeType => {
            let checked = false;

            if (this.currentLink.type == globals.ANY_NODETYPE) {
                if (this.currentLink.type == edgeType) {
                    checked = true;
                }
            } else {
                if (this.currentLink.type && this.currentLink.type.includes(edgeType)) {
                    checked = true;
                }
            }

            let behaviour = checked ? 'check' : 'uncheck';

            // Had to call twice or it does not work
            $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
                .checkbox(behaviour)
            $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
                .checkbox({
                    onChange: () => this.clickEdgeTypeCheckboxCb(edgeType)
                });
        })
    }

    clickEdgeTypeCheckboxCb(edgeType) {
        if (edgeType == globals.ANY_EDGETYPE) {
            this.mainGraph.edgeTypes.forEach(et => {
                $(`#${this.idCheckBox(et)}.ui.checkbox`)
                    .checkbox("set unchecked")
            })
        } else {
            $(`#${this.idCheckBox(globals.ANY_EDGETYPE)}.ui.checkbox`)
                .checkbox("set unchecked")
        }

        this.astController.toggleLinkLabel(this.currentLink.id(), edgeType);

        this.updateCurrentLinkLabel(edgeType);
        this.disableIfOneCheck();
    }

    updateCurrentLinkLabel(edgeType) {
        // if (edgeType == globals.ANY_NODETYPE) {
        //     this.currentLink.label = edgeType;
        // } else {
        //     if (this.currentLink.label.includes(edgeType)) {
        //         this.currentLink.label = this.currentLink.label.filter(et => et != edgeType);
        //     } else {
        //         if (Array.isArray(this.currentLink.label)) {
        //             this.currentLink.label.push(edgeType);
        //         } else {
        //             this.currentLink.label = [edgeType];
        //         }
        //     }
        // }

        if (edgeType == globals.ANY_NODETYPE) {
            this.currentLink.type = edgeType;
        } else {
            if (this.currentLink.type.includes(edgeType)) {
                this.currentLink.type = this.currentLink.type.filter(et => et != edgeType);
            } else {
                if (Array.isArray(this.currentLink.type)) {
                    this.currentLink.type.push(edgeType);
                } else {
                    this.currentLink.type = [edgeType];
                }
            }
        }

    }

    disableIfOneCheck() {
        [globals.ANY_EDGETYPE, ...this.mainGraph.linkTypes].forEach(edgeType => {
            $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
                .removeClass("disabled")
        })

        if (this.isOneBoxChecked()) {
            [globals.ANY_EDGETYPE, ...this.mainGraph.linkTypes].forEach(edgeType => {
                if (this.currentLink.label == edgeType) {
                    $(`#${this.idCheckBox(edgeType)}.ui.checkbox`)
                        .addClass("disabled")
                }
            })
        }
    }

    isOneBoxChecked() {
        // return (this.currentLink.label == globals.ANY_EDGETYPE) || (Array.isArray(this.currentLink.label) && this.currentLink.label.length == 1)
        return (this.currentLink.type == globals.ANY_EDGETYPE) || (Array.isArray(this.currentLink.type) && this.currentLink.type.length == 1)
    }

    idCheckBox(edgeType) {
        return `checkbox-${edgeType}`;
    }

    initCheckbox(link, edgeType) {
        let checkbox = d3.create("div")
            .classed("ui checkbox", true)
            .attr("id", this.idCheckBox(edgeType));

        checkbox.append("label").html(edgeType);

        let input = checkbox.append("input")
            .attr("type", "checkbox")
            .attr("value", edgeType);

        let checkboxSel = checkbox.node();

        return checkboxSel;
    }

    removeNode(node) {
        this.graph.removeNode(node.id);

        if (this.astController.hasNode(node.id)) {
            this.astController.removeNode(node.id);
        }

        if (this.unconnectedNodes.includes(node)) {
            this.unconnectedNodes = this.unconnectedNodes.filter(n => n != node);
        }

        this.updateVis();
    }

    removeLink(link) {
        let isLinkBridge = this.graph.isLinkBridge(link);

        if (isLinkBridge) {
            this.fillErrorDiv("The graph must be connected.");
        } else {
            this.astController.removeLink(link.id());
        }
    }

    createNewNode(x, y) {
        let nodeType = this.entityTypeLegendVis.selectedNodeType;
        let nodeId = this.generateId(nodeType);
        if (nodeType == globals.ANY_NODETYPE) {
            nodeType = null;
        }

        // let nodePrefixId = this.nodeIdPrefix(nodeType);
        // let nodeId = this.astController.ast.scopeGlobal.next_id(1, nodePrefixId);

        let newNode = new Node(nodeId, null, nodeType, x, y);
        this.graph.addNode(newNode);
        this.unconnectedNodes.push(newNode);

        this.updateVis();
        this.setupMouseEvents();
        this.setupHover();
        // this.queryStateManager.unhighlight();
    }

    generateId(nodeType) {
        let length;
        if (this.graph) {
            length = this.graph.getNodesByType(nodeType).length;
        } else {
            length = 0;
        }

        let count = 1;
        if (this.graph) {
            while (this.graph.nodes.map(n => n.id).includes(`${this.nodeIdPrefix(nodeType)}${length + count}`)) {
                count += 1
            }
        }
        return `${this.nodeIdPrefix(nodeType)}${length + count}`;
    }

    nodeIdPrefix(nodeType) {
        if (nodeType.length >= 3) {
            return nodeType.substring(0, 3).toLowerCase();
        } else {
            return nodeType.substring(0, 1).toLowerCase();
        }
    }

    createNewLink(d1, d2) {
        if (d1.type == d2.type) {
            this.fillErrorDiv(`Connections mus be between ${this.mainGraph.personType} and ${this.mainGraph.documentType}.`);
            return;
        }

        let linkType = this.colorPalette.selectedEdgeType;
        let linkId = this.astController.ast.scopeGlobal.next_id(2, "l");

        // We have to create twice the same object because d3 force will transform it in links
        let newLink = new Link(d1.id, d2.id, linkType, null, linkId);

        this.graph.addLink(newLink);

        if (linkType == globals.ANY_EDGETYPE) {
            linkType = null;
        }

        let sourceType = d1.type;
        let targetType = d2.type;
        if (sourceType == globals.ANY_NODETYPE) {
            sourceType = null;
        }
        if (targetType == globals.ANY_NODETYPE) {
            targetType = null;
        }

        this.filterUnconnectedNodes([d1, d2]);
        this.updateVis();
        this.setupMouseEvents();

        this.astController.addLink(d1.id, d2.id, linkType, sourceType, targetType);
        // this.queryStateManager.unhighlight();

        this.queryStateManager.cypherEditor.changeCypher(this.astController.ast.toCypher());
    }

    filterUnconnectedNodes(nodes) {
        this.unconnectedNodes = this.unconnectedNodes.filter(d => !nodes.includes(d));
    }

    updateDragLine(e) {
        super.updateDragLine(e);
        this.dragLine
            .attr('stroke', this.colorPalette.selectedEdgeTypeColor);
    }

    setNodeAttributeToRender(property, colorScale) {
        this.graphVis.setNodeAttributeToRender(property, colorScale);
    }

    fillErrorDiv(errorMessage) {
        this.errorDiv.html(errorMessage);
    }

    emptyErrorDiv() {
        this.errorDiv.html(null);
    }

    beginDragLine(e, d) {
        super.beginDragLine(e, d);
        this.hideCursor();
    }

    hideDragLine() {
        super.hideDragLine();
        this.showCursor();
    }
}

export {QueryGraphPainter};