import * as d3 from "d3";

import {QueryStateManager} from "./visualQueryComponents/QueryStateManager.js";
import {globals} from './globals.js';
import Visualizer from "./graphVis/Visualizer.js";
import DocumentVisualizer from "./graphVis/DocumentVisualizer";
import {BipartiteDynGraph, Graph, Property} from "dynbipgraph";
import Popup from "./components/Popup";
import Selector from "./behaviours/Selector.js";


class VisualQueries {
    constructor() {
        this.mainCanvas = d3.select("#canvas-main");
        this.mapCanvas = d3.select("#canvas-hypergraph");

        this.resizeCanvas();
        this.setupConfigMenu();
        this.popup = new Popup();

        Property.setCategoricalThreshold(25);
    }

    resizeCanvas() {
        this.mainCanvas.node().width = this.mainCanvas.node().parentNode.offsetWidth;
        this.mainCanvas.node().height = this.mainCanvas.node().parentNode.offsetHeight;
        this.mapCanvas.node().width = this.mapCanvas.node().parentNode.offsetWidth;
        this.mapCanvas.node().height = this.mapCanvas.node().parentNode.offsetHeight;
    }

    setupConfigMenu() {
        $("#file-upload").change(this.uploadFileCb);
    }

    uploadFileCb = async (e) => {
        let fileReader = new FileReader();
        fileReader.onload = async (e) => {
            let graphData = JSON.parse(e.target.result);

            this.popup.show();
            await this.uploadDataBackend(graphData);
            await this.loadDataFromNeo4j();
            this.popup.hide();
        };

        fileReader.readAsText(e.target.files[0]);
    }

    changeUnhighlightOpacity = (e) => {
        globals.UNHIGHLIGHT_OPACITY = e;
        d3.select("#slider-opacity-value")
            .html(e);
        this.visualizer.graphVis.render();
        this.documentVisualizer.render();
    }

    changeRadius = (e) => {
        globals.RADIUS = e;
        d3.select("#slider-radius-value")
            .html(e);
        this.visualizer.graphVis.render();
    }

    init = async (data, properties) => {

        await this.initGraph(data, properties);
        await this.initMainLayout();
        await this.loadHypergraph();
        this.postLayoutsInit();
    }

    initGraph = async (data, properties) => {
        this.bipGraph = new BipartiteDynGraph(data);
        this.datasetName = data["metadata"]["datasetName"];
        this.fillDataInputSection();
        this.selector = new Selector(this.selectionCb);
    }

    selectionCb = () => {
        this.visualizer.render();
        this.documentVisualizer.render();
        this.queryStateManager.queryResultMain.updateTables();
    }

    fillDataInputSection() {
        $("#dataset-name-span").html(this.datasetName);
        $("#n-persons-span").html(`${this.bipGraph.nPersons} ${this.bipGraph.personType}S.`);
        $("#n-documents-span").html(`${this.bipGraph.nDocuments} ${this.bipGraph.documentType}S.`);
    }

    async initMainLayout() {
        this.visualizer = new Visualizer(this.bipGraph, this.mainCanvas, this.selector);
        await this.visualizer.init();
        await this.visualizer.run();
        this.visualizer.render();

        this.visualizer.addSliderOption("Non Matched Opacity", this.changeUnhighlightOpacity, 0, 1, 0.1, 0.4);
        this.visualizer.addSliderOption("Radius", this.changeRadius, 3, 20, 1, globals.RADIUS);
        this.visualizer.topMenuBar.addCheckBoxOption("Show node labels", this.showNodeLabelsCb);
    }

    showNodeLabelsCb = () => {
        this.visualizer.toggleRenderLabels();
        this.documentVisualizer.toggleRenderLabels();
    }

    async initHypergraphLayout(documentGraph, repetitionGraph) {
        console.log("init hypergraph")
        this.documentGraphData = new Graph(documentGraph);
        this.documentGraphData.removeNodesCoordinates();

        this.repetitionGraphData = new Graph(repetitionGraph);

        this.documentVisualizer = new DocumentVisualizer(this.bipGraph, this.documentGraphData, this.repetitionGraphData, this.mapCanvas, this.visualizer.graphVis.linkScale, this.selector);
        await this.documentVisualizer.init();

        this.documentVisualizer.run();
        console.log("end hypergraph")
    }

    postLayoutsInit() {
        this.queryStateManager = new QueryStateManager(this.bipGraph, this.visualizer, this.documentVisualizer, this.selector);
        this.linkTwoViews();

        this.detectResize();
    }

    detectResize() {
        let observer = new MutationObserver((mutations) => {
            this.fitBothCanvas();
        });
        observer.observe(this.visualizer.graphVis.canvasWrapper, {attributes: true});

        let observerMainDiv = new MutationObserver((mutations) => {
            this.fitBothCanvas();
        });
        observerMainDiv.observe(document.querySelector("#div-main-graphs"), {attributes: true});

        let observerGraphPainter = new MutationObserver((mutations) => {
            this.queryStateManager.graphPaint.fitToContainer();;
        });
        observerGraphPainter.observe(document.querySelector("#div-graph-drawing"), {attributes: true});

        // When window is resized
        window.addEventListener('resize', () => {this.fitBothCanvas()});
    }

    fitBothCanvas() {
        this.visualizer.graphVis.fitToContainer();
        this.documentVisualizer.graphVis.fitToContainer();

        this.visualizer.render();
        this.documentVisualizer.render();
    }

    linkTwoViews() {
        this.visualizer.graphVis.setNodeHoverCbs(this.documentVisualizer.graphVis.selectPerson, this.documentVisualizer.graphVis.deselectPerson);
    }

    async uploadDataBackend(data) {
        await fetch(globals.route("loadData"), {
            method: "POST",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
    }

    async loadDataFromNeo4j() {
        this.popup.show();
        console.log("start data fetching");
        await fetch(globals.route("getDatabaseJson"), {
            method: "GET",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(r => {
                return r.json()
            })
            .then(async json => {
                console.log("data fetched");
                await this.init(json["data"], json["properties"]);
            })
        this.popup.hide();
    }

    async loadHypergraph() {
        let url = globals.route("getHypergraph")

        await fetch(url.toString(), {
            method: "GET",
            mode: "cors",
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(r => {
                return r.json()
            })
            .then(async json => {
                let documentGraph = json['documentGraph']
                let repetitionGraph = json['repetitionGraph']

                await this.initHypergraphLayout(documentGraph, repetitionGraph)
            })
    }
}

export {VisualQueries}