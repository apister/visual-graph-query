import * as d3 from "d3";
import {globals} from "../globals";
import {sleep} from '../utils/utils';
import {getPropertyType} from "../visualQueryComponents/Widgets/EntityWidgets";

import {PropertyTypesEnum} from "dynbipgraph";

export default class AttributesMenuSummary {
    constructor(properties, container, selectAttributeCb, deselectAttributeCb) {
        this.nodeTypeToProperties = properties;
        this.container = container;
        this.selectAttributeCb = selectAttributeCb;
        this.deselectAttributeCb = deselectAttributeCb;

        this.noValueColor = "white";
        this.resetOptions();
    }

    resetOptions() {
        this.showGlobalDistribution = true;
        this.plotSankey = false;
    }

    setColorScales() {
        for (let [nodeType, properties] of Object.entries(this.nodeTypeToProperties)) {
            for (let property of properties) {
                let type = property.type;
                let domain = property.domain;

                let colorScale;
                if (type == PropertyTypesEnum.NUMERIC) {
                    colorScale = d3.scaleSequential().domain(domain).interpolator(d3.interpolateViridis);
                } else if (type == PropertyTypesEnum.NOMIMAL || type == PropertyTypesEnum.CATEGORICAL) {
                    // colorScale = d3.scaleOrdinal().domain(domain).range(d3.schemeCategory10);
                    colorScale = d3.scaleOrdinal().domain(domain).range(d3.schemeSet3);
                    // colorScale.domain([undefined, ...colorScale.domain()]);
                    // colorScale.domain(["undefined", ...colorScale.domain()]);
                    // colorScale.range([this.noValueColor, ...colorScale.range()]);
                }

                property.colorScale = colorScale;
            }
        }
    }

    init() {
        this.setColorScales();
        this.initOptions();

        this.gridContainer = this.container
            .append("div")
            .style("display", "grid")
            .style("grid-template-columns", "repeat(3, 1fr)")
            .style("grid-gap", "10px");

        // TODO: change
        this.sortTypes(Object.keys(this.nodeTypeToProperties)).forEach(nodeType => {
            let properties = this.nodeTypeToProperties[nodeType];
            this.initNodeType(nodeType, properties);
        })

        // TODO: finish showing distribution of roles
        // this.initLinkAttribute();
    }

    sortTypes(types) {
        types = types.sort((a,b) => {
            if (a == "PERSON") {
                return -1
            } else if (b == "All") {
                return 1
            }
        })
        return types
    }

    initNodeType(nodeType, properties) {
        this.nodeTypeContent = this.gridContainer
            .append("div")

        this.nodeTypeContent
            .append("div")
            .style("margin-bottom", "0px")
            .style("margin-top", "5px")
            .classed("ui header", true)
            .html(nodeType)

        for (let property of properties) {
            if (!globals.ATTRIBUTES_FILTERED.includes(property.name)) {

                if (property.type != PropertyTypesEnum.NOMIMAL || ["_ville", "_departement", "_department", "_region", "school", "oai"].includes(property.name)) {
                // if (property.domain.length < 80 || ["ville"].includes(property.name)) {
                    this.initProperty(property, nodeType);
                }

                // if (getPropertyType(property) != 'nominal', 60) {
                //     this.initProperty(property, nodeType);
                // }
            }
        }
    }

    initProperty(property, nodeType) {
        this.content = this.nodeTypeContent
            .append("div")
            .datum(nodeType)
            .html(property.name)
            .classed("tiny ui button", true)
            .style("margin", "2px 5px")
            .on("mouseover", (e, d) => {
                this.selectAttributeCb(property, d);
            })
            .on("mouseout", () => {
                if (this.selectedProperty != property) {
                    this.deselectAttributeCb(property, this.selectedProperty);
                }
            })
            .on("click", (e, d) => {
                if (this.selectedProperty == property) {
                    e.target.classList.toggle("active");
                    this.selectedProperty = null;
                } else {
                    this.container.selectAll(".button").classed('active', false);
                    e.target.classList.add("active");

                    if (this.selectedProperty) {
                        this.deselectAttributeCb(this.selectedProperty);
                    }

                    this.selectedProperty = property;
                    this.selectAttributeCb(property, d);
                }
            })
    }


    // TODO: not finished yet
    initLinkAttribute() {
        this.nodeTypeContent = this.container
            .append("div")

        this.nodeTypeContent
            .append("span")
            .classed("ui header", true)
            .html("Link")

        let property = "role";

        this.nodeTypeContent
            .append("div")
            .html("Role")
            .classed("tiny ui button", true)
            .style("margin", "2px 5px")
            .on("mouseover", (e, d) => {
                this.selectAttributeCb(property, d);
            })
            .on("mouseout", () => {
                if (this.selectedProperty != property) {
                    this.deselectAttributeCb(property, this.selectedProperty);
                }
            })
            .on("click", (e, d) => {
                if (this.selectedProperty == property) {
                    e.target.classList.toggle("active");
                    this.selectedProperty = null;
                } else {
                    this.container.selectAll(".button").classed('active', false);
                    e.target.classList.add("active");

                    if (this.selectedProperty) {
                        this.deselectAttributeCb(this.selectedProperty);
                    }

                    this.selectedProperty = property;
                }
            })
    }

    initOptions() {
        let checkedGlobal = this.showGlobalDistribution ? "checked" : "";
        this.globalDistributionCheckbox = this.container.append("div")
            .classed("ui checkbox", true)
            .html(
                `<input ${checkedGlobal} type="checkbox" value="global">
                 <label>Compare with global distribution</label>`
            )
            .attr("id", "checkbox-global-distribution")
            .on("click", (e) => {
                this.toggleGlobalDistribution();
            })


        let nodeTypes = Object.keys(this.nodeTypeToProperties);

        let checked = this.plotSankey ? "checked" : "";
        this.sankeyCheckbox = this.container.append("div")
            .style("display", "block")
            .classed("ui checkbox", true)
            .html(
                `<input ${checked} id="sankey-box" type="checkbox" value="global">
                 <label>Sankey plots (There needs to be at least two ${nodeTypes[1].toLowerCase()}s or ${nodeTypes[2].toLowerCase()}s in the query)</label>`
            )
            .attr("id", "checkbox-sankey")
            .on("click", (e) => {
                this.toggleSankey();
            })
    }

    toggleGlobalDistribution = async () => {
        await sleep(1); // remove the delay in the checking of the box
        this.showGlobalDistribution = !this.showGlobalDistribution;
        // this.renderPlotsCb();
    }

    toggleSankey() {
        this.plotSankey = !this.plotSankey;
    }

    isSankey() {
        let checked = $(this.sankeyCheckbox.node()).checkbox("is checked");
        return checked
    }
}