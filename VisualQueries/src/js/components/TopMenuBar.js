import InsideElement from "./InsideElement.js";

export default class TopMenuBar extends InsideElement {
    constructor(container, backgroundColor, paddingRight) {
        super(container, "0", null, null, null,  "100%");
        this.backgroundColor = backgroundColor;
        this.paddingRight = paddingRight;
    }

    initCss() {
        this.element
            .style("display", "flex")
            .style("flex-direction", "row")
            .style("padding-left", "10px")
            .style("padding-right", this.paddingRight)
            .style("background-color", this.backgroundColor)
            .style("max-height", "40px")
    }

    addTitle(title) {
        this.element
            .insert("div")
            .style("flex", "0.8")
            .style("padding-right", "25px")
            .append("strong")
            .html(title);
    }

    addSliderOption(title, onMoveCb, min, max, step, start) {
        let sliderDiv = this.element
            .append("div")
            .style("flex", "1")
            // .style("max-width", "100px")

        let textSlider = sliderDiv
            .append("span")
            .style("padding-left", "6px")
            .text(title)

        let slider = sliderDiv
            .append("div")
            .classed("ui slider", true)
            .style("max-width", "70%")
            .style("padding-bottom", "0")
            .style("padding-top", "0")

        $(slider.node())
            .slider({
                min: min,
                max: max,
                start: start,
                step: step,
                onMove: onMoveCb
            })
    }

    addChoiceOption(text, text2, values, onChangeCb) {
        let optionDiv = this.element
            .append("div")
            .style("flex", "1")
            // .style("max-width", "100px")

        let textBlock1 = optionDiv
            .append("span")
            .style("padding-right", "6px")
            .text(text)

        let dropdown = optionDiv
            .append("div")
            .classed("ui dropdown", true)

        dropdown.append("div").classed("text", true)
        dropdown.append("i")
            .classed("dropdown icon", true)
            .style("margin-left", "0")

        values = values.map(v => {
            return {"name":v, "value":v}
        })
        values[0].selected = true;

        $(dropdown.node())
            .dropdown({
                "values": values,
                onChange: onChangeCb
            })

        let textBlock2 = optionDiv
            .append("span")
            .style("padding-left", "6px")
            .text(text2)
    }

    addCheckBoxOption(text, onChangeCb, flex=1) {
        let optionDiv = this.element
            .append("div")
            .style("flex", flex)
            .style("padding-top", "10px")

        let checkbox = optionDiv
            .append("div")
            .classed("ui checkbox", true)

        checkbox.html(`
            <input type="checkbox" name="example">
            <label>${text}</label>`
        )

        $(checkbox.node())
            .checkbox("check")

        $(checkbox.node())
            .checkbox({
                onChange: onChangeCb
            })
    }
}