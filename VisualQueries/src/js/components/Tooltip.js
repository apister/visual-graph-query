export default class Tooltip {
    constructor(element) {
        this.element = element;
        this.element.classed("tooltip", true)
        this.offset = 10;

        this.setupStyle();
    }

    setupStyle() {
        this.element
            .style("position", "fixed")
            .style("padding", "8px")
            .style("z-index", "10001")
            .style("background-color", "#ffffff")
            .style("border", "1px solid #000000")
            .style("visibility", "hidden")
    }

    hide() {
        this.element
            .style("visibility", "hidden")
    }

    show() {
        this.element
            .style('visibility', 'visible')
    }

    updatePosition(x, y) {
        this.element
            .style("top", (y + this.offset) + "px")
            .style("left", (x + this.offset) + "px")
    }
}