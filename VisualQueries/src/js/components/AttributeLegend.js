import InsideElement from "./InsideElement";
import {colorScaleLegend, Swatches} from "../utils/colorLegend";
import {PropertyTypesEnum} from "dynbipgraph";

export default class AttributeLegend extends InsideElement {
    constructor(container, top, bottom, left, right, width) {
        super(container, top, bottom, left, right, width);
    }

    fillLegendPanel(property) {
        if (property) {
            if (property.type == PropertyTypesEnum.NUMERIC) {
                this.fillLegendPanelNumerical(property);
            } else {
                this.fillLegendPanelCategoricalSwatches(property);
                // this.fillLegendPanelCategorical(property);
            }
        } else {
            this.clear();
        }
    }

    fillLegendPanelNumerical(property) {
        this.clear();
        let svgLegend = colorScaleLegend(property.colorScale);
        this.element.append(() => svgLegend);
    }

    fillLegendPanelCategoricalSwatches(property) {
        this.clear();
        let svgLegend = Swatches(property.colorScale);
        this.element.append(() => svgLegend.node());
    }

    fillLegendPanelCategorical(property) {
        this.clear();
        this.element
            .selectAll("div")
            .data(property.domain)
            .join("div")
            .classed("mini ui button", true)
            .style("background-color", d => property.colorScale(d))
            .style("color", "white")
            .style("margin", "2px")
            .html(d => d)
    }
}