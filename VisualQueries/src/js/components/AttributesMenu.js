import * as d3 from "d3";
import {globals} from "../globals";
import colorScaleLegend from "../utils/colorLegend";
import InsideElement from "./InsideElement";

export default class AttributesMenu {
    constructor(graphVis, queryGraphPainter, properties, container) {
        this.nodeTypeToProperties = properties;
        this.queryGraphPainter = queryGraphPainter;
        this.container = container;
        this.graphVis = graphVis;

        this.noValueColor = "white";
    }

    setColorScales() {
        for (let [nodeType, properties] of Object.entries(this.nodeTypeToProperties)) {
            for (let property of properties) {

                let type = property.type;

                let colorScale;
                if (type == "INTEGER" || type == "FLOAT") {
                    // colorScale = d3.scaleSequential().domain(property.domain).interpolator(d3.interpolatePuRd);
                    colorScale = d3.scaleSequential().domain(property.domain).interpolator(d3.interpolateViridis);
                } else {
                    colorScale = d3.scaleOrdinal().domain(property.domain).range(d3.schemeCategory20);
                    colorScale.domain([undefined, ...colorScale.domain()]);
                    colorScale.range([this.noValueColor, ...colorScale.range()]);
                }

                property.colorScale = colorScale;
            }
        }
    }

    init() {
        this.setColorScales();

        this.container = this.container
            .append("div")
            .classed("ui accordion", true)

        for (let [nodeType, properties] of Object.entries(this.nodeTypeToProperties)) {
            this.initNodeType(nodeType, properties);
        }

        this.legendPanel = new InsideElement(d3.select(this.graphVis.canvas.node().parentNode), null, 30, 30, null, "40%");
        this.legendPanel.init();
    }

    initNodeType(nodeType, properties) {
        // this.container
        //     .append("h4")
        //     .classed("ui header", true)
        //     .html(nodeType)

        this.title = this.container
            .append("div")
            .classed("title", true)
        this.title
            .append("i")
            .classed("dropdown icon", true)
        this.title.node().innerHTML += nodeType;

        this.nodeTypeContent = this.container
            .append("div")
            .classed("content", true)

        // this.accordion = this.container
        //     .append("div")
        //     .classed("content", true)
        //     .append("div")
        //     .classed("ui accordion", true)

        for (let property of properties) {
            if (!globals.ATTRIBUTES_FILTERED.includes(property.name)) {
                this.initProperty(property);
            }
        }
    }

    initProperty(property) {
        this.content = this.nodeTypeContent
            .append("div")
            .html(property.name)
            .classed("ui button", true)
            .style("display", "block")
            .on("mouseover", () => {
                this.fillLegendPanel(property);
                this.applyNodeColorScale(property);
            })
            .on("mouseout", () => {
                if (this.selectedProperty) {
                    this.fillLegendPanel(this.selectedProperty);
                    this.applyNodeColorScale(this.selectedProperty);
                }
            })
            .on("click", (e, d) => {
                this.container.selectAll(".button").classed('active', false);
                e.target.classList.add("active");
                this.selectedProperty = property;

                if (this.selectedProperty == property) {
                    e.target.classList.toggle("active");
                    this.selectedProperty = null;
                }
            })
    }

    fillLegendPanel(property) {
        if (property.type == "INTEGER" || property.type == "FLOAT") {
             this.fillLegendPanelNumerical(property);
         } else {
             this.fillLegendPanelCategorical(property)
         }
    }

    fillLegendPanelNumerical (property) {
        this.legendPanel.clear();
        let svgLegend = colorScaleLegend(property.colorScale);
        this.legendPanel.element.append(() => svgLegend);
    }

    fillLegendPanelCategorical(property) {
        this.legendPanel.clear();
        this.legendPanel.element
            .selectAll("div")
            .data(property.domain)
            .join("div")
            .classed("mini ui button", true)
            .style("background-color", d => property.colorScale(d))
            .style("color", "white")
            .style("margin", "2px")
            .html(d => d)
    }

    initCategoricalProperty(property) {
        property.domain.forEach(value => {
            this.content
                .append("div")
                .classed("ui button", true)
                .style("background-color", property.colorScale(value))
                .on("mouseover", () => this.applyNodeColorScale(property))
                .html(value);
        })
    }

    initNumericalProperty(property) {
        let svgLegend = colorScaleLegend(property.colorScale);
        this.content.append(() => svgLegend);
    }

    applyNodeColorScale = (property) => {
        this.graphVis.setNodeAttributeToRender(property.name, property.colorScale);
        this.queryGraphPainter.setNodeAttributeToRender(property.name, property.colorScale);

        this.graphVis.render();
        this.queryGraphPainter.graphVis.render()
    }
}