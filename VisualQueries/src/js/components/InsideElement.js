export default class InsideElement {
    constructor(container, top, bottom, left, right, width) {
        this.container = container;

        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;

        this.width = width
    }

    init() {
        this.element = this.container
            .append("div")
                .style("position", "absolute")

        if (this.top) { this.element.style("top", `${this.top}px`) }
        if (this.bottom) { this.element.style("bottom", `${this.bottom}px`) }
        if (this.left) { this.element.style("left", `${this.left}px`) }
        if (this.right) { this.element.style("right", `${this.right}px`) }

        if (this.width) { this.element.style("width", this.width) };

        this.initCss();
    }

    initCss() {
        //
    }

    clear() {
        this.element.html("");
    }
}