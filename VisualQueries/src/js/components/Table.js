import * as d3 from 'd3';

// Semantic Ui Style is not working
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';

// Parcel with datatable seem to work better with require
var $ = require('jquery');
require('datatables.net')(window, $)
require('jszip')(window, $)
require('datatables.net-dt')(window, $)
require('datatables.net-buttons-dt')(window, $)
require('datatables.net-buttons/js/buttons.html5.js')(window, $)
require('datatables.net-buttons/js/buttons.print.js')(window, $)


import {PropertyTypesEnum} from "dynbipgraph";
import {processFloat, processNumber} from "../utils/utils";
import SummaryContainer from "../visualQueryComponents/SummaryContainer";
import {globals} from "../globals";


export class Table {
    dataTable;

    constructor(container, properties, tableId, rowClickCb, selector) {
        this.container = container;
        this.properties = properties;
        this.tableId = tableId;
        this.rowClickCb = rowClickCb;
        this.selector = selector;

        this.createTable();
    }

    createTable() {
        this.table = this.container
            .insert("table")
            .attr("id", this.tableId)
    }

    killTable() {
        this.dataTable.destroy();
        this.table.html("");
    }

    render(data) {
        if (this.dataTable) {
            this.killTable();
            // $(this.tableElementSelector).empty();
        }

        this.data = this.processData(data);
        this.initDatatable();

        this.initRowSelection();
        if (this.selector) {
            this.selectRowsFromSelector();
        }
    }

    update() {
        if (this.selector) {
            this.selectRowsFromSelector();
            this.orderByLastColumn();
        }
    }

    processData() {
        throw "Abstract Method";
    }

    initDatatable() {
        throw "Abstract Method";
    }

    initRowSelection() {
        $(`#${this.tableId} tbody`).on('click', 'tr', (e) => {
            $(e.currentTarget).toggleClass('selected');

            let tr = $(e.currentTarget).closest('tr');
            let row = this.dataTable.row(tr);

            this.rowClick(row);
        });
    }

    rowClick(row) {
        let nodeId = row.data().id;
        this.rowClickCb(nodeId);
    }

    selectRowsFromSelector() {
        this.dataTable.rows().every( (i, d) => {
            let row = this.dataTable.row(i);
            let data = row.data();

            if (this.selector.has(data.id)) {
                $(row.node()).addClass('selected');
            } else {
                $(row.node()).removeClass('selected');
            }
        });
    }

    orderByLastColumn() {
        // this.dataTable.column(-1).cells().invalidate().render();
        this.dataTable.column(-1).cells().invalidate()
        this.dataTable.columns(-1).order('desc').draw();
    }
}


export class EntityTable extends Table {
    constructor(container, properties, tableId, selector) {
        super(container, properties, tableId, null, selector);
    }

    initDatatable() {
        this.dataTable = $(this.table.node()).DataTable({
            autoWidth: true,
            paging: true,
            ordering: true,
            info: true,
            dom: 'Bfrtip',
            data: this.data,
            columns: this.getColumnsInput()
        })
    }

    rowClick(row) {
        let nodeId = row.data().id;
        this.selector.toggleNodeId(nodeId);
        this.orderByLastColumn();
    }

    orderValue = (d) => {
        return this.selector.has(d.id);
    }

    getColumnsInput() {
        let columns = this.properties.filter(property => property.name != "documents" && property.name != "name").map(property => {
            if (property.type == PropertyTypesEnum.NUMERIC) {
                return {title: property.name, data: (d) => d.get(property.name) ? processNumber(d.get(property.name)) : ""};
            } else {
                return {title: property.name, data: (d) => d.get(property.name) ? d.get(property.name) : ""};
            }
        })

        let idColumn = {title: "id", data: (d) => d.id};
        columns.unshift(idColumn);
        if (this.properties.filter(p => p.name == "name").length > 0) {
            let nameColumn = {title: "name", data: (d) => d.get("name") ? d.get("name") : ""};
            columns.unshift(nameColumn);
        }

        this.addOrderingColumn(columns);
        return columns;
    }

    addOrderingColumn(columns) {
        let orderingColumn = {data: this.orderValue, visible: false};
        columns.push(orderingColumn);
    }
}


export class PersonsTable extends EntityTable {
    constructor(container, properties, tableId, selector) {
        super(container, properties, tableId, selector);
    }

    processData(persons) {
        return persons;
    }
}

export class DocumentsTable extends EntityTable {
    constructor(container, properties, tableId, rowClickCb) {
        super(container, properties, tableId, rowClickCb);
    }

    processData(documents) {
        return documents
    }
}

function coloredSquare(color, w=20, h=20) {
    return `<svg width="${w}" height="${h}" style="{vertical-align: middle;}">
        <rect x="0" y="0" width="${w}" height="${h}" fill="${color}"></rect>
    </svg>`
}

export class PersonsComparisonTable extends EntityTable {
    constructor(container, properties, tableId, selector) {
        super(container, properties, tableId, selector);
    }

    processData(persons) {
        return persons
    }

    getColumnsInput() {
        let columns = this.properties.filter(property => property.name != "documents" && property.name != "name").map(property => {
            return {title: property.name, data: (d) => d.attributes[property.name] ? d.attributes[property.name] : ""};
        })

        let idColumn = {title: "id", data: (d) => d.id};
        columns.unshift(idColumn);
        if (this.properties.filter(p => p.name == "name").length > 0) {
            let nameColumn = {title: "name", data: (d) => d.attributes["name"] ? d.attributes["name"] : ""};
            columns.unshift(nameColumn);
        }

        // let AColumn = {title: "A", data: (d) => d.A};
        // let BColumn = {title: "B", data: (d) => d.B};
        let AColumn = {title: "A", data: (d) => d["A"] ? coloredSquare(globals.mainQueryColor) : ""};
        let BColumn = {title: "B", data: (d) => d["B"] ? coloredSquare(globals.comparisonQueryColor) : ""};

        columns.push(AColumn);
        columns.push(BColumn);

        this.addOrderingColumn(columns);

        return columns;
    }
}

export class DocumentsComparisonTable extends EntityTable {
    constructor(container, properties, tableId, selector) {
        super(container, properties, tableId, selector);
    }

    processData(documents) {
        return documents
    }

    getColumnsInput() {
        let columns = this.properties.filter(property => property.name != "documents" && property.name != "name").map(property => {
            return {title: property.name, data: (d) => d.attributes[property.name] ? d.attributes[property.name] : ""};
        })

        let idColumn = {title: "id", data: (d) => d.id};
        columns.unshift(idColumn);
        if (this.properties.filter(p => p.name == "name").length > 0) {
            let nameColumn = {title: "name", data: (d) => d.attributes["name"] ? d.attributes["name"] : ""};
            columns.unshift(nameColumn);
        }

        // let AColumn = {title: "A", data: (d) => d["A"] ? d["A"] : ""};
        // let BColumn = {title: "B", data: (d) => d["B"] ? d["B"] : ""};
        let AColumn = {title: "A", data: (d) => d["A"] ? coloredSquare(globals.mainQueryColor) : ""};
        let BColumn = {title: "B", data: (d) => d["B"] ? coloredSquare(globals.comparisonQueryColor) : ""};

        columns.push(AColumn);
        columns.push(BColumn);

        this.addOrderingColumn(columns);

        return columns;
    }
}


export class GraphMetricsTable extends Table {
    constructor(container, tableId) {
        super(container, null, tableId, null, null);
    }

    processData([graphMetrics, graphMetrics2]) {
        let dataProcessed = [];
        for (let key of Object.keys(graphMetrics)) {
            let item = [key, SummaryContainer.prettifyGraphValue(graphMetrics[key]), SummaryContainer.prettifyGraphValue(graphMetrics2[key])];
            dataProcessed.push(item)
        }
        return dataProcessed;
    }

    initDatatable() {
        this.dataTable = $(this.table.node()).DataTable({
            autoWidth: true,
            paging: true,
            ordering: true,
            info: true,
            dom: 'Bfrtip',
            data: this.data,
            columns: this.getColumnsInput()
        })
    }

    getColumnsInput() {
        return [{title: "Metric"}, {title: "A"}, {title: "B"}];
    }
}




