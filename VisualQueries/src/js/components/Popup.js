
export default class Popup {
    constructor() {
        this.overlay = $("<div>").appendTo("body");
        this.loader = $("<div>").appendTo("body");
        this.setOverlayCss();
        this.setLoaderCss();
    }

    setOverlayCss() {
        this.overlay.css({
            background: "#e9e9e9",
            display: "none",
            position: "absolute",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            opacity: 0.5
        })
    }

    setLoaderCss() {
        this.loader.css({
            position: "absolute",
            top: "50%",
            left: "50%",
            /*margin-right: -50%;*/
            transform: "translate(-50%, -50%)",
            "border": "16px solid #f3f3f3",
            "border-radius": "50%",
            "border-top": "16px solid #3498db",
            width: "120px",
            height: "120px",
            "-webkit-animation": "spin 2s linear infinite",
            animation: "spin 2s linear infinite",
            display: "none",
        })
    }

    show() {
        this.overlay.show();
        this.loader.show();
    }

    hide() {
        this.overlay.hide();
        this.loader.hide();
    }
}