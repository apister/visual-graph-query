import {globals} from "../globals.js";


export const ShapeEnum = {
    CIRCLE: 0,
    SQUARE: 1,
    STAR: 2
}

export class Legend {
    constructor(domainList, shapes = null) {
        this.domainList = domainList;

        this.legend = {};
        if (shapes) {
            if (domainList.length != shapes.length) {
                throw "domainList and shapes should have same length";
            }
            this.domainList.forEach((domainValue, i) => {
                this.legend[domainValue] = shapes[i]
            })
        }
    }

    initAutoShapeLegend() {
        // let shapes = [...globals.SHAPES];
        let shapes = [ShapeEnum.CIRCLE, ShapeEnum.SQUARE];

        // Person goes to circle by default
        if (this.domainList.includes("PERSON")) { // TODO : auto this
             this.legend["PERSON"] = ShapeEnum.CIRCLE;
             shapes = shapes.filter(s => s != ShapeEnum.CIRCLE);
        }

        for (let value of this.domainList) {
            if (value != "PERSON") {
                this.legend[value] = shapes.pop();
            }
        }

        this.legend[globals.ANY_NODETYPE] = ShapeEnum.STAR;
    }
}