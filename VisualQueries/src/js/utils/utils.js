export const requestFile = async (fp) => {
    const response = await fetch(fp);
    const json = await response.json();
    return json;
}

export const saveData = (function () {
    let a = document.createElement("a");

    return function (data, fileName) {
        // let json = JSON.stringify(data),
        let json = data,
            blob = new Blob([json], {type: "octet/stream"}),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function generateLinkIdFromLinkObject(link) {
    return link.source.id + link.type + link.target.id;
}

export function generateReverseLinkIdFromLinkObject(link) {
    return link.target.id + link.type + link.source.id;
}

export function generateLinkIdFromLinkList(link) {
    // return `${link[0]}${link[1]}${link[2]}`;

    // This string concatenation method is apparently faster
    return link[0] + link[1] + link[2]
}

export function generateUndirectedLinkId(node1, node2) {
    if (parseInt(node1) < parseInt(node2)) {
        return node1.toString() + "-" + node2.toString();
    } else {
        return node2.toString() + "-" + node1.toString();
    }
}

export function generateEntityClassName(entityId) {
    return `node-${entityId}`
}

export function processFloat(number) {
    try {
        return number.toFixed(3);
    } catch (e) {
        return number;
    }
}

export function isInt(n) {
    return n % 1 === 0;
}

export function processNumber(n) {
    if (!isInt(n)) {
        return processFloat(n);
    } else {
        return n;
    }
}


// entriesFilterFunc : (key, value) => {...}
export function filterObject(object, entriesFilterFunc) {
    return Object.fromEntries(
        Object.entries(object).filter(entriesFilterFunc)
    );
}


// from https://talk.observablehq.com/t/dom-context2d-vs-dom-canvas-what-am-i-doing-wrong/3836
export const DOMcontext2d = (width, height, dpi) => {
    if (dpi == null) dpi = devicePixelRatio;
    let canvas = document.createElement("canvas");
    canvas.width = width * dpi;
    canvas.height = height * dpi;
    canvas.style.width = width + "px";
    let context = canvas.getContext("2d");
    context.scale(dpi, dpi);
    return context;
}


// https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
// const colorShade = (col, amt) => {
//   col = col.replace(/^#/, '')
//   if (col.length === 3) col = col[0] + col[0] + col[1] + col[1] + col[2] + col[2]
//
//   let [r, g, b] = col.match(/.{2}/g);
//   ([r, g, b] = [parseInt(r, 16) + amt, parseInt(g, 16) + amt, parseInt(b, 16) + amt])
//
//   r = Math.max(Math.min(255, r), 0).toString(16)
//   g = Math.max(Math.min(255, g), 0).toString(16)
//   b = Math.max(Math.min(255, b), 0).toString(16)
//
//   const rr = (r.length < 2 ? '0' : '') + r
//   const gg = (g.length < 2 ? '0' : '') + g
//   const bb = (b.length < 2 ? '0' : '') + b
//
//   return `#${rr}${gg}${bb}`
// }


export function isPointInRect(x, y, width, height, xRect, yRect) {
    return xRect <= x && x <= xRect + width &&
        yRect <= y && y <= yRect + height;
}