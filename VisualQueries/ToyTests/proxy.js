let l = {
    a: 10,
    b: 20
}

var targetProxy = new Proxy(l, {
  set: function (target, key, value) {
      console.log(`${key} set to ${value}`);
      target[key] = value;
      return true;
  }
});

// targetProxy.hello_world = "test";
l.hello_world = "test"